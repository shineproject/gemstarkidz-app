package com.example.shine_rahul.paymentgetway;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

public class PaymentDetail extends AppCompatActivity {
    TextView txt_id, txt_amount, txt_status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_detail);
        txt_id = (TextView) findViewById(R.id.txt_id);
        txt_amount = (TextView) findViewById(R.id.txt_amount);
        txt_status = (TextView) findViewById(R.id.txt_status);
        Intent intent = getIntent();
        try {
            JSONObject jsonObject = new JSONObject(intent.getStringExtra("PaymentDetails"));
            showDetail(jsonObject.getJSONObject("response"), intent.getStringExtra("PaymentAmount"));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showDetail(JSONObject response, String paymentAmount) {
        try {
            txt_id.setText(response.getString("id"));
            txt_amount.setText(response.getString(String.format("$%s",paymentAmount)));
            txt_status.setText(response.getString("state"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

}
