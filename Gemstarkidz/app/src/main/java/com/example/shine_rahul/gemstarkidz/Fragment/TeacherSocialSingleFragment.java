package com.example.shine_rahul.gemstarkidz.Fragment;


import android.app.FragmentTransaction;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.borjabravo.readmoretextview.ReadMoreTextView;
import com.example.shine_rahul.gemstarkidz.Adapter.SocialSingleAdapter;
import com.example.shine_rahul.gemstarkidz.Adapter.TagAdapter;
import com.example.shine_rahul.gemstarkidz.Model.SocialSingleModel;
import com.example.shine_rahul.gemstarkidz.Model.TeacherClassModel;
import com.example.shine_rahul.gemstarkidz.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class TeacherSocialSingleFragment extends android.app.Fragment {


    private boolean isSelect = true;
    TextView txt_menu_title;
    TextView txt_comments;
    TextView txt_social_single_social;
    TextView txt_social_single_chat;
    TextView txt_social_single_event;
    TextView txt_social_single_attendance;
    VideoView social_video;
    TextView txt_name;
    ReadMoreTextView txt_content;
    MediaController mediaController;
    TextView txt_readmore;
    ImageView img_tag;
    ImageView img_comment;
    ImageView img_like;
    ImageView img_share;
    RecyclerView rv_social_list;
    ArrayList<SocialSingleModel> data;
    SocialSingleAdapter socialSingleAdapter;
    SocialSingleModel socialSingleModel;
    int[] image = {R.drawable.image1, R.drawable.image8, R.drawable.image3, R.drawable.image4, R.drawable.image5, R.drawable.image6, R.drawable.image7, R.drawable.image8, R.drawable.image9, R.drawable.image10};
    String[] name = {"Kamlesh", "Bhargav", "Dhruv", "Jaymin", "Bilal", "Akshay", "Harshal", "Vineet", "Lucky", "Vipul"};
    String[] time = {"03:30 pm", "10:33 pm", "01:00 pm", "03:22 am", "4:30 am", "06:30 pm", "08:30 pm", "05:30 pm", "07:44 pm", "11:30 pm"};
    String[] message = {"Offers almost all of the same options as the mobile app",
            "Chatting is a lot faster using a keyboard",
            "Offers real time synchronization between app and PC",
            "Well-designed interface",
            "Mobile device must be connected in order to use it",
            "ome options are missing",
            "Was it worth the wait?",
            "by Matarazy Stunna Cull Hello And how are you doing I really like what's up I hope i.",
            "chat with friends and new friends on word and see photos friend.",
            "I love wats because it increases the knowledge and brings closer friends and m"};
    String[] url = {"https://android--code.blogspot.in/2015/05/android-textview-underline.html",
            "https://android--code.blogspot.in/2015/05/android-textview-underline.html",
            "https://android--code.blogspot.in/2015/05/android-textview-underline.html",
            "https://android--code.blogspot.in/2015/05/android-textview-underline.html",
            "https://android--code.blogspot.in/2015/05/android-textview-underline.html",
            "https://android--code.blogspot.in/2015/05/android-textview-underline.html",
            "https://android--code.blogspot.in/2015/05/android-textview-underline.html",
            "https://android--code.blogspot.in/2015/05/android-textview-underline.html",
            "https://android--code.blogspot.in/2015/05/android-textview-underline.html",
            "https://android--code.blogspot.in/2015/05/android-textview-underline.html"};


    private boolean isSelect1 = true;
    TeacherClassModel teacherClassModel;
    TagAdapter teacherClassStudentListAdapter;
    ArrayList<TeacherClassModel> teacherClassModelArrayList;
    int[] images22 = {R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1};
    String[] name22 = {"Kamlesh", "Bhargav", "Dhruv", "Jaymin", "Bilal", "Akshay", "Harshal", "Vineet", "Lucky", "Vipul", "Kamlesh", "Bhargav", "Dhruv", "Jaymin", "Bilal", "Akshay", "Harshal", "Vineet", "Lucky", "Vipul", "Kamlesh", "Bhargav", "Dhruv", "Jaymin", "Bilal", "Akshay", "Harshal", "Vineet", "Lucky", "Vipul"};


    public TeacherSocialSingleFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_teacher_social_single, container, false);

        txt_comments = (TextView) view.findViewById(R.id.txt_comments);
        txt_social_single_social = (TextView) view.findViewById(R.id.txt_social_single_social);
        txt_social_single_chat = (TextView) view.findViewById(R.id.txt_social_single_chat);
        txt_social_single_event = (TextView) view.findViewById(R.id.txt_social_single_event);
        txt_social_single_attendance = (TextView) view.findViewById(R.id.txt_social_single_attendance);
        txt_social_single_social.setTextColor(getResources().getColor(R.color.colorTheme));
        txt_name = (TextView) view.findViewById(R.id.txt_name);
        txt_content = (ReadMoreTextView) view.findViewById(R.id.txt_content);
        social_video = (VideoView) view.findViewById(R.id.social_video);
//        txt_readmore = (TextView) view.findViewById(R.id.txt_readmore);
        rv_social_list = (RecyclerView) view.findViewById(R.id.rv_social_list);
        txt_menu_title = (TextView) getActivity().findViewById(R.id.txt_menu_title);
        img_tag = (ImageView) view.findViewById(R.id.img_tag);
        img_like = (ImageView) view.findViewById(R.id.img_like);
        img_share = (ImageView) view.findViewById(R.id.img_share);
        img_comment = (ImageView) view.findViewById(R.id.img_comment);
//        txt_readmore.setPaintFlags(txt_readmore.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        mediaController = new MediaController(getActivity());
        final Uri uri = Uri.parse("http://abhiandroid-8fb4.kxcdn.com/ui/wp-content/uploads/2016/04/videoviewtestingvideo.mp4");
        social_video.setVideoURI(uri);
        social_video.setMediaController(mediaController);
        mediaController.setAnchorView(social_video);
        social_video.requestFocus();
        social_video.start();
        Typeface style = Typeface.createFromAsset(getActivity().getAssets(), "Raleway-SemiBold.ttf");
        Typeface style1 = Typeface.createFromAsset(getActivity().getAssets(), "Raleway-Regular.ttf");
        txt_name.setTypeface(style);
        txt_content.setTypeface(style1);
        //ResizableCustomView.doResizeTextView(txt_content, MAX_LINES, "Read More", true);

        data = new ArrayList<>();
        for (int i = 0; i < name.length; i++) {
            socialSingleModel = new SocialSingleModel();
            socialSingleModel.setImage(image[i]);
            socialSingleModel.setName(name[i]);
            socialSingleModel.setTime(time[i]);
            socialSingleModel.setDescription(message[i]);
            socialSingleModel.setUrl(url[i]);
            data.add(socialSingleModel);
        }
        socialSingleAdapter = new SocialSingleAdapter(getActivity(), data);
        socialSingleAdapter.notifyDataSetChanged();
        rv_social_list.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayout.VERTICAL, false));
        rv_social_list.setAdapter(socialSingleAdapter);

        Typeface style5 = Typeface.createFromAsset(getActivity().getAssets(), "Raleway-Bold.ttf");
        txt_social_single_social.setTypeface(style5);
        txt_social_single_chat.setTypeface(style5);
        txt_social_single_event.setTypeface(style5);
        txt_social_single_attendance.setTypeface(style5);
        txt_comments.setTypeface(style5);


        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                Log.i("HELLO", "keyCode: " + keyCode);
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                    android.app.Fragment someFragment = new TeacherSocialFragment();
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.content, someFragment); // give your fragment container id in first parameter
                    transaction.addToBackStack(null);  // if written, this transaction will be added to backstack
                    transaction.commit();
                    txt_menu_title.setText("Social ");
                    Typeface style = Typeface.createFromAsset(getActivity().getAssets(), "Raleway-SemiBold.ttf");
                    txt_menu_title.setTypeface(style);
                    return true;
                }
                return false;
            }

        });

        return view;
    }

}
