package com.example.shine_rahul.gemstarkidz.Model;

/**
 * Created by Shine-Rahul on 28/2/2018.
 */

public class ParentChatModel {
    String id;
    String user_id;
    String user_name;
    String user_type;
    String user_mobile_number;
    String user_status;
    String user_email;
    String user_image;
    String user_class_id;


    public ParentChatModel() {
    }

    public ParentChatModel(String id, String user_id, String user_name, String user_type, String user_mobile_number, String user_status, String user_email, String user_image, String user_class_id) {
        this.id = id;
        this.user_id = user_id;
        this.user_name = user_name;
        this.user_type = user_type;
        this.user_mobile_number = user_mobile_number;
        this.user_status = user_status;
        this.user_email = user_email;
        this.user_image = user_image;
        this.user_class_id = user_class_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_type() {
        return user_type;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }

    public String getUser_mobile_number() {
        return user_mobile_number;
    }

    public void setUser_mobile_number(String user_mobile_number) {
        this.user_mobile_number = user_mobile_number;
    }

    public String getUser_status() {
        return user_status;
    }

    public void setUser_status(String user_status) {
        this.user_status = user_status;
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public String getUser_image() {
        return user_image;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }

    public String getUser_class_id() {
        return user_class_id;
    }

    public void setUser_class_id(String user_class_id) {
        this.user_class_id = user_class_id;
    }
}
