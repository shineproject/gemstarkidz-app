package com.example.shine_rahul.gemstarkidz.Fragment;


import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shine_rahul.gemstarkidz.Adapter.StudentPresentAbsentAdapter;
import com.example.shine_rahul.gemstarkidz.Adapter.TeacherClassStudentListAdapter;
import com.example.shine_rahul.gemstarkidz.JSONParser.JSONParser;
import com.example.shine_rahul.gemstarkidz.Model.AbsentPresentListModel;
import com.example.shine_rahul.gemstarkidz.Model.TeacherClassModel;
import com.example.shine_rahul.gemstarkidz.R;
import com.example.shine_rahul.gemstarkidz.Service.Services;
import com.example.shine_rahul.gemstarkidz.WelcomeActivity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class TeacherAttendanceReportFragment extends android.app.Fragment {
    GridView gv_attendance_select_date;
    TextView txt_select_date;
    TextView txt_date;
    TextView txt_nodata;
    ImageView btn_selected_submit;
    String end_date;
    Calendar calendar;
    SimpleDateFormat mdformat, dateFormat;
    String strDate;
    ProgressDialog progressDialog;

    AbsentPresentListModel absentPresentListModel;
    StudentPresentAbsentAdapter studentPresentAbsentAdapter;
    ArrayList<AbsentPresentListModel> data = new ArrayList<>();
    String Cla_Id, absent_present_flag;
    DatePickerDialog datePickerDialog;

    TextView txt_edit_toolbar;
    ImageView img_search, img_close;
    EditText edt_search;
    TextView txt_menu_title;


    public TeacherAttendanceReportFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_teacher_attendance_report, container, false);

        txt_edit_toolbar = (TextView) getActivity().findViewById(R.id.txt_edit_toolbar);
        img_search = (ImageView) getActivity().findViewById(R.id.img_search);
        img_close = (ImageView) getActivity().findViewById(R.id.img_close);
        edt_search = (EditText) getActivity().findViewById(R.id.edt_search);
        txt_menu_title = (TextView) getActivity().findViewById(R.id.txt_menu_title);


        gv_attendance_select_date = (GridView) view.findViewById(R.id.gv_attendance_select_date);
        btn_selected_submit = (ImageView) view.findViewById(R.id.btn_selected_submit);
        txt_edit_toolbar = (TextView) getActivity().findViewById(R.id.txt_edit_toolbar);
        txt_edit_toolbar.setVisibility(View.GONE);
        txt_select_date = (TextView) view.findViewById(R.id.txt_select_date);
        txt_date = (TextView) view.findViewById(R.id.txt_date);
        txt_nodata = (TextView) view.findViewById(R.id.txt_nodata);

        txt_menu_title.setVisibility(View.VISIBLE);
        txt_menu_title.setText("Attendance");
        txt_edit_toolbar.setVisibility(View.GONE);
        img_search.setVisibility(View.VISIBLE);
        edt_search.setVisibility(View.GONE);
        img_close.setVisibility(View.GONE);

        SharedPreferences splogin = getActivity().getSharedPreferences("login", 0);
        Cla_Id = splogin.getString("Cla_Id", "");

        calendar = Calendar.getInstance();
        mdformat = new SimpleDateFormat("yyyy-MM-dd");
        strDate = mdformat.format(calendar.getTime());

        dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        end_date = dateFormat.format(calendar.getTime());
        txt_date.setText(end_date);

        txt_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectDate();
            }
        });
        new showPresentAbsentList().execute();
        btn_selected_submit.setVisibility(View.GONE);

        btn_selected_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new showPresentAbsentList().execute();
            }
        });

        img_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                img_search.setVisibility(View.GONE);
                txt_menu_title.setVisibility(View.GONE);
                edt_search.setVisibility(View.VISIBLE);
                img_close.setVisibility(View.VISIBLE);
                edt_search.setText("");
                edt_search.requestFocus();
                edt_search.setFocusable(true);
            }
        });
        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                img_close.setVisibility(View.GONE);
                edt_search.setVisibility(View.GONE);
                img_search.setVisibility(View.VISIBLE);
                txt_menu_title.setVisibility(View.VISIBLE);
                txt_menu_title.setText("Attendance");
                new showPresentAbsentList().execute();
                getActivity().overridePendingTransition(0, 0);

            }
        });


        edt_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                studentPresentAbsentAdapter.filter(edt_search.getText().toString());


            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });



        return view;
    }

    private void selectDate() {

        final Calendar calendar = Calendar.getInstance();
        int YEAR, MONTH, DAY;
        YEAR = calendar.get(Calendar.YEAR);
        MONTH = calendar.get(Calendar.MONTH);
        DAY = calendar.get(Calendar.DAY_OF_MONTH);


        datePickerDialog = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        strDate = year + "-" + ((monthOfYear + 1) > 9 ? (monthOfYear + 1) : "0" + (monthOfYear + 1)) + "-" +
                                (dayOfMonth > 9 ? dayOfMonth : "0" + dayOfMonth);
                        new showPresentAbsentList().execute();
                        SimpleDateFormat update_start_date = new SimpleDateFormat("yyyy-MM-dd");
                        Date date = null;
                        String set_start_date = "";
                        try {
                            date = update_start_date.parse(strDate);
                            update_start_date.applyPattern("dd/MM/yyyy");
                            if (date != null) {
                                set_start_date = update_start_date.format(date);
                            }
                            txt_date.setText(set_start_date);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }


                    }
                }, YEAR, MONTH, DAY);
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();
    }


    //Absent presnt list display

    public class showPresentAbsentList extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Loading");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            HashMap<String, String> mapParams = new HashMap<>();
            mapParams.put("class_id", Cla_Id);
            mapParams.put("date", strDate);
            String response = new JSONParser().sendPostRequest(Services.PRESENT_ABSENT_LIST, mapParams);
            return response;
        }

        protected void onPostExecute(String response) {
            progressDialog.dismiss();
            if (response != null && !response.equals("")) {
                try {
                    JSONObject mjsonObject = new JSONObject(response);
                    String error = mjsonObject.getString("error");
                    String message = mjsonObject.getString("message");
                    if (error.equals("200")) {
                        JSONArray jsonArray = mjsonObject.getJSONArray("data");
                        if (jsonArray != null && jsonArray.length() > 0) {
                            txt_nodata.setVisibility(View.GONE);
                            gv_attendance_select_date.setVisibility(View.VISIBLE);
                            data.clear();
                            AbsentPresentListModel teacherClassModel;
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                teacherClassModel = new AbsentPresentListModel();
                                absent_present_flag = jsonObject.getString("Att_Pre_Abs");
                                JSONArray result_jsonArray = jsonObject.getJSONArray("att_student");
                                for (int j = 0; j < result_jsonArray.length(); j++) {
                                    JSONObject result_jsonObject = result_jsonArray.getJSONObject(j);
                                    teacherClassModel = new AbsentPresentListModel();
                                    teacherClassModel.setStudent_absent_present(absent_present_flag);
                                    teacherClassModel.setStudent_name(result_jsonObject.getString("Std_Name"));
                                    teacherClassModel.setStudent_image(result_jsonObject.getString("Std_Image"));
                                }
                                data.add(teacherClassModel);
                                studentPresentAbsentAdapter = new StudentPresentAbsentAdapter(getActivity(), data);
                                studentPresentAbsentAdapter.notifyDataSetChanged();
                                gv_attendance_select_date.setAdapter(studentPresentAbsentAdapter);
                            }
                        } else {
                            data.clear();
                            gv_attendance_select_date.setVisibility(View.GONE);
                            txt_nodata.setVisibility(View.VISIBLE);
//                            Toast.makeText(getActivity(), "No data found", Toast.LENGTH_LONG).show();
                        }


                    } else {
                        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
