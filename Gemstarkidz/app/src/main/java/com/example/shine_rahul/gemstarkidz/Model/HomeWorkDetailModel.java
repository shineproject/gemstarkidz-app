package com.example.shine_rahul.gemstarkidz.Model;

/**
 * Created by Shine-Rahul on 14/3/2018.
 */

public class HomeWorkDetailModel {

    String Hmw_Id;
    String Hmw_Cla_Id;
    String Hmw_Subject;
    String Hmw_Chapter;
    String Hmw_Date;
    String Hmw_Page_No;
    String Hmw_Desscription;
    String Hmw_CreatedBy;
    String Hmw_CreatedAt;
    String Hmw_UpdatedBy;
    String Hmw_UpdatedAt;

    String Cla_Class;
    String Cla_Section;

    public String getHmw_Id() {
        return Hmw_Id;
    }

    public void setHmw_Id(String hmw_Id) {
        Hmw_Id = hmw_Id;
    }

    public String getHmw_Cla_Id() {
        return Hmw_Cla_Id;
    }

    public void setHmw_Cla_Id(String hmw_Cla_Id) {
        Hmw_Cla_Id = hmw_Cla_Id;
    }

    public String getHmw_Subject() {
        return Hmw_Subject;
    }

    public void setHmw_Subject(String hmw_Subject) {
        Hmw_Subject = hmw_Subject;
    }

    public String getHmw_Chapter() {
        return Hmw_Chapter;
    }

    public void setHmw_Chapter(String hmw_Chapter) {
        Hmw_Chapter = hmw_Chapter;
    }

    public String getHmw_Date() {
        return Hmw_Date;
    }

    public void setHmw_Date(String hmw_Date) {
        Hmw_Date = hmw_Date;
    }

    public String getHmw_Page_No() {
        return Hmw_Page_No;
    }

    public void setHmw_Page_No(String hmw_Page_No) {
        Hmw_Page_No = hmw_Page_No;
    }

    public String getHmw_Desscription() {
        return Hmw_Desscription;
    }

    public void setHmw_Desscription(String hmw_Desscription) {
        Hmw_Desscription = hmw_Desscription;
    }

    public String getHmw_CreatedBy() {
        return Hmw_CreatedBy;
    }

    public void setHmw_CreatedBy(String hmw_CreatedBy) {
        Hmw_CreatedBy = hmw_CreatedBy;
    }

    public String getHmw_CreatedAt() {
        return Hmw_CreatedAt;
    }

    public void setHmw_CreatedAt(String hmw_CreatedAt) {
        Hmw_CreatedAt = hmw_CreatedAt;
    }

    public String getHmw_UpdatedBy() {
        return Hmw_UpdatedBy;
    }

    public void setHmw_UpdatedBy(String hmw_UpdatedBy) {
        Hmw_UpdatedBy = hmw_UpdatedBy;
    }

    public String getHmw_UpdatedAt() {
        return Hmw_UpdatedAt;
    }

    public void setHmw_UpdatedAt(String hmw_UpdatedAt) {
        Hmw_UpdatedAt = hmw_UpdatedAt;
    }

    public String getCla_Class() {
        return Cla_Class;
    }

    public void setCla_Class(String cla_Class) {
        Cla_Class = cla_Class;
    }

    public String getCla_Section() {
        return Cla_Section;
    }

    public void setCla_Section(String cla_Section) {
        Cla_Section = cla_Section;
    }
}
