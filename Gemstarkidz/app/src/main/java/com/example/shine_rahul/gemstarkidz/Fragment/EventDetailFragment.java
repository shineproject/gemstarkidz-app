package com.example.shine_rahul.gemstarkidz.Fragment;


import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shine_rahul.gemstarkidz.Adapter.EventDetailListAdapter;
import com.example.shine_rahul.gemstarkidz.JSONParser.JSONParser;
import com.example.shine_rahul.gemstarkidz.Model.EventDetailListModel;
import com.example.shine_rahul.gemstarkidz.ParentActivity;
import com.example.shine_rahul.gemstarkidz.R;
import com.example.shine_rahul.gemstarkidz.Service.Services;
import com.example.shine_rahul.gemstarkidz.SigninActivity;
import com.example.shine_rahul.gemstarkidz.TeacherActivity;
import com.example.shine_rahul.gemstarkidz.WelcomeActivity;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.net.ssl.HttpsURLConnection;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class EventDetailFragment extends android.app.Fragment {
    CircleImageView img_event_detail_profile;
    TextView txt_menu_title;
    TextView txt_event_detail_count;
    TextView txt_event_detail_name;
    TextView txt_event_detail_description;
    TextView txt_event_detaildate;
    TextView txt_event_detail_people;
    RecyclerView rv_event_detail_list;
    EventDetailListModel eventDetailListModel;
    EventDetailListAdapter eventDetailListAdapter;
    ArrayList<EventDetailListModel> data = new ArrayList<>();
    int[] images = {R.drawable.image1, R.drawable.image8, R.drawable.image3, R.drawable.image4};
    ImageView img_event_accept;
    ImageView img_event_decline;
    Bundle bundle;
    String IMAGE;
    String event_id, user_id, Use_Id, accept_event, accept_decline;
    ImageView search, img_close;
    int flag;
    EditText edt_search;


    public EventDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_event_detail, container, false);
        img_event_detail_profile = (CircleImageView) view.findViewById(R.id.img_event_detail_profile123);
        txt_event_detaildate = (TextView) view.findViewById(R.id.txt_event_detaildate);
        txt_menu_title = (TextView) getActivity().findViewById(R.id.txt_menu_title);
        txt_event_detail_count = (TextView) view.findViewById(R.id.txt_event_detail_count);
        txt_event_detail_people = (TextView) view.findViewById(R.id.txt_event_detail_people);
        txt_event_detail_name = (TextView) view.findViewById(R.id.txt_event_detail_name);
        rv_event_detail_list = (RecyclerView) view.findViewById(R.id.rv_event_detail_list);
        img_event_accept = (ImageView) view.findViewById(R.id.img_event_accept);
        img_event_decline = (ImageView) view.findViewById(R.id.img_event_decline);
        txt_event_detail_description = (TextView) view.findViewById(R.id.txt_event_detail_description);
        search = (ImageView) getActivity().findViewById(R.id.img_search);
        img_close = (ImageView) getActivity().findViewById(R.id.img_close);
        edt_search = (EditText) getActivity().findViewById(R.id.edt_search);
        edt_search.setVisibility(View.GONE);
        txt_menu_title.setText("Event");


        SharedPreferences splogin = getActivity().getSharedPreferences("login", 0);
        user_id = splogin.getString("user_id", "");


        bundle = getArguments();
        IMAGE = bundle.getString("IMAGE");
        Picasso.with(getActivity())
                .load(Services.IMAGES_PATH + "event/" + IMAGE)
                .placeholder(R.drawable.logo)
                .into(img_event_detail_profile);
        txt_event_detaildate.setText(bundle.getString("DATE"));
        txt_event_detail_people.setText(bundle.getString("PEOPLE") + " peoples");
        txt_event_detail_count.setText("+ " + bundle.getString("PEOPLE"));
        txt_event_detail_name.setText(bundle.getString("NAME"));
        txt_event_detail_description.setText(bundle.getString("Description"));
        event_id = bundle.getString("Eve_Id");
        accept_event = bundle.getString("accept_event");
        //  Toast.makeText(getActivity(), event_id, Toast.LENGTH_LONG).show();
        Typeface style = Typeface.createFromAsset(getActivity().getAssets(), "Raleway-SemiBold.ttf");
        txt_event_detail_name.setTypeface(style);
        search.setVisibility(View.GONE);
        img_close.setVisibility(View.GONE);


        new showUserList().execute();
        new acceptDeclineEvent().execute();


        img_event_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flag = 1;
                new acceptEvent().execute();

            }
        });

        img_event_decline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                flag = 0;
                new acceptEvent().execute();

                //FancyToast.makeText(getActivity(), "You are not participate this event...", FancyToast.LENGTH_LONG, FancyToast.INFO, false).show();

            }
        });
        return view;
    }

    private void eventReminder() {
        try {

            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View view1 = layoutInflater.inflate(R.layout.reminder, null);
            final AlertDialog.Builder alertdialog = new AlertDialog.Builder(getActivity());
            alertdialog.setView(view1);
            final AlertDialog dialog = alertdialog.create();
            final ImageView image_reminder = (ImageView) view1.findViewById(R.id.image_reminder);
            final ImageView image_cancel = (ImageView) view1.findViewById(R.id.image_cancel);
            final ImageView img_event_detail_profile_reminder = (ImageView) view1.findViewById(R.id.img_event_detail_profile_reminder);
            final TextView txt_event_name_reminder = (TextView) view1.findViewById(R.id.txt_event_name_reminder);
            final TextView txt_event_date_reminder = (TextView) view1.findViewById(R.id.txt_event_date_reminder);
            image_reminder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(getActivity(), "We will reminder latter...", Toast.LENGTH_LONG).show();

                    //FancyToast.makeText(getActivity(), "Remainder latter...", FancyToast.LENGTH_LONG, FancyToast.INFO, false).show();
                    dialog.dismiss();

                }
            });
            img_event_detail_profile_reminder.setImageResource(Integer.parseInt(bundle.getString("IMAGE")));
            txt_event_name_reminder.setText(bundle.getString("NAME"));
            txt_event_date_reminder.setText(bundle.getString("DATE"));
            image_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Message", e.getMessage().toString());
        }
    }

    public class acceptEvent extends AsyncTask<String, Void, String> {

        private ProgressDialog Dialog = new ProgressDialog(getActivity());

        protected void onPreExecute() {
            Dialog.show();
            Dialog.setMessage("Please Wait....");
            Dialog.setCancelable(false);
        }

        protected String doInBackground(String... arg0) {

            try {

                URL url = new URL(Services.EVENT_ACCEPT);
                JSONObject postDataParams = new JSONObject();
                postDataParams.put("event_id", event_id);
                postDataParams.put("user_id", user_id);
                postDataParams.put("flag", flag);


                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(postDataParams));

                writer.flush();
                writer.close();
                os.close();

                int responseCode = conn.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK) {

                    BufferedReader in = new BufferedReader(new
                            InputStreamReader(
                            conn.getInputStream()));

                    StringBuffer sb = new StringBuffer("");
                    String line = "";

                    while ((line = in.readLine()) != null) {

                        sb.append(line);

                        break;
                    }

                    in.close();
                    return sb.toString();

                } else {
                    return new String("false : " + responseCode);
                }
            } catch (Exception e) {
                return new String("Exception: " + e.getMessage());
            }

        }

        @Override
        protected void onPostExecute(String result) {
            Dialog.dismiss();
            Deliverydatacategory(result);
        }
    }


    public String getPostDataString(JSONObject params) throws Exception {

        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while (itr.hasNext()) {

            String key = itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        return result.toString();
    }


    public class showUserList extends AsyncTask<String, Void, String> {

        private ProgressDialog Dialog = new ProgressDialog(getActivity());

        protected void onPreExecute() {
            Dialog.show();
            Dialog.setMessage("Please Wait....");
            Dialog.setCancelable(false);
        }

        protected String doInBackground(String... arg0) {

            try {

                URL url = new URL(Services.USER_ACCEPT_EVENT);
                JSONObject postDataParams = new JSONObject();
                postDataParams.put("event_id", event_id);

                Log.e("params", postDataParams.toString());

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(postDataParams));

                writer.flush();
                writer.close();
                os.close();

                int responseCode = conn.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK) {

                    BufferedReader in = new BufferedReader(new
                            InputStreamReader(
                            conn.getInputStream()));

                    StringBuffer sb = new StringBuffer("");
                    String line = "";

                    while ((line = in.readLine()) != null) {

                        sb.append(line);

                        break;
                    }

                    in.close();
                    return sb.toString();

                } else {
                    return new String("false : " + responseCode);
                }
            } catch (Exception e) {
                return new String("Exception: " + e.getMessage());
            }

        }

        @Override
        protected void onPostExecute(String result) {
            Dialog.dismiss();
            Deliverydatacategory1(result);
        }
    }

    private void Deliverydatacategory1(String response) {

        if (!response.equals("")) {
            try {

                JSONObject jsobjectcategory = new JSONObject(response);

                String message = jsobjectcategory.getString("message");
                String error = jsobjectcategory.getString("error");


                if (error.equals("200")) {
                    JSONArray jsonObject = jsobjectcategory.getJSONArray("data");
                    for (int i = 0; i < jsonObject.length(); i++) {
                        eventDetailListModel = new EventDetailListModel();
                        JSONObject RjsonObject = jsonObject.getJSONObject(i);
                        eventDetailListModel.setId(RjsonObject.getString("Cnt_Id"));
                        JSONArray jsonArray = RjsonObject.getJSONArray("cnt_user");
                        for (int j = 0; j < jsonArray.length(); j++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(j);
                            eventDetailListModel = new EventDetailListModel();
                            eventDetailListModel.setImages_list(jsonObject1.getString("Use_Image"));
                            data.add(eventDetailListModel);
                        }
                        eventDetailListAdapter = new EventDetailListAdapter(getActivity(), data);
                        eventDetailListAdapter.notifyDataSetChanged();
                        rv_event_detail_list.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayout.HORIZONTAL, false));
                        rv_event_detail_list.setAdapter(eventDetailListAdapter);
                    }
                } else {
                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private void Deliverydatacategory(String response) {

        if (!response.equals("")) {
            try {

                JSONObject jsobjectcategory = new JSONObject(response);

                String message = jsobjectcategory.getString("message");
                String requestcode = jsobjectcategory.getString("status");
                String error = jsobjectcategory.getString("error");


                if (error.equals("200")) {
                    String profile = jsobjectcategory.getString("data");
                    JSONObject jsonObject = new JSONObject(profile);
                    img_event_decline.setVisibility(View.VISIBLE);
                    img_event_accept.setVisibility(View.GONE);
                    Use_Id = jsonObject.getString("Cnt_User_Id");
                    android.app.Fragment someFragment = new EventFragment();
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.content, someFragment); // give your fragment container id in first parameter
                    transaction.addToBackStack(null);  // if written, this transaction will be added to backstack
                    transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                    transaction.commit();
                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();

                } else {
                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    public class acceptDeclineEvent extends AsyncTask<String, Void, String> {

        private ProgressDialog Dialog = new ProgressDialog(getActivity());

        protected void onPreExecute() {
            Dialog.show();
            Dialog.setMessage("Please Wait....");
            Dialog.setCancelable(false);
        }

        protected String doInBackground(String... arg0) {

            try {

                URL url = new URL(Services.ACCEPT_DECLINE_EVENT);
                JSONObject postDataParams = new JSONObject();
                postDataParams.put("event_id", event_id);
                postDataParams.put("user_id", user_id);

                Log.e("params", postDataParams.toString());

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(postDataParams));

                writer.flush();
                writer.close();
                os.close();

                int responseCode = conn.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK) {

                    BufferedReader in = new BufferedReader(new
                            InputStreamReader(
                            conn.getInputStream()));

                    StringBuffer sb = new StringBuffer("");
                    String line = "";

                    while ((line = in.readLine()) != null) {

                        sb.append(line);

                        break;
                    }

                    in.close();
                    return sb.toString();

                } else {
                    return new String("false : " + responseCode);
                }
            } catch (Exception e) {
                return new String("Exception: " + e.getMessage());
            }

        }

        @Override
        protected void onPostExecute(String result) {
            Dialog.dismiss();
            DeliveryAcceptDecline(result);
        }
    }

    private void DeliveryAcceptDecline(String response) {

        if (!response.equals("")) {
            try {

                JSONObject jsobjectcategory = new JSONObject(response);

                String message = jsobjectcategory.getString("message");
                String error = jsobjectcategory.getString("error");


                if (error.equals("200")) {
                    JSONObject jsonObject = jsobjectcategory.getJSONObject("data");
                    accept_decline = jsonObject.getString("Cnt_Acc_Dec");
                    if (accept_decline.equals("1")) {
                        img_event_accept.setVisibility(View.GONE);
                        img_event_decline.setVisibility(View.VISIBLE);
                    } else {
                        img_event_accept.setVisibility(View.VISIBLE);
                        img_event_decline.setVisibility(View.GONE);
                    }
                } else if (error.equals("401")) {
                    img_event_accept.setVisibility(View.VISIBLE);
                    img_event_decline.setVisibility(View.GONE);
                } else {
                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


}
