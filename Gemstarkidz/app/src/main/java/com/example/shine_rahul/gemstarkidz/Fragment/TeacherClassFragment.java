package com.example.shine_rahul.gemstarkidz.Fragment;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.circulardialog.CDialog;
import com.example.circulardialog.extras.CDConstants;
import com.example.shine_rahul.gemstarkidz.Adapter.TeacherClassStudentListAdapter;
import com.example.shine_rahul.gemstarkidz.JSONParser.JSONParser;
import com.example.shine_rahul.gemstarkidz.Model.TeacherClassModel;
import com.example.shine_rahul.gemstarkidz.R;
import com.example.shine_rahul.gemstarkidz.Service.Services;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;

import javax.net.ssl.HttpsURLConnection;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class TeacherClassFragment extends android.app.Fragment {
    CircleImageView img_teacher_class;
    int i = 0;
    private boolean isOpen = true;
    private ArrayList<Integer> mSelected = new ArrayList<Integer>();
    TextView txt_edit_toolbar;
    TextView txt_class_class;
    TextView txt_teacher_class_name;
    GridView gv_class_attendance;
    Calendar calendar;
    SimpleDateFormat mdformat;
    int flag;
    String strDate;


    GridView gv_teacher_class;
    TeacherClassModel teacherClassModel;
    TeacherClassStudentListAdapter teacherClassStudentListAdapter;
    ArrayList<TeacherClassModel> teacherClassModelArrayList = new ArrayList<>();
    ImageView search;
    int[] images = {R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1};
    String[] names = {"Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh"};
    String user = "", type = "", phone = "", uid = "", email = "", image = "", user_class, user_section, user_reg_type, Cla_Id;

    public TeacherClassFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_teacher_class, container, false);
        img_teacher_class = (CircleImageView) view.findViewById(R.id.img_teacher_class);
        txt_class_class = (TextView) view.findViewById(R.id.txt_class_class);
        txt_teacher_class_name = (TextView) view.findViewById(R.id.txt_teacher_class_name);
        search = (ImageView) getActivity().findViewById(R.id.img_search);
        txt_edit_toolbar = (TextView) getActivity().findViewById(R.id.txt_edit_toolbar);
        txt_edit_toolbar.setVisibility(View.GONE);
        search.setVisibility(View.INVISIBLE);


        SharedPreferences splogin = getActivity().getSharedPreferences("login", 0);
        user = splogin.getString("username", "");
        type = splogin.getString("type", "");
        phone = splogin.getString("user_phone", "");
        uid = splogin.getString("user_id", "");
        email = splogin.getString("user_email", "");
        image = splogin.getString("user_image", "");
        user_class = splogin.getString("user_class", "");
        user_section = splogin.getString("user_section", "");
        user_reg_type = splogin.getString("user_reg_type", "");
        Cla_Id = splogin.getString("Cla_Id", "");

        txt_teacher_class_name.setText(user_section);

        Picasso.with(getActivity())
                .load(Services.IMAGES_PATH + "profile/" + image)
                .placeholder(R.drawable.logo)
                .into(img_teacher_class);


//click image and open attendance list
        img_teacher_class.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                i++;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (i == 1) {
                            flag = 2;
                            submitAttendance();
                            new showStudentList().execute();

                        } else if (i == 2) {

                            flag = 1;
                            new EntireClassAttendance().execute();
                        }
                        i = 0;

                    }
                }, 300);

            }
        });

//        img_teacher_class.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                new EntireClassAttendance().execute();
//            }
//        });

        calendar = Calendar.getInstance();
        mdformat = new SimpleDateFormat("yyyy-MM-dd");
        strDate = mdformat.format(calendar.getTime());
        return view;
    }


    //set student list in gridview for dialogbox
    private void submitAttendance() {
        try {

            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View view1 = layoutInflater.inflate(R.layout.class_attendance, null);
            final AlertDialog.Builder alertdialog = new AlertDialog.Builder(getActivity());
            alertdialog.setView(view1);
            final AlertDialog dialog = alertdialog.create();
            gv_class_attendance = (GridView) view1.findViewById(R.id.gv_class_attendance);

            final ImageView btn_class_attendance = (ImageView) view1.findViewById(R.id.btn_class_attendance);
            dialog.setCanceledOnTouchOutside(false);

            btn_class_attendance.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new EntireClassAttendance().execute();
                    dialog.dismiss();
                }
            });
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Message", e.getMessage().toString());
        }
    }

    //   @SuppressLint("StaticFieldLeak")
//    public class showStudentList extends AsyncTask<String, Void, String> {
//
//        private ProgressDialog Dialog = new ProgressDialog(getActivity());
//
//        protected void onPreExecute() {
//            Dialog.show();
//            Dialog.setMessage("Please Wait....");
//            Dialog.setCancelable(false);
//        }
//
//        protected String doInBackground(String... arg0) {
//
//            try {
//
//                URL url = new URL(Services.ATTENDANCE_LIST);
//                JSONObject postDataParams = new JSONObject();
//                postDataParams.put("user_id ", uid);
//
//                Log.e("params", postDataParams.toString());
//
//                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//                conn.setReadTimeout(15000);
//                conn.setConnectTimeout(15000);
//                conn.setRequestMethod("POST");
//                conn.setDoInput(true);
//                conn.setDoOutput(true);
//
//
//                OutputStream os = conn.getOutputStream();
//                BufferedWriter writer = new BufferedWriter(
//                        new OutputStreamWriter(os, "UTF-8"));
//                writer.write(getPostDataString(postDataParams));
//
//                writer.flush();
//                writer.close();
//                os.close();
//
//                int responseCode = conn.getResponseCode();
//
//                if (responseCode == HttpsURLConnection.HTTP_OK) {
//
//                    BufferedReader in = new BufferedReader(new
//                            InputStreamReader(
//                            conn.getInputStream()));
//
//                    StringBuffer sb = new StringBuffer(" ");
//                    String line = "";
//
//                    while ((line = in.readLine()) != null) {
//
//                        sb.append(line);
//
//                        break;
//                    }
//
//                    in.close();
//                    String message = sb.toString();
//
//                    return sb.toString();
//
//                } else {
//                    return new String("false : " + responseCode);
//                }
//            } catch (Exception e) {
//                return new String("Exception: " + e.getMessage());
//            }
//
//        }
//
//        @Override
//        protected void onPostExecute(String result) {
//            Dialog.dismiss();
//            Deliverydatacategory1(result);
//        }
//    }
//
//    private void Deliverydatacategory1(String response) {
//
//        if (!response.equals("")) {
//            try {
//
//                JSONObject jsobjectcategory = new JSONObject(response);
//
//                String message = jsobjectcategory.getString("message");
//                String error = jsobjectcategory.getString("error");
//
//
//                if (error.equals("200")) {
//                    JSONArray jsonObject = jsobjectcategory.getJSONArray("data");
//                    for (int i = 0; i < jsonObject.length(); i++) {
//                        teacherClassModel = new TeacherClassModel();
//                        JSONObject RjsonObject = jsonObject.getJSONObject(i);
//                        teacherClassModel.setId(RjsonObject.getString("Use_Id"));
//                        JSONArray jsonArray = RjsonObject.getJSONArray("use_student");
//                        for (int j = 0; j < jsonArray.length(); j++) {
//                            JSONObject jsonObject1 = jsonArray.getJSONObject(j);
//                            teacherClassModel = new TeacherClassModel();
//                            teacherClassModel.setStudent_name(jsonObject1.getString("Std_Name"));
//                            teacherClassModel.setStudent_image(jsonObject1.getString("Std_Image"));
//                            teacherClassModelArrayList.add(teacherClassModel);
//
//                        }
//                        teacherClassStudentListAdapter = new TeacherClassStudentListAdapter(getActivity(), teacherClassModelArrayList);
//                        teacherClassStudentListAdapter.notifyDataSetChanged();
//                        gv_class_attendance.setAdapter(teacherClassStudentListAdapter);
//
//                    }
//
//                } else {
//                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//    }
//
//    public String getPostDataString(JSONObject params) throws Exception {
//
//        StringBuilder result = new StringBuilder();
//        boolean first = true;
//
//        Iterator<String> itr = params.keys();
//
//        while (itr.hasNext()) {
//
//            String key = itr.next();
//            Object value = params.get(key);
//
//            if (first)
//                first = false;
//            else
//                result.append("&");
//
//            result.append(URLEncoder.encode(key, "UTF-8"));
//            result.append("=");
//            result.append(URLEncoder.encode(value.toString(), "UTF-8"));
//
//        }
//        return result.toString();
//    }


    public class showStudentList extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Loading");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            HashMap<String, String> mapParams = new HashMap<>();
            mapParams.put("user_id", uid);
            String response = new JSONParser().sendPostRequest(Services.ATTENDANCE_LIST, mapParams);
            return response;
        }

        protected void onPostExecute(String response) {
            progressDialog.dismiss();
            if (response != null && !response.equals("")) {
                try {
                    JSONObject mjsonObject = new JSONObject(response);
                    String error = mjsonObject.getString("error");
                    String mesage = mjsonObject.getString("message");
                    if (error.equals("200")) {
                        JSONArray jsonArray = mjsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            teacherClassModel = new TeacherClassModel();
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            teacherClassModel.setTeacher_id(jsonObject.getString("Use_Id"));
                            JSONArray jsonArray_user_list = jsonObject.getJSONArray("use_student");
                            teacherClassModelArrayList.clear();
                            for (int j = 0; j < jsonArray_user_list.length(); j++) {
                                JSONObject jsonObject_student_list = jsonArray_user_list.getJSONObject(j);
                                teacherClassModel = new TeacherClassModel();
                                teacherClassModel.setStudent_id(jsonObject_student_list.getString("Std_Id"));
                                teacherClassModel.setStudent_image(jsonObject_student_list.getString("Std_Image"));
                                teacherClassModel.setStudent_name(jsonObject_student_list.getString("Std_Name"));
                                teacherClassModelArrayList.add(teacherClassModel);
                            }

                        }
                        teacherClassStudentListAdapter = new TeacherClassStudentListAdapter(getActivity(), teacherClassModelArrayList);
                        teacherClassStudentListAdapter.notifyDataSetChanged();
                        gv_class_attendance.setAdapter(teacherClassStudentListAdapter);
                    } else {
                        Toast.makeText(getActivity(), mesage, Toast.LENGTH_LONG).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }


    public class EntireClassAttendance extends AsyncTask<String, Void, String> {

        private ProgressDialog Dialog = new ProgressDialog(getActivity());

        protected void onPreExecute() {
            Dialog.show();
            Dialog.setMessage("Please Wait....");
            Dialog.setCancelable(false);
        }

        protected String doInBackground(String... arg0) {

            try {

                URL url = new URL(Services.ATTENDANCE_SUBMIT);
                JSONObject postDataParams = new JSONObject();

                String str = "";
                if (TeacherClassStudentListAdapter.studentId != null) {
                    if (TeacherClassStudentListAdapter.studentId.size() > 0) {
                        str = android.text.TextUtils.join(",", TeacherClassStudentListAdapter.studentId);
                    }
                }

                postDataParams.put("user_id", uid);
                postDataParams.put("class_id", Cla_Id);
                postDataParams.put("flag", flag);
                postDataParams.put("date", strDate);
                postDataParams.put("student_id", str);

                Log.e("params", postDataParams.toString());

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(postDataParams));

                writer.flush();
                writer.close();
                os.close();

                int responseCode = conn.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK) {

                    BufferedReader in = new BufferedReader(new
                            InputStreamReader(
                            conn.getInputStream()));

                    StringBuffer sb = new StringBuffer("");
                    String line = "";

                    while ((line = in.readLine()) != null) {

                        sb.append(line);

                        break;
                    }

                    in.close();
                    return sb.toString();

                } else {
                    return new String("false : " + responseCode);
                }
            } catch (Exception e) {
                return new String("Exception: " + e.getMessage());
            }

        }

        @Override
        protected void onPostExecute(String result) {
            Dialog.dismiss();
            Deliverydatacategory(result);

        }
    }

    public String getPostDataString(JSONObject params) throws Exception {

        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while (itr.hasNext()) {

            String key = itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        return result.toString();
    }

    private void Deliverydatacategory(String response) {

        if (!response.equals("")) {
            try {

                JSONObject jsobjectcategory = new JSONObject(response);

                String message = jsobjectcategory.getString("message");
                String requestcode = jsobjectcategory.getString("status");
                String error = jsobjectcategory.getString("error");


                if (error.equals("200")) {
                    String profile = jsobjectcategory.getString("data");
                    JSONObject jsonObject = new JSONObject(profile);
                    String Att_Cla_Id = jsonObject.getString("Att_Cla_Id");
                    String Att_Stu_Id = jsonObject.getString("Att_Stu_Id");
                    String Att_Pre_Abs = jsonObject.getString("Att_Pre_Abs");
                    String Att_Flag = jsonObject.getString("Att_Flag");
                    String Att_Date = jsonObject.getString("Att_Date");
                    String Att_CreatedBy = jsonObject.getString("Att_CreatedBy");
                    String Att_UpdatedBy = jsonObject.getString("Att_UpdatedBy");
                    String id = jsonObject.getString("id");

                    new CDialog(getActivity()).createAlert(message,
                            CDConstants.SUCCESS,   // Type of dialog
                            CDConstants.LARGE)    //  size of dialog
                            .setAnimation(CDConstants.SCALE_FROM_BOTTOM_TO_TOP)     //  Animation for enter/exit
                            .setDuration(2000)   // in milliseconds
                            .setTextSize(CDConstants.SMALL)  // CDConstants.LARGE_TEXT_SIZE, CDConstants.NORMAL_TEXT_SIZE
                            .show();


                } else if (error.equals("401")) {
                    new CDialog(getActivity()).createAlert(message,
                            CDConstants.ERROR,   // Type of dialog
                            CDConstants.LARGE)    //  size of dialog
                            .setAnimation(CDConstants.SCALE_FROM_BOTTOM_TO_TOP)     //  Animation for enter/exit
                            .setDuration(2000)   // in milliseconds
                            .setTextSize(CDConstants.SMALL)  // CDConstants.LARGE_TEXT_SIZE, CDConstants.NORMAL_TEXT_SIZE
                            .show();

                } else {
                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


}
