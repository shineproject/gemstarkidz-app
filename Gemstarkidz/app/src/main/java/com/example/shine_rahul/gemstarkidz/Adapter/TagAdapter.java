package com.example.shine_rahul.gemstarkidz.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.shine_rahul.gemstarkidz.Model.TeacherClassModel;
import com.example.shine_rahul.gemstarkidz.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Shine-Rahul on 16/3/2018.
 */

public class TagAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<TeacherClassModel> data;
    private LayoutInflater layoutInflater = null;

    public TagAdapter(Context context, ArrayList<TeacherClassModel> data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        if (convertView == null) {
            layoutInflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.tag, null);
        }
        try {
            final CircleImageView img_entire_class;
            TextView txt_entire_class_student_name;
            txt_entire_class_student_name = (TextView) view.findViewById(R.id.txt_entire_class_student_name);
            img_entire_class = (CircleImageView) view.findViewById(R.id.img_entire_class);

            final TeacherClassModel teacherClassModel = data.get(position);
            txt_entire_class_student_name.setText(teacherClassModel.getStudent_name());

            Picasso.with(context)
                    .load(teacherClassModel.getStudent_image())
                    .error(R.drawable.profile)
                    .into(img_entire_class);
            view.setOnClickListener(new View.OnClickListener() {
                @SuppressLint("NewApi")
                @Override
                public void onClick(View v) {
//                    img_teacher_class_student_list.setBackground(context.getResources().getDrawable(R.drawable.image_background));


                    if (img_entire_class.getBorderColor() == context.getResources().getColor(R.color.colorTheme)) {
                        img_entire_class.setBorderColor(context.getResources().getColor(R.color.colorRing));
                        img_entire_class.setBorderWidth(10);
                    } else {
                        img_entire_class.setBorderColor(context.getResources().getColor(R.color.colorTheme));
                        img_entire_class.setBorderWidth(10);
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Error", e.getMessage().toString());
        }
        return view;
    }
}
