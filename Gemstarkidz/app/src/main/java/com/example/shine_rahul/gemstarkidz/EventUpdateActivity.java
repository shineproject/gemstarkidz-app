package com.example.shine_rahul.gemstarkidz;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shine_rahul.gemstarkidz.Fragment.ProfileParentFragment;
import com.example.shine_rahul.gemstarkidz.Fragment.SocialFragment;
import com.example.shine_rahul.gemstarkidz.Fragment.TeacherEventFragment;
import com.example.shine_rahul.gemstarkidz.Model.EventModel;
import com.example.shine_rahul.gemstarkidz.Service.Services;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class EventUpdateActivity extends AppCompatActivity implements View.OnClickListener {
    CircleImageView img_update_event;
    CircleImageView img_edit_update_event;
    ImageView btn_update_event;
    EditText edt_event_update_title;
    EditText edt_start_update_date;
    EditText edt_end_update_date;
    EditText edt_event_update_Description;
    ActionBar actionBar;
    private Bitmap bitmap;
    String Eve_Id, Eve_Use_Id, Eve_user_Name, Eve_Start_Date, Eve_End_Date, Eve_Description, Eve_user_Image, Eve_user_Accept;
    Bundle bundle;
    Calendar myCalendar = Calendar.getInstance();
    private int mYear = myCalendar.get(Calendar.YEAR);
    private int mMonth = myCalendar.get(Calendar.MONTH);
    private int mDay = myCalendar.get(Calendar.DAY_OF_MONTH);
    String responseimage;
    int GALLERY_PICK = 101, CAMERA_PICK = 132;
    //    String image_path = "", start_date, end_date;
    TextView txt_menu_title;
    DatePickerDialog datePickerDialog;
    String images, userimage = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_update);
        actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }


        btn_update_event = (ImageView) findViewById(R.id.btn_update_event);
        img_update_event = (CircleImageView) findViewById(R.id.img_update_event);
        img_edit_update_event = (CircleImageView) findViewById(R.id.img_edit_update_event);
        edt_event_update_title = (EditText) findViewById(R.id.edt_event_update_title);
        edt_start_update_date = (EditText) findViewById(R.id.edt_start_update_date);
        edt_end_update_date = (EditText) findViewById(R.id.edt_end_update_date);
        edt_event_update_Description = (EditText) findViewById(R.id.edt_event_update_Description);
        txt_menu_title = (TextView) findViewById(R.id.txt_menu_title);


        Intent intent = this.getIntent();
        Eve_Id = intent.getStringExtra("Eve_Id");
        Eve_Use_Id = intent.getStringExtra("Eve_CreatedBy");
        Eve_user_Name = intent.getStringExtra("Eve_user_Name");
        Eve_Start_Date = intent.getStringExtra("Eve_Start_Date");
        Eve_End_Date = intent.getStringExtra("Eve_End_Date");
        Eve_Description = intent.getStringExtra("Eve_Description");
        Eve_user_Accept = intent.getStringExtra("Eve_user_Accept");
        Eve_user_Image = intent.getStringExtra("Eve_user_Image");
        Picasso.with(EventUpdateActivity.this)
                .load(Services.IMAGES_PATH + "event/" + Eve_user_Image)
                .placeholder(R.drawable.logo)
                .into(img_update_event);


        SimpleDateFormat start_date1 = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        String set_start_date = "";
        try {
            date = start_date1.parse(Eve_Start_Date);
            start_date1.applyPattern("dd/MM/yyyy");
            if (date != null) {
                set_start_date = start_date1.format(date);
            }
            edt_start_update_date.setText(set_start_date);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        SimpleDateFormat end_date1 = new SimpleDateFormat("yyyy-MM-dd");
        Date date1 = null;
        String set_end_date = "";
        try {
            date1 = end_date1.parse(Eve_End_Date);
            end_date1.applyPattern("dd/MM/yyyy");
            if (date1 != null) {
                set_end_date = end_date1.format(date1);
            }
            edt_end_update_date.setText(set_end_date);
        } catch (Exception e) {
            e.printStackTrace();
        }


        edt_event_update_title.setText(Eve_user_Name);
        edt_event_update_Description.setText(Eve_Description);


        edt_start_update_date.setOnClickListener(this);
        edt_end_update_date.setOnClickListener(this);
        edt_start_update_date.setFocusable(false);
        edt_end_update_date.setFocusable(false);
        edt_start_update_date.setFocusableInTouchMode(false);
        edt_end_update_date.setFocusableInTouchMode(false);
        img_edit_update_event.setOnClickListener(this);
        btn_update_event.setOnClickListener(this);


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                overridePendingTransition(0, 0);
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View v) {
        if (v == edt_start_update_date) {
            final Calendar calendar = Calendar.getInstance();
            int YEAR, MONTH, DAY;
            YEAR = calendar.get(Calendar.YEAR);
            MONTH = calendar.get(Calendar.MONTH);
            DAY = calendar.get(Calendar.DAY_OF_MONTH);


            datePickerDialog = new DatePickerDialog(EventUpdateActivity.this,
                    new DatePickerDialog.OnDateSetListener() {


                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {


                            Eve_Start_Date = year + "-" + ((monthOfYear + 1) > 9 ? (monthOfYear + 1) : "0" + (monthOfYear + 1)) + "-" +
                                    (dayOfMonth > 9 ? dayOfMonth : "0" + dayOfMonth);

                            SimpleDateFormat update_start_date = new SimpleDateFormat("yyyy-MM-dd");
                            Date date = null;
                            String set_start_date = "";
                            try {
                                date = update_start_date.parse(Eve_Start_Date);
                                update_start_date.applyPattern("dd/MM/yyyy");
                                if (date != null) {
                                    set_start_date = update_start_date.format(date);
                                }
                                edt_start_update_date.setText(set_start_date);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }


                    }, YEAR, MONTH, DAY);
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            datePickerDialog.show();
        }
        if (v == edt_end_update_date)

        {
            final Calendar calendar = Calendar.getInstance();
            int YEAR, MONTH, DAY;
            YEAR = calendar.get(Calendar.YEAR);
            MONTH = calendar.get(Calendar.MONTH);
            DAY = calendar.get(Calendar.DAY_OF_MONTH);


            datePickerDialog = new DatePickerDialog(EventUpdateActivity.this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {

                            Eve_End_Date = year + "-" + ((monthOfYear + 1) > 9 ? (monthOfYear + 1) : "0" + (monthOfYear + 1)) + "-" +
                                    (dayOfMonth > 9 ? dayOfMonth : "0" + dayOfMonth);
                            //  Toast.makeText(EventUpdateActivity.this, start_date, Toast.LENGTH_LONG).show();

                            if (Eve_End_Date.compareTo(Eve_Start_Date) < 0) {
                                Toast.makeText(EventUpdateActivity.this, "start date are grater for end date", Toast.LENGTH_LONG).show();
                            } else {
                                SimpleDateFormat update_start_date = new SimpleDateFormat("yyyy-MM-dd");
                                Date date = null;
                                String set_start_date = "";
                                try {
                                    date = update_start_date.parse(Eve_End_Date);
                                    update_start_date.applyPattern("dd/MM/yyyy");
                                    if (date != null) {
                                        set_start_date = update_start_date.format(date);
                                    }
                                    edt_end_update_date.setText(set_start_date);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            }


                        }
                    }, YEAR, MONTH, DAY);


            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            datePickerDialog.show();
        }
        if (v == img_edit_update_event)

        {
            showPictureDialog();
        }
        if (v == btn_update_event) {
            if (Eve_End_Date.compareTo(Eve_Start_Date) < 0) {
                Toast.makeText(EventUpdateActivity.this, "start date are grater for end date", Toast.LENGTH_LONG).show();
            } else {
                Eve_user_Name = edt_event_update_title.getText().toString().trim();
                Eve_Description = edt_event_update_Description.getText().toString().trim();
                try {

                    new AsyncTaskEventUpdate().execute();

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }

    }


    private class AsyncTaskEventUpdate extends AsyncTask<String, String, String> {

        private ProgressDialog Dialog = new ProgressDialog(EventUpdateActivity.this);

        @Override
        protected String doInBackground(String... params) {
            try {
                MultipartBody.Builder builder = new MultipartBody.Builder();

                builder.setType(MultipartBody.FORM);
                final MediaType MEDIA_TYPE = MediaType.parse("image/jpg");

                builder.addFormDataPart("event_id", Eve_Id);
                builder.addFormDataPart("user_id", Eve_Use_Id);
                if (userimage.equals("")) {
                    builder.addFormDataPart("image", "");

                } else {
                    builder.addFormDataPart("image", userimage, RequestBody.create(MEDIA_TYPE, new File(userimage)));
                }
                builder.addFormDataPart("title", Eve_user_Name);
                builder.addFormDataPart("description", Eve_Description);
                builder.addFormDataPart("start_date", Eve_Start_Date);
                builder.addFormDataPart("end_date", Eve_End_Date);
                RequestBody requestBody = builder.build();

                try {
                    OkHttpClient client = new OkHttpClient.Builder().build();
                    Request
                            request = new Request.Builder()
                            .url(Services.EVENT_UPDATE)
                            .post(requestBody)
                            .build();
                    Response resp = client.newCall(request).execute();
                    responseimage = resp.body().string();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            Dialog.dismiss();
            Deliverydatacategory(responseimage);
            super.onPostExecute(s);

        }

        @Override
        protected void onPreExecute() {
            Dialog.show();
            Dialog.setMessage("Please Wait....");
            Dialog.setCancelable(false);
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
        }

    }

    private void Deliverydatacategory(String response) {
        try {
            if (!response.equals("")) {


                JSONObject jsobjectcategory = new JSONObject(response);

                String error_code = jsobjectcategory.getString("error");
                String message = jsobjectcategory.getString("message");


                if (error_code.equals("200")) {
                    String profile = jsobjectcategory.getString("data");
                    JSONObject jsonObject = new JSONObject(profile);
                    Eve_Use_Id = jsonObject.getString("Eve_UpdatedBy");
                    Eve_user_Name = jsonObject.getString("Eve_Name");
                    Eve_Start_Date = jsonObject.getString("Eve_Date");
                    Eve_End_Date = jsonObject.getString("Eve_End_Date");
                    Eve_Description = jsonObject.getString("Eve_Description");
                    Eve_user_Image = jsonObject.getString("Eve_Image");
                    Toast.makeText(EventUpdateActivity.this, message, Toast.LENGTH_LONG).show();
                    onBackPressed();
                    finish();

                } else if (error_code.equals("401")) {
                    Toast.makeText(EventUpdateActivity.this, message, Toast.LENGTH_LONG).show();
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void showPictureDialog() {
        final String[] option = {"Select photo from gallery", "Capture photo from camera", "Cancel"};
        final AlertDialog.Builder pictureDialog = new AlertDialog.Builder(EventUpdateActivity.this);
        pictureDialog.setTitle("Select Action");

        pictureDialog.setItems(option, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                if (option[i].equals("Select photo from gallery")) {
                    Intent GALLERY = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    GALLERY.setType("image/*");
                    startActivityForResult(Intent.createChooser(GALLERY, "Select Image"), GALLERY_PICK);
                } else if (option[i].equals("Capture photo from camera")) {

                    Intent intent_camera = new Intent("android.media.action.IMAGE_CAPTURE");
                    File file = new File(Environment.getExternalStorageDirectory() + File.separator + "image.jpg");
                    intent_camera.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
                    startActivityForResult(intent_camera, CAMERA_PICK);

                } else if (option[i].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        pictureDialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == GALLERY_PICK) {
                Uri uri = data.getData();
                String[] FILE = {MediaStore.Images.Media.DATA, MediaStore.Video.Media.DATA};
                Cursor cursor = EventUpdateActivity.this.getContentResolver().query(uri, FILE, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(FILE[0]);
                userimage = cursor.getString(columnIndex);
                cursor.close();
                bitmap = BitmapFactory.decodeFile(userimage);
                img_update_event.setImageBitmap(bitmap);

            } else if (requestCode == CAMERA_PICK) {
                try {
                    userimage = Environment.getExternalStorageDirectory() + File.separator + "image.jpg";
                    bitmap = BitmapFactory.decodeFile(userimage);
                    img_update_event.setImageBitmap(bitmap);
                } catch (Exception e) {

                }

            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
