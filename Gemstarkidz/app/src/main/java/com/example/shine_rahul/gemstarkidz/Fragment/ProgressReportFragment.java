package com.example.shine_rahul.gemstarkidz.Fragment;


import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.shine_rahul.gemstarkidz.Adapter.ProgressAdapter;
import com.example.shine_rahul.gemstarkidz.Model.ProgressModel;
import com.example.shine_rahul.gemstarkidz.Model.TeacherSocialModel;
import com.example.shine_rahul.gemstarkidz.Model.TeacherSocialModelImage;
import com.example.shine_rahul.gemstarkidz.R;
import com.example.shine_rahul.gemstarkidz.Service.Services;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.formatter.LargeValueFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProgressReportFragment extends android.app.Fragment {

    BarChart chart;
    float groupSpace = 0.012f;
    float barSpace = 0.02f;
    float barWidth = 0.60f;
    int groupCount = 12;

    TextView txt_edit_toolbar;
    TextView txt_assessment;
    RecyclerView rv_progress_report;
    ArrayList<ProgressModel> data1;
    ProgressAdapter progressAdapter;
    ProgressModel progressModel;
    String[] month = {"January", "February", "March", "Aprial", "May", "June", "July", "Auguest", "September", "October", "November", "December"};
    int[] image_profile = {R.drawable.image1, R.drawable.image8, R.drawable.image3, R.drawable.image4, R.drawable.image5, R.drawable.image6, R.drawable.image7, R.drawable.image8, R.drawable.image9, R.drawable.image10, R.drawable.readmsgicon, R.drawable.image};

    String username = "", user_type = "", user_phone = "", user_id = "", user_email = "", user_image = "", user_reg_type = "";

    public ProgressReportFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_progress_report, container, false);
        chart = (BarChart) view.findViewById(R.id.barChart);
        txt_edit_toolbar = (TextView) getActivity().findViewById(R.id.txt_edit_toolbar);
        txt_edit_toolbar.setVisibility(View.GONE);
        chart.setDescription(null);
        chart.setPinchZoom(false);
        chart.setScaleEnabled(false);
        chart.setDrawBarShadow(false);
        chart.setDrawGridBackground(false);


        SharedPreferences splogin = getActivity().getSharedPreferences("login", 0);
        username = splogin.getString("username", "");
        user_type = splogin.getString("type", "");
        user_phone = splogin.getString("user_phone", "");
        user_id = splogin.getString("user_id", "");
        user_email = splogin.getString("user_email", "");
        user_image = splogin.getString("user_image", "");
        user_reg_type = splogin.getString("user_reg_type", "");


        ArrayList xVals = new ArrayList();

        xVals.add("Jan");
        xVals.add("Feb");
        xVals.add("Mar");
        xVals.add("Apr");
        xVals.add("May");
        xVals.add("Jun");

        ArrayList yVals1 = new ArrayList();
        ArrayList yVals2 = new ArrayList();

        yVals1.add(new BarEntry(1, (float) 1));
        yVals2.add(new BarEntry(2, (float) 2));
        yVals1.add(new BarEntry(3, (float) 3));
        yVals2.add(new BarEntry(4, (float) 4));
        yVals1.add(new BarEntry(5, (float) 5));
        yVals2.add(new BarEntry(6, (float) 6));
        yVals1.add(new BarEntry(7, (float) 7));
        yVals2.add(new BarEntry(8, (float) 8));
        yVals1.add(new BarEntry(9, (float) 9));
        yVals2.add(new BarEntry(10, (float) 10));
        yVals1.add(new BarEntry(11, (float) 11));
        yVals2.add(new BarEntry(12, (float) 12));

        BarDataSet set1, set2;
        set1 = new BarDataSet(yVals1, "A");
        set1.setColor(Color.RED);
        set2 = new BarDataSet(yVals2, "B");
        set2.setColor(Color.BLUE);
        BarData data = new BarData(set1, set2);
        data.setValueFormatter(new LargeValueFormatter());
        chart.setData(data);
        chart.getBarData().setBarWidth(barWidth);
        chart.getXAxis().setAxisMinimum(0);
        chart.getXAxis().setAxisMaximum(0 + chart.getBarData().getGroupWidth(groupSpace, barSpace) * 1);
        chart.groupBars(0, groupSpace, barSpace);
        chart.getData().setHighlightEnabled(false);
        chart.invalidate();

        Legend l = chart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(true);
        l.setYOffset(0f);
        l.setXOffset(0f);
        l.setYEntrySpace(0f);
        l.setTextSize(0f);

        XAxis xAxis = chart.getXAxis();
        xAxis.setGranularity(1f);
        xAxis.setGranularityEnabled(true);
        xAxis.setCenterAxisLabels(true);
        xAxis.setDrawGridLines(false);
        xAxis.setAxisMaximum(12);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setValueFormatter(new IndexAxisValueFormatter(xVals));
//Y-axis
        chart.getAxisRight().setEnabled(false);
        YAxis leftAxis = chart.getAxisLeft();
        leftAxis.setValueFormatter(new LargeValueFormatter());
        leftAxis.setDrawGridLines(true);
        leftAxis.setSpaceTop(35f);
        leftAxis.setAxisMinimum(0f);

        txt_assessment = (TextView) view.findViewById(R.id.txt_assessment);
        rv_progress_report = (RecyclerView) view.findViewById(R.id.rv_progress_report);
        chart.animateY(6000);
        Typeface style = Typeface.createFromAsset(getActivity().getAssets(), "Raleway-Regular.ttf");
        txt_assessment.setTypeface(style);

        data1 = new ArrayList<>();
        for (int i = 0; i < month.length; i++) {
            progressModel = new ProgressModel();
            progressModel.setImage(image_profile[i]);
            progressModel.setMonth(month[i]);
            data1.add(progressModel);
        }
        progressAdapter = new ProgressAdapter(getActivity(), data1);
        progressAdapter.notifyDataSetChanged();
        rv_progress_report.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        rv_progress_report.setAdapter(progressAdapter);
        return view;
    }


}
