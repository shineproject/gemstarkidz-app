package com.example.shine_rahul.gemstarkidz.Fragment;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.shine_rahul.gemstarkidz.Adapter.Student_Class_Adapter;
import com.example.shine_rahul.gemstarkidz.Model.Student_Class_Model;
import com.example.shine_rahul.gemstarkidz.R;

import java.util.ArrayList;

public class AttendanceFragment extends android.app.Fragment {
    TextView txt_attendance_social;
    TextView txt_attendance_chat;
    TextView txt_attendance_event;
    TextView txt_attendance_attendance;
    TextView txt_menu_title;
    Spinner sp_attendance_class;
    Spinner sp_attendance_section;
    String[] Class = {"I", "II", "III", "IV", "V", "VI", "VII", "VIII", "XI", "X"};
    String[] Section = {"A", "B", "C", "D", "E",};
    Student_Class_Adapter student_class__adapter;
    ArrayList<Student_Class_Model> data;
    TextView txt_attendance_daily;
    TextView txt_attendance_monthly;
    TextView txt_edit_toolbar;
    View view_attendance_daily;
    View view_attendance_monthly;
    ImageView calender_visible_attendance, search;
    CalendarView calendar;


    public AttendanceFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_attendance, container, false);
        calendar = (CalendarView) view.findViewById(R.id.calendar);
        txt_attendance_social = (TextView) view.findViewById(R.id.txt_attendance_social);
        txt_attendance_chat = (TextView) view.findViewById(R.id.txt_attendance_chat);
        txt_attendance_event = (TextView) view.findViewById(R.id.txt_attendance_event);
        txt_attendance_attendance = (TextView) view.findViewById(R.id.txt_attendance_attendance);
        txt_menu_title = (TextView) getActivity().findViewById(R.id.txt_menu_title);
        sp_attendance_class = (Spinner) view.findViewById(R.id.sp_attendance_class);
        sp_attendance_section = (Spinner) view.findViewById(R.id.sp_attendance_section);
        txt_attendance_daily = (TextView) view.findViewById(R.id.txt_attendance_daily);
        txt_attendance_monthly = (TextView) view.findViewById(R.id.txt_attendance_monthly);
        view_attendance_daily = (View) view.findViewById(R.id.view_attendance_daily);
        view_attendance_monthly = (View) view.findViewById(R.id.view_attendance_monthly);
        txt_attendance_attendance.setTextColor(getResources().getColor(R.color.colorTheme));
        calender_visible_attendance = (ImageView) view.findViewById(R.id.calender_visible_attendance);
        search = (ImageView) getActivity().findViewById(R.id.img_search);
        txt_edit_toolbar = (TextView) getActivity().findViewById(R.id.txt_edit_toolbar);
        txt_edit_toolbar.setVisibility(View.GONE);
        calendar.setVisibility(View.GONE);


        Typeface style = Typeface.createFromAsset(getActivity().getAssets(), "Raleway-Bold.ttf");
        txt_attendance_social.setTypeface(style);
        txt_attendance_chat.setTypeface(style);
        txt_attendance_event.setTypeface(style);
        txt_attendance_attendance.setTypeface(style);

        txt_attendance_daily.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                view_attendance_monthly.setVisibility(View.INVISIBLE);
                view_attendance_daily.setVisibility(View.VISIBLE);
            }
        });
        txt_attendance_monthly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                view_attendance_monthly.setVisibility(View.VISIBLE);
                view_attendance_daily.setVisibility(View.INVISIBLE);
            }
        });

        txt_attendance_social.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt_attendance_attendance.setTextColor(getResources().getColor(R.color.colorText));
                txt_attendance_social.setTextColor(getResources().getColor(R.color.colorTheme));
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        android.app.Fragment fragment = new SocialFragment();
                        FragmentManager fragmentManager = getActivity().getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.content, fragment);
                        fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commit();
                        search.setVisibility(View.VISIBLE);
                        txt_menu_title.setText("Social");

                    }
                }, 100);

            }
        });
        txt_attendance_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt_attendance_attendance.setTextColor(getResources().getColor(R.color.colorText));
                txt_attendance_chat.setTextColor(getResources().getColor(R.color.colorTheme));
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        android.app.Fragment fragment = new ParentChatFragment();
                        FragmentManager fragmentManager = getActivity().getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.content, fragment);
                        fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commit();
                        search.setVisibility(View.VISIBLE);
                        txt_menu_title.setText("Chat");

                    }
                }, 100);

            }
        });
        txt_attendance_event.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt_attendance_attendance.setTextColor(getResources().getColor(R.color.colorText));
                txt_attendance_event.setTextColor(getResources().getColor(R.color.colorTheme));
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        android.app.Fragment fragment = new EventFragment();
                        FragmentManager fragmentManager = getActivity().getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.content, fragment);
                        fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commit();
                        search.setVisibility(View.VISIBLE);
                        txt_menu_title.setText("Event");

                    }
                }, 100);

            }
        });

        //set class data in spinner
        data = new ArrayList<>();
        for (int i = 0; i < Class.length; i++) {
            Student_Class_Model student_class__model = new Student_Class_Model();
            student_class__model.setName(Class[i]);
            data.add(student_class__model);
        }
        student_class__adapter = new Student_Class_Adapter(getActivity(), data);
        sp_attendance_class.setAdapter(student_class__adapter);


        //set Section data in spinner
        data = new ArrayList<>();
        for (int i = 0; i < Section.length; i++) {
            Student_Class_Model student_class__model = new Student_Class_Model();
            student_class__model.setName(Section[i]);
            data.add(student_class__model);
        }
        student_class__adapter = new Student_Class_Adapter(getActivity(), data);
        sp_attendance_section.setAdapter(student_class__adapter);


        calender_visible_attendance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (calendar.getVisibility() == View.GONE) {
                    calendar.setVisibility(View.VISIBLE);
                } else {
                    calendar.setVisibility(View.GONE);
                }
            }
        });
        return view;
    }

}
