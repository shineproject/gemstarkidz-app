package com.example.shine_rahul.gemstarkidz.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.shine_rahul.gemstarkidz.Model.CommentListModel;
import com.example.shine_rahul.gemstarkidz.R;
import com.example.shine_rahul.gemstarkidz.Service.Services;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Shine-Rahul on 23/3/2018.
 */

public class CommentListAdapter extends RecyclerView.Adapter<CommentListAdapter.ViewHolder> {
    private Context context;
    private ArrayList<CommentListModel> data;

    public CommentListAdapter(Context context, ArrayList<CommentListModel> data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public CommentListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.comment_list, null);
        return new CommentListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CommentListAdapter.ViewHolder holder, int position) {

        CommentListModel commentListModel = data.get(position);
        holder.txt_comment_name.setText(commentListModel.getUse_Name());
        holder.txt_comment.setText(commentListModel.getSac_Comment());
        holder.txt_comment_time.setText(commentListModel.getSac_CreatedAt());

        Picasso.with(context)
                .load(Services.IMAGES_PATH + "profile/" + commentListModel.getUse_Image())
                .placeholder(R.drawable.logo)
                .into(holder.img_comment);

        // Glide.with(context).load(commentListModel.getImage()).placeholder(R.drawable.logo).into(holder.img_comment);

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img_comment;
        TextView txt_comment_name;
        TextView txt_comment;
        TextView txt_comment_time;

        public ViewHolder(View itemView) {
            super(itemView);
            img_comment = (ImageView) itemView.findViewById(R.id.img_comment);
            txt_comment_name = (TextView) itemView.findViewById(R.id.txt_comment_name);
            txt_comment = (TextView) itemView.findViewById(R.id.txt_comment);
            txt_comment_time = (TextView) itemView.findViewById(R.id.txt_comment_time);
        }
    }
}
