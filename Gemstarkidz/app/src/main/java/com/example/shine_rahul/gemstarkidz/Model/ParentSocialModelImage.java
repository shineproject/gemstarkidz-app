package com.example.shine_rahul.gemstarkidz.Model;

public class ParentSocialModelImage {

    String Sod_Id;
    String Sod_Use_Id;
    String Sod_Soa_Id;
    String Sod_Uploadtype;
    String Sod_Img_Video;
    String Sod_CreatedAt;
    String Sod_UpdatedAt;

    public String getSod_Id() {
        return Sod_Id;
    }

    public void setSod_Id(String sod_Id) {
        Sod_Id = sod_Id;
    }

    public String getSod_Use_Id() {
        return Sod_Use_Id;
    }

    public void setSod_Use_Id(String sod_Use_Id) {
        Sod_Use_Id = sod_Use_Id;
    }

    public String getSod_Soa_Id() {
        return Sod_Soa_Id;
    }

    public void setSod_Soa_Id(String sod_Soa_Id) {
        Sod_Soa_Id = sod_Soa_Id;
    }

    public String getSod_Uploadtype() {
        return Sod_Uploadtype;
    }

    public void setSod_Uploadtype(String sod_Uploadtype) {
        Sod_Uploadtype = sod_Uploadtype;
    }

    public String getSod_Img_Video() {
        return Sod_Img_Video;
    }

    public void setSod_Img_Video(String sod_Img_Video) {
        Sod_Img_Video = sod_Img_Video;
    }

    public String getSod_CreatedAt() {
        return Sod_CreatedAt;
    }

    public void setSod_CreatedAt(String sod_CreatedAt) {
        Sod_CreatedAt = sod_CreatedAt;
    }

    public String getSod_UpdatedAt() {
        return Sod_UpdatedAt;
    }

    public void setSod_UpdatedAt(String sod_UpdatedAt) {
        Sod_UpdatedAt = sod_UpdatedAt;
    }
}
