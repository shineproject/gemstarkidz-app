package com.example.shine_rahul.gemstarkidz.Fragment;


import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.shine_rahul.gemstarkidz.Adapter.TeacherReportAdapter;
import com.example.shine_rahul.gemstarkidz.Model.TeacherReportModel;
import com.example.shine_rahul.gemstarkidz.Model.TeacherSocialModel;
import com.example.shine_rahul.gemstarkidz.Model.TeacherSocialModelImage;
import com.example.shine_rahul.gemstarkidz.R;
import com.example.shine_rahul.gemstarkidz.SearchActivity;
import com.example.shine_rahul.gemstarkidz.Service.Services;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class TeacherReportFragment extends android.app.Fragment {
    int[] image_profile = {R.drawable.image1, R.drawable.image8, R.drawable.image3, R.drawable.image4, R.drawable.image5, R.drawable.image6, R.drawable.image7, R.drawable.image8, R.drawable.image9, R.drawable.image10};
    int[] chart = {R.drawable.chart, R.drawable.chart, R.drawable.chart, R.drawable.chart, R.drawable.chart, R.drawable.chart, R.drawable.chart, R.drawable.chart, R.drawable.chart, R.drawable.chart};

    String[] name = {"Kamlesh", "Bhargav", "Dhruv", "Jaymin", "Bilal", "Akshay", "Harshal", "Vineet", "Lucky", "Vipul"};
    String[] grnumber = {"11524", "21545", "25445", "32154", "35258", "21455", "11245", "24545", "245", "35534"};
    String[] student_class = {"II", "III", "XII", "XI", "X", "I", "IV", "V", "VI", "III"};
    String[] section = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J"};

    RecyclerView rv_teacher_report;
    TeacherReportAdapter teacherReportAdapter;
    ArrayList<TeacherReportModel> dataReport;

    TextView txt_edit_toolbar;
    ImageView img_search, img_close;
    EditText edt_search;
    TextView txt_menu_title;

    String resultReport = "";
    String user = "", type = "", phone = "", uid = "", email = "", user_image = "", user_class, user_section, Cla_Id = "";

    public TeacherReportFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_teacher_report, container, false);

        txt_edit_toolbar = (TextView) getActivity().findViewById(R.id.txt_edit_toolbar);
        img_search = (ImageView) getActivity().findViewById(R.id.img_search);
        img_close = (ImageView) getActivity().findViewById(R.id.img_close);
        edt_search = (EditText) getActivity().findViewById(R.id.edt_search);
        txt_menu_title = (TextView) getActivity().findViewById(R.id.txt_menu_title);

        rv_teacher_report = (RecyclerView) view.findViewById(R.id.rv_teacher_report);
        txt_edit_toolbar = (TextView) getActivity().findViewById(R.id.txt_edit_toolbar);
        edt_search = (EditText) getActivity().findViewById(R.id.edt_search);
        img_close = (ImageView) getActivity().findViewById(R.id.img_close);
        txt_menu_title = (TextView) getActivity().findViewById(R.id.txt_menu_title);
        txt_menu_title.setVisibility(View.VISIBLE);
        txt_menu_title.setText("Report");
        txt_edit_toolbar.setVisibility(View.GONE);
        img_search.setVisibility(View.VISIBLE);
        edt_search.setVisibility(View.GONE);
        img_close.setVisibility(View.GONE);

        SharedPreferences splogin = getActivity().getSharedPreferences("login", 0);
        user = splogin.getString("username", "");
        type = splogin.getString("type", "");
        phone = splogin.getString("user_phone", "");
        uid = splogin.getString("user_id", "");
        email = splogin.getString("user_email", "");
        user_image = splogin.getString("user_image", "");
        user_class = splogin.getString("user_class", "");
        user_section = splogin.getString("user_section", "");
        Cla_Id = splogin.getString("Cla_Id", "");

        new AsyncTaskRunnerGetPostData().execute();

//        data = new ArrayList<>();
//        for (int i = 0; i < image_profile.length; i++) {
//            teacherReportModel = new TeacherReportModel();
//            teacherReportModel.setImage(image_profile[i]);
//            teacherReportModel.setName(name[i]);
//            teacherReportModel.setGrnumber(grnumber[i]);
//            teacherReportModel.setStudent_class(student_class[i]);
//            teacherReportModel.setSection(section[i]);
//            teacherReportModel.setChart(chart[i]);
//            data.add(teacherReportModel);
//        }
//        teacherReportAdapter = new TeacherReportAdapter(getActivity(), data);
//        teacherReportAdapter.notifyDataSetChanged();
//        rv_teacher_report.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayout.VERTICAL, false));
//        rv_teacher_report.setAdapter(teacherReportAdapter);

        img_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                img_search.setVisibility(View.GONE);
                txt_menu_title.setVisibility(View.GONE);
                edt_search.setVisibility(View.VISIBLE);
                img_close.setVisibility(View.VISIBLE);
                edt_search.setText("");
                edt_search.requestFocus();
                edt_search.setFocusable(true);
            }
        });

        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                img_close.setVisibility(View.GONE);
                edt_search.setVisibility(View.GONE);
                img_search.setVisibility(View.VISIBLE);
                txt_menu_title.setVisibility(View.VISIBLE);
                txt_menu_title.setText("Event");
            }
        });

        edt_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                teacherReportAdapter.filter(edt_search.getText().toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        return view;
    }

    private class AsyncTaskRunnerGetPostData extends AsyncTask<String, String, String> {

        private ProgressDialog Dialog = new ProgressDialog(getActivity());

        @Override
        protected String doInBackground(String... params) {

            MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
            builder.addFormDataPart("user_id",uid );
            builder.addFormDataPart("class_id",Cla_Id );
            RequestBody requestBody = builder.build();

            try {
                OkHttpClient client = new OkHttpClient.Builder().build();
                Request request = new Request.Builder()
                        .url(Services.SOCIAL_SCREEN_REPORT)
                        .post(requestBody)
                        .build();
                Response resp = client.newCall(request).execute();
                resultReport = resp.body().string();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            Dialog.dismiss();
            GetReportData(resultReport);
            super.onPostExecute(s);
        }

        @Override
        protected void onPreExecute() {
            Dialog.show();
            Dialog.setMessage("Please Wait....");
            Dialog.setCancelable(false);
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
        }

    }

    private void GetReportData(String response) {

        TeacherReportModel teacherReportModel;
        dataReport = new ArrayList<>();

        if (!response.equals("")) {
            try {

                JSONObject jsobjectcategory = new JSONObject(response);
                String message = jsobjectcategory.getString("message");
                // Updated upstream
                String error = jsobjectcategory.getString("error");

                if (error.equals("200")) {

                    JSONArray jsonArray = jsobjectcategory.getJSONArray("data");
                    JSONObject jsonObject = null;
                    for (int i = 0; i < jsonArray.length(); i++) {
                        jsonObject = jsonArray.getJSONObject(i);

                        teacherReportModel = new TeacherReportModel();

                        JSONArray jsonArrayD = jsonObject.getJSONArray("use_student");
                        for (int j = 0; j < jsonArrayD.length(); j++) {
                            JSONObject jsonObjectD = jsonArrayD.getJSONObject(j);
                            teacherReportModel.setCla_Class(jsonObjectD.getString("Cla_Class"));
                            teacherReportModel.setCla_Section(jsonObjectD.getString("Cla_Section"));
                        }

                        teacherReportModel.setStd_Id(jsonObject.getString("Std_Id"));
                        teacherReportModel.setStd_Name(jsonObject.getString("Std_Name"));
                        teacherReportModel.setStd_Gr_No(jsonObject.getString("Std_Gr_No"));
                        teacherReportModel.setStd_Image(jsonObject.getString("Std_Image"));

                        dataReport.add(teacherReportModel);
                    }

                    teacherReportAdapter = new TeacherReportAdapter(getActivity(), dataReport);
                    rv_teacher_report.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
                    rv_teacher_report.setAdapter(teacherReportAdapter);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

        }
    }
}
