package com.example.shine_rahul.gemstarkidz;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.daasuu.bl.BubbleLayout;
import com.daasuu.bl.BubblePopupHelper;
import com.example.shine_rahul.gemstarkidz.Fragment.AttendanceFragment;
import com.example.shine_rahul.gemstarkidz.Fragment.ParentChatFragment;
import com.example.shine_rahul.gemstarkidz.Fragment.EventFragment;
import com.example.shine_rahul.gemstarkidz.Fragment.HomeworkDetailFragment;
import com.example.shine_rahul.gemstarkidz.Fragment.PaymentFragment;
import com.example.shine_rahul.gemstarkidz.Fragment.ProfileParentFragment;
import com.example.shine_rahul.gemstarkidz.Fragment.ProgressReportFragment;
import com.example.shine_rahul.gemstarkidz.Fragment.SocialFragment;
import com.example.shine_rahul.gemstarkidz.Fragment.SummaryFragment;
import com.example.shine_rahul.gemstarkidz.Model.MenuModel;
import com.example.shine_rahul.gemstarkidz.Service.Services;
import com.example.shine_rahul.gemstarkidz.Session.Session;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ParentActivity extends Activity
        implements NavigationView.OnNavigationItemSelectedListener {

    ImageView img_menu;
    RecyclerView rv_drawer_menu_list;
    MenuAdapter menuAdapter;
    ArrayList<MenuModel> data;


//     R.drawable.homework, R.drawable.report, R.drawable.map, R.drawable.logout};

    String[] name = {"Profile", "Social", "Chat", "Event", "Attendance", "Homework", "Tracker", "Progress Report", "Payment", "Logout"};
    int[] image = {R.drawable.profile, R.drawable.social, R.drawable.chatmenu, R.drawable.event, R.drawable.attendance, R.drawable.homework, R.drawable.map, R.drawable.progress, R.drawable.payment, R.drawable.logout};
    ImageView img_drawer_close;
    ImageView img_search;
    ImageView img_close;
    TextView txt_menu_title;
    Session session;
    public static TextView txt_drawer_user_name;
    MenuModel menuModel;
    android.app.FragmentManager fragmentManager;
    boolean isOpen = true;
    EditText edt_search;
    String user_phone, username, user_id, user_email, user_type, user_image = "", user_reg_type, flag = "";
    CircleImageView img_drawer_user;
    String image_path = "", responseimage, uimage = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        fragmentManager = getFragmentManager();
        session = Session.getSession(ParentActivity.this);
        img_search = (ImageView) findViewById(R.id.img_search);
        img_close = (ImageView) findViewById(R.id.img_close);
        txt_menu_title = (TextView) findViewById(R.id.txt_menu_title);
        edt_search = (EditText) findViewById(R.id.edt_search);
        menuModel = new MenuModel();
        // new AsyncTaskRunnername().execute();
        //  new AsyncTaskRunnerimage().execute();

        SharedPreferences splogin = getApplicationContext().getSharedPreferences("login", 0);
        username = splogin.getString("username", "");
        user_type = splogin.getString("type", "");
        user_phone = splogin.getString("user_phone", "");
        user_id = splogin.getString("user_id", "");
        user_email = splogin.getString("user_email", "");
        user_image = splogin.getString("user_image", "");
        user_reg_type = splogin.getString("user_reg_type", "");
        flag = splogin.getString("flag", "");

        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt_search.setVisibility(View.GONE);
                img_close.setVisibility(View.GONE);
                txt_menu_title.setVisibility(View.VISIBLE);
                img_search.setVisibility(View.VISIBLE);
            }
        });


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        img_drawer_close = (ImageView) findViewById(R.id.img_drawer_close);
        //setSupportActionBar(toolbar);


        img_menu = (ImageView) findViewById(R.id.img_menu);
        rv_drawer_menu_list = (RecyclerView) findViewById(R.id.rv_drawer_menu_list);


        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        drawer.closeDrawer(GravityCompat.START);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        txt_drawer_user_name = (TextView) navigationView.findViewById(R.id.txt_drawer_user_name);
        img_drawer_user = (CircleImageView) navigationView.findViewById(R.id.img_drawer_user);
        //txt_drawer_user_name.setText(session.getUSER_EMAIL());
        txt_drawer_user_name.setText(username);

        if (user_reg_type.equals("0")) {
            if (user_image.equals("null")) {

            } else {
                Picasso.with(getApplicationContext())
                        .load(Services.IMAGES_PATH + "profile/" + user_image)
                        .placeholder(R.drawable.logo)
                        .into(img_drawer_user);
            }

        } else if (user_reg_type.equals("1")) {
            if (user_image.equals("null")) {

            } else {
                if (flag.equals("")) {
                    Picasso.with(getApplicationContext())
                            .load("https://graph.facebook.com/" + user_image + "/picture?type=large")
                            .placeholder(R.drawable.logo)
                            .into(img_drawer_user);
                } else if (flag.equals("3")) {
                    Picasso.with(getApplicationContext())
                            .load(Services.IMAGES_PATH + "profile/" + user_image)
                            .placeholder(R.drawable.logo)
                            .into(img_drawer_user);
                }

            }

        } else if (user_reg_type.equals("2")) {
            if (user_image.equals("null")) {

            } else {
                if (flag.equals("")) {
                    Picasso.with(getApplicationContext())
                            .load(user_image)
                            .placeholder(R.drawable.logo)
                            .into(img_drawer_user);
                } else if (flag.equals("3")) {
                    Picasso.with(getApplicationContext())
                            .load(Services.IMAGES_PATH + "profile/" + user_image)
                            .placeholder(R.drawable.logo)
                            .into(img_drawer_user);
                }

            }

        }


        img_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                boolean mSlideState = false;
                if (drawer.isDrawerOpen(Gravity.START)) {
                    drawer.closeDrawer(Gravity.START);
                } else {
                    drawer.openDrawer(Gravity.START);
                }
            }
        });

        data = new ArrayList<>();
        for (int i = 0; i < image.length; i++) {
            menuModel = new MenuModel();
            menuModel.setName(name[i]);
            menuModel.setImage(image[i]);
            data.add(menuModel);
        }

        menuAdapter = new MenuAdapter(ParentActivity.this, data);
        menuAdapter.notifyDataSetChanged();
        rv_drawer_menu_list.setLayoutManager(new LinearLayoutManager(ParentActivity.this, LinearLayout.VERTICAL, false));
        rv_drawer_menu_list.setAdapter(menuAdapter);

        Fragment someFragment = new ProfileParentFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.content, someFragment); // give your fragment container id in first parameter
        transaction.addToBackStack(null);  // if written, this transaction will be added to backstack
        transaction.commit();
        drawer.closeDrawer(GravityCompat.START);
        txt_menu_title.setText("Profile");
        img_search.setVisibility(View.INVISIBLE);
        Typeface style = Typeface.createFromAsset(getApplicationContext().getAssets(), "Raleway-SemiBold.ttf");
        txt_menu_title.setTypeface(style);
        img_drawer_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.closeDrawers();
            }
        });

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
            return;
        } else if (!isOpen) {
            // super.onBackPressed();
            Fragment someFragment = new SocialFragment();
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.replace(R.id.content, someFragment); // give your fragment container id in first parameter
            transaction.addToBackStack(null);  // if written, this transaction will be added to backstack
            transaction.commit();
            drawer.closeDrawer(GravityCompat.START);
            txt_menu_title.setText("Social ");
            img_search.setVisibility(View.VISIBLE);
            Typeface style = Typeface.createFromAsset(getApplicationContext().getAssets(), "Raleway-SemiBold.ttf");
            txt_menu_title.setTypeface(style);
            isOpen = true;
        } else if (isOpen) {
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        String name = menuModel.getName();
        if (item.equals(menuModel.getName().equals("Social"))) {
            Fragment someFragment = new SocialFragment();
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.replace(R.id.content, someFragment); // give your fragment container id in first parameter
            transaction.addToBackStack(null);  // if written, this transaction will be added to backstack
            transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            transaction.commit();
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);
            img_search.setVisibility(View.VISIBLE);
            txt_menu_title.setText(name);
            Toast.makeText(getApplicationContext(), "Hello", Toast.LENGTH_LONG).show();
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.ViewHolder> {
        private Context context;
        private ArrayList<MenuModel> data;
        private Fragment fragment = null;


        public MenuAdapter(Context context, ArrayList<MenuModel> data) {
            this.context = context;
            this.data = data;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.menu_list, parent, false);

            return new ViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {

            final MenuModel menuModel = data.get(position);
            holder.txt_menu.setText("" + menuModel.getName());
            Glide.with(context).load(menuModel.getImage()).placeholder(R.mipmap.ic_launcher).error(R.mipmap.ic_launcher).into(holder.img_menu);
            holder.view.setVisibility(View.GONE);
            holder.img_popup.setImageResource(R.drawable.popup);

            holder.img_popup.setOnClickListener(new View.OnClickListener() {
                @SuppressLint("ResourceType")
                @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                @Override
                public void onClick(View v) {

                    BubbleLayout bubbleLayout = (BubbleLayout) LayoutInflater.from(ParentActivity.this).inflate(R.layout.bubble_design, null);
                    final PopupWindow popupWindow = BubblePopupHelper.create(ParentActivity.this, bubbleLayout);
                    popupWindow.showAsDropDown(v);
                    final TextView txt_attandance = (TextView) bubbleLayout.findViewById(R.id.txt_attandance);
                    final TextView txt_summary = (TextView) bubbleLayout.findViewById(R.id.txt_summary);
                    txt_attandance.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            txt_attandance.setTextColor(getResources().getColor(R.color.colorTheme));
                            txt_summary.setTextColor(getResources().getColor(R.color.colorText));


                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    Fragment someFragment1 = new AttendanceFragment();
                                    FragmentTransaction transaction1 = getFragmentManager().beginTransaction();
                                    transaction1.replace(R.id.content, someFragment1); // give your fragment container id in first parameter
                                    transaction1.addToBackStack(null);
                                    transaction1.commit();
                                    DrawerLayout drawer1 = (DrawerLayout) findViewById(R.id.drawer_layout);
                                    drawer1.closeDrawer(GravityCompat.START);
                                    txt_menu_title.setText("Attendance Report");
                                    img_search.setVisibility(View.INVISIBLE);
                                    popupWindow.dismiss();
                                }
                            }, 200);
                        }
                    });
                    txt_summary.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            txt_summary.setTextColor(getResources().getColor(R.color.colorTheme));
                            txt_attandance.setTextColor(getResources().getColor(R.color.colorText));
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    Fragment someFragment1 = new SummaryFragment();
                                    FragmentTransaction transaction1 = getFragmentManager().beginTransaction();
                                    transaction1.replace(R.id.content, someFragment1); // give your fragment container id in first parameter
                                    transaction1.addToBackStack(null);  // if written, this transaction will be added to backstack
                                    transaction1.commit();
                                    DrawerLayout drawer1 = (DrawerLayout) findViewById(R.id.drawer_layout);
                                    drawer1.closeDrawer(GravityCompat.START);
                                    txt_menu_title.setText("Summary");
                                    img_search.setVisibility(View.INVISIBLE);
                                    popupWindow.dismiss();
                                }
                            }, 200);

                        }
                    });
                }
            });
            if (position == 4) {
                holder.img_popup.setVisibility(View.GONE);
            }


            holder.txt_menu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    isOpen = false;
                    String name = menuModel.getName();
                    if (name.equals("Profile")) {
                        Fragment someFragment = new ProfileParentFragment();
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.replace(R.id.content, someFragment);
                        transaction.addToBackStack(null);
                        transaction.commit();
                        img_search.setVisibility(View.INVISIBLE);
                        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                        drawer.closeDrawer(GravityCompat.START);
                        txt_menu_title.setText(name);
                    } else if (name.equals("Social")) {
                        Fragment someFragment = new SocialFragment();
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.replace(R.id.content, someFragment); // give your fragment container id in first parameter
                        transaction.addToBackStack(null);  // if written, this transaction will be added to backstack
                        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                        transaction.commit();
                        img_search.setVisibility(View.VISIBLE);
                        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                        drawer.closeDrawer(GravityCompat.START);
                        txt_menu_title.setText(name);
                    } else if (name.equals("Chat")) {
                        Fragment someFragment = new ParentChatFragment();
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.replace(R.id.content, someFragment); // give your fragment container id in first parameter
                        transaction.addToBackStack(null);  // if written, this transaction will be added to backstack
                        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                        transaction.commit();
                        img_search.setVisibility(View.VISIBLE);
                        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                        drawer.closeDrawer(GravityCompat.START);
                        txt_menu_title.setText(name);
                    } else if (name.equals("Event")) {
                        Fragment someFragment = new EventFragment();
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.replace(R.id.content, someFragment); // give your fragment container id in first parameter
                        transaction.addToBackStack(null);  // if written, this transaction will be added to backstack
                        transaction.commit();
                        img_search.setVisibility(View.VISIBLE);
                        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                        drawer.closeDrawer(GravityCompat.START);
                        txt_menu_title.setText(name);
                    } else if (name.equals("Attendance")) {
                        Fragment someFragment = new SummaryFragment();
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.replace(R.id.content, someFragment); // give your fragment container id in first parameter
                        transaction.addToBackStack(null);  // if written, this transaction will be added to backstack
                        transaction.commit();
                        img_search.setVisibility(View.VISIBLE);
                        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                        drawer.closeDrawer(GravityCompat.START);
                        txt_menu_title.setText(name);
                    } else if (name.equals("Homework")) {
                        Fragment someFragment = new HomeworkDetailFragment();
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.replace(R.id.content, someFragment); // give your fragment container id in first parameter
                        transaction.addToBackStack(null);  // if written, this transaction will be added to backstack
                        transaction.commit();
                        img_search.setVisibility(View.VISIBLE);
                        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                        drawer.closeDrawer(GravityCompat.START);
                        txt_menu_title.setText(name);
                    } else if (name.equals("Tracker")) {
                        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                        drawer.closeDrawer(GravityCompat.START);
                        Intent intent = new Intent(ParentActivity.this, MapsActivity.class);
                        startActivity(intent);

                    } else if (name.equals("Progress Report")) {
                        Fragment someFragment = new ProgressReportFragment();
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.replace(R.id.content, someFragment); // give your fragment container id in first parameter
                        transaction.addToBackStack(null);  // if written, this transaction will be added to backstack
                        transaction.commit();
                        img_search.setVisibility(View.INVISIBLE);
                        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                        drawer.closeDrawer(GravityCompat.START);
                        txt_menu_title.setText(name);
                    } else if (name.equals("Payment")) {
                        Fragment someFragment = new PaymentFragment();
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.replace(R.id.content, someFragment); // give your fragment container id in first parameter
                        transaction.addToBackStack(null);  // if written, this transaction will be added to backstack
                        transaction.commit();
                        img_search.setVisibility(View.INVISIBLE);
                        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                        drawer.closeDrawer(GravityCompat.START);
                        txt_menu_title.setText(name);
                    } else if (name.equals("Logout")) {

                        SharedPreferences spuser = ParentActivity.this.getSharedPreferences("login", 0);
                        SharedPreferences.Editor Eduser = spuser.edit();
                        Eduser.putString("username", null);
                        Eduser.putString("type", null);
                        Eduser.putString("password", null);
                        Eduser.putString("user_phone", null);
                        Eduser.putString("user_id", null);
                        Eduser.putString("user_email", null);
                        Eduser.putString("user_image", null);
                        Eduser.putString("user_class", null);
                        Eduser.putString("user_section", null);
                        Eduser.putString("user_reg_type", null);
                        Eduser.putString("flag", null);
                        Eduser.apply();

                        Intent intent = new Intent(ParentActivity.this, WelcomeActivity.class);
                        startActivity(intent);
                        finish();
                    }

                }
            });

        }


        @Override
        public int getItemCount() {
            return data.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            ImageView img_menu;
            final ImageView img_popup;
            TextView txt_menu;
            View view;

            public ViewHolder(View itemView) {
                super(itemView);
                img_menu = (ImageView) itemView.findViewById(R.id.img_menu);
                img_popup = (ImageView) itemView.findViewById(R.id.img_popup);
                txt_menu = (TextView) itemView.findViewById(R.id.txt_menu);
                view = (View) itemView.findViewById(R.id.view);
            }
        }

    }


    public String getUser_id() {
        return user_id;
    }

    public String getUsername() {
        return username;
    }

    public String getUser_phone() {
        return String.valueOf(user_phone);
    }

    public String getUser_email() {
        return user_email;
    }

    public String getUser_type() {
        return user_type;
    }

    public String getUser_image() {
        return user_image;
    }


    private class AsyncTaskRunnername extends AsyncTask<String, String, String> {

        private ProgressDialog Dialog = new ProgressDialog(ParentActivity.this);

        @Override
        protected String doInBackground(String... params) {

            MultipartBody.Builder builder = new MultipartBody.Builder();

            builder.setType(MultipartBody.FORM);
            //  builder.addFormDataPart("name", username);
            // builder.addFormDataPart("user_id", user_id);

//            RequestBody requestBody = builder.build();
            try {

                OkHttpClient client = new OkHttpClient.Builder().build();
                Request request = new Request.Builder()
                        .url(Services.UPDATE_PROFILE)
                        //       .post(requestBody)
                        .build();
                Response resp = client.newCall(request).execute();
                responseimage = resp.body().string();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            Dialog.dismiss();
            Deliverydataname(responseimage);
            super.onPostExecute(s);

        }

        @Override
        protected void onPreExecute() {
            Dialog.show();
            Dialog.setMessage("Please Wait....");
            Dialog.setCancelable(false);
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
        }

    }

    private void Deliverydataname(String response) {

        if (!response.equals("")) {
            try {

                JSONObject jsobjectcategory = new JSONObject(response);

                String message = jsobjectcategory.getString("message");

                String error = jsobjectcategory.getString("error");

                if (error.equals("200")) {
                    String profile = jsobjectcategory.getString("data");
                    JSONObject jsonObject = new JSONObject(profile);
                    String name = jsonObject.getString("Use_Name");

                    username = name;
                    txt_drawer_user_name.setText(username);

                    SharedPreferences spuser = ParentActivity.this.getSharedPreferences("login", 0);
                    SharedPreferences.Editor Eduser = spuser.edit();
                    Eduser.putString("username", username);
                    Eduser.apply();


                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private class AsyncTaskRunnerimage extends AsyncTask<String, String, String> {

        private ProgressDialog Dialog = new ProgressDialog(ParentActivity.this);

        @Override
        protected String doInBackground(String... params) {

            MultipartBody.Builder builder = new MultipartBody.Builder();

            builder.setType(MultipartBody.FORM);
            final MediaType MEDIA_TYPE = MediaType.parse("image/jpg");
            builder.addFormDataPart("image", image_path, RequestBody.create(MEDIA_TYPE, new File(image_path)));
            builder.addFormDataPart("user_id", user_id);

            RequestBody requestBody = builder.build();

            try {

                OkHttpClient client = new OkHttpClient.Builder().build();
                Request request = new Request.Builder()
                        .url(Services.UPDATE_PROFILE)
                        .post(requestBody)
                        .build();
                Response resp = client.newCall(request).execute();
                responseimage = resp.body().string();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            Dialog.dismiss();
            Deliverydataimage(responseimage);
            super.onPostExecute(s);

        }

        @Override
        protected void onPreExecute() {
            Dialog.show();
            Dialog.setMessage("Please Wait....");
            Dialog.setCancelable(false);
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
        }

    }

    private void Deliverydataimage(String response) {

        if (!response.equals("")) {
            try {

                JSONObject jsobjectcategory = new JSONObject(response);

                String message = jsobjectcategory.getString("message");

                String error = jsobjectcategory.getString("error");

                if (error.equals("200")) {
                    String profile = jsobjectcategory.getString("data");
                    JSONObject jsonObject = new JSONObject(profile);
                    String image = jsonObject.getString("Use_Image");

                    user_image = image;
                    Picasso.with(ParentActivity.this)
                            .load("http://192.168.1.11/gemstarkidz/public/images/profile/" + user_image)
                            .placeholder(R.drawable.logo)
                            .into(img_drawer_user);


                    SharedPreferences spuser = ParentActivity.this.getSharedPreferences("login", 0);
                    SharedPreferences.Editor Eduser = spuser.edit();
                    Eduser.putString("user_image", user_image);
                    Eduser.apply();

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


}