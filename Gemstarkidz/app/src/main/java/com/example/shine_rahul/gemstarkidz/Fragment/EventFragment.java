package com.example.shine_rahul.gemstarkidz.Fragment;


import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shine_rahul.gemstarkidz.Adapter.EventAdapter;
import com.example.shine_rahul.gemstarkidz.Helper.RecyclerItemClickListener;
import com.example.shine_rahul.gemstarkidz.Model.EventModel;
import com.example.shine_rahul.gemstarkidz.R;
import com.example.shine_rahul.gemstarkidz.Service.Services;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class EventFragment extends android.app.Fragment {
    TextView txt_event_social;
    TextView txt_event_chat;
    TextView txt_event_event;
    TextView txt_event_attendance;
    CalendarView event_calender;
    RecyclerView rv_event_list;
    ArrayList<EventModel> data = new ArrayList<>();
    EventModel eventModel;
    EventAdapter eventAdapter;
    ImageView calender_visible_parent;
    String Eve_Name, Eve_Date, Eve_Accept, Eve_Image, msg;
    JSONObject categories;
    String Eve_Id, Eve_Use_Id, Eve_user_Name, Eve_Start_Date, Eve_End_Date, Eve_Description, Eve_user_Image, Eve_user_Accept, accept_event, accept_event_id;
    SwipeRefreshLayout swiperefresh;
    String response_message;

    //search
    TextView txt_edit_toolbar;
    ImageView img_search, img_close;
    EditText edt_search;
    TextView txt_menu_title;

    public EventFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_event, container, false);

        txt_edit_toolbar = (TextView) getActivity().findViewById(R.id.txt_edit_toolbar);
        img_search = (ImageView) getActivity().findViewById(R.id.img_search);
        img_close = (ImageView) getActivity().findViewById(R.id.img_close);
        edt_search = (EditText) getActivity().findViewById(R.id.edt_search);
        txt_menu_title = (TextView) getActivity().findViewById(R.id.txt_menu_title);

        swiperefresh = (SwipeRefreshLayout) view.findViewById(R.id.swiperefresh);
        txt_event_social = (TextView) view.findViewById(R.id.txt_event_social);
        txt_event_chat = (TextView) view.findViewById(R.id.txt_event_chat);
        txt_event_event = (TextView) view.findViewById(R.id.txt_event_event);
        txt_event_attendance = (TextView) view.findViewById(R.id.txt_event_attendance);
        event_calender = (CalendarView) view.findViewById(R.id.event_calender);
        rv_event_list = (RecyclerView) view.findViewById(R.id.rv_event_list);
        calender_visible_parent = (ImageView) view.findViewById(R.id.calender_visible_parent);
        event_calender.setVisibility(View.GONE);

        txt_menu_title.setVisibility(View.VISIBLE);
        txt_menu_title.setText("Event");
        txt_edit_toolbar.setVisibility(View.GONE);
        img_search.setVisibility(View.VISIBLE);
        edt_search.setVisibility(View.GONE);
        img_close.setVisibility(View.GONE);
        new GetDetails().execute();


        txt_event_event.setTextColor(getResources().getColor(R.color.colorTheme));
        event_calender.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                //Toast.makeText(getActivity(), "" + dayOfMonth, LENGTH_LONG).show();
            }
        });

        Typeface style = Typeface.createFromAsset(getActivity().getAssets(), "Raleway-Bold.ttf");
        txt_event_social.setTypeface(style);
        txt_event_chat.setTypeface(style);
        txt_event_event.setTypeface(style);
        txt_event_attendance.setTypeface(style);

        SharedPreferences accept1 = getActivity().getSharedPreferences("accept", 0);
        accept_event = accept1.getString("accept_event", "");


        calender_visible_parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (event_calender.getVisibility() == View.VISIBLE) {
                    event_calender.setVisibility(View.GONE);
                } else {
                    event_calender.setVisibility(View.VISIBLE);
                }
            }
        });

        rv_event_list.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        try {

                            EventDetailFragment eventDetailFragment = new EventDetailFragment();
                            Bundle bundle = new Bundle();
                            bundle.putString("Eve_Id", data.get(position).getId());
                            bundle.putString("NAME", data.get(position).getName());
                            bundle.putString("PEOPLE", data.get(position).getPeople());
                            bundle.putString("DATE", data.get(position).getDate());
                            bundle.putString("Description", data.get(position).getDescription());
                            bundle.putString("IMAGE", String.valueOf(data.get(position).getImage()));
                            bundle.putString("accept_event", accept_event);
                            eventDetailFragment.setArguments(bundle);
                            getFragmentManager()
                                    .beginTransaction()
                                    .replace(R.id.content, eventDetailFragment)
                                    .commit();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                })
        );

        txt_event_social.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt_event_event.setTextColor(getResources().getColor(R.color.colorText));
                txt_event_social.setTextColor(getResources().getColor(R.color.colorTheme));
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        android.app.Fragment fragment = new SocialFragment();
                        FragmentManager fragmentManager = getActivity().getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.content, fragment);
                        fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commit();
                        img_search.setVisibility(View.VISIBLE);
                        txt_menu_title.setText("Social");

                    }
                }, 100);

            }
        });
        txt_event_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt_event_event.setTextColor(getResources().getColor(R.color.colorText));
                txt_event_chat.setTextColor(getResources().getColor(R.color.colorTheme));
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        android.app.Fragment fragment = new ParentChatFragment();
                        FragmentManager fragmentManager = getActivity().getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.content, fragment);
                        fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commit();
                        img_search.setVisibility(View.VISIBLE);
                        txt_menu_title.setText("Chat");

                    }
                }, 100);

            }
        });
        txt_event_attendance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt_event_event.setTextColor(getResources().getColor(R.color.colorText));
                txt_event_attendance.setTextColor(getResources().getColor(R.color.colorTheme));
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        android.app.Fragment fragment = new AttendanceFragment();
                        FragmentManager fragmentManager = getActivity().getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.content, fragment);
                        fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commit();
                        img_search.setVisibility(View.INVISIBLE);
                        txt_menu_title.setText("Attendance Report");

                    }
                }, 100);

            }
        });

        event_calender.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {


                String date = year + "-" + ((month + 1) > 9 ? (month + 1) : "0" + (month + 1)) + "-" +
                        (dayOfMonth > 9 ? dayOfMonth : "0" + dayOfMonth);

                eventAdapter.filter1(date);
                event_calender.setVisibility(View.GONE);
            }
        });


        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                Log.i("HELLO", "keyCode: " + keyCode);
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                    android.app.Fragment someFragment = new SocialFragment();
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.content, someFragment); // give your fragment container id in first parameter
                    transaction.addToBackStack(null);  // if written, this transaction will be added to backstack
                    transaction.commit();
                    txt_menu_title.setText("Social ");
                    Typeface style = Typeface.createFromAsset(getActivity().getAssets(), "Raleway-SemiBold.ttf");
                    txt_menu_title.setTypeface(style);
                    return true;
                }
                return false;
            }

        });


        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                data.clear();
                swiperefresh.setRefreshing(true);
                new GetDetails().execute();
            }
        });
        img_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    img_search.setVisibility(View.GONE);
                    txt_menu_title.setVisibility(View.GONE);
                    edt_search.setVisibility(View.VISIBLE);
                    img_close.setVisibility(View.VISIBLE);
                    edt_search.setText("");
                    edt_search.requestFocus();
                    edt_search.setFocusable(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                img_close.setVisibility(View.GONE);
                edt_search.setVisibility(View.GONE);
                img_search.setVisibility(View.VISIBLE);
                txt_menu_title.setVisibility(View.VISIBLE);
                txt_menu_title.setText("Event");
                new GetDetails().execute();
                getActivity().overridePendingTransition(0, 0);

            }
        });

        edt_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                eventAdapter.filter(edt_search.getText().toString());


            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        return view;
    }


    public class GetDetails extends AsyncTask<String, Process, String> {

        String line, response;
        private ProgressDialog Dialog = new ProgressDialog(getActivity());

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Dialog.show();
            Dialog.setMessage("Please Wait....");
            Dialog.setCancelable(false);
        }

        @Override
        protected String doInBackground(String... params) {

            String url = Services.EVENT_LIST;

            try {
                URL url1 = new URL(url);
                URLConnection urlc = url1.openConnection();
                BufferedReader bfr = new BufferedReader(new InputStreamReader(urlc.getInputStream()));

                while ((line = bfr.readLine()) != null) {
                    Log.e("product list data", "in while postexecute" + line);
                    categories = new JSONObject(line);
                    msg = categories.getString("error");
                    response_message = categories.getString("message");

                    if (msg.equals("200")) {
                        JSONArray jsonArray = categories.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            eventModel = new EventModel();
                            Eve_Id = jsonObject.getString("Eve_Id");
                            Eve_Use_Id = jsonObject.getString("Eve_Use_Id");
                            Eve_user_Name = jsonObject.getString("Eve_Name");
                            Eve_Start_Date = jsonObject.getString("Eve_Date");
                            Eve_End_Date = jsonObject.getString("Eve_End_Date");
                            Eve_Description = jsonObject.getString("Eve_Description");
                            Eve_user_Image = jsonObject.getString("Eve_Image");
                            Eve_user_Accept = jsonObject.getString("Eve_Count");


                            if (Eve_user_Accept.equals("null")) {
                                eventModel.setPeople("0");
                            } else {
                                eventModel.setPeople(Eve_user_Accept);
                            }

                            eventModel.setId(Eve_Id);
                            eventModel.setName(Eve_user_Name);
                            eventModel.setDate(Eve_Start_Date);
                            eventModel.setImage(Eve_user_Image);
                            eventModel.setDescription(Eve_Description);
                            data.add(eventModel);
                        }

                    }

                    response = bfr.toString();
                    Log.e("response", response);
                }

            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            Dialog.dismiss();

            if (msg != null) {

                if (msg.equals("200")) {

                    rv_event_list.setVisibility(View.VISIBLE);
                    eventAdapter = new EventAdapter(getActivity(), data);
                    rv_event_list.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayout.VERTICAL, false));
                    rv_event_list.setAdapter(eventAdapter);


                } else {
                    Toast.makeText(getActivity(), response_message, Toast.LENGTH_LONG).show();
                }
                swiperefresh.setRefreshing(false);
            }
        }
    }


}
