package com.example.shine_rahul.gemstarkidz.Adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.shine_rahul.gemstarkidz.Model.SocialSingleModel;
import com.example.shine_rahul.gemstarkidz.Model.Teacher_Social_Single_Model;
import com.example.shine_rahul.gemstarkidz.R;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class Teacher_Social_Single_Adapter extends RecyclerView.Adapter<Teacher_Social_Single_Adapter.ViewHolder> {
    private Context context;
    private ArrayList<Teacher_Social_Single_Model> data;

    public Teacher_Social_Single_Adapter(Context context, ArrayList<Teacher_Social_Single_Model> data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.teacher_social_single, null);
        return new Teacher_Social_Single_Adapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Teacher_Social_Single_Model teacherSocialSingleModel = data.get(position);

        holder.txt_teacher_social_single_name.setText(teacherSocialSingleModel.getName());
        holder.txt_teacher_social_single_time.setText(teacherSocialSingleModel.getTime());
        holder.txt_teacher_social_single_description.setText(teacherSocialSingleModel.getDescription());
        holder.txt_teacher_social_single_url.setText(teacherSocialSingleModel.getUrl());
        Glide.with(context).load(teacherSocialSingleModel.getImage()).error(R.drawable.logo).into(holder.img_teacher_social_single);

        Typeface style1 = Typeface.createFromAsset(context.getAssets(), "Raleway-SemiBold.ttf");
        Typeface style2 = Typeface.createFromAsset(context.getAssets(), "Raleway-Regular.ttf");
        holder.txt_teacher_social_single_name.setTypeface(style1);
        holder.txt_teacher_social_single_description.setTypeface(style2);
        holder.txt_teacher_social_single_url.setTypeface(style2);

        holder.txt_teacher_social_single_replay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater layoutInflater = LayoutInflater.from(context);
                View view1 = layoutInflater.inflate(R.layout.replay_design, null);
                final AlertDialog.Builder alertdialog = new AlertDialog.Builder(context);
                alertdialog.setView(view1);
                final AlertDialog dialog = alertdialog.create();
                dialog.setCanceledOnTouchOutside(false);
                ImageView btn_Cancel = (ImageView) view1.findViewById(R.id.img_reply);
                final EditText edt_save_detail = (EditText) view1.findViewById(R.id.edt_reply);
                dialog.setCanceledOnTouchOutside(true);
                dialog.show();
            }
        });


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView img_teacher_social_single;
        TextView txt_teacher_social_single_name;
        TextView txt_teacher_social_single_time;
        TextView txt_teacher_social_single_description;
        TextView txt_teacher_social_single_url;
        TextView txt_teacher_social_single_replay;

        public ViewHolder(View itemView) {
            super(itemView);
            img_teacher_social_single = (CircleImageView) itemView.findViewById(R.id.img_teacher_social_single);
            txt_teacher_social_single_name = (TextView) itemView.findViewById(R.id.txt_teacher_social_single_name);
            txt_teacher_social_single_time = (TextView) itemView.findViewById(R.id.txt_teacher_social_single_time);
            txt_teacher_social_single_description = (TextView) itemView.findViewById(R.id.txt_teacher_social_single_description);
            txt_teacher_social_single_url = (TextView) itemView.findViewById(R.id.txt_teacher_social_single_url);
            txt_teacher_social_single_replay = (TextView) itemView.findViewById(R.id.txt_teacher_social_single_replay);
        }
    }
}
