package com.example.shine_rahul.gemstarkidz.Fragment;


import android.app.FragmentTransaction;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.shine_rahul.gemstarkidz.Adapter.MonthAdapter;
import com.example.shine_rahul.gemstarkidz.Adapter.PaymentCardTypeAdapter;
import com.example.shine_rahul.gemstarkidz.Adapter.PaymentMonthAdapter;
import com.example.shine_rahul.gemstarkidz.Model.MonthModel;
import com.example.shine_rahul.gemstarkidz.Model.PaymentModel;
import com.example.shine_rahul.gemstarkidz.R;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class SummaryFragment extends android.app.Fragment {
    CircleImageView img_summary_student_profile;
    TextView txt_summary_name_title;
    TextView txt_summary_name;
    TextView txt_summary_class_title;
    TextView txt_summary_class;
    TextView txt_summary_grno;
    Spinner sp_summary_month;
    TextView txt_summary_present_title;
    TextView txt_summary_present;
    TextView txt_summary_absent_title;
    TextView txt_summary_absent;
    TextView txt_summary_report_title;
    TextView txt_summary_report;
    String[] Payment_Expire_Month = {"Select Month", "January", "February", "March", "April", "May", "June", "July", "Auguest", "September", "October", "November", "December"};
    ArrayList<MonthModel> data;
    MonthAdapter monthAdapter;
    TextView txt_menu_title;
    TextView txt_edit_toolbar;
    ImageView img_search;


    public SummaryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_summary, container, false);
        txt_menu_title = (TextView) getActivity().findViewById(R.id.txt_menu_title);
        img_summary_student_profile = (CircleImageView) view.findViewById(R.id.img_summary_student_profile);
        sp_summary_month = (Spinner) view.findViewById(R.id.sp_summary_month);
        txt_summary_name_title = (TextView) view.findViewById(R.id.txt_summary_name_title);
        txt_summary_name = (TextView) view.findViewById(R.id.txt_summary_name);
        txt_summary_class_title = (TextView) view.findViewById(R.id.txt_summary_class_title);
        txt_summary_class = (TextView) view.findViewById(R.id.txt_summary_class);
        txt_summary_grno = (TextView) view.findViewById(R.id.txt_summary_grno);
        txt_summary_present_title = (TextView) view.findViewById(R.id.txt_summary_present_title);
        txt_summary_present = (TextView) view.findViewById(R.id.txt_summary_present);
        txt_summary_absent_title = (TextView) view.findViewById(R.id.txt_summary_absent_title);
        txt_summary_absent = (TextView) view.findViewById(R.id.txt_summary_absent);
        txt_summary_report_title = (TextView) view.findViewById(R.id.txt_summary_report_title);
        txt_summary_report = (TextView) view.findViewById(R.id.txt_summary_report);
        txt_edit_toolbar = (TextView) getActivity().findViewById(R.id.txt_edit_toolbar);
        img_search = (ImageView) getActivity().findViewById(R.id.img_search);
        txt_edit_toolbar.setVisibility(View.GONE);
        img_search.setVisibility(View.GONE);

        Typeface font1 = Typeface.createFromAsset(getActivity().getAssets(), "Raleway-SemiBold.ttf");
        Typeface font2 = Typeface.createFromAsset(getActivity().getAssets(), "Raleway-Regular.ttf");
        txt_summary_name.setTypeface(font2);
        txt_summary_present_title.setTypeface(font2);
        txt_summary_absent_title.setTypeface(font2);
        txt_summary_report_title.setTypeface(font1);
        txt_summary_report.setTypeface(font2);


        data = new ArrayList<>();
        for (int i = 0; i < Payment_Expire_Month.length; i++) {
            MonthModel monthModel = new MonthModel();
            monthModel.setMonth(Payment_Expire_Month[i]);
            data.add(monthModel);
        }
        monthAdapter = new MonthAdapter(getActivity(), data);
        monthAdapter.notifyDataSetChanged();
        sp_summary_month.setAdapter(monthAdapter);


        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                Log.i("HELLO", "keyCode: " + keyCode);
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
//                    Log.i("HELLO", "onKey Back listener is working!!!");
//                    getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    android.app.Fragment someFragment = new SocialFragment();
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.content, someFragment); // give your fragment container id in first parameter
                    transaction.addToBackStack(null);  // if written, this transaction will be added to backstack
                    transaction.commit();
                    txt_menu_title.setText("Social ");
                    Typeface style = Typeface.createFromAsset(getActivity().getAssets(), "Raleway-SemiBold.ttf");
                    txt_menu_title.setTypeface(style);
                    return true;
                }
                return false;
            }

        });
        return view;
    }

}
