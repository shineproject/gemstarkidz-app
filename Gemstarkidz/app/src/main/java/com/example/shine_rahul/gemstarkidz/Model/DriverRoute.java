package com.example.shine_rahul.gemstarkidz.Model;

public class DriverRoute {

    String latFrom;
    String lngFrom;
    String latTo;
    String lngTo;

    public String getLatFrom() {
        return latFrom;
    }

    public void setLatFrom(String latFrom) {
        this.latFrom = latFrom;
    }

    public String getLngFrom() {
        return lngFrom;
    }

    public void setLngFrom(String lngFrom) {
        this.lngFrom = lngFrom;
    }

    public String getLatTo() {
        return latTo;
    }

    public void setLatTo(String latTo) {
        this.latTo = latTo;
    }

    public String getLngTo() {
        return lngTo;
    }

    public void setLngTo(String lngTo) {
        this.lngTo = lngTo;
    }
}
