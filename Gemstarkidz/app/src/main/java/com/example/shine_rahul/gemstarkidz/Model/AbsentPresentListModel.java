package com.example.shine_rahul.gemstarkidz.Model;

public class AbsentPresentListModel {
    String student_id;
    String student_class_id;
    String student_absent_present;
    String student_;
    String student_grnumber;
    String student_name;
    String student_image;

    public AbsentPresentListModel() {
    }

    public AbsentPresentListModel(String student_id, String student_class_id, String student_absent_present, String student_, String student_grnumber, String student_name, String student_image) {
        this.student_id = student_id;
        this.student_class_id = student_class_id;
        this.student_absent_present = student_absent_present;
        this.student_ = student_;
        this.student_grnumber = student_grnumber;
        this.student_name = student_name;
        this.student_image = student_image;
    }

    public String getStudent_id() {
        return student_id;
    }

    public void setStudent_id(String student_id) {
        this.student_id = student_id;
    }

    public String getStudent_class_id() {
        return student_class_id;
    }

    public void setStudent_class_id(String student_class_id) {
        this.student_class_id = student_class_id;
    }

    public String getStudent_absent_present() {
        return student_absent_present;
    }

    public void setStudent_absent_present(String student_absent_present) {
        this.student_absent_present = student_absent_present;
    }

    public String getStudent_() {
        return student_;
    }

    public void setStudent_(String student_) {
        this.student_ = student_;
    }

    public String getStudent_grnumber() {
        return student_grnumber;
    }

    public void setStudent_grnumber(String student_grnumber) {
        this.student_grnumber = student_grnumber;
    }

    public String getStudent_name() {
        return student_name;
    }

    public void setStudent_name(String student_name) {
        this.student_name = student_name;
    }

    public String getStudent_image() {
        return student_image;
    }

    public void setStudent_image(String student_image) {
        this.student_image = student_image;
    }
}