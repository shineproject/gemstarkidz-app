package com.example.shine_rahul.gemstarkidz.Model;

public class MonthModel {
    String id;
    String month;

    public MonthModel() {
    }

    public MonthModel(String id, String month) {
        this.id = id;
        this.month = month;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }
}
