package com.example.shine_rahul.gemstarkidz.Adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.shine_rahul.gemstarkidz.CommentActivity;
import com.example.shine_rahul.gemstarkidz.Fragment.SocialSinglepageFragment;
import com.example.shine_rahul.gemstarkidz.Fragment.TeacherSocialSingleListFragment;
import com.example.shine_rahul.gemstarkidz.Model.SocialModel;
import com.example.shine_rahul.gemstarkidz.Model.SocialSingleModel;
import com.example.shine_rahul.gemstarkidz.Model.TeacherClassModel;
import com.example.shine_rahul.gemstarkidz.R;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

public class TeacherSocialSingleListAdapter extends RecyclerView.Adapter<TeacherSocialSingleListAdapter.ViewHolder> {
    private Activity context;
    private ArrayList<SocialSingleModel> data;
    private Fragment currentFragment;
    int count;
    String pos;
    private Animation mFadeIn;
    private boolean isSelect = true;
    TeacherClassModel teacherClassModel;
    TagAdapter teacherClassStudentListAdapter;
    ArrayList<TeacherClassModel> teacherClassModelArrayList;
    int[] images = {R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1};
    String[] name = {"Kamlesh", "Bhargav", "Dhruv", "Jaymin", "Bilal", "Akshay", "Harshal", "Vineet", "Lucky", "Vipul", "Kamlesh", "Bhargav", "Dhruv", "Jaymin", "Bilal", "Akshay", "Harshal", "Vineet", "Lucky", "Vipul", "Kamlesh", "Bhargav", "Dhruv", "Jaymin", "Bilal", "Akshay", "Harshal", "Vineet", "Lucky", "Vipul"};

    public TeacherSocialSingleListAdapter(Activity context, ArrayList<SocialSingleModel> data) {
        this.context = context;
        this.data = data;
        //this.currentFragment = currentFragment;
    }

    public TeacherSocialSingleListAdapter() {

    }

    @Override
    public TeacherSocialSingleListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.social, null);
        return new TeacherSocialSingleListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final TeacherSocialSingleListAdapter.ViewHolder holder, final int position) {

        final SocialSingleModel socialModel = data.get(position);
        holder.txt_social_title.setText(socialModel.getTime());
        holder.txt_social_time.setText(socialModel.getTime());
        holder.txt_social_description.setText("Read More" + socialModel.getDescription());
        //  Glide.with(context).load(socialModel.getImage()).error(R.drawable.logo).into(holder.img_social_profile);
        Typeface style8 = Typeface.createFromAsset(context.getAssets(), "Raleway-SemiBold.ttf");
        holder.txt_social_title.setTypeface(style8);
        holder.txt_social_description.setTypeface(style8);
        com.squareup.picasso.Transformation transformation = new RoundedTransformationBuilder()
                .borderColor(Color.BLACK)
                .borderWidthDp(0)
                .cornerRadiusDp(10)
                .oval(false)
                .build();

        Picasso.with(context)
                .load(socialModel.getImage())
                .fit()
                .transform(transformation)
                .into(holder.img_social_profile);
        holder.txt_social_readmore.setPaintFlags(holder.txt_social_readmore.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        holder.txt_social_readmore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment socialSinglepageFragment = new Fragment();
                FragmentTransaction getFragmentManager = context.getFragmentManager().beginTransaction();
                getFragmentManager.replace(R.id.content, socialSinglepageFragment);
                getFragmentManager.addToBackStack(null);
                getFragmentManager.commit();

            }
        });

        holder.img_social_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Toast.makeText(context,"hiii",Toast.LENGTH_LONG).show();
                        /*Intent share = new Intent(android.content.Intent.ACTION_SEND);
                        share.setType("image*//*,text/plain");
                        share.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);*/


//                Bitmap bitmap = Bitmap.createBitmap(f.getWidth(), f.getHeight(), Bitmap.Config.ARGB_8888);
//                Canvas canvas = new Canvas(bitmap);
//                f.draw(canvas);
                // holder.img_social_profile.setDrawingCacheEnabled(true);
                holder.li_comment_send.setVisibility(View.GONE);
                holder.img_social_profile.buildDrawingCache();
                Bitmap bitmap = holder.img_social_profile.getDrawingCache();
                bitmapConvertToFile(bitmap, "full");
                //  holder.img_social_profile.destroyDrawingCache();

                String pathofBmp = MediaStore.Images.Media.insertImage(context.getContentResolver(), bitmap, "title", null);
                Uri bmpUri = Uri.parse(pathofBmp);
                Intent imageIntent = new Intent(Intent.ACTION_SEND);
                imageIntent.putExtra(Intent.EXTRA_TEXT, socialModel.getDescription());
                imageIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
                imageIntent.setType("image/*");
                context.startActivity(imageIntent);


            }


        });
        holder.img_social_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                holder.txt_count.setText("Like");
                notifyDataSetChanged();
                holder.li_comment_send.setVisibility(View.GONE);
                AlphaAnimation alphaAnim = new AlphaAnimation(1.0f, 0.0f);
                alphaAnim.setDuration(1500);
                alphaAnim.setAnimationListener(new Animation.AnimationListener() {
                    public void onAnimationEnd(Animation animation) {
                        // holder.newRoomView.setVisibility(View.INVISIBLE);
                    }

                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }
                });
                holder.lilike.startAnimation(alphaAnim);
                if (!isSelect) {
                    holder.img_social_like.setImageResource(R.mipmap.like_icon);
                    isSelect = true;
                } else {
                    holder.img_social_like.setImageResource(R.mipmap.hand);
                    isSelect = false;
                }


            }
        });
        holder.img_social_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, CommentActivity.class);
                context.startActivity(intent);

            }
        });
        holder.img_send_social_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.li_comment_send.setVisibility(View.GONE);

            }
        });
        holder.img_social_tag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTagDialog1();
            }
        });

    }

    public File bitmapConvertToFile(Bitmap bitmap, String str) {
        FileOutputStream fileOutputStream = null;
        File bitmapFile = null;
        Long time = System.currentTimeMillis();
        try {

            if (str.equals("edit")) {

            } else {
                File file = new File(Environment.getExternalStoragePublicDirectory(context.getString(R.string.app_name)), "");
                if (!file.exists()) {
                    file.mkdir();
                }
                if (str.equals("blur")) {
                    bitmapFile = new File(file, "IMG2.jpg");
                } else if (str.equals("share")) {
                    bitmapFile = new File(file, "" + time + "share.jpg");
                } else if (str.equals("profile")) {
                    bitmapFile = new File(file, "profile.jpg");
                } else {
                    Log.d("datetume : ", "" + time);
                    bitmapFile = new File(file, "" + time + ".jpg");
                }

                fileOutputStream = new FileOutputStream(bitmapFile);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
                MediaScannerConnection.scanFile(context, new String[]{bitmapFile.getAbsolutePath()}, null, new MediaScannerConnection.MediaScannerConnectionClient() {
                    @Override
                    public void onMediaScannerConnected() {

                    }

                    @Override
                    public void onScanCompleted(String path, Uri uri) {
                        context.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                            }
                        });
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.flush();
                    fileOutputStream.close();
                } catch (Exception e) {
                }
            }
        }
        return bitmapFile;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img_send_social_comment;
        ImageView img_social_profile;
        TextView txt_social_title;
        TextView txt_social_time;
        TextView txt_social_description;
        ImageView img_social_like;
        ImageView img_social_comment;
        ImageView img_social_share;
        ImageView img_social_tag;
        TextView txt_social_readmore;
        TextView txt_count;
        LinearLayout li_comment_send;
        EditText edt_social_enter_comment;
        LinearLayout lilike;

        public ViewHolder(View itemView) {
            super(itemView);
            lilike = (LinearLayout) itemView.findViewById(R.id.lilike);
            img_social_profile = (ImageView) itemView.findViewById(R.id.img_social_profile);
            img_send_social_comment = (ImageView) itemView.findViewById(R.id.img_send_social_comment);
            txt_social_title = (TextView) itemView.findViewById(R.id.txt_social_title);
            txt_social_time = (TextView) itemView.findViewById(R.id.txt_social_time);
            txt_social_description = (TextView) itemView.findViewById(R.id.txt_social_description);
            img_social_like = (ImageView) itemView.findViewById(R.id.img_social_like);
            img_social_comment = (ImageView) itemView.findViewById(R.id.img_social_comment);
            img_social_share = (ImageView) itemView.findViewById(R.id.img_social_share);
            img_social_tag = (ImageView) itemView.findViewById(R.id.img_social_tag);
            txt_social_readmore = (TextView) itemView.findViewById(R.id.txt_social_readmore);
            txt_count = (TextView) itemView.findViewById(R.id.txt_count);
            li_comment_send = (LinearLayout) itemView.findViewById(R.id.li_comment_send);
            edt_social_enter_comment = (EditText) itemView.findViewById(R.id.edt_social_enter_comment);
        }
    }

    private void showTagDialog1() {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view1 = layoutInflater.inflate(R.layout.entire_class_tag, null);
        final AlertDialog.Builder alertdialog = new AlertDialog.Builder(context);
        alertdialog.setView(view1);
        final AlertDialog dialog = alertdialog.create();
        final TextView textv = view1.findViewById(R.id.textv);
        final ImageView btn_entire_tag_student_submit = (ImageView) view1.findViewById(R.id.btn_entire_tag_student_submit);
        final GridView gv_entire_select_student = (GridView) view1.findViewById(R.id.gv_entire_select_student);
        textv.setVisibility(View.GONE);

//        teacherClassModelArrayList = new ArrayList<>();
//        for (int i = 0; i < images.length; i++) {
//            teacherClassModel = new TeacherClassModel();
//            teacherClassModel.setStudent_name(name[i]);
//            teacherClassModel.setStudent_image(images[i]);
//            teacherClassModelArrayList.add(teacherClassModel);
//        }
//        teacherClassStudentListAdapter = new TagAdapter(context, teacherClassModelArrayList);
//        teacherClassStudentListAdapter.notifyDataSetChanged();
//        gv_entire_select_student.setAdapter(teacherClassStudentListAdapter);
        dialog.show();
        btn_entire_tag_student_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Tag Successfully...", Toast.LENGTH_LONG).show();
                dialog.dismiss();
            }
        });
    }
}