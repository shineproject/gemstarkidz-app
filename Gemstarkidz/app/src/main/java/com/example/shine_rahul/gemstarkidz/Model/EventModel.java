package com.example.shine_rahul.gemstarkidz.Model;

/**
 * Created by Shine-Rahul on 5/3/2018.
 */

public class EventModel {
    String id;
    String image;
    String name;
    String date;
    String people;
    String end_date;
    String description;

    public EventModel() {
    }

    public EventModel(String id, String image, String name, String date, String people, String end_date, String description) {
        this.id = id;
        this.image = image;
        this.name = name;
        this.date = date;
        this.people = people;
        this.end_date = end_date;
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPeople() {
        return people;
    }

    public void setPeople(String people) {
        this.people = people;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
