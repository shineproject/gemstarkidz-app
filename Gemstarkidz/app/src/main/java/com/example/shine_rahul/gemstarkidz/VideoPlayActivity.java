package com.example.shine_rahul.gemstarkidz;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.VideoView;

public class VideoPlayActivity extends Activity {
    VideoView social_video;
    MediaController mediaController;
    ImageView img_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_video_play);
        social_video = (VideoView) findViewById(R.id.video);
        img_back = (ImageView) findViewById(R.id.img_back);
        mediaController = new MediaController(VideoPlayActivity.this);
        Intent intent = this.getIntent();
        Uri uri = Uri.parse(intent.getStringExtra("video"));
        social_video.setVideoURI(uri);
        social_video.setMediaController(mediaController);
        mediaController.setAnchorView(social_video);
        social_video.requestFocus();
        social_video.start();
        social_video.setClickable(false);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
