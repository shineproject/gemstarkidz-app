package com.example.shine_rahul.gemstarkidz.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.shine_rahul.gemstarkidz.Model.Student_Class_Model;
import com.example.shine_rahul.gemstarkidz.Model.Student_Class_Section_Model;
import com.example.shine_rahul.gemstarkidz.R;

import java.util.ArrayList;

/**
 * Created by Shine-Rahul on 22/2/2018.
 */

public class Student_Class_Section_Adapter extends BaseAdapter {

    private Context context;
    private ArrayList<Student_Class_Section_Model> data;
    private LayoutInflater layoutInflater = null;

    public Student_Class_Section_Adapter(Context context, ArrayList<Student_Class_Section_Model> data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (convertView == null) {
            layoutInflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.spinner_item, null);
        }
        TextView txt_spinner_item;
        txt_spinner_item = (TextView) view.findViewById(R.id.txt_spinner_item);
        txt_spinner_item.setText("" + data.get(position).getName());
        return view;
    }
}
