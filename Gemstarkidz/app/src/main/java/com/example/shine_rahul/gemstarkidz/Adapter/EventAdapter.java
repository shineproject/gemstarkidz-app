package com.example.shine_rahul.gemstarkidz.Adapter;

import android.app.Activity;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.shine_rahul.gemstarkidz.Model.EventModel;
import com.example.shine_rahul.gemstarkidz.R;
import com.example.shine_rahul.gemstarkidz.Service.Services;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Shine-Rahul on 5/3/2018.
 */

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.ViewHolder> {
    private Activity context;
    private ArrayList<EventModel> data;
    private ArrayList<EventModel> data1;
    String user_image = "";

    public EventAdapter(Activity context, ArrayList<EventModel> data) {
        this.context = context;
        this.data = data;
        this.data1 = new ArrayList<>();
        this.data1.addAll(data);
    }

    @Override
    public EventAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_list, null);
        return new EventAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(EventAdapter.ViewHolder holder, int position) {

        EventModel eventModel = data.get(position);
        holder.txt_event_name.setText(eventModel.getName());

        SimpleDateFormat stare_date = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        String set_start_date = "";
        try {
            date = stare_date.parse(eventModel.getDate());
            stare_date.applyPattern("dd/MM/yyyy");
            if (date != null) {
                set_start_date = stare_date.format(date);
            }
            holder.txt_event_date.setText(set_start_date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        holder.txt_event_people.setText(eventModel.getPeople() + " peoples");
        user_image = String.valueOf(eventModel.getImage());

        Picasso.with(context)
                .load(Services.IMAGES_PATH + "event/" + user_image)
                .placeholder(R.drawable.logo)
                .into(holder.img_event_profile);

        Typeface style = Typeface.createFromAsset(context.getAssets(), "Raleway-SemiBold.ttf");
        holder.txt_event_name.setTypeface(style);

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView img_event_profile;
        TextView txt_event_name, txt_event_date, txt_event_people;

        public ViewHolder(View itemView) {
            super(itemView);
            img_event_profile = (CircleImageView) itemView.findViewById(R.id.img_event_profile);
            txt_event_name = (TextView) itemView.findViewById(R.id.txt_event_name);
            txt_event_date = (TextView) itemView.findViewById(R.id.txt_event_date);
            txt_event_people = (TextView) itemView.findViewById(R.id.txt_event_people);
        }
    }


    public void filter1(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        data.clear();
        if (charText.length() == 0) {
            data.addAll(data1);
        } else {
            for (EventModel wp : data1) {
                if (wp.getDate().toLowerCase(Locale.getDefault()).contains(charText)) {
                    data.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        data.clear();
        if (charText.length() == 0) {
            data.addAll(data1);
        } else {
            for (EventModel wp : data1) {
                if (wp.getName().toLowerCase(Locale.getDefault()).contains(charText) || wp.getDate().toLowerCase(Locale.getDefault()).contains(charText)) {
                    data.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }
}
