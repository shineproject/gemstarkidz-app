package com.example.shine_rahul.gemstarkidz;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.example.shine_rahul.gemstarkidz.Adapter.ChatGroupAdapter;
import com.example.shine_rahul.gemstarkidz.Model.ChatGroupModel;

import java.io.File;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class ParentCreateChatGroupActivity extends AppCompatActivity {

    EditText edt_group_name;
    CircleImageView img_create_group, img_create_group_image;
    RecyclerView rv_create_group_list, rv_chat_create_list;
    ChatGroupModel chatGroupModel;
    ChatGroupAdapter chatGroupAdapter;
    ArrayList<ChatGroupModel> data;
    int GALLERY_PICK = 101, CAMERA_PICK = 132;
    String image_path = "";
    Bitmap bitmap;
    ActionBar actionBar;
    String[] name = {"Kamlesh", "Bhargav", "Dhruv", "Jaymin", "Bilal", "Akshay", "Harshal", "Vineet", "Lucky", "Vipul"};
    int[] image_profile = {R.drawable.image1, R.drawable.image8, R.drawable.image3, R.drawable.image4, R.drawable.image5, R.drawable.image6, R.drawable.image7, R.drawable.image8, R.drawable.image9, R.drawable.image10};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_chat_group);
        edt_group_name = (EditText) findViewById(R.id.edt_group_name);
        img_create_group = (CircleImageView) findViewById(R.id.img_create_group);
        img_create_group_image = (CircleImageView) findViewById(R.id.img_create_group_image);
        rv_create_group_list = (RecyclerView) findViewById(R.id.rv_create_group_list);
        rv_chat_create_list = (RecyclerView) findViewById(R.id.rv_chat_create_list);

        actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }


        data = new ArrayList<>();
        for (int i = 0; i < image_profile.length; i++) {
            chatGroupModel = new ChatGroupModel();
            chatGroupModel.setName(name[i]);
            chatGroupModel.setImage(image_profile[i]);
            data.add(chatGroupModel);
        }
        chatGroupAdapter = new ChatGroupAdapter(ParentCreateChatGroupActivity.this, data);
        chatGroupAdapter.notifyDataSetChanged();
        rv_chat_create_list.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
        rv_chat_create_list.setAdapter(chatGroupAdapter);
        img_create_group_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPictureDialog();
            }
        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                overridePendingTransition(0, 0);
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showPictureDialog() {
        final String[] option = {"Select photo from gallery", "Capture photo from camera", "Cancel"};
        final AlertDialog.Builder pictureDialog = new AlertDialog.Builder(ParentCreateChatGroupActivity.this);
        pictureDialog.setTitle("Select Action");

        pictureDialog.setItems(option, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                if (option[i].equals("Select photo from gallery")) {
                    Intent GALLERY = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    GALLERY.setType("image/*");
                    startActivityForResult(Intent.createChooser(GALLERY, "Select Image"), GALLERY_PICK);
                } else if (option[i].equals("Capture photo from camera")) {

                    Intent intent_camera = new Intent("android.media.action.IMAGE_CAPTURE");
                    File file = new File(Environment.getExternalStorageDirectory() + File.separator + "image.jpg");
                    intent_camera.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
                    startActivityForResult(intent_camera, CAMERA_PICK);

                } else if (option[i].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        pictureDialog.show();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == GALLERY_PICK) {
                Uri uri = data.getData();
                String[] FILE = {MediaStore.Images.Media.DATA, MediaStore.Video.Media.DATA};
                Cursor cursor = getApplicationContext().getContentResolver().query(uri, FILE, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(FILE[0]);
                image_path = cursor.getString(columnIndex);
                cursor.close();
                bitmap = BitmapFactory.decodeFile(image_path);
                img_create_group_image.setImageBitmap(bitmap);

            } else if (requestCode == CAMERA_PICK) {
                try {
                    image_path = Environment.getExternalStorageDirectory() + File.separator + "image.jpg";
                    bitmap = BitmapFactory.decodeFile(image_path);
                    img_create_group_image.setImageBitmap(bitmap);
                } catch (Exception e) {

                }

            }
        }
    }
}
