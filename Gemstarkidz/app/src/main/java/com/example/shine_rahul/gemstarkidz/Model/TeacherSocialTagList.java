package com.example.shine_rahul.gemstarkidz.Model;

public class TeacherSocialTagList {

    String Tag_Id;
    String Tag_Use_Id;
    String Tag_Soa_Id;
    String Tag_Flag;
    String Std_Id;
    String Std_Cla_Id;
    String Std_Gr_No;
    String Std_Name;
    String Std_Image;

    public String getTag_Id() {
        return Tag_Id;
    }

    public void setTag_Id(String tag_Id) {
        Tag_Id = tag_Id;
    }

    public String getTag_Use_Id() {
        return Tag_Use_Id;
    }

    public void setTag_Use_Id(String tag_Use_Id) {
        Tag_Use_Id = tag_Use_Id;
    }

    public String getTag_Soa_Id() {
        return Tag_Soa_Id;
    }


    public void setTag_Soa_Id(String tag_Soa_Id) {
        Tag_Soa_Id = tag_Soa_Id;
    }

    public String getTag_Flag() {
        return Tag_Flag;
    }

    public void setTag_Flag(String tag_Flag) {
        Tag_Flag = tag_Flag;
    }

    public String getStd_Id() {
        return Std_Id;
    }

    public void setStd_Id(String std_Id) {
        Std_Id = std_Id;
    }

    public String getStd_Cla_Id() {
        return Std_Cla_Id;
    }

    public void setStd_Cla_Id(String std_Cla_Id) {
        Std_Cla_Id = std_Cla_Id;
    }

    public String getStd_Gr_No() {
        return Std_Gr_No;
    }

    public void setStd_Gr_No(String std_Gr_No) {
        Std_Gr_No = std_Gr_No;
    }

    public String getStd_Name() {
        return Std_Name;
    }

    public void setStd_Name(String std_Name) {
        Std_Name = std_Name;
    }

    public String getStd_Image() {
        return Std_Image;
    }

    public void setStd_Image(String std_Image) {
        Std_Image = std_Image;
    }
}
