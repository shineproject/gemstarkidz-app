package com.example.shine_rahul.gemstarkidz.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shine_rahul.gemstarkidz.Model.HomeWorkDetailModel;
import com.example.shine_rahul.gemstarkidz.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Shine-Rahul on 13/3/2018.
 */

public class HomeWorkDetailAdapter extends RecyclerView.Adapter<HomeWorkDetailAdapter.ViewHolder> {
    private Context context;
    private ArrayList<HomeWorkDetailModel> data;

    public HomeWorkDetailAdapter(Context context, ArrayList<HomeWorkDetailModel> data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public HomeWorkDetailAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.parent_homework_detail_list, null);
        return new HomeWorkDetailAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(HomeWorkDetailAdapter.ViewHolder holder, int position) {
        HomeWorkDetailModel homeWorkDetailModel = data.get(position);


        SimpleDateFormat stare_date = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat stare_date1 = new SimpleDateFormat("EEEE");

        Date date = null;
        String set_start_date = "";
        String name = "";
        try {
            date = stare_date.parse(homeWorkDetailModel.getHmw_Date());
            stare_date.applyPattern("dd/MM/yyyy");
            stare_date1.applyPattern("EEEE");
            if (date != null) {
                set_start_date = stare_date.format(date);
                name = stare_date1.format(date);
            }
            holder.txt_date_homework.setText(set_start_date);
            holder.txt_homework_day.setText(name);

        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txt_date_homework;
        TextView txt_homework_day;

        public ViewHolder(View itemView) {
            super(itemView);
            txt_date_homework = (TextView) itemView.findViewById(R.id.txt_date_homework);
            txt_homework_day = (TextView) itemView.findViewById(R.id.txt_homework_day);
        }
    }
}
