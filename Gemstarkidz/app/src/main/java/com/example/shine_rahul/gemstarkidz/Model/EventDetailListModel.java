package com.example.shine_rahul.gemstarkidz.Model;

/**
 * Created by Shine-Rahul on 5/3/2018.
 */

public class EventDetailListModel {
    String id;
    String date;
    String people;
    String Description;
    String images_list;

    public EventDetailListModel() {
    }

    public EventDetailListModel(String id, String date, String people, String description, String images_list) {
        this.id = id;
        this.date = date;
        this.people = people;
        Description = description;
        this.images_list = images_list;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPeople() {
        return people;
    }

    public void setPeople(String people) {
        this.people = people;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getImages_list() {
        return images_list;
    }

    public void setImages_list(String images_list) {
        this.images_list = images_list;
    }
}
