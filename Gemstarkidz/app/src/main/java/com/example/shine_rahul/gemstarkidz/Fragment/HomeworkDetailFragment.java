package com.example.shine_rahul.gemstarkidz.Fragment;


import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.shine_rahul.gemstarkidz.Adapter.HomeWorkDetailAdapter;
import com.example.shine_rahul.gemstarkidz.Adapter.Student_Class_Adapter;
import com.example.shine_rahul.gemstarkidz.Adapter.Student_Section_Adapter;
import com.example.shine_rahul.gemstarkidz.Model.HomeWorkDetailModel;
import com.example.shine_rahul.gemstarkidz.Model.Student_Class_Model;
import com.example.shine_rahul.gemstarkidz.Model.Student_Section_Model;
import com.example.shine_rahul.gemstarkidz.R;
import com.example.shine_rahul.gemstarkidz.Service.Services;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeworkDetailFragment extends android.app.Fragment {
    RecyclerView rv_homework_detail;
    HomeWorkDetailAdapter homeWorkDetailAdapter;
    ArrayList<HomeWorkDetailModel> detailModelArrayList;
    String set_id, set_class_id, set_subject, set_chapter, set_date, getSet_pageno, set_class, set_section, set_description;
    TextView txt_edit_toolbar, txt_menu_title;
    ImageView img_search, img_close;
    EditText edt_search;

    String responseHomeworkList = "";
    String user = "", type = "", phone = "", uid = "", email = "", user_image = "", user_class, user_section, Cla_Id = "";

    public HomeworkDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_homework_detail, container, false);
        rv_homework_detail = (RecyclerView) view.findViewById(R.id.rv_homework_detail);
        txt_edit_toolbar = (TextView) getActivity().findViewById(R.id.txt_edit_toolbar);
        edt_search = (EditText) getActivity().findViewById(R.id.edt_search);
        img_search = (ImageView) getActivity().findViewById(R.id.img_search);
        img_close = (ImageView) getActivity().findViewById(R.id.img_close);
        img_search = (ImageView) getActivity().findViewById(R.id.img_search);
        img_search.setVisibility(View.GONE);
        img_close.setVisibility(View.GONE);
        txt_edit_toolbar.setVisibility(View.GONE);
        edt_search.setVisibility(View.GONE);

        SharedPreferences splogin = getActivity().getSharedPreferences("login", 0);
        user = splogin.getString("username", "");
        type = splogin.getString("type", "");
        phone = splogin.getString("user_phone", "");
        uid = splogin.getString("user_id", "");
        email = splogin.getString("user_email", "");
        user_image = splogin.getString("user_image", "");
        user_class = splogin.getString("user_class", "");
        user_section = splogin.getString("user_section", "");
        Cla_Id = splogin.getString("Cla_Id", "");

        new AsyncTaskRunnerGetClassSection().execute();

        return view;
    }

    private class AsyncTaskRunnerGetClassSection extends AsyncTask<String, String, String> {

        private ProgressDialog Dialog = new ProgressDialog(getActivity());

        @Override
        protected String doInBackground(String... params) {

            MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);

            // JSONArray jsArray = null;
            builder.addFormDataPart("parent_id", uid);

            RequestBody requestBody = builder.build();

            try {

                OkHttpClient client = new OkHttpClient.Builder().build();
                Request request = new Request.Builder()
                        .url(Services.HOMEWORK_LIST)
                        .post(requestBody)
                        .build();

                Response resp = client.newCall(request).execute();

                assert resp.body() != null;
                responseHomeworkList = resp.body().string();

            } catch (Exception e) {
                e.printStackTrace();
            }

            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            Dialog.dismiss();
            DeliveryDataHomework(responseHomeworkList);
            super.onPostExecute(s);
        }

        @Override
        protected void onPreExecute() {
            Dialog.show();
            Dialog.setMessage("Please Wait....");
            Dialog.setCancelable(false);
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
        }
    }

    private void DeliveryDataHomework(String response) {

        HomeWorkDetailModel homeWorkDetailModel;
        detailModelArrayList = new ArrayList<>();

        if (!response.equals("")) {
            try {
                JSONObject jsobjectcategory = new JSONObject(response);
                String message = jsobjectcategory.getString("message");
                // Updated upstream
                String error = jsobjectcategory.getString("error");

                if (error.equals("200")) {

                    JSONArray jsonArray = jsobjectcategory.getJSONArray("data");
                    JSONObject jsonObject = null;
                    JSONObject jsonObject1 = null;

                    for (int i = 0; i < jsonArray.length(); i++) {
                        jsonObject = jsonArray.getJSONObject(i);

                        homeWorkDetailModel = new HomeWorkDetailModel();
                        set_id = jsonObject.getString("Hmw_Id");
                        set_class_id = jsonObject.getString("Hmw_Cla_Id");
                        set_subject = jsonObject.getString("Hmw_Subject");
                        set_chapter = jsonObject.getString("Hmw_Id");
                        set_date = jsonObject.getString("Hmw_Date");
                        getSet_pageno = jsonObject.getString("Hmw_Page_No");
                        set_description = jsonObject.getString("Hmw_Desscription");

                        homeWorkDetailModel.setHmw_Id(set_id);
                        homeWorkDetailModel.setHmw_Cla_Id(set_class_id);
                        homeWorkDetailModel.setHmw_Subject(set_subject);
                        homeWorkDetailModel.setHmw_Chapter(set_chapter);
                        homeWorkDetailModel.setHmw_Date(set_date);
                        homeWorkDetailModel.setHmw_Page_No(getSet_pageno);
                        homeWorkDetailModel.setHmw_Desscription(set_description);

                        JSONArray jsonArray1 = jsonObject.getJSONArray("section");
                        for (int j = 0; j < jsonArray1.length(); j++) {
                            jsonObject1 = jsonArray1.getJSONObject(j);
                            set_class = jsonObject1.getString("Cla_Class");
                            set_section = jsonObject1.getString("Cla_Section");
                            homeWorkDetailModel.setCla_Class(set_class);
                            homeWorkDetailModel.setCla_Section(set_section);
                        }

                        detailModelArrayList.add(homeWorkDetailModel);
                    }

                    homeWorkDetailAdapter = new HomeWorkDetailAdapter(getActivity(), detailModelArrayList);
                    rv_homework_detail.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
                    rv_homework_detail.setAdapter(homeWorkDetailAdapter);

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

        }

    }
}