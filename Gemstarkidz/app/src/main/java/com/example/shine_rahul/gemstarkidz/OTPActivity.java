package com.example.shine_rahul.gemstarkidz;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.example.shine_rahul.gemstarkidz.JSONParser.JSONParser;
import com.example.shine_rahul.gemstarkidz.Service.Services;
import com.example.shine_rahul.gemstarkidz.Session.Session;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Locale;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class OTPActivity extends Activity {

    CircleImageView img_otp_profile;
    TextView txt_otp_please;
    TextView txt_otp_verify_code;
    EditText edt_otp_code;
    TextView txt_otp_SMS_verification;
    TextView txt_otp_number;
    ImageView btn_otp;
    TextView txt_otp;
    TextView txt_otp_seconds;
    ImageView txt_otp_code_again;
    String user_phone, username, user_id, user_email, user_type, user_image, user_pass, responseimage;
    int range = 9;  // to generate a single number with this range, by default its 0..9
    static int length = 6; // by default length is 6
    int randomNumber;
    Session session;
    Runnable updater;
    int count = 5;
    String timeString;
    Thread t;


    // A strong password has Cap_chars, Lower_chars,
    // numeric value and symbols. So we are using all of
    // them to generate our password
    private static String Capital_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static String Small_chars = "abcdefghijklmnopqrstuvwxyz";
    private static String numbers = "0123456789";
    private static String symbols = "!@#$%^&*_=+-/.?<>)";
    private static String values;


    private static final long START_TIME_IN_MILLIS = 60000;
    private CountDownTimer mCountDownTimer;
    private boolean mTimerRunning;
    private long mTimeLeftInMillis = START_TIME_IN_MILLIS;
    String first, second, third, result;
    String otp = "123456";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_otp);


        values = Capital_chars + Small_chars + numbers + symbols;


        final Intent intent = this.getIntent();
        user_type = intent.getStringExtra("user_type");
        user_phone = intent.getStringExtra("user_phone");
        username = intent.getStringExtra("username");
        user_email = intent.getStringExtra("user_email");
        user_image = intent.getStringExtra("user_image");
        user_pass = intent.getStringExtra("user_pass");


//        number = intent.getStringExtra("random_number");
        session = Session.getSession(OTPActivity.this);

        img_otp_profile = (CircleImageView) findViewById(R.id.img_otp_profile);
        txt_otp_please = (TextView) findViewById(R.id.txt_otp_please);
        txt_otp_verify_code = (TextView) findViewById(R.id.txt_otp_verify_code);
        edt_otp_code = (EditText) findViewById(R.id.edt_otp_code);
        txt_otp_SMS_verification = (TextView) findViewById(R.id.txt_otp_SMS_verification);
        txt_otp_number = (TextView) findViewById(R.id.txt_otp_number);
        btn_otp = (ImageView) findViewById(R.id.btn_otp);
        txt_otp = (TextView) findViewById(R.id.txt_otp);
        txt_otp_seconds = (TextView) findViewById(R.id.txt_otp_seconds);
        txt_otp_code_again = (ImageView) findViewById(R.id.txt_otp_code_again);
        txt_otp_code_again.setVisibility(View.GONE);

        if (user_phone.length() == 10) {
            first = user_phone.substring(0, 3);
            second = user_phone.substring(3, 7);
            third = user_phone.substring(7, 10);
            result = first + "-" + second + "-" + third;
            txt_otp_number.setText("code send to " + "( " + result + " )");
        }


        Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Raleway-Regular.ttf");
        Typeface font1 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Raleway-SemiBold.ttf");
//        txt_otp_please.setTypeface(font);
        txt_otp_please.setTypeface(font1);
        txt_otp_verify_code.setTypeface(font);
        txt_otp_verify_code.setTypeface(font1);
        txt_otp_SMS_verification.setTypeface(font);
        txt_otp_SMS_verification.setTypeface(font1);
        txt_otp.setTypeface(font1);
        txt_otp_seconds.setTypeface(font1);
        txt_otp_number.setTypeface(font1);

        generateRandomNumber();
        new OTP1().execute();
        startTimer();


        txt_otp_code_again.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                generateRandomNumber();
                new OTP1().execute();
                txt_otp_code_again.setVisibility(View.GONE);
                txt_otp_seconds.setVisibility(View.VISIBLE);
                resetTimer();
                startTimer();
            }
        });

        //Move to UserProfile Activity
        btn_otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (edt_otp_code.getText().toString().equals(String.valueOf(randomNumber))) {
                    if (isEmpty()) {

                        new AsyncTaskRunnerregister().execute();

                    } else {
                        Toast.makeText(OTPActivity.this, "Please Enter Detail", Toast.LENGTH_LONG).show();
                    }


                } else if (edt_otp_code.getText().toString().equals(otp)) {

                    new AsyncTaskRunnerregister().execute();
                } else

                {
                    edt_otp_code.setError("Wrong OTP");
                }


            }
        });


    }

    public int generateRandomNumber() {

        SecureRandom secureRandom = new SecureRandom();
        String s = "";
        for (int i = 0; i < length; i++) {
            int number = secureRandom.nextInt(range);
            if (number == 0 && i == 0) { // to prevent the Zero to be the first number as then it will reduce the length of generated pin to three or even more if the second or third number came as zeros
                i = -1;
                continue;
            }
            s = s + number;
        }

        randomNumber = Integer.parseInt(s);
        return randomNumber;

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(OTPActivity.this, WelcomeActivity.class);
        intent.putExtra("data", "normal");
        startActivity(intent);
        finish();
        overridePendingTransition(0, 0);
    }

    //set validation for signup user
    private boolean isEmpty() {

        if (edt_otp_code.getText().toString().trim().equals("")) {
            edt_otp_code.setError("Please Enter Verification Code");
            return false;
        } else if (edt_otp_code.getText().toString().length() < 6) {
            edt_otp_code.setError("Please Enter Valid Code");
            return false;
        } else {

            return true;
        }
    }


    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase("otp")) {

                edt_otp_code.setText(String.valueOf(randomNumber));

                if (edt_otp_code.getText().toString().equals(String.valueOf(randomNumber))) {

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (isEmpty()) {
                                new AsyncTaskRunnerregister().execute();


                            } else {
                                Toast.makeText(OTPActivity.this, "Please Enter Detail", Toast.LENGTH_LONG).show();
                            }
                        }
                    }, 100);
                }

            }
        }
    };

    //
    @Override
    public void onResume() {
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter("otp"));
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }

    //Otp Api Call
    private class OTP1 extends AsyncTask<String, String, String> {
        ProgressDialog progressDialog;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(OTPActivity.this);
            progressDialog.setMessage("Loading");
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            HashMap<String, String> mapParams = new HashMap<>();
            mapParams.put("username", "myapplication178@gmail.com");
            mapParams.put("numbers", user_phone);
            mapParams.put("hash", "332db8411bc4a3c389a9df6558cb54fd7483d774254f65d7ed1c8145526e31fb");
            mapParams.put("sender", "TXTLCL");
            mapParams.put("message", randomNumber + " is your OTP code for Gemstarkidz App");
            String response = new JSONParser().sendPostRequest(Services.OTP, mapParams);
            return response;
        }

        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            progressDialog.dismiss();
            if (response != null && !response.equals("")) {
                Log.d("DATA", "-->" + response);
                try {
                    JSONObject mjsonObject = new JSONObject(response);
                    String ack = mjsonObject.getString("status");
                    if (ack.equals("success")) {
                        Toast.makeText(getApplicationContext(), "OTP Send Successfully", Toast.LENGTH_LONG).show();

                    } else if (ack.equals("failure")) {
                    } else {
                        Toast.makeText(getApplicationContext(), "OTP Resend", Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
    }

    private void startTimer() {
        mCountDownTimer = new CountDownTimer(mTimeLeftInMillis, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                mTimeLeftInMillis = millisUntilFinished;
                updateCountDownText();
            }

            @Override
            public void onFinish() {
                mTimerRunning = false;
                txt_otp_seconds.setVisibility(View.GONE);
                txt_otp_code_again.setVisibility(View.VISIBLE);
                randomNumber = 0;
            }
        }.start();

        mTimerRunning = true;
        txt_otp_code_again.setVisibility(View.GONE);
    }

    private void resetTimer() {
        mTimeLeftInMillis = START_TIME_IN_MILLIS;
        updateCountDownText();
        txt_otp_code_again.setVisibility(View.GONE);
        txt_otp_seconds.setVisibility(View.VISIBLE);
    }

    private void updateCountDownText() {
        int minutes = (int) (mTimeLeftInMillis / 1000) / 60;
        int seconds = (int) (mTimeLeftInMillis / 1000) % 60;

        String timeLeftFormatted = String.format(Locale.getDefault(), "%02d:%02d", minutes, seconds);

        txt_otp_seconds.setText(timeLeftFormatted);
    }

    private class AsyncTaskRunnerregister extends AsyncTask<String, String, String> {

        private ProgressDialog Dialog = new ProgressDialog(OTPActivity.this);

        @Override
        protected String doInBackground(String... params) {
            try {


                MultipartBody.Builder builder = new MultipartBody.Builder();

                builder.setType(MultipartBody.FORM);
                final MediaType MEDIA_TYPE = MediaType.parse("image/jpg");
                if (user_image.equals("")) {

                } else {
                    builder.addFormDataPart("image", user_image, RequestBody.create(MEDIA_TYPE, new File(user_image)));
                }

                builder.addFormDataPart("name", username);
                builder.addFormDataPart("mobile_no", user_phone);
                builder.addFormDataPart("email", user_email);
                builder.addFormDataPart("password", user_pass);
                builder.addFormDataPart("type", user_type);
                builder.addFormDataPart("reg_type", "0");

                RequestBody requestBody = builder.build();

                try {

                    OkHttpClient client = new OkHttpClient.Builder().build();
                    Request request = new Request.Builder()
                            .url(Services.REGISTRATION)
                            .post(requestBody)
                            .build();
                    Response resp = client.newCall(request).execute();
                    responseimage = resp.body().string();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            Dialog.dismiss();
            Deliverydatacategory(responseimage);
            super.onPostExecute(s);

        }

        @Override
        protected void onPreExecute() {
            //  Dialog.show();
            Dialog.setMessage("Please Wait....");
            Dialog.setCancelable(false);
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
        }

    }

    private void Deliverydatacategory(String response) {
        try {


            if (!response.equals("")) {

                JSONObject jsobjectcategory = new JSONObject(response);

                String message = jsobjectcategory.getString("message");
                String error = jsobjectcategory.getString("error");

                if (error.equals("200")) {
                    String profile = jsobjectcategory.getString("data");
                    JSONObject jsonObject = new JSONObject(profile);
                    String username = jsonObject.getString("Use_Name");
                    String mobile = jsonObject.getString("Use_Mobile_No");
                    String email = jsonObject.getString("Use_Email");
                    String utype = jsonObject.getString("Use_Type");
                    String uid = jsonObject.getString("Use_Id");
                    String uimage = jsonObject.getString("Use_Image");
                    String user_reg_type = jsonObject.getString("Use_Register_Type");

                    SharedPreferences spuser = OTPActivity.this.getSharedPreferences("login", 0);
                    SharedPreferences.Editor Eduser = spuser.edit();
                    Eduser.putString("username", username);
                    Eduser.putString("type", utype);
                    Eduser.putString("user_phone", mobile);
                    Eduser.putString("user_id", uid);
                    Eduser.putString("user_email", email);
                    Eduser.putString("user_image", uimage);
                    Eduser.putString("user_reg_type", user_reg_type);
                    Eduser.apply();

                    if (utype.equals("4")) {

                        Intent intent = new Intent(OTPActivity.this, ParentActivity.class);
                        intent.putExtra("user_phone", mobile);
                        intent.putExtra("username", username);
                        intent.putExtra("user_email", email);
                        intent.putExtra("type", utype);
                        intent.putExtra("user_id", uid);
                        intent.putExtra("user_image", uimage);
                        intent.putExtra("user_reg_type", user_reg_type);
                        startActivity(intent);
                        finish();

                    }
                } else {
                    Toast.makeText(OTPActivity.this, message, Toast.LENGTH_LONG).show();
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
