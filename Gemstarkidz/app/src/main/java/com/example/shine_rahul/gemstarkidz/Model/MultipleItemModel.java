package com.example.shine_rahul.gemstarkidz.Model;

/**
 * Created by Shine-Rahul on 21/3/2018.
 */

public class MultipleItemModel {
    String id;
    int images;

    public MultipleItemModel() {
    }

    public MultipleItemModel(String id, int images) {
        this.id = id;
        this.images = images;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getImages() {
        return images;
    }

    public void setImages(int images) {
        this.images = images;
    }
}
