package com.example.shine_rahul.gemstarkidz;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

public class ImageDisplayActivity extends Activity {
    ImageView img_display;
    String image;
    ImageView img_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_image_display);
        img_display = (ImageView) findViewById(R.id.img_display);
        img_back = (ImageView) findViewById(R.id.img_back);
        Intent intent = this.getIntent();
        image = intent.getStringExtra("image");
        img_display.setImageURI(Uri.parse(image));
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
