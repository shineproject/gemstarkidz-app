package com.example.shine_rahul.gemstarkidz.Adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.shine_rahul.gemstarkidz.Model.TeacherReportModel;
import com.example.shine_rahul.gemstarkidz.R;
import com.example.shine_rahul.gemstarkidz.Service.Services;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Shine-Rahul on 10/3/2018.
 */

public class TeacherReportAdapter extends RecyclerView.Adapter<TeacherReportAdapter.ViewHolder> {
    private Activity context;
    private ArrayList<TeacherReportModel> data;
    private ArrayList<TeacherReportModel> data1;

    public TeacherReportAdapter(Activity context, ArrayList<TeacherReportModel> data) {
        this.context = context;
        this.data = data;
        this.data1 = new ArrayList<>();
        this.data1.addAll(data);
    }

    @Override
    public TeacherReportAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.teacher_report, null);
        return new TeacherReportAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TeacherReportAdapter.ViewHolder holder, int position) {

        final TeacherReportModel reportModel = data.get(position);

        holder.txt_report_name.setText(reportModel.getStd_Name());
        holder.txt_report_grnumber.setText(reportModel.getStd_Gr_No());
        holder.txt_report_class.setText(reportModel.getCla_Class());
        holder.txt_report_section.setText(reportModel.getCla_Section());

        Picasso.with(context)
                .load(Services.IMAGES_PATH + "profile/" +reportModel.getStd_Image())
                .error(R.drawable.profile)
                .into(holder.img_teacher_report_student_list);

//        Picasso.with(context)
//                .load(reportModel.getChart())
//                .error(R.drawable.profile)
//                .into(holder.img_teacher_report_chart);

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView img_teacher_report_student_list;
        ImageView img_teacher_report_chart;
        TextView txt_report_name;
        TextView txt_report_grnumber;
        TextView txt_report_class;
        TextView txt_report_section;

        public ViewHolder(View itemView) {
            super(itemView);
            img_teacher_report_student_list = (CircleImageView) itemView.findViewById(R.id.img_teacher_report_student_list);
            img_teacher_report_chart = (ImageView) itemView.findViewById(R.id.img_teacher_report_chart);
            txt_report_name = (TextView) itemView.findViewById(R.id.txt_report_name);
            txt_report_grnumber = (TextView) itemView.findViewById(R.id.txt_report_grnumber);
            txt_report_class = (TextView) itemView.findViewById(R.id.txt_report_class);
            txt_report_section = (TextView) itemView.findViewById(R.id.txt_report_section);
        }
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        data.clear();
        if (charText.length() == 0) {
            data.addAll(data1);
        } else {
            for (TeacherReportModel wp : data1) {
                if (wp.getStd_Name().toLowerCase(Locale.getDefault()).contains(charText) || wp.getStd_Gr_No().toLowerCase(Locale.getDefault()).contains(charText)) {
                    data.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }
}
