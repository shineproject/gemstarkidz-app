package com.example.shine_rahul.gemstarkidz.Fragment;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.FragmentManager;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.Time;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.shine_rahul.gemstarkidz.Adapter.ChatArrayAdapter;
import com.example.shine_rahul.gemstarkidz.Model.ChatSingleModel;
import com.example.shine_rahul.gemstarkidz.PDFActivity;
import com.example.shine_rahul.gemstarkidz.R;
import com.example.shine_rahul.gemstarkidz.Utilities;
import com.example.shine_rahul.gemstarkidz.VideoPlayActivity;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.example.shine_rahul.gemstarkidz.FilesManager.getDataColumn;
import static com.example.shine_rahul.gemstarkidz.FilesManager.isDownloadsDocument;
import static com.example.shine_rahul.gemstarkidz.FilesManager.isExternalStorageDocument;
import static com.example.shine_rahul.gemstarkidz.FilesManager.isGooglePhotosUri;
import static com.example.shine_rahul.gemstarkidz.FilesManager.isMediaDocument;

/**
 * A simple {@link Fragment} subclass.
 */
public class TeacherChatSinglePageFragment extends android.app.Fragment {
    CircleImageView img_teacher_chat_single;
    TextView txt_teacher_chat_single_time;
    TextView txt_teacher_chat_single_name;
    ImageView img__teacher_chat_select;
    RelativeLayout rl_menu;
    RecyclerView rv_chat_single_list;
    ChatSingleModel chatSingleModel;
    ArrayList<ChatSingleModel> dataselect = new ArrayList<>();
    ChatSingleDetailAdapter chatSingleDetailAdapter;
    EditText edt_teacher_type_message;
    ImageView img__teacher_chat_send;
    ImageView img_document;
    ImageView img_audio;
    String datetime, datetime1;
    Chronometer chronometerTimer;
    ImageView img_comment;


    //meterial send  like image,video,file,audio
    ImageView img_chat_send_image, img_chat_send_video;

    //Image send code
    private Bitmap bitmap1;
    int GALLERY_PICK = 101, CAMERA_PICK = 132, VIDEO = 133, PDf = 134;
    String path = "";
    String imageDecode, selecteddata, encodedImageData, imageDecode1;
    ImageView image_play;
    SeekBar seek;
    TextView txtTitle;
    MediaPlayer mediaPlayer;
    private Handler mHandler = new Handler();
    private Utilities utils;
    public static String time;

    // audio send

    private MediaRecorder mMediaRecorder;
    private File mOutputFile;
    long timecurrent;
    String AudioSavePathInDevice = null;
    private MediaRecorder mRecorder;
    private int lastProgress = 0;
    private final int MY_PERMISSIONS_RECORD_AUDIO = 1;

    private boolean isRecording = false;
    private static final String TAG = "Recorder";
    public static int count = 0;
    String Audio = "";

    private String getpath(String path) {
        return path;
    }

    public TeacherChatSinglePageFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_teacher_chat_single_page, container, false);
        chronometerTimer = (Chronometer) view.findViewById(R.id.chronometerTimer);
        chronometerTimer.setBase(SystemClock.elapsedRealtime());
        utils = new Utilities();

        /*final Time today = new Time(Time.getCurrentTimezone());
        today.setToNow();*/
        img_audio = (ImageView) view.findViewById(R.id.img_audio);
        img_document = (ImageView) view.findViewById(R.id.img_document);
        img__teacher_chat_send = (ImageView) view.findViewById(R.id.img__teacher_chat_send);
        img_chat_send_image = (ImageView) view.findViewById(R.id.img_chat_send_image);
        img_chat_send_video = (ImageView) view.findViewById(R.id.img_chat_send_video);
        rv_chat_single_list = (RecyclerView) view.findViewById(R.id.rv_chat_single_list);
        edt_teacher_type_message = (EditText) view.findViewById(R.id.edt_teacher_type_message);
        img_teacher_chat_single = (CircleImageView) view.findViewById(R.id.img_teacher_chat_single);
        img__teacher_chat_select = (ImageView) view.findViewById(R.id.img__teacher_chat_select);
        txt_teacher_chat_single_name = (TextView) view.findViewById(R.id.txt_teacher_chat_single_name);
        txt_teacher_chat_single_time = (TextView) view.findViewById(R.id.txt_teacher_chat_single_time);
        img_comment = (ImageView) view.findViewById(R.id.img_comment);
        rl_menu = (RelativeLayout) view.findViewById(R.id.rl_menu);
        Bundle bundle = getArguments();
        edt_teacher_type_message.setTextColor(Color.BLACK);


        img_teacher_chat_single.setImageResource(Integer.parseInt(bundle.getString("IMAGE")));
        txt_teacher_chat_single_name.setText(bundle.getString("NAME"));
        txt_teacher_chat_single_time.setText(bundle.getString("TIME"));
        img__teacher_chat_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rl_menu.getVisibility() == View.GONE) {
                    rl_menu.setVisibility(View.VISIBLE);
                } else {
                    rl_menu.setVisibility(View.GONE);
                }

            }
        });
        img_audio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (count == 0) {
                    chronometerTimer.setVisibility(View.VISIBLE);
                    img_audio.setImageResource(R.drawable.stop);
//                    requestAudioPermissions();
                    startRecording();
                    count = 1;
                } else {
                    chronometerTimer.setVisibility(View.GONE);
                    img_audio.setImageResource(R.drawable.audio);
                    stopRecording();
                    count = 0;


                    chatSingleModel = new ChatSingleModel();
                    chatSingleModel.setImage(R.drawable.sendmsgicon);


//                    chatSingleModel.setSend_image(Integer.parseInt(path));
                    chatSingleModel.setSend_image(Audio);
//                    chatSingleModel.setPath(encodedImageData);
                    Calendar c = Calendar.getInstance();
                    final SimpleDateFormat dateformat = new SimpleDateFormat("hh:mm aa");
                    datetime = dateformat.format(c.getTime());
                    chatSingleModel.setTime(datetime1);


                    dataselect.add(chatSingleModel);
                    chatSingleDetailAdapter = new ChatSingleDetailAdapter(getActivity(), dataselect);
                    chatSingleDetailAdapter.notifyDataSetChanged();
                    rv_chat_single_list.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
                    rv_chat_single_list.setAdapter(chatSingleDetailAdapter);
                    rv_chat_single_list.smoothScrollToPosition(chatSingleDetailAdapter.getItemCount());
                    datetime = "";
                }

            }


        });
        img_chat_send_image.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {
                showPictureDialog();
            }
        });

        img_chat_send_video.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {
                Intent intent;
                if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)) {
                    intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
                } else {
                    intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Video.Media.INTERNAL_CONTENT_URI);
                }
                intent.setType("video/mp4");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.putExtra("return-data", true);
                startActivityForResult(intent, VIDEO);
            }
        });
        img_document.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("application/pdf");
                intent.setAction(Intent.EXTRA_STREAM);
                intent.putExtra("return-data", true);
                startActivityForResult(intent, PDf);
            }
        });
        img__teacher_chat_send.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {
                // Toast.makeText(getActivity(),today.format("%k:%M").toString(),Toast.LENGTH_LONG).show();
                //Toast.makeText(getActivity(),datetime.toString(),Toast.LENGTH_LONG).show();
                if (edt_teacher_type_message.getText().toString().trim().equals("")) {
                    edt_teacher_type_message.setText("");
                    edt_teacher_type_message.requestFocus();
                } else {
                    String message = edt_teacher_type_message.getText().toString().trim();
                    // data.add(new ChatSingleModel(message));
                    chatSingleModel = new ChatSingleModel();
                    chatSingleModel.setSend_image(message);
                    chatSingleModel.setImage(R.drawable.sendmsgicon);
                    edt_teacher_type_message.requestFocusFromTouch();


//                    chatSingleModel.setSend_image(Integer.parseInt(path));
//                    chatSingleModel.setSend_image(Integer.parseInt(imageDecode));
                    Calendar c = Calendar.getInstance();
                    final SimpleDateFormat dateformat = new SimpleDateFormat("hh:mm aa");
                    datetime = dateformat.format(c.getTime());
                    chatSingleModel.setTime(datetime);


                    dataselect.add(chatSingleModel);
                    chatSingleDetailAdapter = new ChatSingleDetailAdapter(getActivity(), dataselect);
                    chatSingleDetailAdapter.notifyDataSetChanged();
                    rv_chat_single_list.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
                    rv_chat_single_list.setAdapter(chatSingleDetailAdapter);
                    rv_chat_single_list.smoothScrollToPosition(chatSingleDetailAdapter.getItemCount());
                    edt_teacher_type_message.setText("");
                    datetime = "";

                }
            }
        });


        return view;
    }
    //audio permission

    private void requestAudioPermissions() {
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.RECORD_AUDIO)
                != PackageManager.PERMISSION_GRANTED) {

            //When permission is not granted by user, show them message why this permission is needed.
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.RECORD_AUDIO)) {
                Toast.makeText(getActivity(), "Please grant permissions to record audio", Toast.LENGTH_LONG).show();

                //Give user option to still opt-in the permissions
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.RECORD_AUDIO},
                        MY_PERMISSIONS_RECORD_AUDIO);

            } else {
                // Show user dialog to grant permission to record audio
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.RECORD_AUDIO},
                        MY_PERMISSIONS_RECORD_AUDIO);
            }
        }
        //If permission is granted, then go ahead recording audio
        else if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.RECORD_AUDIO)
                == PackageManager.PERMISSION_GRANTED) {

            //Go ahead with recording audio now
            startRecording();
        }
    }

    //Handling callback
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_RECORD_AUDIO: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay!
                    startRecording();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(getActivity(), "Permissions Denied to record audio", Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }


    private void startRecording() {
        //we use the MediaRecorder class to record
        timecurrent = System.currentTimeMillis();
        AudioSavePathInDevice = timecurrent + "AudioRecording.mp3";

        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
        mRecorder.setOutputFile(getActivity().getCacheDir().getAbsolutePath() + "/" + AudioSavePathInDevice);

        try {
            mRecorder.prepare();
            mRecorder.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
        lastProgress = 0;
        chronometerTimer.setBase(SystemClock.elapsedRealtime());
        chronometerTimer.start();
    }


    private void stopRecording() {
        try {
            mRecorder.stop();
            mRecorder.release();

            Audio = getActivity().getCacheDir().getPath() + "/" + AudioSavePathInDevice;

        } catch (Exception e) {
            e.printStackTrace();
        }
        mRecorder = null;
        //starting the chronometer
        chronometerTimer.stop();
        chronometerTimer.setBase(SystemClock.elapsedRealtime());
        //showing the play button
//        Toast.makeText(getActivity(), "Recording saved successfully.", Toast.LENGTH_SHORT).show();

    }


    private void releaseMediaRecorder() {
        if (mMediaRecorder != null) {
            // clear recorder configuration
            mMediaRecorder.reset();
            // release the recorder object
            mMediaRecorder.release();
            mMediaRecorder = null;
            // Lock camera for later use i.e taking it back from MediaRecorder.
            // MediaRecorder doesn't need it anymore and we will release it if the activity pauses.
            // mCamera.lock();
        }
    }


    private void showPictureDialog() {
        final String[] option = {"Select photo from gallery", "Cancel"};
        final AlertDialog.Builder pictureDialog = new AlertDialog.Builder(getActivity());
        pictureDialog.setTitle("Select Action");

        pictureDialog.setItems(option, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                if (option[i].equals("Select photo from gallery")) {
                    Intent GALLERY = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    GALLERY.setType("image/*");
                    startActivityForResult(Intent.createChooser(GALLERY, "Select Image"), GALLERY_PICK);
                } else if (option[i].equals("Capture photo from camera")) {
                    Intent CAMERA = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(CAMERA, CAMERA_PICK);
                } else if (option[i].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        pictureDialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == GALLERY_PICK) {
                selecteddata = "image";
                Uri URI = data.getData();
                String[] FILE = {MediaStore.Images.Media.DATA, MediaStore.Video.Media.DATA};
                Cursor cursor = getActivity().getContentResolver().query(URI, FILE, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(FILE[0]);
                imageDecode = cursor.getString(columnIndex);


                chatSingleModel = new ChatSingleModel();
                chatSingleModel.setImage(R.drawable.sendmsgicon);

                chatSingleModel.setSend_image(imageDecode);
                Calendar c = Calendar.getInstance();
                final SimpleDateFormat dateformat = new SimpleDateFormat("hh:mm aa");
                datetime = dateformat.format(c.getTime());
                chatSingleModel.setTime(datetime);


                dataselect.add(chatSingleModel);
                chatSingleDetailAdapter = new ChatSingleDetailAdapter(getActivity(), dataselect);
                chatSingleDetailAdapter.notifyDataSetChanged();
                rv_chat_single_list.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
                rv_chat_single_list.setAdapter(chatSingleDetailAdapter);
                rv_chat_single_list.smoothScrollToPosition(chatSingleDetailAdapter.getItemCount());

                datetime = "";

            } else if (requestCode == CAMERA_PICK) {
                try {

                    Bitmap imageBitmap = (Bitmap) data.getExtras().get("data");
                    String image = String.valueOf(imageBitmap);
                    chatSingleModel = new ChatSingleModel();
                    chatSingleModel.setImage(R.drawable.sendmsgicon);

                    chatSingleModel.setSend_image(image);
                    Calendar c = Calendar.getInstance();
                    final SimpleDateFormat dateformat = new SimpleDateFormat("hh:mm aa");
                    datetime = dateformat.format(c.getTime());
                    chatSingleModel.setTime(datetime);


                    dataselect.add(chatSingleModel);
                    chatSingleDetailAdapter = new ChatSingleDetailAdapter(getActivity(), dataselect);
                    chatSingleDetailAdapter.notifyDataSetChanged();
                    rv_chat_single_list.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
                    rv_chat_single_list.setAdapter(chatSingleDetailAdapter);
                    rv_chat_single_list.smoothScrollToPosition(chatSingleDetailAdapter.getItemCount());

                } catch (Exception e) {

                }

            } else if (requestCode == VIDEO) {
                try {
                    encodedImageData = getPath(getActivity(), data.getData());

                    chatSingleModel = new ChatSingleModel();
                    chatSingleModel.setImage(R.drawable.sendmsgicon);


                    chatSingleModel.setSend_image(encodedImageData);
                    Calendar c = Calendar.getInstance();
                    final SimpleDateFormat dateformat = new SimpleDateFormat("hh:mm aa");
                    datetime1 = dateformat.format(c.getTime());
                    chatSingleModel.setTime(datetime1);


                    dataselect.add(chatSingleModel);
                    chatSingleDetailAdapter = new ChatSingleDetailAdapter(getActivity(), dataselect);
                    chatSingleDetailAdapter.notifyDataSetChanged();
                    rv_chat_single_list.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
                    rv_chat_single_list.setAdapter(chatSingleDetailAdapter);
                    rv_chat_single_list.smoothScrollToPosition(chatSingleDetailAdapter.getItemCount());
                    datetime = "";

                } catch (Exception e) {

                }

            } else if (requestCode == PDf) {
                try {
                    Uri data1 = data.getData();
                    String pathe = data1.getPath();
                    String path1 = getpath(pathe);
//                String [] s = path1.split("/external_files");
//                String p2 = s[0];

                    if (path1.contains("/external_files")) {
                        String[] seprated = path1.split("/external_files");
                        String p1 = seprated[1];
                        path = p1;
                        Toast.makeText(getActivity(), path, Toast.LENGTH_SHORT).show();
                    } else {
                        String[] seprated = path1.split("/storage/emulated/0");
                        String p1 = seprated[1];
                        path = p1;
                        Toast.makeText(getActivity(), path + "msg", Toast.LENGTH_SHORT).show();
                    }


                    chatSingleModel = new ChatSingleModel();
                    chatSingleModel.setImage(R.drawable.sendmsgicon);

                    chatSingleModel.setSend_image(path);
                    Calendar c = Calendar.getInstance();
                    final SimpleDateFormat dateformat = new SimpleDateFormat("hh:mm aa");
                    String datetime2 = dateformat.format(c.getTime());
                    chatSingleModel.setTime(datetime2);


                    dataselect.add(chatSingleModel);
                    chatSingleDetailAdapter = new ChatSingleDetailAdapter(getActivity(), dataselect);
                    chatSingleDetailAdapter.notifyDataSetChanged();
                    rv_chat_single_list.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
                    rv_chat_single_list.setAdapter(chatSingleDetailAdapter);
                    rv_chat_single_list.smoothScrollToPosition(chatSingleDetailAdapter.getItemCount());
                    datetime = "";
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("Error", e.getMessage().toString());
                }

            }

        }
    }

    @SuppressLint("NewApi")
    public static String getPath(final Context context, final Uri uri) {

        //check here to KITKAT or new version
        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {

            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    class ChatSingleDetailAdapter extends RecyclerView.Adapter<ChatSingleDetailAdapter.ViewHolder> {
        private Context context;
        private ArrayList<ChatSingleModel> data;

        public ChatSingleDetailAdapter(Context context, ArrayList<ChatSingleModel> data) {
            this.context = context;
            this.data = data;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_single_detail, null);
            return new ChatSingleDetailAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            final ChatSingleModel chatSingleModel = data.get(position);

            if (chatSingleModel.getSend_image().contains("jpg")) {
                holder.linear_text.setVisibility(View.GONE);
                holder.card_image.setVisibility(View.VISIBLE);
                holder.cv_recording.setVisibility(View.GONE);
                holder.video_play.setVisibility(View.GONE);
                holder.txt_chat_time1.setText(chatSingleModel.getTime());
                //send image set in imageview for chat list
                com.squareup.picasso.Transformation transformation = new RoundedTransformationBuilder()
                        .borderColor(Color.BLACK)
                        .borderWidthDp(0)
                        .cornerRadiusDp(10)
                        .oval(false)
                        .build();
                Glide
                        .with(context)
                        .load(chatSingleModel.getSend_image())
                        .into(holder.img_chat_send_single);

                imageDecode = "";

            }else if (chatSingleModel.getSend_image().contains("png")){

                holder.linear_text.setVisibility(View.GONE);
                holder.card_image.setVisibility(View.VISIBLE);
                holder.cv_recording.setVisibility(View.GONE);
                holder.video_play.setVisibility(View.GONE);
                holder.txt_chat_time1.setText(chatSingleModel.getTime());
                //send image set in imageview for chat list
                com.squareup.picasso.Transformation transformation = new RoundedTransformationBuilder()
                        .borderColor(Color.BLACK)
                        .borderWidthDp(0)
                        .cornerRadiusDp(10)
                        .oval(false)
                        .build();
                Glide
                        .with(context)
                        .load(chatSingleModel.getSend_image())
                        .into(holder.img_chat_send_single);

                imageDecode = "";

            }
            else if (chatSingleModel.getSend_image().contains("mp4")) {
                holder.linear_text.setVisibility(View.GONE);
                holder.card_image.setVisibility(View.VISIBLE);
                holder.video_play.setVisibility(View.VISIBLE);
                holder.cv_recording.setVisibility(View.GONE);
                holder.txt_chat_time1.setText(chatSingleModel.getTime());

            } else if (chatSingleModel.getSend_image().contains("pdf")) {
                holder.linear_text.setVisibility(View.GONE);
                holder.card_image.setVisibility(View.VISIBLE);
                holder.video_play.setVisibility(View.GONE);
                holder.cv_recording.setVisibility(View.GONE);
                holder.txt_chat_time1.setText(chatSingleModel.getTime());
                holder.img_chat_send_single.setImageResource(R.drawable.pdfimage);

            } else if (chatSingleModel.getSend_image().contains("mp3")) {
                holder.linear_text.setVisibility(View.GONE);
                holder.card_image.setVisibility(View.GONE);
                holder.cv_recording.setVisibility(View.VISIBLE);
                holder.video_play.setVisibility(View.GONE);
//                holder.txt_chat_time.setText(chatSingleModel.getTime());

            } else {
                holder.linear_text.setVisibility(View.VISIBLE);
                holder.card_image.setVisibility(View.GONE);
                holder.cv_recording.setVisibility(View.GONE);
                holder.txt_chat_message.setText(chatSingleModel.getSend_image());
                holder.txt_chat_time.setText(chatSingleModel.getTime());
            }


            //send message and set image mark to sen message
            Picasso.with(context)
                    .load(chatSingleModel.getImage())
                    .error(R.drawable.profile)
                    .into(holder.img_chat_send);

            selecteddata = "";
            holder.video_play.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, VideoPlayActivity.class);
                    intent.putExtra("video", chatSingleModel.getSend_image());
                    context.startActivity(intent);
                }
            });

            holder.img_chat_send_single.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Toast.makeText(getActivity(), "PDF Upload", Toast.LENGTH_LONG).show();
                    if (chatSingleModel.getSend_image().contains("pdf")) {
//                        Toast.makeText(getActivity(), "PDF", Toast.LENGTH_LONG).show();

                        Intent intent = new Intent(context, PDFActivity.class);
                        intent.putExtra("pdf1", chatSingleModel.getSend_image());
                        context.startActivity(intent);
                    }
                }
            });


            holder.img_play_recording.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alert(chatSingleModel.getSend_image());
                }
            });

        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public TextView txt_chat_date;
            public TextView txt_recording;
            public TextView txt_chat_message;
            public TextView txt_chat_time;
            public TextView txt_chat_time1;
            public ImageView img_chat_send;
            public ImageView img_play_recording;
            public ImageView img_chat_send_single;
            public ImageView video_play;
            public LinearLayout linear_text;
            public CardView card_image;
            public CardView cv_recording;


            public ViewHolder(View itemView) {
                super(itemView);

                //txt_chat_date = (TextView) itemView.findViewById(R.id.txt_chat_date);
                txt_chat_message = (TextView) itemView.findViewById(R.id.txt_chat_message);
                txt_recording = (TextView) itemView.findViewById(R.id.txt_recording);
                txt_chat_time = (TextView) itemView.findViewById(R.id.txt_chat_time);
                txt_chat_time1 = (TextView) itemView.findViewById(R.id.txt_chat_time1);
                img_chat_send = (ImageView) itemView.findViewById(R.id.img_chat_send);
                img_chat_send_single = (ImageView) itemView.findViewById(R.id.img_chat_send_single);
                linear_text = (LinearLayout) itemView.findViewById(R.id.textlayout);
                card_image = (CardView) itemView.findViewById(R.id.cardimage);
                cv_recording = (CardView) itemView.findViewById(R.id.cv_recording);
                video_play = (ImageView) itemView.findViewById(R.id.img_play);
                img_play_recording = (ImageView) itemView.findViewById(R.id.img_play_recording);
            }
        }
    }


    private void alert(final String name) {
        Dialog dialog = new Dialog(getActivity());
        dialog.setCancelable(true);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.audio_play);
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.copyFrom(dialog.getWindow().getAttributes());
        dialog.getWindow().setAttributes(lp);

        image_play = (ImageView) dialog.findViewById(R.id.img_dialogplay);
        seek = dialog.findViewById(R.id.seek);
        txtTitle = (TextView) dialog.findViewById(R.id.txt);

        mediaPlayer = new MediaPlayer();

        seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                mHandler.removeCallbacks(mUpdateTimeTask);
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                mHandler.removeCallbacks(mUpdateTimeTask);
                int totalDuration = mediaPlayer.getDuration();
                int currentPosition = utils.progressToTimer(seekBar.getProgress(), totalDuration);

                mediaPlayer.seekTo(currentPosition);
                updateProgressBar();
            }
        });

        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                image_play.setImageResource(android.R.drawable.ic_media_play);
            }
        });

        txtTitle.setText(time);
        playSong(name);

        image_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mediaPlayer.isPlaying()) {
                    if (mediaPlayer != null) {
                        mediaPlayer.pause();
                        image_play.setImageResource(android.R.drawable.ic_media_play);
                        if (mediaPlayer.isPlaying()) {
                            seek.setProgress(mediaPlayer.getCurrentPosition());
                        }
                    }
                } else {
                    // Resume song
                    if (mediaPlayer != null) {
                        mediaPlayer.start();
                        if (mediaPlayer.isPlaying()) {
                            seek.setProgress(mediaPlayer.getCurrentPosition());
                        } else {
                            playSong(name);
                        }
                        image_play.setImageResource(android.R.drawable.ic_media_pause);
                    }
                }
            }
        });

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                mediaPlayer.stop();
            }
        });
    }


    private Runnable mUpdateTimeTask = new Runnable() {
        @SuppressLint("SetTextI18n")
        public void run() {
            long totalDuration = mediaPlayer.getDuration();
            long currentDuration = mediaPlayer.getCurrentPosition();

            // Displaying Total Duration time
//                songTotalDurationLabel.setText(""+utils.milliSecondsToTimer(totalDuration));
            // Displaying time completed playing
            txtTitle.setText("" + utils.milliSecondsToTimer(currentDuration));

            int progress = (int) (utils.getProgressPercentage(currentDuration, totalDuration));
            seek.setProgress(progress);

            // Running this thread after 100 milliseconds
            mHandler.postDelayed(this, 100);
        }
    };

    public void updateProgressBar() {
        mHandler.postDelayed(mUpdateTimeTask, 100);
    }

    public void playSong(String data) {
//            // Play song
        try {
            mediaPlayer.reset();
            mediaPlayer.setDataSource(data);
            mediaPlayer.prepare();
            mediaPlayer.start();

            image_play.setImageResource(android.R.drawable.ic_media_pause);

            seek.setProgress(0);
            seek.setMax(100);

            updateProgressBar();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
