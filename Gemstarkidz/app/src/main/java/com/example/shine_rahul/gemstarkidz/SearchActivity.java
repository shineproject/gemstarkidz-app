package com.example.shine_rahul.gemstarkidz;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.shine_rahul.gemstarkidz.Adapter.SimpleAdapter;

import java.util.ArrayList;
import java.util.List;

public class SearchActivity extends Activity {
    ImageView back_list;
    public EditText search;
    public List<String> list = new ArrayList<String>();
    SimpleAdapter mAdapter;
    String name = "";
    TextView RecentSearch;
    RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_search);

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        RecentSearch = findViewById(R.id.RecentSearch);

        Toolbar toolbar = (Toolbar) findViewById(R.id.main_toolbar);
        search = toolbar.findViewById(R.id.search);
        back_list = toolbar.findViewById(R.id.search_back);
        search.setVisibility(View.VISIBLE);
        back_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setReverseLayout(true);

        Intent intent = getIntent();
        name = intent.getStringExtra("name");


        countryList();  // in this method, Create a list of items.

        mAdapter = new SimpleAdapter(list, SearchActivity.this);
        mRecyclerView.setAdapter(mAdapter);

        RecentSearch.setVisibility(View.GONE);

        addTextListener();

    }

    public void countryList() {
        list.add("Kamlesh");
        list.add("Dhruv");
        list.add("Bhargav");
        list.add("Bilal");
        list.add("Harshal");
        list.add("Lucky");
        list.add("Singing");
        list.add("VIpul");
        list.add("Department");
        list.add("Carrear");
        list.add("Insurance");
    }


    public void addTextListener() {
        search.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence query, int start, int before, int count) {
                query = query.toString().toLowerCase();
                final List<String> filteredList = new ArrayList<>();
                for (int i = 0; i < list.size(); i++) {
                    final String text = list.get(i).toLowerCase();
                    if (text.contains(query)) {
                        filteredList.add(list.get(i));
                    }
                }

                mRecyclerView.setLayoutManager(new LinearLayoutManager(SearchActivity.this));
                mAdapter = new SimpleAdapter(filteredList, SearchActivity.this);
                mRecyclerView.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();  // data set changed
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
