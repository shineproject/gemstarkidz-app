package com.example.shine_rahul.gemstarkidz.Model;

/**
 * Created by Shine-Rahul on 12/3/2018.
 */

public class ChatSingleModel {
    String id;
    String text_message;
    String time;
    String date;
    int image;
    String send_image;
    String path;


    public ChatSingleModel() {
    }

    public ChatSingleModel(String id, String text_message, String time, String date, int image, String send_image, String path) {
        this.id = id;
        this.text_message = text_message;
        this.time = time;
        this.date = date;
        this.image = image;
        this.send_image = send_image;
        this.path = path;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText_message() {
        return text_message;
    }

    public void setText_message(String text_message) {
        this.text_message = text_message;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getSend_image() {
        return send_image;
    }

    public void setSend_image(String send_image) {
        this.send_image = send_image;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
