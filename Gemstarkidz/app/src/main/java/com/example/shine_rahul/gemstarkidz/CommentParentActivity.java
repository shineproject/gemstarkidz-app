package com.example.shine_rahul.gemstarkidz;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.shine_rahul.gemstarkidz.Adapter.CommentListAdapter;
import com.example.shine_rahul.gemstarkidz.Model.CommentListModel;
import com.example.shine_rahul.gemstarkidz.Service.Services;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class CommentParentActivity extends AppCompatActivity {

    RecyclerView rv_display_comment;
    EditText edt_comment11;
    ImageView img_send;
    String comment = "", resultPostComment = "", resultCommentList = "", datetime = "", uid = "", soa_id = "";
    public static String commentP = "";
    ArrayList<CommentListModel> data = new ArrayList<>();
    CommentListAdapter commentListAdapter;
    ActionBar actionBar;

    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_comment);
        actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        Intent intent = getIntent();
        uid = intent.getStringExtra("uid");
        soa_id = intent.getStringExtra("soa_id");

        new AsyncTaskRunnerGetCommentList().execute();

        rv_display_comment = (RecyclerView) findViewById(R.id.rv_display_comment);
        edt_comment11 = (EditText) findViewById(R.id.edt_comment11);
        edt_comment11.requestFocus();
        img_send = (ImageView) findViewById(R.id.img_send);

        img_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt_comment11.setText("");
                edt_comment11.requestFocus();
            }
        });

        edt_comment11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                img_send.setVisibility(View.VISIBLE);
            }
        });

        // send comment
        img_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_comment11.getText().toString().trim().equals("")) {
                    img_send.setVisibility(View.GONE);
                } else {
                    img_send.setVisibility(View.VISIBLE);
                    commentP = edt_comment11.getText().toString();
                    edt_comment11.setText("");
                    edt_comment11.requestFocus();
                    new AsyncTaskRunnerPostComment().execute();
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                overridePendingTransition(0, 0);
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private class AsyncTaskRunnerPostComment extends AsyncTask<String, String, String> {

        //     private ProgressDialog Dialog = new ProgressDialog(CommentActivity.this);

        @Override
        protected String doInBackground(String... params) {

            MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
            builder.addFormDataPart("user_id", uid);
            builder.addFormDataPart("soa_id", soa_id);
            builder.addFormDataPart("comment", commentP);
            builder.addFormDataPart("user_type", "4");
            RequestBody requestBody = builder.build();

            try {
                OkHttpClient client = new OkHttpClient.Builder().build();
                Request request = new Request.Builder()
                        .url(Services.SOCIAL_SCREEN_POST_COMMENT)
                        .post(requestBody)
                        .build();

                Response resp = client.newCall(request).execute();
                resultPostComment = resp.body().string();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            //   Dialog.dismiss();
            GetPostData(resultPostComment);
            new AsyncTaskRunnerGetCommentList().execute();
            super.onPostExecute(s);
        }

        @Override
        protected void onPreExecute() {
//            Dialog.show();
//            Dialog.setMessage("Please Wait....");
//            Dialog.setCancelable(false);
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
        }

    }

    private void GetPostData(String response) {

        if (!response.equals("")) {
            try {

                JSONObject jsobjectcategory = new JSONObject(response);
                String message = jsobjectcategory.getString("message");
                // Updated upstream
                String error = jsobjectcategory.getString("error");
                //   Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

        }
    }

    private class AsyncTaskRunnerGetCommentList extends AsyncTask<String, String, String> {

        private ProgressDialog Dialog = new ProgressDialog(CommentParentActivity.this);

        @Override
        protected String doInBackground(String... params) {

            MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
            builder.addFormDataPart("soa_id", soa_id);
            builder.addFormDataPart("user_id", uid);
            RequestBody requestBody = builder.build();

            try {
                OkHttpClient client = new OkHttpClient.Builder().build();
                Request request = new Request.Builder()
                        .url(Services.SOCIAL_SCREEN_COMMENT_LIST)
                        .post(requestBody)
                        .build();
                Response resp = client.newCall(request).execute();
                resultCommentList = resp.body().string();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            Dialog.dismiss();
            data.clear();
            GetCommentData(resultCommentList);
            super.onPostExecute(s);
        }

        @Override
        protected void onPreExecute() {
            Dialog.show();
            Dialog.setMessage("Please Wait....");
            Dialog.setCancelable(false);
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
        }

    }

    private void GetCommentData(String response) {

        CommentListModel commentListModel;

        if (!response.equals("")) {
            try {
                JSONObject jsobjectcategory = new JSONObject(response);
                String message = jsobjectcategory.getString("message");
                // Updated upstream
                String error = jsobjectcategory.getString("error");

                if (error.equals("200")) {

                    JSONArray jsonArray = jsobjectcategory.getJSONArray("data");
                    JSONObject jsonObject = null;
                    for (int i = 0; i < jsonArray.length(); i++) {
                        jsonObject = jsonArray.getJSONObject(i);

                        commentListModel = new CommentListModel();

                        JSONArray jsonArrayD = jsonObject.getJSONArray("soa_comment");
                        for (int j = 0; j < jsonArrayD.length(); j++) {
                            JSONObject jsonObjectD = jsonArrayD.getJSONObject(j);

                            commentListModel.setUse_Id(jsonObjectD.getString("Use_Id"));
                            commentListModel.setUse_Name(jsonObjectD.getString("Use_Name"));
                            commentListModel.setUse_Type(jsonObjectD.getString("Use_Type"));
                            commentListModel.setUse_Mobile_No(jsonObjectD.getString("Use_Mobile_No"));
                            commentListModel.setUse_Status(jsonObjectD.getString("Use_Status"));
                            commentListModel.setUse_Image(jsonObjectD.getString("Use_Image"));
                        }

                        commentListModel.setSac_Id(jsonObject.getString("Sac_Id"));
                        commentListModel.setSac_Comment(jsonObject.getString("Sac_Comment"));
                        commentListModel.setSac_CreatedAt(jsonObject.getString("Sac_CreatedAt"));

                        data.add(commentListModel);
                    }

                    commentListAdapter = new CommentListAdapter(CommentParentActivity.this, data);
                    rv_display_comment.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
                    rv_display_comment.setAdapter(commentListAdapter);
                    rv_display_comment.scrollToPosition(data.size() - 1);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

        }
    }
}
