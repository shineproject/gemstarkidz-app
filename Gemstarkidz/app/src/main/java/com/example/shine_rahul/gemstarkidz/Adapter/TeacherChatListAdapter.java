package com.example.shine_rahul.gemstarkidz.Adapter;

import android.app.Activity;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.shine_rahul.gemstarkidz.Model.TeacherChatListModel;
import com.example.shine_rahul.gemstarkidz.R;
import com.example.shine_rahul.gemstarkidz.Service.Services;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

public class TeacherChatListAdapter extends RecyclerView.Adapter<TeacherChatListAdapter.ViewHolder> {
    private Activity context;
    private ArrayList<TeacherChatListModel> data;
    private ArrayList<TeacherChatListModel> data1;

    public TeacherChatListAdapter(Activity context, ArrayList<TeacherChatListModel> data) {
        this.context = context;
        this.data = data;
        this.data1 = new ArrayList<>();
        this.data1.addAll(data);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.teacher_chat_list, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        TeacherChatListModel teacherChatListModel = data.get(position);

        holder.txt_teacher_chat_list_name.setText(teacherChatListModel.getUser_name());

        Picasso.with(context)
                .load(Services.IMAGES_PATH + "profile/" + teacherChatListModel.getUser_image())
                .placeholder(R.drawable.logo)
                .into(holder.img_teacher_chat_list_profile);
        Typeface style1 = Typeface.createFromAsset(context.getAssets(), "Raleway-SemiBold.ttf");
        Typeface style2 = Typeface.createFromAsset(context.getAssets(), "Raleway-Regular.ttf");
        holder.txt_teacher_chat_list_name.setTypeface(style1);


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CircleImageView img_teacher_chat_list_profile;
        TextView txt_teacher_chat_list_name;
        TextView txt_teacher_chat_list_time;
        ImageView img_teacher_chat_list_readmsg;
        ImageView img_teacher_chat_list_senmsg;
        ImageView img_teacher_chat_list_sendmsg_deliver;
        TextView txt_teacher_chat_list_message;

        public ViewHolder(View itemView) {
            super(itemView);

            img_teacher_chat_list_profile = (CircleImageView) itemView.findViewById(R.id.img_teacher_chat_list_profile);
            txt_teacher_chat_list_name = (TextView) itemView.findViewById(R.id.txt_teacher_chat_list_name);
            txt_teacher_chat_list_time = (TextView) itemView.findViewById(R.id.txt_teacher_chat_list_time);
            txt_teacher_chat_list_message = (TextView) itemView.findViewById(R.id.txt_teacher_chat_list_message);
            img_teacher_chat_list_readmsg = (ImageView) itemView.findViewById(R.id.img_teacher_chat_list_readmsg);
            img_teacher_chat_list_senmsg = (ImageView) itemView.findViewById(R.id.img_teacher_chat_list_senmsg);
            img_teacher_chat_list_sendmsg_deliver = (ImageView) itemView.findViewById(R.id.img_teacher_chat_list_sendmsg_deliver);
        }
    }


    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        data.clear();
        if (charText.length() == 0) {
            data.addAll(data1);
        } else {
            for (TeacherChatListModel wp : data1) {
                if (wp.getUser_name().toLowerCase(Locale.getDefault()).contains(charText)) {
                    data.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }
}
