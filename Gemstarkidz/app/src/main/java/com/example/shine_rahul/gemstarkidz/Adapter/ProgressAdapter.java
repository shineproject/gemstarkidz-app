package com.example.shine_rahul.gemstarkidz.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Movie;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.shine_rahul.gemstarkidz.Model.ProgressModel;
import com.example.shine_rahul.gemstarkidz.R;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Shine-Rahul on 1/3/2018.
 */

public class ProgressAdapter extends RecyclerView.Adapter<ProgressAdapter.ViewHolder> {
    private Context context;
    private ArrayList<ProgressModel> data;

    public ProgressAdapter(Context context, ArrayList<ProgressModel> data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public ProgressAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.progress_support, null);
        return new ProgressAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ProgressAdapter.ViewHolder holder, int position) {
        ProgressModel progressModel = data.get(position);
        holder.txt_progress_month.setText(progressModel.getMonth());
        Glide.with(context).load(progressModel.getImage()).error(R.drawable.logo).into(holder.img_progress_profile);
//        holder.cv_color.setCardBackgroundColor(progressModel.getColor());
       // holder.cv_color.setBackgroundResource(progressModel.getColor());
        Typeface font = Typeface.createFromAsset(context.getAssets(), "Raleway-Regular.ttf");
        holder.txt_progress_month.setTypeface(font);
        Random rnd = new Random();
        int currentColor = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
        holder.cv_color.setCardBackgroundColor(currentColor);
    }

    private RatingBar.OnRatingBarChangeListener onRatingChangedListener(final ViewHolder holder, final int position) {
        return new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                ProgressModel item = data.get(position);
                item.setRating(String.valueOf(v));
                Log.i("Adapter", "star: " + v);
            }
        };
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout li_color;
        ImageView img_progress_profile;
        TextView txt_progress_month;
        SimpleRatingBar rtb_progress_rating;
        ImageView img_progress_forward;
        CardView cv_color;

        public ViewHolder(View itemView) {
            super(itemView);
            li_color = (LinearLayout) itemView.findViewById(R.id.li_color);
            img_progress_profile = (ImageView) itemView.findViewById(R.id.img_progress_profile);
            txt_progress_month = (TextView) itemView.findViewById(R.id.txt_progress_month);
            rtb_progress_rating = (SimpleRatingBar) itemView.findViewById(R.id.rtb_progress_rating);
            img_progress_forward = (ImageView) itemView.findViewById(R.id.img_progress_forward);
            cv_color = (CardView) itemView.findViewById(R.id.cv_color);
        }
    }
}
