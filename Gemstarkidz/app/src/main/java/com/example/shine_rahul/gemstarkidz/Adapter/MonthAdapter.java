package com.example.shine_rahul.gemstarkidz.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.shine_rahul.gemstarkidz.Model.MonthModel;
import com.example.shine_rahul.gemstarkidz.Model.PaymentModel;
import com.example.shine_rahul.gemstarkidz.R;

import java.util.ArrayList;

/**
 * Created by Shine-Rahul on 26/2/2018.
 */

public class MonthAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<MonthModel> data;
    private LayoutInflater layoutInflater = null;

    public MonthAdapter(Context context, ArrayList<MonthModel> data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (convertView == null) {
            layoutInflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.payment, null);
        }
        TextView txt_payment;
        txt_payment = (TextView) view.findViewById(R.id.txt_payment);
        txt_payment.setText("" + data.get(position).getMonth());
        return view;
    }
}

