package com.example.shine_rahul.gemstarkidz.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.shine_rahul.gemstarkidz.Model.ChatSingleModel;
import com.example.shine_rahul.gemstarkidz.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Shine-Rahul on 12/3/2018.
 */

public class ChatArrayAdapter extends ArrayAdapter<ChatSingleModel> {
    TextView txt_chat_message;
    private List<ChatSingleModel> MessageList = new ArrayList<ChatSingleModel>();
    private LinearLayout layout;

    public ChatArrayAdapter(@NonNull Context context, int resource) {
        super(context, resource);
    }

    public void add(ChatSingleModel object) {
        MessageList.add(object);
        super.add(object);
    }

    public int getCount() {
        return this.MessageList.size();
    }

    public ChatSingleModel getItem(int index) {

        return this.MessageList.get(index);
    }

    public View getView(int position, View ConvertView, ViewGroup parent) {
        View view = ConvertView;

        if (view == null) {

            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.chat_single_detail, parent, false);

        }
        //layout = (LinearLayout)view.findViewById(R.id.Message1);
        ChatSingleModel messageobj = getItem(position);
        txt_chat_message = (TextView) view.findViewById(R.id.txt_chat_message);
        txt_chat_message.setText(messageobj.getText_message());
        return view;
    }

    public Bitmap decodeToBitmap(byte[] decodedByte) {
        return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
    }
}