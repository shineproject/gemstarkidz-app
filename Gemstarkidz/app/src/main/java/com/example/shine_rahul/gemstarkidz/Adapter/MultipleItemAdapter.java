package com.example.shine_rahul.gemstarkidz.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.shine_rahul.gemstarkidz.Fragment.TeacherSocialFragment;
import com.example.shine_rahul.gemstarkidz.Model.MultipleItemModel;
import com.example.shine_rahul.gemstarkidz.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Shine-Rahul on 21/3/2018.
 */

public class MultipleItemAdapter extends RecyclerView.Adapter<MultipleItemAdapter.ViewHolder> {
    private Context context;
    private ArrayList<MultipleItemModel> data;

    public MultipleItemAdapter(Context context, ArrayList<MultipleItemModel> data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.teacher_social_image, parent, false);
        return new MultipleItemAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        MultipleItemModel multipleItemModel = data.get(position);
        Picasso.with(context)
                .load(multipleItemModel.getImages())
                .placeholder(R.drawable.profile)
                .into(holder.img_teacher_social);

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView img_teacher_social;

        public ViewHolder(View itemView) {
            super(itemView);
            img_teacher_social = (CircleImageView) itemView.findViewById(R.id.img_teacher_social);
        }
    }
}
