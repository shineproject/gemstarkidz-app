package com.example.shine_rahul.gemstarkidz.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.TextView;

import com.example.shine_rahul.gemstarkidz.Model.SearchMenuModel;
import com.example.shine_rahul.gemstarkidz.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Shine-Rahul on 24/3/2018.
 */

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.ViewHolder> {
    private Context context;
    private ArrayList<SearchMenuModel> data;
    private ArrayList<SearchMenuModel> filter;

    public SearchAdapter(Context context, ArrayList<SearchMenuModel> data1) {
        this.context = context;
        this.data = data1;
        filter = new ArrayList<>();
        filter = data1;
    }

    @Override
    public SearchAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.searchmenu, null);
        return new SearchAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SearchAdapter.ViewHolder holder, int position) {

        SearchMenuModel searchMenuModel = data.get(position);
        holder.txt_search.setText(searchMenuModel.getName());

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txt_search;

        public ViewHolder(View itemView) {
            super(itemView);
            txt_search = (TextView) itemView.findViewById(R.id.txt_search);
        }
    }


//    public Filter getFilter() {
//
//        return new Filter() {
//            @Override
//            protected FilterResults performFiltering(CharSequence charSequence) {
//
//                String charString = charSequence.toString();
//
//                if (charString.isEmpty()) {
//
//                    filter = data;
//                } else {
//
//                    ArrayList<SearchMenuModel> filteredList = new ArrayList<>();
//
//                    for (SearchMenuModel androidVersion : data) {
//
//                        if (androidVersion.getName().toLowerCase().contains(charString)) {
//
//                            filteredList.add(androidVersion);
//                        }
//                    }
//
//                    filter = filteredList;
//                }
//
//                FilterResults filterResults = new FilterResults();
//                filterResults.values = filter;
//                return filterResults;
//            }
//
//            @Override
//            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
//                filter = (ArrayList<SearchMenuModel>) filterResults.values;
//                notifyDataSetChanged();
//            }
//        };
//    }
}
