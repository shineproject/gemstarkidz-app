package com.example.shine_rahul.gemstarkidz.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.shine_rahul.gemstarkidz.Model.EventDetailListModel;
import com.example.shine_rahul.gemstarkidz.R;
import com.example.shine_rahul.gemstarkidz.Service.Services;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Shine-Rahul on 5/3/2018.
 */

public class EventDetailListAdapter extends RecyclerView.Adapter<EventDetailListAdapter.ViewHolder> {
    private Context context;
    private ArrayList<EventDetailListModel> data;

    public EventDetailListAdapter(Context context, ArrayList<EventDetailListModel> data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_detail_list, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        EventDetailListModel eventDetailListModel = data.get(position);
        Picasso.with(context)
                .load(Services.IMAGES_PATH + "profile/" + eventDetailListModel.getImages_list())
                .placeholder(R.drawable.logo)
                .into(holder.img_event_detail_list_profile);

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView img_event_detail_list_profile;

        public ViewHolder(View itemView) {
            super(itemView);
            img_event_detail_list_profile = (CircleImageView) itemView.findViewById(R.id.img_event_detail_list_profile);
        }
    }
}
