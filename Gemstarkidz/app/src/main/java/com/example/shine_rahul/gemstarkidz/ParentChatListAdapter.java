package com.example.shine_rahul.gemstarkidz;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.shine_rahul.gemstarkidz.Adapter.EventAdapter;
import com.example.shine_rahul.gemstarkidz.Model.ParentChatListModel;
import com.example.shine_rahul.gemstarkidz.Service.Services;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class ParentChatListAdapter extends RecyclerView.Adapter<ParentChatListAdapter.ViewHolder> {
    private Activity context;
    private ArrayList<ParentChatListModel> data;

    public ParentChatListAdapter(Activity context, ArrayList<ParentChatListModel> data) {
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.parent_chat_list_item, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        ParentChatListModel parentChatListModel = data.get(position);
        holder.txt_parent_list_name.setText(parentChatListModel.getName());
        Picasso.with(context)
                .load(Services.IMAGES_PATH + "profile/" + parentChatListModel.getImage())
                .placeholder(R.drawable.logo)
                .into(holder.img_student_list);

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txt_parent_list_name, txt_parent_list_status;
        CircleImageView img_student_list;

        public ViewHolder(View itemView) {
            super(itemView);

            txt_parent_list_name = (TextView) itemView.findViewById(R.id.txt_parent_list_name);
            txt_parent_list_status = (TextView) itemView.findViewById(R.id.txt_parent_list_status);
            img_student_list = (CircleImageView) itemView.findViewById(R.id.img_student_list);
        }
    }
}
