package com.example.shine_rahul.gemstarkidz.Connection;

/**
 * Created by ${Vrund} on 12/30/2017.
 */

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.TextView;

import com.example.shine_rahul.gemstarkidz.R;

/**
 * Created by craftbox on 6/19/2017.
 */

public class Connection {

    public static String PreferenceName = "session";
    public static String EncraptionKey = "Craftbox@2442";

    public static boolean isConnectingToInternet(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connectivity != null) {
            NetworkInfo info = connectivity.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if (info != null) {
                if (info.isConnected()) {
                    return true;
                } else {
                    NetworkInfo info1 = connectivity.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
                    if (info1.isConnected()) {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        }
        return false;
    }

    public static boolean checkMobileInternetConn(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connectivity != null) {
            NetworkInfo info = connectivity.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if (info != null) {
                if (info.isConnected()) {
                    return true;
                } else {
                    NetworkInfo info1 = connectivity.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
                    if (info1.isConnected()) {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        }
        return false;
    }

    @SuppressLint("ResourceType")
    public static void showDialog(final Context context) {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        TextView textView = new TextView(context);
        textView.setBackgroundColor(context.getResources().getColor(R.color.calendar_bg));
        textView.setPadding(10, 15, 5, 5);
        textView.setText("Internet Connection");
        textView.setHeight(100);
//        textView.setText(Color.WHITE);
        // Set Dialog Title
        textView.setTextColor(Color.WHITE);
        alertDialog.setCustomTitle(textView);
        // Set Dialog Message
        alertDialog.setMessage("Please check your internet connection ..");
        alertDialog.setCancelable(false);
        // Set OK Button
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
               /* Intent intent=new Intent(context, ParentActivity.class);
                context.startActivity(intent);*/
            }
        });
        alertDialog.show();
    }


}
