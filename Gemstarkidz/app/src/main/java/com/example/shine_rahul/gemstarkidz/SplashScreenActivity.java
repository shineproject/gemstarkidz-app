package com.example.shine_rahul.gemstarkidz;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Handler;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.zl.reik.dilatingdotsprogressbar.DilatingDotsProgressBar;

public class SplashScreenActivity extends Activity {
    DilatingDotsProgressBar activity_splashscreen;
    TextView txt_content1;
    TextView txt_content2;
    TextView txt_content3;
    View view_splash_screen;
    RelativeLayout relative;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_splash_screen);
       txt_content1 = (TextView)findViewById(R.id.txt_content1);
        txt_content2 = (TextView)findViewById(R.id.txt_content2);
        txt_content3 = (TextView)findViewById(R.id.txt_content3);
       // relative = (RelativeLayout)findViewById(R.id.relative);
        view_splash_screen = (View)findViewById(R.id.view_splash_screen);
        activity_splashscreen = (DilatingDotsProgressBar)findViewById(R.id.progress);
        activity_splashscreen.showNow();//show loading dots

        //custome font set
        Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Raleway-Bold.ttf");

        txt_content1.setTypeface(font);
        txt_content2.setTypeface(font);
        txt_content3.setTypeface(font);


        //move to welcome Screen
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashScreenActivity.this, WelcomeActivity.class);
                intent.putExtra("data", "normal");
                startActivity(intent);
                finish();
            }
        }, 3000);
    }
}
