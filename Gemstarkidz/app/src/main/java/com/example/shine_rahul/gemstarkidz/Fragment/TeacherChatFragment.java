package com.example.shine_rahul.gemstarkidz.Fragment;


import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shine_rahul.gemstarkidz.Adapter.ParentChatlistAdapter;
import com.example.shine_rahul.gemstarkidz.Adapter.TeacherChatListAdapter;
import com.example.shine_rahul.gemstarkidz.Helper.RecyclerItemClickListener;
import com.example.shine_rahul.gemstarkidz.Model.ParentChatModel;
import com.example.shine_rahul.gemstarkidz.Model.TeacherChatListModel;
import com.example.shine_rahul.gemstarkidz.ParentCreateChatGroupActivity;
import com.example.shine_rahul.gemstarkidz.R;
import com.example.shine_rahul.gemstarkidz.SeparateChatActivity;
import com.example.shine_rahul.gemstarkidz.Service.Services;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class TeacherChatFragment extends android.app.Fragment {
    RecyclerView rv_teachet_chat_list;
    ArrayList<TeacherChatListModel> data = new ArrayList<>();
    TeacherChatListAdapter chatlistAdapter;
    TeacherChatListModel chatModel;

    TextView txt_edit_toolbar;
    ImageView img_search, img_close,img_chat_newgroup;
    EditText edt_search;
    TextView txt_menu_title;
    String error, message;
    String user_id, user_name, user_type, user_mobile_number, user_status, user_email, user_image, user_class_id;


    public TeacherChatFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_teacher_chat, container, false);

        txt_edit_toolbar = (TextView) getActivity().findViewById(R.id.txt_edit_toolbar);
        img_search = (ImageView) getActivity().findViewById(R.id.img_search);
        img_close = (ImageView) getActivity().findViewById(R.id.img_close);
        edt_search = (EditText) getActivity().findViewById(R.id.edt_search);
        txt_menu_title = (TextView) getActivity().findViewById(R.id.txt_menu_title);


        rv_teachet_chat_list = (RecyclerView) view.findViewById(R.id.rv_teachet_chat_list);
        img_close = (ImageView) getActivity().findViewById(R.id.img_close);
        img_chat_newgroup = (ImageView) view.findViewById(R.id.img_chat_newgroup);
        txt_menu_title = (TextView) getActivity().findViewById(R.id.txt_menu_title);
        txt_edit_toolbar = (TextView) getActivity().findViewById(R.id.txt_edit_toolbar);
        edt_search = (EditText) getActivity().findViewById(R.id.edt_search);
        txt_menu_title.setVisibility(View.VISIBLE);
        txt_menu_title.setText("Chat");
        txt_edit_toolbar.setVisibility(View.GONE);
        img_search.setVisibility(View.VISIBLE);
        edt_search.setVisibility(View.GONE);
        img_close.setVisibility(View.GONE);

        new shoChatList().execute();
        img_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                img_search.setVisibility(View.GONE);
                txt_menu_title.setVisibility(View.GONE);
                edt_search.setVisibility(View.VISIBLE);
                img_close.setVisibility(View.VISIBLE);
                edt_search.setText("");
                edt_search.requestFocus();
                edt_search.setFocusable(true);
            }
        });

        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                img_close.setVisibility(View.GONE);
                edt_search.setVisibility(View.GONE);
                img_search.setVisibility(View.VISIBLE);
                txt_menu_title.setVisibility(View.VISIBLE);
                txt_menu_title.setText("Chat");
            }
        });


        rv_teachet_chat_list.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Intent intent = new Intent(getActivity(), SeparateChatActivity.class);
                        intent.putExtra("NAME", data.get(position).getUser_name());
                        intent.putExtra("IMAGE",data.get(position).getUser_image());
                        startActivity(intent);
                    }
                })
        );
        view.setFocusableInTouchMode(true);
        view.requestFocus();


        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                Log.i("HELLO", "keyCode: " + keyCode);
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                    android.app.Fragment someFragment = new TeacherSocialFragment();
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.content, someFragment); // give your fragment container id in first parameter
                    transaction.addToBackStack(null);  // if written, this transaction will be added to backstack
                    transaction.commit();
                    txt_menu_title.setText("Social ");
                    Typeface style = Typeface.createFromAsset(getActivity().getAssets(), "Raleway-SemiBold.ttf");
                    txt_menu_title.setTypeface(style);
                    return true;
                }
                return false;
            }

        });

        edt_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                chatlistAdapter.filter(edt_search.getText().toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        img_chat_newgroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(), ParentCreateChatGroupActivity.class);
                startActivity(intent);
            }
        });
        return view;
    }


    public class shoChatList extends AsyncTask<String, Process, String> {

        String line, response;
        private ProgressDialog Dialog = new ProgressDialog(getActivity());

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Dialog.show();
            Dialog.setMessage("Please Wait....");
            Dialog.setCancelable(false);
        }

        @Override
        protected String doInBackground(String... params) {

            String url = Services.CHAT_LIST;

            try {
                URL url1 = new URL(url);
                URLConnection urlc = url1.openConnection();
                BufferedReader bfr = new BufferedReader(new InputStreamReader(urlc.getInputStream()));

                while ((line = bfr.readLine()) != null) {
                    JSONObject jsonObject1 = new JSONObject(line);
                    error = jsonObject1.getString("error");
                    message = jsonObject1.getString("message");
                    if (error.equals("200")) {
                        JSONArray jsonArray = jsonObject1.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            chatModel = new TeacherChatListModel();
                            user_id = jsonObject.getString("Use_Id");
                            user_name = jsonObject.getString("Use_Name");
                            user_type = jsonObject.getString("Use_Type");
                            user_mobile_number = jsonObject.getString("Use_Mobile_No");
                            user_status = jsonObject.getString("Use_Status");
                            user_email = jsonObject.getString("Use_Email");
                            user_image = jsonObject.getString("Use_Image");
                            user_class_id = jsonObject.getString("Use_Cla_Id");


                            chatModel.setId(user_id);
                            chatModel.setUser_name(user_name);
                            chatModel.setUser_type(user_type);
                            chatModel.setUser_mobile_number(user_mobile_number);
                            chatModel.setUser_status(user_status);
                            chatModel.setUser_email(user_email);
                            chatModel.setUser_image(user_image);
                            chatModel.setUser_class_id(user_class_id);
                            data.add(chatModel);


                        }

                    }

                    message = bfr.toString();
                    Log.e("response", message);
                }

            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            Dialog.dismiss();

            if (error != null) {

                if (error.equals("200")) {

                    chatlistAdapter = new TeacherChatListAdapter(getActivity(), data);
                    chatlistAdapter.notifyDataSetChanged();
                    rv_teachet_chat_list.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayout.VERTICAL, false));
                    rv_teachet_chat_list.setAdapter(chatlistAdapter);


                } else {
                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                }
            }
        }
    }

}
