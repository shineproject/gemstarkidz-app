package com.example.shine_rahul.gemstarkidz;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.shine_rahul.gemstarkidz.JSONParser.JSONParser;
import com.example.shine_rahul.gemstarkidz.Model.ParentChatListModel;
import com.example.shine_rahul.gemstarkidz.Service.Services;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class ContactListActivity extends AppCompatActivity {
    RecyclerView rv_parent_chat_list;
    ArrayList<ParentChatListModel> data=new ArrayList<>();
    ParentChatListAdapter parentChatListAdapter;
    ParentChatListModel parentChatListModel;
    ProgressDialog progressDialog;
    String user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_list);
        rv_parent_chat_list = (RecyclerView) findViewById(R.id.rv_parent_chat_list);
        SharedPreferences splogin = ContactListActivity.this.getSharedPreferences("login", 0);
        user = splogin.getString("user_id", "");
        new showParentChatList().execute();

    }


    public class showParentChatList extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(ContactListActivity.this);
            progressDialog.setMessage("Loading");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            HashMap<String, String> mapParams = new HashMap<>();
            mapParams.put("parent_id", user);
            String response = new JSONParser().sendPostRequest(Services.PARENT_CHAT_LIST, mapParams);
            return response;
        }

        protected void onPostExecute(String response) {
            progressDialog.dismiss();
            if (response != null && !response.equals("")) {
                try {
                    JSONObject mjsonObject = new JSONObject(response);
                    String error = mjsonObject.getString("error");
                    String message = mjsonObject.getString("message");
                    if (error.equals("200")) {
                        JSONArray jsonArray = mjsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            parentChatListModel = new ParentChatListModel();
                            parentChatListModel.setName(jsonObject.getString("Use_Name"));
                            parentChatListModel.setImage(jsonObject.getString("Use_Image"));
                            data.add(parentChatListModel);
                        }
                        parentChatListAdapter = new ParentChatListAdapter(ContactListActivity.this, data);
                        parentChatListAdapter.notifyDataSetChanged();
                        rv_parent_chat_list.setAdapter(parentChatListAdapter);
                        rv_parent_chat_list.setLayoutManager(new LinearLayoutManager(ContactListActivity.this, LinearLayout.VERTICAL, false));
                    } else {
                        Toast.makeText(ContactListActivity.this, message, Toast.LENGTH_LONG).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }


}
