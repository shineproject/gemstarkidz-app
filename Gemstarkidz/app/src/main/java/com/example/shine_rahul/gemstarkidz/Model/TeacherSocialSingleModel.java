package com.example.shine_rahul.gemstarkidz.Model;

public class TeacherSocialSingleModel {

    String id;
    int image;
    String title;
    String time;
    String descriptiopn;
    String img_like;
    String img_comment;
    String img_share;
    String img_tag;
    public int count;

    public TeacherSocialSingleModel() {
    }

    public TeacherSocialSingleModel(String id, int image, String title, String time, String descriptiopn, String img_like, String img_comment, String img_share, String img_tag, int count) {
        this.id = id;
        this.image = image;
        this.title = title;
        this.time = time;
        this.descriptiopn = descriptiopn;
        this.img_like = img_like;
        this.img_comment = img_comment;
        this.img_share = img_share;
        this.img_tag = img_tag;
        this.count = count;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDescriptiopn() {
        return descriptiopn;
    }

    public void setDescriptiopn(String descriptiopn) {
        this.descriptiopn = descriptiopn;
    }

    public String getImg_like() {
        return img_like;
    }

    public void setImg_like(String img_like) {
        this.img_like = img_like;
    }

    public String getImg_comment() {
        return img_comment;
    }

    public void setImg_comment(String img_comment) {
        this.img_comment = img_comment;
    }

    public String getImg_share() {
        return img_share;
    }

    public void setImg_share(String img_share) {
        this.img_share = img_share;
    }

    public String getImg_tag() {
        return img_tag;
    }

    public void setImg_tag(String img_tag) {
        this.img_tag = img_tag;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
