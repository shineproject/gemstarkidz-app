package com.example.shine_rahul.gemstarkidz.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.shine_rahul.gemstarkidz.Model.ParentChatModel;
import com.example.shine_rahul.gemstarkidz.R;
import com.example.shine_rahul.gemstarkidz.Service.Services;
import com.example.shine_rahul.gemstarkidz.SingleImageActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Shine-Rahul on 28/2/2018.
 */

public class ParentChatlistAdapter extends RecyclerView.Adapter<ParentChatlistAdapter.ViewHolder> {
    private Context context;
    private ArrayList<ParentChatModel> data;
    private ArrayList<ParentChatModel> data1;

    public ParentChatlistAdapter(Context context, ArrayList<ParentChatModel> data) {
        this.context = context;
        this.data = data;
        this.data1 = new ArrayList<>();
        this.data1.addAll(data);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_list, null);
        return new ParentChatlistAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final ParentChatModel chatModel = data.get(position);
        holder.txt_chat_list_name.setText(chatModel.getUser_name());

        Picasso.with(context)
                .load(Services.IMAGES_PATH + "profile/" + chatModel.getUser_image())
                .placeholder(R.drawable.logo)
                .into(holder.img_chat_list_profile);
        Typeface style1 = Typeface.createFromAsset(context.getAssets(), "Raleway-SemiBold.ttf");
        Typeface style2 = Typeface.createFromAsset(context.getAssets(), "Raleway-Regular.ttf");
        holder.txt_chat_list_name.setTypeface(style1);
        holder.txt_chat_list_message.setTypeface(style2);

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView img_chat_list_profile;
        TextView txt_chat_list_name;
        TextView txt_chat_list_time;
        ImageView img_chat_list_readmsg;
        ImageView img_chat_list_senmsg;
        ImageView img_chat_list_senmsg_deliver;
        TextView txt_chat_list_message;

        public ViewHolder(View itemView) {
            super(itemView);
            img_chat_list_profile = (CircleImageView) itemView.findViewById(R.id.img_chat_list_profile);
            txt_chat_list_name = (TextView) itemView.findViewById(R.id.txt_chat_list_name);
            txt_chat_list_time = (TextView) itemView.findViewById(R.id.txt_chat_list_time);
            txt_chat_list_message = (TextView) itemView.findViewById(R.id.txt_chat_list_message);
            img_chat_list_readmsg = (ImageView) itemView.findViewById(R.id.img_chat_list_readmsg);
            img_chat_list_senmsg = (ImageView) itemView.findViewById(R.id.img_chat_list_senmsg);
            img_chat_list_senmsg_deliver = (ImageView) itemView.findViewById(R.id.img_chat_list_sendmsg_deliver);
        }
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        data.clear();
        if (charText.length() == 0) {
            data.addAll(data1);
        } else {
            for (ParentChatModel wp : data1) {
                if (wp.getUser_name().toLowerCase(Locale.getDefault()).contains(charText)) {
                    data.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }

}
