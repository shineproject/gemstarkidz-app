package com.example.shine_rahul.gemstarkidz.Adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.shine_rahul.gemstarkidz.Model.SocialSingleModel;
import com.example.shine_rahul.gemstarkidz.R;

import java.util.ArrayList;

/**
 * Created by Shine-Rahul on 28/2/2018.
 */

public class SocialSingleAdapter extends RecyclerView.Adapter<SocialSingleAdapter.ViewHolder> {
    private Context context;
    private ArrayList<SocialSingleModel> data;

    public SocialSingleAdapter(Context context, ArrayList<SocialSingleModel> data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public SocialSingleAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.social_single, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SocialSingleAdapter.ViewHolder holder, int position) {
        SocialSingleModel socialSingleModel = data.get(position);
        holder.txt_social_single_name.setText(socialSingleModel.getName());
        holder.txt_social_single_time.setText(socialSingleModel.getTime());
        holder.txt_social_single_description.setText(socialSingleModel.getDescription());
        holder.txt_social_single_url.setText(socialSingleModel.getUrl());
        Glide.with(context).load(socialSingleModel.getImage()).error(R.drawable.logo).into(holder.img_social_single);
        Typeface style1 = Typeface.createFromAsset(context.getAssets(), "Raleway-SemiBold.ttf");
        Typeface style2 = Typeface.createFromAsset(context.getAssets(), "Raleway-Regular.ttf");
        holder.txt_social_single_name.setTypeface(style1);
        holder.txt_social_single_description.setTypeface(style2);
        holder.txt_social_single_url.setTypeface(style2);
        holder.txt_social_single_replay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater layoutInflater = LayoutInflater.from(context);
                View view1 = layoutInflater.inflate(R.layout.replay_design, null);
                final AlertDialog.Builder alertdialog = new AlertDialog.Builder(context);
                alertdialog.setView(view1);
                final AlertDialog dialog = alertdialog.create();
                dialog.setCanceledOnTouchOutside(false);
                ImageView btn_Cancel = (ImageView) view1.findViewById(R.id.img_reply);
                final EditText edt_save_detail = (EditText) view1.findViewById(R.id.edt_reply);
                dialog.setCanceledOnTouchOutside(true);
                dialog.show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img_social_single;
        TextView txt_social_single_name;
        TextView txt_social_single_time;
        TextView txt_social_single_description;
        TextView txt_social_single_url;
        TextView txt_social_single_replay;
        LinearLayout li_comment;
        EditText edt_comment;
        ImageView btn_send;

        public ViewHolder(View itemView) {
            super(itemView);
            img_social_single = (ImageView) itemView.findViewById(R.id.img_social_single);
            txt_social_single_name = (TextView) itemView.findViewById(R.id.txt_social_single_name);
            txt_social_single_time = (TextView) itemView.findViewById(R.id.txt_social_single_time);
            txt_social_single_description = (TextView) itemView.findViewById(R.id.txt_social_single_description);
            txt_social_single_url = (TextView) itemView.findViewById(R.id.txt_social_single_url);
            txt_social_single_replay = (TextView) itemView.findViewById(R.id.txt_social_single_replay);
            li_comment = (LinearLayout) itemView.findViewById(R.id.li_comment);
            edt_comment = (EditText) itemView.findViewById(R.id.edt_comment);
            btn_send = (ImageView) itemView.findViewById(R.id.btn_send);
        }
    }
}
