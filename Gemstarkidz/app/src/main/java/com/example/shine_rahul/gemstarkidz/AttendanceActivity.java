package com.example.shine_rahul.gemstarkidz;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.GridView;
import android.widget.TextView;

import com.example.shine_rahul.gemstarkidz.Adapter.TeacherClassStudentListAdapter;
import com.example.shine_rahul.gemstarkidz.Model.TeacherClassModel;

import java.util.ArrayList;


public class AttendanceActivity extends AppCompatActivity {
    GridView gv_teacher_class;
    TeacherClassModel teacherClassModel;
    TeacherClassStudentListAdapter teacherClassStudentListAdapter;
    ArrayList<TeacherClassModel> teacherClassModelArrayList;
    int[] images = {R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1};
    String[] names = {"Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance);
        gv_teacher_class = (GridView) findViewById(R.id.gv_class_attendance1);

        teacherClassModelArrayList = new ArrayList<>();
        for (int i = 0; i < images.length; i++) {
            teacherClassModel = new TeacherClassModel();
            teacherClassModel.setStudent_name(names[i]);
//            teacherClassModel.setStudent_image(images[i]);
            teacherClassModelArrayList.add(teacherClassModel);
        }
        teacherClassStudentListAdapter = new TeacherClassStudentListAdapter(AttendanceActivity.this, teacherClassModelArrayList);
        teacherClassStudentListAdapter.notifyDataSetChanged();
        gv_teacher_class.setAdapter(teacherClassStudentListAdapter);
    }
}
