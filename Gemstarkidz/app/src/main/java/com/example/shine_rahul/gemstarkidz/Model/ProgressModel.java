package com.example.shine_rahul.gemstarkidz.Model;

/**
 * Created by Shine-Rahul on 1/3/2018.
 */

public class ProgressModel {
    String id;
    int image;
    int color;
    String month;
    String rating;

    public ProgressModel() {
    }

    public ProgressModel(String id, int image, int color, String month, String rating) {
        this.id = id;
        this.image = image;
        this.color = color;
        this.month = month;
        this.rating = rating;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }
}
