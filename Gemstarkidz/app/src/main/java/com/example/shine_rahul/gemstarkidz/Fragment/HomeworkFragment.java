package com.example.shine_rahul.gemstarkidz.Fragment;


import android.app.DatePickerDialog;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shine_rahul.gemstarkidz.Adapter.Student_Class_Adapter;
import com.example.shine_rahul.gemstarkidz.Adapter.Student_Section_Adapter;
import com.example.shine_rahul.gemstarkidz.Adapter.Student_Subject_Adapter;
import com.example.shine_rahul.gemstarkidz.Model.Student_Class_Model;
import com.example.shine_rahul.gemstarkidz.Model.Student_Section_Model;
import com.example.shine_rahul.gemstarkidz.Model.Student_Subject_Model;
import com.example.shine_rahul.gemstarkidz.R;
import com.example.shine_rahul.gemstarkidz.Service.Services;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeworkFragment extends android.app.Fragment {

    Spinner sp_homework_class;
    Spinner sp_homework_section;
    Spinner sp_homework_subject;
    EditText edt_homework_chapter;
    EditText edt_homework_pageno;
    EditText edt_homework_description;
    TextView txt_menu_title;
    EditText txt_homework_date;
    ImageView img_homework_save;

    TextView txt_class;
    TextView txt_section;
    TextView txt_subject;
    TextView txt_date_home;
    TextView tx_chapter;
    TextView txt_page;
    TextView txt_description;
    private int mYear, mMonth, mDay;

    String select_class = "";
    String select_section = "";
    String select_subject = "";
    String select_date;
    String chapter_number;
    String page_number;
    String description;
    TextView txt_edit_toolbar;

    String responseData = "", responseCreateHomework = "";
    String user = "", type = "", phone = "", uid = "", email = "", user_image = "", user_class, user_section, Cla_Id = "";

    ArrayList<Student_Class_Model> classData;
    ArrayList<Student_Section_Model> sectionData;
    ArrayList<Student_Subject_Model> subjectData;

    Student_Class_Adapter student_class__adapter;
    Student_Section_Adapter student_section_adapter;
    Student_Subject_Adapter student_subject_adapter;

    String[] Subject = {"Select", "Gujarati", "English", "Maths", "Drawing", "Social", "Science"};

    Calendar calendar;
    SimpleDateFormat mdformat;
    String strDate;

    public HomeworkFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_homework, container, false);

        SharedPreferences splogin = getActivity().getSharedPreferences("login", 0);
        user = splogin.getString("username", "");
        type = splogin.getString("type", "");
        phone = splogin.getString("user_phone", "");
        uid = splogin.getString("user_id", "");
        email = splogin.getString("user_email", "");
        user_image = splogin.getString("user_image", "");
        user_class = splogin.getString("user_class", "");
        user_section = splogin.getString("user_section", "");
        Cla_Id = splogin.getString("Cla_Id", "");

        txt_class = (TextView) view.findViewById(R.id.txt_class);
        txt_section = (TextView) view.findViewById(R.id.txt_section);
        txt_subject = (TextView) view.findViewById(R.id.txt_subject);
        txt_date_home = (TextView) view.findViewById(R.id.txt_date_home);
        tx_chapter = (TextView) view.findViewById(R.id.tx_chapter);
        txt_page = (TextView) view.findViewById(R.id.txt_page);
        txt_description = (TextView) view.findViewById(R.id.txt_description);
        txt_menu_title = (TextView) getActivity().findViewById(R.id.txt_menu_title);
        img_homework_save = (ImageView) view.findViewById(R.id.img_homework_save);
        sp_homework_class = (Spinner) view.findViewById(R.id.sp_homework_class);
        sp_homework_section = (Spinner) view.findViewById(R.id.sp_homework_section);
        txt_homework_date = (EditText) view.findViewById(R.id.txt_homework_date);
        edt_homework_chapter = (EditText) view.findViewById(R.id.edt_homework_chapter);
        edt_homework_pageno = (EditText) view.findViewById(R.id.edt_homework_pageno);
        edt_homework_description = (EditText) view.findViewById(R.id.edt_homework_description);
        sp_homework_subject = (Spinner) view.findViewById(R.id.sp_homework_subject);
        txt_edit_toolbar = (TextView) getActivity().findViewById(R.id.txt_edit_toolbar);
        txt_edit_toolbar.setVisibility(View.GONE);

        new AsyncTaskRunnerGetClassSection().execute();

        calendar = Calendar.getInstance();
        mdformat = new SimpleDateFormat("yyyy-MM-dd");
        strDate = mdformat.format(calendar.getTime());

        SimpleDateFormat update_start_date = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        String set_start_date = "";
        try {
            date = update_start_date.parse(strDate);
            update_start_date.applyPattern("dd/MM/yyyy");
            if (date != null) {
                set_start_date = update_start_date.format(date);
            }
            txt_homework_date.setText(set_start_date);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        //Font Style Set For title each componenet
        Typeface style1 = Typeface.createFromAsset(getActivity().getAssets(), "Raleway-SemiBold.ttf");
        txt_menu_title.setTypeface(style1);
        txt_class.setTypeface(style1);
        txt_section.setTypeface(style1);
        txt_subject.setTypeface(style1);
        txt_date_home.setTypeface(style1);
        tx_chapter.setTypeface(style1);
        txt_page.setTypeface(style1);
        txt_description.setTypeface(style1);

        subjectData = new ArrayList<>();
        Student_Subject_Model student_subject_model;
        for (int i = 0; i < Subject.length; i++) {
            student_subject_model = new Student_Subject_Model();
            student_subject_model.setCla_Subject(Subject[i]);
            subjectData.add(student_subject_model);
        }

        student_subject_adapter = new Student_Subject_Adapter(getActivity(), subjectData);
        sp_homework_subject.setAdapter(student_subject_adapter);

//select and fill all component

        chapter_number = edt_homework_chapter.getText().toString();
        page_number = edt_homework_pageno.getText().toString();
        description = edt_homework_description.getText().toString();
        select_date = txt_homework_date.getText().toString();

        txt_homework_date.setFocusable(false);
        txt_homework_date.setFocusableInTouchMode(false);

        txt_homework_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar calendar = Calendar.getInstance();
                mYear = calendar.get(Calendar.YEAR);
                mMonth = calendar.get(Calendar.MONTH);
                mDay = calendar.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                strDate = year + "-" + ((monthOfYear + 1) > 9 ? (monthOfYear + 1) : "0" + (monthOfYear + 1)) + "-" +
                                        (dayOfMonth > 9 ? dayOfMonth : "0" + dayOfMonth);

                                SimpleDateFormat update_start_date = new SimpleDateFormat("yyyy-MM-dd");
                                Date date = null;
                                String set_start_date = "";
                                try {
                                    date = update_start_date.parse(strDate);
                                    update_start_date.applyPattern("dd/MM/yyyy");
                                    if (date != null) {
                                        set_start_date = update_start_date.format(date);
                                    }
                                    txt_homework_date.setText(set_start_date);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePickerDialog.show();
            }
        });

        sp_homework_class.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                select_class = classData.get(position).getCla_Class();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        sp_homework_section.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                select_section = sectionData.get(position).getCla_Section();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        sp_homework_subject.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                select_subject = subjectData.get(position).getCla_Subject();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        img_homework_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isEmpty()) {
                    new AsyncTaskRunnerCreateHomework().execute();

                    android.app.Fragment someFragment = new HomeworkFragment();
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.content, someFragment); // give your fragment container id in first parameter
                    transaction.addToBackStack(null);  // if written, this transaction will be added to backstack
                    transaction.commit();
                    txt_menu_title.setText("Homework ");
                    Typeface style = Typeface.createFromAsset(getActivity().getAssets(), "Raleway-SemiBold.ttf");
                    txt_menu_title.setTypeface(style);
                    getActivity().overridePendingTransition(0,0);

                } else {

                }

            }
        });


        return view;
    }

    private boolean isEmpty() {
        if (txt_homework_date.getText().toString().trim().equals("")) {
            txt_homework_date.setError("Please Select Date");
            Toast.makeText(getActivity(), "Please Select Date", Toast.LENGTH_LONG).show();
            return false;
        } else if (edt_homework_chapter.getText().toString().trim().equals("")) {
            edt_homework_chapter.setError("Please Enter Chapter Number");
            edt_homework_chapter.requestFocus();
            return false;
        } else if (edt_homework_pageno.getText().toString().trim().equals("")) {
            edt_homework_pageno.setError("Please Enter Page Number");
            edt_homework_pageno.requestFocus();
            return false;
        } else if (edt_homework_description.getText().toString().trim().equals("")) {
            edt_homework_description.setError("Please Enter Chapter Description");
            edt_homework_description.requestFocus();
            return false;
        } else if (select_class.equals("Select")) {
            sp_homework_class.requestFocus();
            Toast.makeText(getActivity(), "Plese Select Class", Toast.LENGTH_LONG).show();
            return false;
        } else if (select_section.equals("Select")) {
            sp_homework_class.requestFocus();
            Toast.makeText(getActivity(), "Plese Select Section", Toast.LENGTH_LONG).show();
            return false;
        } else if (select_subject.equals("Select")) {
            sp_homework_class.requestFocus();
            Toast.makeText(getActivity(), "Plese Select Subject", Toast.LENGTH_LONG).show();
            return false;
        } else {
            return true;
        }
    }

    private class AsyncTaskRunnerGetClassSection extends AsyncTask<String, String, String> {

        private ProgressDialog Dialog = new ProgressDialog(getActivity());

        @Override
        protected String doInBackground(String... params) {

            MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);

            // JSONArray jsArray = null;
            builder.addFormDataPart("user_id", uid);

            RequestBody requestBody = builder.build();

            try {

                OkHttpClient client = new OkHttpClient.Builder().build();
                Request request = new Request.Builder()
                        .url(Services.HOMEWORK_CLASS_SECTION_LIST_SCREEN)
                        .post(requestBody)
                        .build();

                Response resp = client.newCall(request).execute();

                assert resp.body() != null;
                responseData = resp.body().string();

            } catch (Exception e) {
                e.printStackTrace();
            }

            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            Dialog.dismiss();
            DeliveryDataClassSection(responseData);
            super.onPostExecute(s);
        }

        @Override
        protected void onPreExecute() {
            Dialog.show();
            Dialog.setMessage("Please Wait....");
            Dialog.setCancelable(false);
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
        }
    }

    private void DeliveryDataClassSection(String response) {

        Student_Class_Model student_class_model;
        Student_Section_Model student_section_model;

        classData = new ArrayList<>();
        sectionData = new ArrayList<>();

        if (!response.equals("")) {
            try {
                JSONObject jsobjectcategory = new JSONObject(response);
                String message = jsobjectcategory.getString("message");
                // Updated upstream
                String error = jsobjectcategory.getString("error");

                if (error.equals("200")) {

                    JSONArray jsonArray = jsobjectcategory.getJSONArray("data");
                    JSONObject jsonObject = null;
                    JSONObject jsonObject1 = null;
                    student_class_model = new Student_Class_Model();
                    student_section_model = new Student_Section_Model();
                    student_class_model.setCla_Class("Select");
                    student_section_model.setCla_Section("Select");
                    classData.add(student_class_model);
                    sectionData.add(student_section_model);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        jsonObject = jsonArray.getJSONObject(i);

                        student_class_model = new Student_Class_Model();
                        student_class_model.setCla_Class(jsonObject.getString("Cla_Class"));
                        classData.add(student_class_model);

                        JSONArray jsonArray2 = jsonObject.getJSONArray("section");
                        for (int j = 0; j < jsonArray2.length(); j++) {
                            jsonObject1 = jsonArray2.getJSONObject(j);
                            student_section_model = new Student_Section_Model();
                            student_section_model.setCla_Section(jsonObject1.getString("Cla_Section"));
                            sectionData.add(student_section_model);
                        }

                    }

                    student_class__adapter = new Student_Class_Adapter(getActivity(), classData);
                    sp_homework_class.setAdapter(student_class__adapter);

                    student_section_adapter = new Student_Section_Adapter(getActivity(), sectionData);
                    sp_homework_section.setAdapter(student_section_adapter);

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

        }
    }

    private class AsyncTaskRunnerCreateHomework extends AsyncTask<String, String, String> {

        private ProgressDialog Dialog = new ProgressDialog(getActivity());

        @Override
        protected String doInBackground(String... params) {

            MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);

            // JSONArray jsArray = null;
            builder.addFormDataPart("class", select_class);
            builder.addFormDataPart("user_id", uid);
            builder.addFormDataPart("subject", select_subject);
            builder.addFormDataPart("chapter", edt_homework_chapter.getText().toString().trim());
            builder.addFormDataPart("date", strDate);
            builder.addFormDataPart("page_no", edt_homework_pageno.getText().toString().trim());
            builder.addFormDataPart("section", select_section);
            builder.addFormDataPart("description", edt_homework_description.getText().toString().trim());

            RequestBody requestBody = builder.build();

            try {

                OkHttpClient client = new OkHttpClient.Builder().build();
                Request request = new Request.Builder()
                        .url(Services.HOMEWORK_CREATE_HOMEWORK)
                        .post(requestBody)
                        .build();

                Response resp = client.newCall(request).execute();

                assert resp.body() != null;
                responseCreateHomework = resp.body().string();

            } catch (Exception e) {
                e.printStackTrace();
            }

            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            Dialog.dismiss();
            select_class = "";
            select_date = "";
            select_section = "";
            select_subject = "";
            DeliveryDataCreateHomework(responseCreateHomework);
            super.onPostExecute(s);
        }

        @Override
        protected void onPreExecute() {
            Dialog.show();
            Dialog.setMessage("Please Wait....");
            Dialog.setCancelable(false);
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
        }
    }

    private void DeliveryDataCreateHomework(String response) {

        if (!response.equals("")) {
            try {
                JSONObject jsobjectcategory = new JSONObject(response);
                String message = jsobjectcategory.getString("message");
                // Updated upstream
                String error = jsobjectcategory.getString("error");

                Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                edt_homework_description.setText("");
                edt_homework_chapter.setText("");
                edt_homework_pageno.setText("");

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

        }
    }
}
