package com.example.shine_rahul.gemstarkidz.Model;

/**
 * Created by Shine-Rahul on 26/2/2018.
 */

public class PaymentModel {
    String id;
    String card_type;
    String expire_month;
    String expire_year;

    public PaymentModel() {
    }

    public PaymentModel(String id, String card_type, String expire_month, String expire_year) {
        this.id = id;
        this.card_type = card_type;
        this.expire_month = expire_month;
        this.expire_year = expire_year;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCard_type() {
        return card_type;
    }

    public void setCard_type(String card_type) {
        this.card_type = card_type;
    }

    public String getExpire_month() {
        return expire_month;
    }

    public void setExpire_month(String expire_month) {
        this.expire_month = expire_month;
    }

    public String getExpire_year() {
        return expire_year;
    }

    public void setExpire_year(String expire_year) {
        this.expire_year = expire_year;
    }
}
