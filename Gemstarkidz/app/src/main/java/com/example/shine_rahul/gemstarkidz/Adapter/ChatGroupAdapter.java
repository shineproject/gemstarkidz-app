package com.example.shine_rahul.gemstarkidz.Adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.shine_rahul.gemstarkidz.Model.ChatGroupModel;
import com.example.shine_rahul.gemstarkidz.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class ChatGroupAdapter extends RecyclerView.Adapter<ChatGroupAdapter.ViewHolder> {
    private Activity context;
    private ArrayList<ChatGroupModel> data;

    public ChatGroupAdapter(Activity context, ArrayList<ChatGroupModel> data) {
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.group_chat_list, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ChatGroupModel chatGroupModel = data.get(position);
        holder.txt_group_user_name.setText(chatGroupModel.getName());

        Picasso.with(context)
                .load(chatGroupModel.getImage())
                .placeholder(R.drawable.logo)
                .into(holder.img_group);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView img_group, img_group_right;
        TextView txt_group_user_name;
        CardView cv_chat_list;

        public ViewHolder(View itemView) {
            super(itemView);
            img_group = (CircleImageView) itemView.findViewById(R.id.img_group);
            img_group_right = (CircleImageView) itemView.findViewById(R.id.img_group_right);
            txt_group_user_name = (TextView) itemView.findViewById(R.id.txt_group_user_name);
            cv_chat_list = (CardView) itemView.findViewById(R.id.cv_chat_list);

            cv_chat_list.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (img_group_right.getVisibility() == View.VISIBLE) {
                        img_group_right.setVisibility(View.GONE);

                    } else {
                        img_group_right.setVisibility(View.VISIBLE);
                    }
                }
            });
        }
    }
}
