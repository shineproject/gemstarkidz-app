package com.example.shine_rahul.gemstarkidz.Adapter;

import android.app.Activity;
import android.content.Context;
import android.icu.text.UnicodeSetSpanner;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shine_rahul.gemstarkidz.Model.AbsentPresentListModel;
import com.example.shine_rahul.gemstarkidz.Model.EventModel;
import com.example.shine_rahul.gemstarkidz.Model.TeacherClassModel;
import com.example.shine_rahul.gemstarkidz.R;
import com.example.shine_rahul.gemstarkidz.Service.Services;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

public class StudentPresentAbsentAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<AbsentPresentListModel> data;
    private ArrayList<AbsentPresentListModel> data1;
    private LayoutInflater inflater = null;
    String accept;


    public StudentPresentAbsentAdapter(Context context, ArrayList<AbsentPresentListModel> data) {
        this.context = context;
        this.data = data;
        this.data1 = new ArrayList<>();
        this.data1.addAll(data);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (convertView == null) {
            inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.absent_present_list, null);
        }
        TextView txt_student_name_absent;
        CircleImageView img_absent_present_list;

        txt_student_name_absent = (TextView) view.findViewById(R.id.txt_student_name_absent);
        img_absent_present_list = (CircleImageView) view.findViewById(R.id.img_absent_present_list);
        AbsentPresentListModel absentPresentListModel = data.get(position);

        txt_student_name_absent.setText(absentPresentListModel.getStudent_name());
        Picasso.with(context)
                .load(Services.IMAGES_PATH + "profile/" + absentPresentListModel.getStudent_image())
                .placeholder(R.drawable.logo)
                .into(img_absent_present_list);

        accept = absentPresentListModel.getStudent_absent_present();

        if (accept.equals("1")) {
            img_absent_present_list.setBorderColor(context.getResources().getColor(R.color.colorTheme));
            img_absent_present_list.setBorderWidth(15);
        } else {
            img_absent_present_list.setBorderColor(context.getResources().getColor(R.color.colorRing));
            img_absent_present_list.setBorderWidth(15);
        }

        return view;
    }


    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        data.clear();
        if (charText.length() == 0) {
            data.addAll(data1);
        } else {
            for (AbsentPresentListModel wp : data1) {
                if (wp.getStudent_name().toLowerCase(Locale.getDefault()).contains(charText)) {
                    data.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }

}



