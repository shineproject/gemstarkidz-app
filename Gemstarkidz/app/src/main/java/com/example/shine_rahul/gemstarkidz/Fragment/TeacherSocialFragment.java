package com.example.shine_rahul.gemstarkidz.Fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.daasuu.bl.BubbleLayout;
import com.daasuu.bl.BubblePopupHelper;
import com.example.shine_rahul.gemstarkidz.Adapter.PostSocialImagePager;
import com.example.shine_rahul.gemstarkidz.Adapter.TeacherClassStudentListAdapter;
import com.example.shine_rahul.gemstarkidz.CommentActivity;
import com.example.shine_rahul.gemstarkidz.ImageDisplayActivity;
import com.example.shine_rahul.gemstarkidz.JSONParser.JSONParser;
import com.example.shine_rahul.gemstarkidz.Model.TeacherClassModel;
import com.example.shine_rahul.gemstarkidz.Model.TeacherSocialModel;
import com.example.shine_rahul.gemstarkidz.Model.TeacherSocialModelImage;
import com.example.shine_rahul.gemstarkidz.Model.TeacherSocialTagList;
import com.example.shine_rahul.gemstarkidz.R;
import com.example.shine_rahul.gemstarkidz.Service.Services;
import com.example.shine_rahul.gemstarkidz.VideoPlayActivity;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class TeacherSocialFragment extends android.app.Fragment implements View.OnClickListener {

    ImageView img_teacher_social_post, img_teacher_social_photo, img_teacher_social_video, img_teacher_social_user_profile,
            img_teacher_social_select;
    TextView txt_teacher_social_activity;
    String title = "", description = "", path = "";

    private ArrayList<TeacherSocialModel> getPostData;
    private ArrayList<TeacherSocialModelImage> getPostDataImage;
    private ArrayList<TeacherSocialTagList> getTagList;
    public String flagTag = "0";

    TeacherSocialAdapter socialAdapter;
    RecyclerView rv_teacher_social_image, rv_teacher_social;

    TeacherClassModel teacherClassModel;
    TeacherClassStudentListAdapter teacherClassStudentListAdapter;
    TeacherTagListAdapter teacherTagListAdapter;
    ArrayList<TeacherClassModel> teacherClassModelArrayList = new ArrayList<>();

    String responseimage = "", resultPostDetail = "";

    private Bitmap bitmap;
    int GALLERY_PICK = 101, CAMERA_PICK = 132;
    int VIDEO = 105;
    int i;
    public static String post_id = "", resultTagDetail = "";
    private MainAdapter mAdapter;
    CircleImageView singleimage;
    Context context;
    //multiple video select
    private static final int SELECT_VIDEOS = 1;
    String imageandvideo = "";
    String singleimageandvideo = "";

    EditText edt_post_title, edt_post_description;
    String user = "", type = "", phone = "", uid = "", email = "", user_image = "", user_class, user_section, Cla_Id = "";
    ArrayList<File> imagePostList;
    String file_type = "";
    // HEAD
    TextView txt_edit_toolbar;
    TextView txt_menu_title;
    ImageView img_search, img_close;
    EditText edt_search;

    private ArrayList<Uri> mArrayUri;
    private ArrayList<String> filePath;

    int height, width;

    public TeacherSocialFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_teacher_social, container, false);

        context = this.getActivity();

        SharedPreferences splogin = getActivity().getSharedPreferences("login", 0);
        user = splogin.getString("username", "");
        type = splogin.getString("type", "");
        phone = splogin.getString("user_phone", "");
        uid = splogin.getString("user_id", "");
        email = splogin.getString("user_email", "");
        user_image = splogin.getString("user_image", "");
        user_class = splogin.getString("user_class", "");
        user_section = splogin.getString("user_section", "");
        Cla_Id = splogin.getString("Cla_Id", "");

        txt_edit_toolbar = (TextView) getActivity().findViewById(R.id.txt_edit_toolbar);
        img_search = (ImageView) getActivity().findViewById(R.id.img_search);
        img_close = (ImageView) getActivity().findViewById(R.id.img_close);
        edt_search = (EditText) getActivity().findViewById(R.id.edt_search);
        txt_menu_title = (TextView) getActivity().findViewById(R.id.txt_menu_title);

        txt_edit_toolbar = (TextView) getActivity().findViewById(R.id.txt_edit_toolbar);
        rv_teacher_social = (RecyclerView) view.findViewById(R.id.rv_teacher_social);
        img_teacher_social_photo = (ImageView) view.findViewById(R.id.img_teacher_social_photo);
        img_teacher_social_post = (ImageView) view.findViewById(R.id.img_teacher_social_post);
        img_teacher_social_video = (ImageView) view.findViewById(R.id.img_teacher_social_video);
        img_teacher_social_user_profile = (ImageView) view.findViewById(R.id.img_teacher_social_user_profile);
        img_teacher_social_select = (ImageView) view.findViewById(R.id.img_teacher_social_select);
        txt_teacher_social_activity = (TextView) view.findViewById(R.id.txt_teacher_social_activity);
        rv_teacher_social_image = (RecyclerView) view.findViewById(R.id.rv_teacher_social_image);
        singleimage = (CircleImageView) view.findViewById(R.id.img_teacher_socialone);
        edt_post_title = view.findViewById(R.id.edt_post_title);
        edt_post_description = (EditText) view.findViewById(R.id.edt_post_description);

        txt_menu_title.setVisibility(View.VISIBLE);
        txt_menu_title.setText("Social");
        txt_edit_toolbar.setVisibility(View.GONE);
        img_search.setVisibility(View.VISIBLE);
        edt_search.setVisibility(View.GONE);
        img_close.setVisibility(View.GONE);
        txt_edit_toolbar.setVisibility(View.GONE);

        Typeface style = Typeface.createFromAsset(getActivity().getAssets(), "Raleway-Bold.ttf");
        txt_teacher_social_activity.setTypeface(style);

        description = edt_post_description.getText().toString();
        title = edt_post_title.getText().toString();

        mArrayUri = new ArrayList<Uri>();
        filePath = new ArrayList<String>();
        mAdapter = new MainAdapter();
        imagePostList = new ArrayList<>();

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;

        new AsyncTaskRunnerGetPostData().execute();
        new showStudentList().execute();

        txt_teacher_social_activity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt_post_description.setFocusableInTouchMode(true);
                edt_post_title.setVisibility(View.VISIBLE);
                edt_post_description.setVisibility(View.VISIBLE);
                edt_post_title.setFocusable(true);
                edt_post_title.requestFocus();
                txt_teacher_social_activity.setVisibility(View.GONE);
            }
        });

        com.squareup.picasso.Transformation transformation = new RoundedTransformationBuilder()
                .borderColor(Color.BLACK)
                .borderWidthDp(0)
                .cornerRadiusDp(10)
                .oval(false)
                .build();

        Picasso.with(getActivity())
                .load(R.drawable.aaa1)
                .fit()
                .transform(transformation)
                .into(img_teacher_social_user_profile);

        rv_teacher_social_image.setLayoutManager
                (new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));

        rv_teacher_social_image.setHasFixedSize(true);
        rv_teacher_social_image.setAdapter(mAdapter);

        img_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                img_search.setVisibility(View.GONE);
                txt_menu_title.setVisibility(View.GONE);
                edt_search.setVisibility(View.VISIBLE);
                img_close.setVisibility(View.VISIBLE);
                edt_search.setText("");
                edt_search.requestFocus();
                edt_search.setFocusable(true);
            }
        });

        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                img_close.setVisibility(View.GONE);
                edt_search.setVisibility(View.GONE);
                img_search.setVisibility(View.VISIBLE);
                txt_menu_title.setVisibility(View.VISIBLE);
                txt_menu_title.setText("Social");
                new AsyncTaskRunnerGetPostData().execute();

            }
        });

        edt_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                socialAdapter.filter(edt_search.getText().toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        img_teacher_social_select.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                BubbleLayout bubbleLayout = (BubbleLayout) LayoutInflater.from(getActivity()).inflate(R.layout.social_popup, null);
                final PopupWindow popupWindow = BubblePopupHelper.create(getActivity(), bubbleLayout);
                popupWindow.showAsDropDown(v);
                final TextView txt_entire_class = (TextView) bubbleLayout.findViewById(R.id.txt_entire_class);
                final TextView txt_select_student = (TextView) bubbleLayout.findViewById(R.id.txt_select_student);

                txt_entire_class.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        flagTag = "1";
                        popupWindow.dismiss();
                    }
                });

                txt_select_student.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        flagTag = "2";
                        txt_select_student.setTextColor(getResources().getColor(R.color.colorTheme));
                        txt_entire_class.setTextColor(getResources().getColor(R.color.colorText));
                        showTagDialog();
                        popupWindow.dismiss();
                    }
                });
            }
        });

//        img_teacher_social_video.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                try {
//                    file_type = "2";
//                    img_teacher_social_video.setImageResource(R.drawable.video_active);
//                    img_teacher_social_photo.setImageResource(R.drawable.photo_deactive);
//                    Intent intent;
//                    if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)) {
//                        intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
//                    } else {
//                        intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Video.Media.INTERNAL_CONTENT_URI);
//                    }
//                    intent.setType("video/mp4");
//                    intent.setAction(Intent.ACTION_GET_CONTENT);
//                    intent.putExtra("return-data", true);
//                    startActivityForResult(intent, VIDEO);
//                    showPictureDialog();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        });

        img_teacher_social_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    file_type = "1";
                    img_teacher_social_photo.setImageResource(R.drawable.photo_active);
                    img_teacher_social_video.setImageResource(R.drawable.video_deactive);
                    showPictureDialog();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        img_teacher_social_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    file_type = "2";
                    img_teacher_social_video.setImageResource(R.drawable.video_active);
                    img_teacher_social_photo.setImageResource(R.drawable.photo_deactive);
                    if (Build.VERSION.SDK_INT < 19) {
                        Intent intent = new Intent();
                        intent.setType("video/mp4");
                        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent, "Select videos"), SELECT_VIDEOS);
                    } else {
                        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                        intent.addCategory(Intent.CATEGORY_OPENABLE);
                        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                        intent.setType("video/mp4");
                        startActivityForResult(intent, SELECT_VIDEOS);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        img_teacher_social_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    txt_teacher_social_activity.setVisibility(View.VISIBLE);
                    edt_post_description.setVisibility(View.GONE);
                    edt_post_title.setVisibility(View.GONE);
                    title = edt_post_title.getText().toString();
                    description = edt_post_description.getText().toString();

                    if (!title.equals("") && !description.equals("")) {
                        rv_teacher_social_image.setVisibility(View.GONE);
                        singleimage.setVisibility(View.GONE);
                        new AsyncTaskRunnerUploadData().execute();
                    } else {
                        Toast.makeText(getActivity(), "Title/Description for Post", Toast.LENGTH_LONG).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        return view;
    }

    private void showPictureDialog() {
        final String[] option = {"Select photo from gallery", "Capture photo from camera", "Cancel"};
        final AlertDialog.Builder pictureDialog = new AlertDialog.Builder(getActivity());
        pictureDialog.setTitle("Select Action");

        pictureDialog.setItems(option, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                try {

                    if (option[i].equals("Select photo from gallery")) {
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent, "Select Picture"), GALLERY_PICK);
                    } else if (option[i].equals("Capture photo from camera")) {
                        Intent intent_camera = new Intent("android.media.action.IMAGE_CAPTURE");
                        File file = new File(Environment.getExternalStorageDirectory() + File.separator + "imagesocial.jpg");
                        intent_camera.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
                        startActivityForResult(intent_camera, 0);
                    } else if (option[i].equals("Cancel")) {
                        dialog.dismiss();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        pictureDialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (resultCode == RESULT_OK) {
                if (requestCode == GALLERY_PICK) {
                    file_type = "1";
                    imageandvideo = "image";
                    singleimageandvideo = "image";
                    if (data.getData() != null) {
                        // Toast.makeText(getActivity(), "Minimum 2 Photo Require...", Toast.LENGTH_LONG).show();

                        singleimage.setVisibility(View.GONE);
                        rv_teacher_social_image.setVisibility(View.VISIBLE);

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {

                            try {
                                Uri selectedImage = data.getData();
                                mArrayUri.add(selectedImage);
                                filePath.add(getPath(getActivity(), selectedImage));

                                if (mArrayUri != null) {
                                    mAdapter.swapData(mArrayUri);
                                }
                                Log.v("LOG_TAG", "Selected Images" + mArrayUri.size());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                    } else {
                        singleimage.setVisibility(View.GONE);
                        rv_teacher_social_image.setVisibility(View.VISIBLE);

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {

                            try {
                                if (data.getClipData() != null) {
                                    ClipData mClipData = data.getClipData();

                                    for (int i = 0; i < mClipData.getItemCount(); i++) {
                                        ClipData.Item item = mClipData.getItemAt(i);
                                        Uri uri = item.getUri();
                                        mArrayUri.add(uri);
                                        filePath.add(getPath(getActivity(), uri));
                                    }

                                    if (mArrayUri != null) {
                                        mAdapter.swapData(mArrayUri);
                                    }
                                    Log.v("LOG_TAG", "Selected Images" + mArrayUri.size());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                } else if (requestCode == SELECT_VIDEOS) {
                    file_type = "2";
                    imageandvideo = "video";
                    singleimageandvideo = "video";
                    if (data.getData() != null) {
                        //  Toast.makeText(getActivity(), "Minimum 2 Video Require...", Toast.LENGTH_LONG).show();

                        singleimage.setVisibility(View.GONE);
                        rv_teacher_social_image.setVisibility(View.VISIBLE);

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            try {

                                Uri selectedImage = data.getData();

                                String pathUri = selectedImage.getPath();
                                String sizeF = calculateFileSize(pathUri);
                                Float size = Float.valueOf(sizeF);

                                if (size < 2.0) {
                                    mArrayUri.add(selectedImage);
                                    filePath.add(getPath(getActivity(), selectedImage));
                                } else {
                                    Toast.makeText(getActivity(), "Please Select less than 2 mb video", Toast.LENGTH_LONG).show();
                                }

                                if (mArrayUri != null) {
                                    if (mArrayUri.size() > 0) {
                                        mAdapter.swapData(mArrayUri);
                                    }
                                }

                                Log.v("LOG_TAG", "Selected Images" + mArrayUri.size());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } else {

                        singleimage.setVisibility(View.GONE);
                        rv_teacher_social_image.setVisibility(View.VISIBLE);

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            try {
                                if (data.getClipData() != null) {
                                    ClipData mClipData = data.getClipData();
                                    // ArrayList<Uri> mArrayUri = new ArrayList<Uri>();
                                    for (int i = 0; i < mClipData.getItemCount(); i++) {

                                        ClipData.Item item = mClipData.getItemAt(i);
                                        Uri uri = item.getUri();

                                        String pathUri = uri.getPath();
                                        String sizeF = calculateFileSize(pathUri);
                                        Float size = Float.valueOf(sizeF);

                                        if (size < 2.0) {
                                            mArrayUri.add(uri);
                                            filePath.add(getPath(getActivity(), uri));
                                        } else {
                                            Toast.makeText(getActivity(), "Please Select less than 2 mb video", Toast.LENGTH_LONG).show();
                                        }

                                    }

                                    if (mArrayUri != null) {
                                        mAdapter.swapData(mArrayUri);
                                    }

                                    Log.v("LOG_TAG", "Selected Images" + mArrayUri.size());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                } else if (requestCode == 0) {
                    try {
                        file_type = "3";
                        path = Environment.getExternalStorageDirectory() + File.separator + "imagesocial.jpg";

                        rv_teacher_social_image.setVisibility(View.GONE);
                        singleimage.setVisibility(View.VISIBLE);

                        bitmap = BitmapFactory.decodeFile(path);
                        singleimage.setImageBitmap(bitmap);

                        mArrayUri.add(Uri.parse(path));
                        Uri uri1 = Uri.parse(path);
                        filePath.add(getPath(getActivity(), uri1));

                        if (mArrayUri != null) {
                            mAdapter.swapData(mArrayUri);
                        }

                    } catch (Exception e) {

                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_teacher_socialone:
                Toast.makeText(getActivity(), "hiii", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getActivity(), ImageDisplayActivity.class);
                intent.putExtra("image", String.valueOf(singleimage));
                getActivity().startActivity(intent);
        }
    }

    class MainAdapter extends RecyclerView.Adapter<MainAdapter.ViewHolder> {

        private final List<Uri> mData;
        private Context context;

        public MainAdapter(List<Uri> mData, Context context) {
            this.mData = mData;
            this.context = context;
        }

        MainAdapter() {
            mData = new ArrayList<>();
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.teacher_social_image, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, final int position) {
            final Object data = mData.get(position);

            Glide.with(holder.img_teacher_social.getContext())
                    .load(data)
                    .centerCrop()
                    .into(holder.img_teacher_social);

            holder.img_teacher_social.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {

                        if (imageandvideo.equals("video")) {
                            Intent intent = new Intent(getActivity(), VideoPlayActivity.class);
                            intent.putExtra("video", data.toString());
                            getActivity().startActivity(intent);
                        } else if (imageandvideo.equals("image")) {
                            Intent intent = new Intent(getActivity(), ImageDisplayActivity.class);
                            intent.putExtra("image", data.toString());
                            getActivity().startActivity(intent);
                            Log.d("Image", data.toString());
                        } else if (singleimage.equals("image")) {

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return mData.size();
        }

        void swapData(@Nullable List<Uri> data) {
            if (!mData.equals(data)) {
                mData.clear();
                if (data != null) {
                    mData.addAll(data);
                }
                notifyDataSetChanged();
            }
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            private final ImageView img_teacher_social;

            private ViewHolder(View itemView) {
                super(itemView);
                img_teacher_social = (ImageView) itemView.findViewById(R.id.img_teacher_social);
                img_teacher_social.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });
            }


        }

    }

    private void showTagDialog() {
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View view1 = layoutInflater.inflate(R.layout.tag_student, null);
        final AlertDialog.Builder alertdialog = new AlertDialog.Builder(getActivity());
        alertdialog.setView(view1);
        final AlertDialog dialog = alertdialog.create();
        final ImageView btn_tag_entire_class = (ImageView) view1.findViewById(R.id.btn_selected_tag_student_submit);
        GridView gv_tag_select_student = (GridView) view1.findViewById(R.id.gv_tag_select_student);
        gv_tag_select_student.setAdapter(teacherClassStudentListAdapter);

        dialog.show();

        btn_tag_entire_class.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Tag Successfully...", Toast.LENGTH_LONG).show();
                dialog.dismiss();
            }
        });
    }

    private class AsyncTaskRunnerUploadData extends AsyncTask<String, String, String> {

        private ProgressDialog Dialog = new ProgressDialog(getActivity());

        @Override
        protected String doInBackground(String... params) {
            try {


                MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);

                if (mArrayUri.size() > 0) {
                    for (int i = 0; i < mArrayUri.size(); i++) {
                        imagePostList.add(new File(mArrayUri.get(i).toString()));
                    }
                }

                // JSONArray jsArray = null;
                String str = "";
                if (TeacherClassStudentListAdapter.studentId != null) {
                    if (TeacherClassStudentListAdapter.studentId.size() > 0) {
                        flagTag = "2";
                        str = android.text.TextUtils.join(",", TeacherClassStudentListAdapter.studentId);
                    }
                }

                if (str.equals("")) {
                    builder.addFormDataPart("student_id", "0");
                } else {
                    builder.addFormDataPart("student_id", str);
                }

                if (file_type.equals("1")) {
                    for (int i = 0; i < filePath.size(); i++) {
                        File file = new File(filePath.get(i));
                        builder.addFormDataPart("image[]", file.getName(), RequestBody.create(MediaType.parse("image/jpg"), file));
                    }
                } else if (file_type.equals("2")) {
                    for (int i = 0; i < filePath.size(); i++) {
                        File file = new File(filePath.get(i));
                        builder.addFormDataPart("image[]", file.getName(), RequestBody.create(MediaType.parse("video/mp4"), file));
                    }
                } else if (file_type.equals("3")) {
                    File file = new File(path);
                    builder.addFormDataPart("image[]", file.getName(), RequestBody.create(MediaType.parse("image/jpg"), file));
                    file_type = "1";
                }

                builder.addFormDataPart("student_id", str);
                builder.addFormDataPart("user_id", uid);
                builder.addFormDataPart("user_type", "2");
                builder.addFormDataPart("title", title);
                builder.addFormDataPart("comment", description);
                builder.addFormDataPart("file_type", file_type);
                builder.addFormDataPart("flag", flagTag);
                builder.addFormDataPart("class_id", Cla_Id);

                RequestBody requestBody = builder.build();

                try {

                    OkHttpClient client = new OkHttpClient.Builder().build();
                    Request request = new Request.Builder()
                            .url(Services.SOCIAL_SCREEN)
                            .post(requestBody)
                            .build();

                    Response resp = client.newCall(request).execute();

                    assert resp.body() != null;
                    responseimage = resp.body().string();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            mArrayUri = new ArrayList<Uri>();
            filePath = new ArrayList<String>();
            imagePostList = new ArrayList<>();
            mAdapter.notifyDataSetChanged();
            edt_post_title.setText("");
            edt_post_description.setText("");
            flagTag = "0";
            TeacherClassStudentListAdapter.studentId.clear();
            Deliverydatacategory(responseimage);
            Dialog.dismiss();
            new AsyncTaskRunnerGetPostData().execute();
            super.onPostExecute(s);
        }

        @Override
        protected void onPreExecute() {
            Dialog.show();
            Dialog.setMessage("Please Wait....");
            Dialog.setCancelable(false);
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
        }
    }

    private void Deliverydatacategory(String response) {
        try {
            if (!response.equals("")) {
                try {
                    JSONObject jsobjectcategory = new JSONObject(response);
                    String message = jsobjectcategory.getString("message");
                    //  Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("NewApi")
    public static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                @SuppressLint({"NewApi", "LocalSuppress"}) final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                @SuppressLint({"NewApi", "LocalSuppress"}) final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                @SuppressLint({"NewApi", "LocalSuppress"}) final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context       The context.
     * @param uri           The Uri to query.
     * @param selection     (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection, String[]
            selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    private class AsyncTaskRunnerGetPostData extends AsyncTask<String, String, String> {

        private ProgressDialog Dialog = new ProgressDialog(getActivity());

        @Override
        protected String doInBackground(String... params) {

            MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
            builder.addFormDataPart("user_id", uid);
            RequestBody requestBody = builder.build();

            try {
                OkHttpClient client = new OkHttpClient.Builder().build();
                Request request = new Request.Builder()
                        .url(Services.SOCIAL_SCREEN_LIST)
                        .post(requestBody)
                        .build();
                Response resp = client.newCall(request).execute();
                resultPostDetail = resp.body().string();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            Dialog.dismiss();
            GetPostData(resultPostDetail);
            super.onPostExecute(s);
        }

        @Override
        protected void onPreExecute() {
            Dialog.show();
            Dialog.setMessage("Please Wait....");
            Dialog.setCancelable(false);
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
        }

    }

    private void GetPostData(String response) {

        TeacherSocialModel teacherSocialModel;
        TeacherSocialModelImage teacherSocialModelImage;
        getPostData = new ArrayList<>();
        getPostDataImage = new ArrayList<>();

        if (!response.equals("")) {
            try {
                JSONObject jsobjectcategory = new JSONObject(response);
                String message = jsobjectcategory.getString("message");
                // Updated upstream
                String error = jsobjectcategory.getString("error");

                if (error.equals("200")) {

                    JSONArray jsonArray = jsobjectcategory.getJSONArray("data");
                    JSONObject jsonObject = null;
                    for (int i = 0; i < jsonArray.length(); i++) {
                        jsonObject = jsonArray.getJSONObject(i);

                        teacherSocialModel = new TeacherSocialModel();

                        JSONArray jsonArrayD = jsonObject.getJSONArray("soa_images");
                        for (int j = 0; j < jsonArrayD.length(); j++) {
                            JSONObject jsonObjectD = jsonArrayD.getJSONObject(j);
                            teacherSocialModelImage = new TeacherSocialModelImage();
                            teacherSocialModelImage.setSod_Id(jsonObjectD.getString("Sod_Id"));
                            teacherSocialModelImage.setSod_Use_Id(jsonObjectD.getString("Sod_Use_Id"));
                            teacherSocialModelImage.setSod_Soa_Id(jsonObjectD.getString("Sod_Soa_Id"));
                            teacherSocialModelImage.setSod_Uploadtype(jsonObjectD.getString("Sod_Uploadtype"));
                            teacherSocialModelImage.setSod_Img_Video(jsonObjectD.getString("Sod_Img_Video"));
                            teacherSocialModelImage.setSod_CreatedAt(jsonObjectD.getString("Sod_CreatedAt"));
                            teacherSocialModelImage.setSod_UpdatedAt(jsonObjectD.getString("Sod_UpdatedAt"));
                            getPostDataImage.add(teacherSocialModelImage);
                        }

                        if (jsonObject.has("soa_like")) {
                            JSONArray jsonArrayL = jsonObject.getJSONArray("soa_like");
                            try {
                                if (jsonArrayL != null) {
                                    if (jsonArrayL.length() > 0) {
                                        for (int l = 0; l < jsonArrayL.length(); l++) {
                                            JSONObject jsonObjectL = jsonArrayL.getJSONObject(l);
                                            teacherSocialModel.setSal_Like(jsonObjectL.getString("Sal_Like"));
                                        }
                                    } else {
                                        teacherSocialModel.setSal_Like("");
                                    }
                                } else {
                                    teacherSocialModel.setSal_Like("");
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            teacherSocialModel.setSal_Like("");
                        }

                        teacherSocialModel.setSoa_Id(jsonObject.getString("Soa_Id"));
                        teacherSocialModel.setSoa_Use_Id(jsonObject.getString("Soa_Use_Id"));
                        teacherSocialModel.setSoa_Comment(jsonObject.getString("Soa_Comment"));
                        teacherSocialModel.setSoa_Title(jsonObject.getString("Soa_Title"));
                        teacherSocialModel.setSoa_User_Type(jsonObject.getString("Soa_User_Type"));
                        teacherSocialModel.setSoa_Status(jsonObject.getString("Soa_Status"));
                        teacherSocialModel.setSoa_CreatedBy(jsonObject.getString("Soa_CreatedBy"));
                        teacherSocialModel.setSoa_CreatedAt(jsonObject.getString("Soa_CreatedAt"));
                        teacherSocialModel.setSoa_UpdateBy(jsonObject.getString("Soa_UpdatedBy"));
                        teacherSocialModel.setSoa_UpdatedAt(jsonObject.getString("Soa_UpdatedAt"));

                        getPostData.add(teacherSocialModel);
                    }

                    socialAdapter = new TeacherSocialAdapter(getActivity(), getPostData, getPostDataImage, width, uid);
                    rv_teacher_social.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
                    rv_teacher_social.setAdapter(socialAdapter);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

        }
    }

    class TeacherSocialAdapter extends RecyclerView.Adapter<TeacherSocialAdapter.ViewHolder> {

        private Activity context;
        private ArrayList<TeacherSocialModel> dataPost;
        private ArrayList<TeacherSocialModel> dataPost1 = new ArrayList<TeacherSocialModel>();
        private ArrayList<TeacherSocialModelImage> dataPostImage;
        private boolean isSelect = false;
        private ArrayList<String> imageList;
        PostSocialImagePager postSocialImagePager;
        int width;
        String uid = "", resultPostLike = "";
        public String soc_id = "", image = "", text = "";
        public int flag = 0, positionV = 0;
        private Date dateNew;
        private Date datefi;

        public TeacherSocialAdapter(Activity context, ArrayList<TeacherSocialModel> dataPost, ArrayList<TeacherSocialModelImage> dataPostImage, int width, String uid) {
            this.context = context;
            this.dataPost = dataPost;
            this.dataPost1.addAll(dataPost);
            this.dataPostImage = dataPostImage;
            this.width = width;
            this.uid = uid;
        }

        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.teacher_social_list, null);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
            final TeacherSocialModel teacherSocialModel = dataPost.get(position);
            //   final TeacherSocialModelImage teacherSocialModelImage = dataPostImage.get(position);
            if (!teacherSocialModel.getSoa_Title().equals("null")) {
                holder.txt_teacher_social_title.setText(teacherSocialModel.getSoa_Title());
            }
            //  holder.txt_teacher_social_time.setText(teacherSocialModel.getSoa_CreatedAt());
            if (!teacherSocialModel.getSoa_Comment().equals("null")) {
                holder.txt_teacher_social_description.setText(teacherSocialModel.getSoa_Comment());
            }

            if (holder.txt_teacher_social_description.getText().toString().length() > 150) {
                holder.txt_teacher_social_readmore.setVisibility(View.VISIBLE);
            }

            holder.cardView.setMinimumWidth(width);
            holder.layoutL.setMinimumWidth(width);

            try {
                String strDate = teacherSocialModel.getSoa_CreatedAt();
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                Date c = Calendar.getInstance().getTime();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String formattedDate = df.format(c);

                try {
                    dateNew = format.parse(strDate);
                    datefi = df.parse(formattedDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                long different = datefi.getTime() - dateNew.getTime();
                long secondsInMilli = 1000;
                long minutesInMilli = secondsInMilli * 60;
                long hoursInMilli = minutesInMilli * 60;
                long daysInMilli = hoursInMilli * 24;

                long elapsedDays = different / daysInMilli;
                different = different % daysInMilli;
                long elapsedHours = different / hoursInMilli;
                different = different % hoursInMilli;
                long elapsedMinutes = different / minutesInMilli;
                different = different % minutesInMilli;
                long elapsedSeconds = different / secondsInMilli;

                if (elapsedDays >= 1) {
                    holder.txt_teacher_social_time.setText(String.valueOf(elapsedDays) + " days ago");
                } else if (elapsedHours <= 24) {
                    holder.txt_teacher_social_time.setText(String.valueOf(elapsedHours) + " hours ago");
                } else if (elapsedMinutes <= 60) {
                    holder.txt_teacher_social_time.setText(String.valueOf(elapsedMinutes) + " minute ago");
                } else {
                    holder.txt_teacher_social_time.setText("Just Now");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                imageList = new ArrayList<>();
                for (int i = 0; i < dataPostImage.size(); i++) {
                    if (teacherSocialModel.getSoa_Id().equals(dataPostImage.get(i).getSod_Soa_Id())) {
                        imageList.add(dataPostImage.get(i).getSod_Img_Video());
                    }
                }

                if (imageList.size() > 0) {
                    holder.layoutimage.setVisibility(View.VISIBLE);
                    postSocialImagePager = new PostSocialImagePager(imageList, context);
                    holder.viewPagerImage.setAdapter(postSocialImagePager);
                } else {
                    holder.layoutimage.setVisibility(View.GONE);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            holder.img_teacher_social_tag.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    post_id = teacherSocialModel.getSoa_Id();
                    new AsyncTaskRunnerGetTagList().execute();
                }
            });

            holder.img_teacher_social_comment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, CommentActivity.class);
                    intent.putExtra("uid", uid);
                    intent.putExtra("soa_id", teacherSocialModel.getSoa_Id());
                    context.startActivity(intent);
                }
            });

            holder.img_teacher_social_share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    imageList = new ArrayList<>();
                    for (int i = 0; i < dataPostImage.size(); i++) {
                        if (teacherSocialModel.getSoa_Id().equals(dataPostImage.get(i).getSod_Soa_Id())) {
                            imageList.add(dataPostImage.get(i).getSod_Img_Video());
                        }
                    }

                    positionV = holder.viewPagerImage.getCurrentItem();
                    if (imageList.size() > 0) {
                        image = Services.IMAGES_PATH + "social/" + imageList.get(positionV);
                    }

                    try {
                        if (!image.equals("")) {
                            Uri uri = Uri.parse(image);
                            shareImage(String.valueOf(uri), context);
                        } else {
                            Toast.makeText(getActivity(), "You don't have any Image or Video", Toast.LENGTH_LONG).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            if (teacherSocialModel.getSal_Like().equals("1")) {
                holder.img_teacher_social_like.setImageResource(R.mipmap.hand);
            } else {
                holder.img_teacher_social_like.setImageResource(R.mipmap.like_icon);
            }

            holder.li_teacherlike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (teacherSocialModel.getSal_Like().equals("1")) {
                        holder.img_teacher_social_like.setImageResource(R.mipmap.like_icon);
                        flag = 0;
                        soc_id = teacherSocialModel.getSoa_Id();
                        new AsyncTaskRunnerLikePostData().execute();
                    } else {
                        holder.img_teacher_social_like.setImageResource(R.mipmap.hand);
                        flag = 1;
                        soc_id = teacherSocialModel.getSoa_Id();
                        new AsyncTaskRunnerLikePostData().execute();
                    }
                }
            });

            holder.txt_teacher_social_readmore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (holder.txt_teacher_social_readmore.getText().equals("View More...")) {
                        holder.txt_teacher_social_readmore.setText("View Less...");
                        holder.txt_teacher_social_description.setMaxLines(Integer.MAX_VALUE);
                    } else {
                        holder.txt_teacher_social_readmore.setText("View More...");
                        holder.txt_teacher_social_description.setMaxLines(2);
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return dataPost.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            TextView txt_teacher_social_title, txt_teacher_social_time, txt_teacher_social_description, txt_teacher_social_readmore,
                    txt_teacher_count;
            ImageView img_teacher_social_like, img_teacher_social_profile, img_teacher_social_comment,
                    img_teacher_social_share, img_teacher_social_tag;
            ViewPager viewPagerImage;
            CardView cardView;
            LinearLayout layoutL, layoutimage, li_teacherlike;

            public ViewHolder(View itemView) {
                super(itemView);
                viewPagerImage = itemView.findViewById(R.id.viewPagerImage);
                cardView = itemView.findViewById(R.id.cv);
                layoutL = itemView.findViewById(R.id.layoutL);
                layoutimage = itemView.findViewById(R.id.layoutimage);
                txt_teacher_social_title = (TextView) itemView.findViewById(R.id.txt_teacher_social_title);
                txt_teacher_social_time = (TextView) itemView.findViewById(R.id.txt_teacher_social_time);
                txt_teacher_social_description = (TextView) itemView.findViewById(R.id.txt_teacher_social_description);
                txt_teacher_social_readmore = (TextView) itemView.findViewById(R.id.txt_teacher_social_readmore);
                li_teacherlike = (LinearLayout) itemView.findViewById(R.id.li_teacherlike);
                img_teacher_social_like = (ImageView) itemView.findViewById(R.id.img_teacher_social_like);
                txt_teacher_count = (TextView) itemView.findViewById(R.id.txt_teacher_count);
                img_teacher_social_profile = (ImageView) itemView.findViewById(R.id.img_teacher_social_profile);
                img_teacher_social_comment = (ImageView) itemView.findViewById(R.id.img_teacher_social_comment);
                img_teacher_social_share = (ImageView) itemView.findViewById(R.id.img_teacher_social_share);
                img_teacher_social_tag = (ImageView) itemView.findViewById(R.id.img_teacher_social_tag);
            }
        }

        private class AsyncTaskRunnerLikePostData extends AsyncTask<String, String, String> {

            //    private ProgressDialog Dialog = new ProgressDialog(context);

            @Override
            protected String doInBackground(String... params) {

                MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
                builder.addFormDataPart("user_id", uid);
                builder.addFormDataPart("soa_id", soc_id);
                builder.addFormDataPart("flag", String.valueOf(flag));
                builder.addFormDataPart("user_type", "2");

                RequestBody requestBody = builder.build();

                try {
                    OkHttpClient client = new OkHttpClient.Builder().build();
                    Request request = new Request.Builder()
                            .url(Services.SOCIAL_SCREEN_LIKE)
                            .post(requestBody)
                            .build();

                    Response resp = client.newCall(request).execute();
                    resultPostLike = resp.body().string();

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return "";
            }

            @Override
            protected void onPostExecute(String s) {
                // Dialog.dismiss();
                GetPostData(resultPostLike);
                super.onPostExecute(s);
            }

            @Override
            protected void onPreExecute() {
//                Dialog.show();
//                Dialog.setMessage("Please Wait....");
//                Dialog.setCancelable(false);
                super.onPreExecute();
            }

            @Override
            protected void onProgressUpdate(String... values) {
                super.onProgressUpdate(values);
            }
        }

        private void GetPostData(String response) {

            TeacherSocialModel teacherSocialModel;

            if (!response.equals("")) {
                try {

                    JSONObject jsobjectcategory = new JSONObject(response);
                    String message = jsobjectcategory.getString("message");
                    // Updated upstream
                    String error = jsobjectcategory.getString("error");

                    if (error.equals("200")) {
                        // Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                        new AsyncTaskRunnerGetPostData().execute();
                    } else {

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {

            }
        }

        public void filter(String charText) {
            charText = charText.toLowerCase(Locale.getDefault());
            dataPost.clear();
            if (charText.length() == 0) {
                dataPost.addAll(dataPost1);
            } else {
                for (TeacherSocialModel wp : dataPost1) {
                    if (wp.getSoa_Title().toLowerCase(Locale.getDefault()).contains(charText)) {
                        dataPost.add(wp);
                    }
                }
            }
            notifyDataSetChanged();
        }

    }

    static public void shareImage(String url, final Context context) {
        Picasso.with(context).load(url).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("image/*");
                i.putExtra(Intent.EXTRA_STREAM, getLocalBitmapUri(bitmap, context));
                context.startActivity(Intent.createChooser(i, "Share Image"));
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
            }
        });
    }

    static public Uri getLocalBitmapUri(Bitmap bmp, Context context) {
        Uri bmpUri = null;
        try {
            File file = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "share_image_" + System.currentTimeMillis() + ".png");
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }

    public class showStudentList extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Loading");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            HashMap<String, String> mapParams = new HashMap<>();
            mapParams.put("user_id", uid);
            String response = new JSONParser().sendPostRequest(Services.ATTENDANCE_LIST, mapParams);
            return response;
        }

        protected void onPostExecute(String response) {
            progressDialog.dismiss();

            if (response != null && !response.equals("")) {
                try {

                    JSONObject mjsonObject = new JSONObject(response);
                    String error = mjsonObject.getString("error");
                    String mesage = mjsonObject.getString("message");
                    if (error.equals("200")) {

                        JSONArray jsonArray = mjsonObject.getJSONArray("data");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            teacherClassModel = new TeacherClassModel();
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            teacherClassModel.setTeacher_id(jsonObject.getString("Use_Id"));
                            JSONArray jsonArray_user_list = jsonObject.getJSONArray("use_student");
                            teacherClassModelArrayList.clear();

                            for (int j = 0; j < jsonArray_user_list.length(); j++) {
                                JSONObject jsonObject_student_list = jsonArray_user_list.getJSONObject(j);
                                teacherClassModel = new TeacherClassModel();
                                teacherClassModel.setStudent_id(jsonObject_student_list.getString("Std_Id"));
                                teacherClassModel.setStudent_image(jsonObject_student_list.getString("Std_Image"));
                                teacherClassModel.setStudent_name(jsonObject_student_list.getString("Std_Name"));
                                teacherClassModelArrayList.add(teacherClassModel);
                            }

                        }

                        teacherClassStudentListAdapter = new TeacherClassStudentListAdapter(getActivity(), teacherClassModelArrayList);
                        //   teacherClassStudentListAdapter.notifyDataSetChanged();

                    } else {
                        Toast.makeText(getActivity(), mesage, Toast.LENGTH_LONG).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private class AsyncTaskRunnerGetTagList extends AsyncTask<String, String, String> {

        private ProgressDialog Dialog = new ProgressDialog(getActivity());

        @Override
        protected String doInBackground(String... params) {

            MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
            builder.addFormDataPart("soa_id", post_id);
            RequestBody requestBody = builder.build();

            try {
                OkHttpClient client = new OkHttpClient.Builder().build();
                Request request = new Request.Builder()
                        .url(Services.SOCIAL_SCREEN_TAG_LIST)
                        .post(requestBody)
                        .build();
                Response resp = client.newCall(request).execute();
                resultTagDetail = resp.body().string();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            Dialog.dismiss();
            GetTagData(resultTagDetail);
            super.onPostExecute(s);
        }

        @Override
        protected void onPreExecute() {
            Dialog.show();
            Dialog.setMessage("Please Wait....");
            Dialog.setCancelable(false);
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
        }

    }

    private void GetTagData(String response) {

        TeacherSocialTagList teacherSocialTagList;
        getTagList = new ArrayList<>();

        if (!response.equals("")) {
            try {
                JSONObject jsobjectcategory = new JSONObject(response);
                String message = jsobjectcategory.getString("message");
                // Updated upstream
                String error = jsobjectcategory.getString("error");

                if (error.equals("200")) {

                    JSONArray jsonArray = jsobjectcategory.getJSONArray("data");
                    JSONObject jsonObject = null;


                    for (int i = 0; i < jsonArray.length(); i++) {
                        jsonObject = jsonArray.getJSONObject(i);

                        teacherSocialTagList = new TeacherSocialTagList();

                        JSONArray jsonArrayD = jsonObject.getJSONArray("tag_student");
                        for (int j = 0; j < jsonArrayD.length(); j++) {
                            JSONObject jsonObjectD = jsonArrayD.getJSONObject(j);
                            teacherSocialTagList.setStd_Id(jsonObjectD.getString("Std_Id"));
                            teacherSocialTagList.setStd_Cla_Id(jsonObjectD.getString("Std_Cla_Id"));
                            teacherSocialTagList.setStd_Gr_No(jsonObjectD.getString("Std_Gr_No"));
                            teacherSocialTagList.setStd_Image(jsonObjectD.getString("Std_Image"));
                            teacherSocialTagList.setStd_Name(jsonObjectD.getString("Std_Name"));
                        }

                        teacherSocialTagList.setTag_Id(jsonObject.getString("Tag_Id"));
                        teacherSocialTagList.setTag_Flag(jsonObject.getString("Tag_Flag"));
                        teacherSocialTagList.setTag_Soa_Id(jsonObject.getString("Tag_Soa_Id"));
                        teacherSocialTagList.setTag_Use_Id(jsonObject.getString("Tag_Use_Id"));

                        getTagList.add(teacherSocialTagList);
                    }

                    teacherTagListAdapter = new TeacherTagListAdapter(getActivity(), getTagList);

                    if (getTagList.size() > 0) {
                        showTagListDialog();
                    }

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

        }
    }

    private void showTagListDialog() {
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View view1 = layoutInflater.inflate(R.layout.tag_student, null);
        final AlertDialog.Builder alertdialog = new AlertDialog.Builder(getActivity());
        alertdialog.setView(view1);
        final AlertDialog dialog = alertdialog.create();
        final ImageView btn_tag_entire_class = (ImageView) view1.findViewById(R.id.btn_selected_tag_student_submit);
        GridView gv_tag_select_student = (GridView) view1.findViewById(R.id.gv_tag_select_student);
        btn_tag_entire_class.setVisibility(View.GONE);
        gv_tag_select_student.setAdapter(teacherTagListAdapter);

        dialog.show();


    }

    private class TeacherTagListAdapter extends BaseAdapter {

        private Activity activity;
        private ArrayList<TeacherSocialTagList> getTagListData;
        private LayoutInflater inflater = null;

        public TeacherTagListAdapter(Activity activity, ArrayList<TeacherSocialTagList> getTagList) {
            this.activity = activity;
            this.getTagListData = getTagList;
        }

        @Override
        public int getCount() {
            return getTagListData.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, final View convertView, ViewGroup parent) {
            View view = convertView;
            if (convertView == null) {
                inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.teacher_class_student_list, null);
            }
            try {
                final CircleImageView img_teacher_class_student_list;
                TextView txt_student_name;
                txt_student_name = (TextView) view.findViewById(R.id.txt_student_name);
                img_teacher_class_student_list = (CircleImageView) view.findViewById(R.id.img_teacher_class_student_list);

                final TeacherSocialTagList teacherSocialTagList = getTagListData.get(position);
                txt_student_name.setText(teacherSocialTagList.getStd_Name());

                Picasso.with(context)
                        .load(Services.IMAGES_PATH + "" + teacherSocialTagList.getStd_Image())
                        .error(R.drawable.logo)
                        .into(img_teacher_class_student_list);

                view.setOnClickListener(new View.OnClickListener() {
                    @SuppressLint("NewApi")
                    @Override
                    public void onClick(View v) {
                    }

                });
            } catch (Exception e) {
                e.printStackTrace();
                Log.d("Error", e.getMessage());
            }
            return view;
        }
    }

    public String calculateFileSize(String filepath) {
        File file = new File(filepath);
        long fileSizeInBytes = file.length();
        float fileSizeInKB = fileSizeInBytes / 1024;
        float fileSizeInMB = fileSizeInKB / 1024;
        String calString = Float.toString(fileSizeInMB);
        return calString;
    }
}