package com.example.shine_rahul.gemstarkidz.Model;

/**
 * Created by Shine-Rahul on 22/2/2018.
 */

public class Student_Class_Model {

   String Cla_Class;
   String name;

    public String getCla_Class() {
        return Cla_Class;
    }

    public void setCla_Class(String cla_Class) {
        Cla_Class = cla_Class;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
