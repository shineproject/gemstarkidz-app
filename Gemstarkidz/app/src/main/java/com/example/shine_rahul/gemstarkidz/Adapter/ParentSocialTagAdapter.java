package com.example.shine_rahul.gemstarkidz.Adapter;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.shine_rahul.gemstarkidz.Model.ParentSocialTagModel;
import com.example.shine_rahul.gemstarkidz.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class ParentSocialTagAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<ParentSocialTagModel> data;
    private LayoutInflater layoutInflater = null;

    public ParentSocialTagAdapter(Context context, ArrayList<ParentSocialTagModel> data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (convertView == null) ;
        {
            layoutInflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.parent_social_tag_item, null);
        }
        try {
            final CircleImageView img_parent;
            final TextView txt_parent;
            img_parent = (CircleImageView) view.findViewById(R.id.txt_parent);
            txt_parent = (TextView) view.findViewById(R.id.txt_parent);

            final ParentSocialTagModel parentSocialTagModel = data.get(position);

            txt_parent.setText(parentSocialTagModel.getName());
            Picasso.with(context)
                    .load(parentSocialTagModel.getImage())
                    .error(R.drawable.profile)
                    .into(img_parent);
            img_parent.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                @Override
                public void onClick(View v) {
                    img_parent.setBackground(context.getResources().getDrawable(R.drawable.image_background));


                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }
}
