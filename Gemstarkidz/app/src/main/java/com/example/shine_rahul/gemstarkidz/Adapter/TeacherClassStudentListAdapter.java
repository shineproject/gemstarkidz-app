package com.example.shine_rahul.gemstarkidz.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shine_rahul.gemstarkidz.Model.TeacherClassModel;
import com.example.shine_rahul.gemstarkidz.R;
import com.example.shine_rahul.gemstarkidz.Service.Services;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Shine-Rahul on 10/3/2018.
 */

public class TeacherClassStudentListAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<TeacherClassModel> data;
    private LayoutInflater inflater = null;
    int i = 0;
    public static ArrayList<String> studentId = new ArrayList<String>();

    public TeacherClassStudentListAdapter(Context context, ArrayList<TeacherClassModel> data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        View view = convertView;
        if (convertView == null) {
            inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.teacher_class_student_list, null);
        }
        try {
            final CircleImageView img_teacher_class_student_list;
            TextView txt_student_name;
            txt_student_name = (TextView) view.findViewById(R.id.txt_student_name);
            img_teacher_class_student_list = (CircleImageView) view.findViewById(R.id.img_teacher_class_student_list);

            final TeacherClassModel teacherClassModel = data.get(position);
            txt_student_name.setText(teacherClassModel.getStudent_name());
            Picasso.with(context)
                    .load(Services.IMAGES_PATH + "" + teacherClassModel.getStudent_image())
                    .error(R.drawable.logo)
                    .into(img_teacher_class_student_list);

            view.setOnClickListener(new View.OnClickListener() {
                @SuppressLint("NewApi")
                @Override
                public void onClick(View v) {

                    if (studentId.contains(teacherClassModel.getStudent_id())) {
                        studentId.remove(teacherClassModel.getStudent_id());
                    } else {
                        studentId.add(teacherClassModel.getStudent_id());
                    }

                    if (img_teacher_class_student_list.getBorderColor() == context.getResources().getColor(R.color.colorTheme)) {
                        img_teacher_class_student_list.setBorderColor(context.getResources().getColor(R.color.colorRing));
                        img_teacher_class_student_list.setBorderWidth(10);

                    } else {
                        img_teacher_class_student_list.setBorderColor(context.getResources().getColor(R.color.colorTheme));
                        img_teacher_class_student_list.setBorderWidth(10);
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("Error", e.getMessage().toString());
        }
        return view;
    }
}
