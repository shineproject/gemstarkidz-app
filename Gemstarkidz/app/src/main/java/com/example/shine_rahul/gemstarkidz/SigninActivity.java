package com.example.shine_rahul.gemstarkidz;

import android.app.Activity;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.shine_rahul.gemstarkidz.Service.Services;
import com.example.shine_rahul.gemstarkidz.Session.Session;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.Iterator;

import javax.net.ssl.HttpsURLConnection;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class SigninActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {
    CircleImageView img_signin_profile;
    EditText edt_signin_email_number;
    TextInputEditText edt_signin_password;
    ImageView btn_signin;
    TextView txt_signup_social;
    TextView txt_signup_google, txt_signup_facebook, txt_signin_SMS_verification, txt_signin_verify_code;
    Session session;
    String type, google_flag = "3";


    //facebook login
    CallbackManager callbackManager;
    LoginButton login_button;
    TextView text123;
    String str_mobile, str_pass;

    String Use_Id, Use_Name, Use_Email, Use_Type, Use_Address, Use_City, Use_State, Use_Country, Use_Phone_No, Use_Mobile_No,
            Use_Status, Use_Image, Use_Class, Use_Cla_Id, Use_Section, Use_Register_Type, Cla_Id, Cla_Bra_Id,
            Use_CreatedBy, Use_CreatedAt, Use_UpdatedBy, Use_UpdatedAt, Use_Student_Name, Use_Student_Grnumber, Use_Student_Class, Use_Student_Section;


    private GoogleApiClient mGoogleApiClient;
    private static final String TAG = SigninActivity.class.getSimpleName();
    private LoginManager mLoginManager;
    private CallbackManager mFacebookCallbackManager;
    private AccessTokenTracker mAccessTokenTracker;
    private boolean loggedin;
    private static final int RC_SIGN_IN = 007;

    String fb_name = "", fb_email = "", fb_imageid = "", g_name = "", g_email = "", g_image = "", responseimage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_signin);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();


        session = Session.getSession(SigninActivity.this);
        callbackManager = CallbackManager.Factory.create();

        login_button = (LoginButton) findViewById(R.id.login_button);
        edt_signin_email_number = (EditText) findViewById(R.id.edt_signin_email_number);
        edt_signin_password = (TextInputEditText) findViewById(R.id.edt_signin_password);
        txt_signin_SMS_verification = (TextView) findViewById(R.id.txt_signin_SMS_verification);
        btn_signin = (ImageView) findViewById(R.id.btn_signin);
        txt_signup_social = (TextView) findViewById(R.id.txt_signup_social);
        txt_signup_google = (TextView) findViewById(R.id.txt_signup_google);
        txt_signup_facebook = (TextView) findViewById(R.id.txt_signup_facebook);

        Typeface style = Typeface.createFromAsset(getApplicationContext().getAssets(), "Raleway-SemiBold.ttf");
        txt_signin_SMS_verification.setTypeface(style);
        txt_signup_social.setTypeface(style);
        txt_signup_google.setTypeface(style);
        txt_signup_facebook.setTypeface(style);

        btn_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isEmpty()) {
                    str_mobile = edt_signin_email_number.getText().toString().trim();
                    str_pass = edt_signin_password.getText().toString().trim();
                    new SendPostRequest1().execute();
                }
            }
        });

        txt_signup_google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signIn();
            }
        });

        txt_signup_facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setupFacebookStuff();
                handleFacebookLogin();
            }
        });
    }

    private void setupFacebookStuff() {

        // This should normally be on your application class
        mAccessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
//                updateFacebookButtonUI();
            }
        };

        mLoginManager = LoginManager.getInstance();
        mFacebookCallbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(mFacebookCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
//                updateFacebookButtonUI();
                requestObjectUser(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {
//                Toast.makeText(SigninActivity.this, String.valueOf(error), Toast.LENGTH_SHORT).show();
                Log.e("Key", String.valueOf(error));
            }
        });
    }

    private void handleFacebookLogin() {
        if (AccessToken.getCurrentAccessToken() != null) {
            mLoginManager.logOut();
        } else {
            mAccessTokenTracker.startTracking();
            mLoginManager.logInWithReadPermissions(SigninActivity.this, Arrays.asList("public_profile"));
        }

    }

    private void requestObjectUser(final AccessToken accessToken) {
        GraphRequest request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                if (response.getError() != null) {
                    // handle error
                } else {

                    loggedin = true;

                    String personPhotoUrl = object.optString("picture");
                    String userid = object.optString("id");

                    // Facebook


                    fb_name = object.optString("first_name");
                    fb_email = object.optString("email");
                    fb_imageid = object.optString("id");

                    new AsyncTaskRunnerfacebook().execute();

                    try {
                        JSONObject data = new JSONObject(personPhotoUrl);
                        String data1 = data.optString("data");
                        JSONObject data2 = new JSONObject(data1);
                        String url = data.optString("url");


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
        });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,first_name,last_name,email, picture");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        } else {
            mFacebookCallbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            Log.e(TAG, "display name: " + acct.getDisplayName());

            if (acct.getDisplayName().equals("")) {

            } else {
                g_name = acct.getDisplayName();
            }

            if (acct.getPhotoUrl().equals("")) {

            } else {
                g_image = acct.getPhotoUrl().toString();
            }

            if (acct.getEmail().equals("")) {

            } else {
                g_email = acct.getEmail();
            }

            // Google
            new AsyncTaskRunnergoogle().execute();

        } else {
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
        if (opr.isDone()) {
            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
            // and the GoogleSignInResult will be available instantly.
            Log.d(TAG, "Got cached sign-in");
            GoogleSignInResult result = opr.get();
//            handleSignInResult(result);
        } else {
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(GoogleSignInResult googleSignInResult) {
//                    hideProgressDialog();
//                    handleSignInResult(googleSignInResult);
                }
            });
        }
    }

    private boolean isEmpty() {
        if (edt_signin_email_number.getText().toString().trim().equals("")) {
            edt_signin_email_number.setError("Please enter mobile number");
            edt_signin_email_number.requestFocus();
            return false;
        } else if (edt_signin_email_number.getText().toString().length() < 10) {
            edt_signin_email_number.setError("Mobile number must be 10 digit");
            edt_signin_email_number.requestFocus();
            return false;
        } else if (edt_signin_password.getText().toString().trim().equals("")) {
            edt_signin_password.setError("Please enter password");
            edt_signin_password.requestFocus();
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(SigninActivity.this, WelcomeActivity.class);
        intent.putExtra("data", "normal");
        startActivity(intent);
        finish();
        overridePendingTransition(0, 0);
    }

    //login api call
    public class SendPostRequest1 extends AsyncTask<String, Void, String> {

        private ProgressDialog Dialog = new ProgressDialog(SigninActivity.this);

        protected void onPreExecute() {
            Dialog.show();
            Dialog.setMessage("Please Wait....");
            Dialog.setCancelable(false);
        }

        protected String doInBackground(String... arg0) {

            try {

                URL url = new URL(Services.LOGIN);
                JSONObject postDataParams = new JSONObject();
                postDataParams.put("username", str_mobile);
                postDataParams.put("password", str_pass);

                Log.e("params", postDataParams.toString());

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(postDataParams));

                writer.flush();
                writer.close();
                os.close();

                int responseCode = conn.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK) {

                    BufferedReader in = new BufferedReader(new
                            InputStreamReader(
                            conn.getInputStream()));

                    StringBuffer sb = new StringBuffer("");
                    String line = "";

                    while ((line = in.readLine()) != null) {

                        sb.append(line);

                        break;
                    }

                    in.close();
                    return sb.toString();

                } else {
                    return new String("false : " + responseCode);
                }
            } catch (Exception e) {
                return new String("Exception: " + e.getMessage());
            }

        }

        @Override
        protected void onPostExecute(String result) {
            Dialog.dismiss();
            Deliverydatacategory(result);
        }
    }

    private void Deliverydatacategory(String response) {
//1=Admin 2=Teacher 3=Driver 4 = Parent
        if (!response.equals("")) {
            try {

                JSONObject jsobjectcategory = new JSONObject(response);

                String message = jsobjectcategory.getString("message");
                String requestcode = jsobjectcategory.getString("status");
                String error = jsobjectcategory.getString("error");

                if (error.equals("200")) {
                    String profile = jsobjectcategory.getString("data");
                    JSONObject jsonObject = new JSONObject(profile);
                    Use_Id = jsonObject.getString("Use_Id");
                    Use_Name = jsonObject.getString("Use_Name");
                    Use_Email = jsonObject.getString("Use_Email");
                    Use_Type = jsonObject.getString("Use_Type");
                    Use_Address = jsonObject.getString("Use_Address");
                    Use_City = jsonObject.getString("Use_City");
                    Use_Country = jsonObject.getString("Use_Country");
                    Use_Mobile_No = jsonObject.getString("Use_Mobile_No");
                    Use_Image = jsonObject.getString("Use_Image");
                    Use_Register_Type = jsonObject.getString("Use_Register_Type");
                    if (Integer.parseInt(Use_Type) == 4) {
                        Use_Student_Name = jsonObject.getString("Std_Name");
                        Use_Student_Grnumber = jsonObject.getString("Std_Gr_No");
                        Use_Student_Class = jsonObject.getString("Cla_Class");
                        Use_Student_Section = jsonObject.getString("Cla_Section");
                    }

                    if (Integer.parseInt(Use_Type) == 2) {
                        Cla_Id = jsonObject.getString("Cla_Id");
                        Cla_Bra_Id = jsonObject.getString("Cla_Bra_Id");
                        Use_Class = jsonObject.getString("Cla_Class");
                        Use_Section = jsonObject.getString("Cla_Section");
                        String Cla_Status = jsonObject.getString("Cla_Status");
                    }

                    SharedPreferences spuser = SigninActivity.this.getSharedPreferences("login", 0);
                    SharedPreferences.Editor Eduser = spuser.edit();
                    Eduser.putString("username", Use_Name);
                    Eduser.putString("type", Use_Type);
                    Eduser.putString("user_phone", Use_Mobile_No);
                    Eduser.putString("user_id", Use_Id);
                    Eduser.putString("user_email", Use_Email);
                    Eduser.putString("user_image", Use_Image);
                    Eduser.putString("user_student_name", Use_Student_Name);
                    Eduser.putString("user_student_grnumber", Use_Student_Grnumber);
                    Eduser.putString("user_student_class", Use_Student_Class);
                    Eduser.putString("user_student_section", Use_Student_Section);

                    if (Integer.parseInt(Use_Type) == 2) {
                        Eduser.putString("user_class_name", Use_Class);
                        Eduser.putString("user_section", Use_Section);
                        Eduser.putString("Cla_Id", Cla_Id);
                        Eduser.putString("Cla_Bra_Id", Cla_Bra_Id);
                    }

                    Eduser.putString("user_reg_type", Use_Register_Type);

                    if (Use_Type.equals("3")) {
                        Use_State = jsonObject.getString("Use_State");
                        Use_Phone_No = jsonObject.getString("Use_Phone_No");
                        Use_Status = jsonObject.getString("Use_Status");
                        Use_Cla_Id = jsonObject.getString("Use_Cla_Id");
                        Use_CreatedBy = jsonObject.getString("Use_CreatedBy");
                        Use_CreatedAt = jsonObject.getString("Use_CreatedAt");
                        Use_UpdatedBy = jsonObject.getString("Use_UpdatedBy");
                        Use_UpdatedAt = jsonObject.getString("Use_UpdatedAt");
                    }

                    Eduser.putString("user_address", Use_Address);
                    Eduser.putString("user_city", Use_City);
                    Eduser.putString("user_state", Use_State);
                    Eduser.putString("user_country", Use_Country);
                    Eduser.putString("user_mobile", Use_Phone_No);
                    Eduser.putString("user_status", Use_Status);
                    ;
                    Eduser.putString("user_class", Use_Cla_Id);
                    Eduser.putString("user_created_at", Use_CreatedAt);
                    Eduser.putString("user_created_by", Use_CreatedBy);
                    Eduser.putString("user_updated_at", Use_UpdatedAt);
                    Eduser.putString("user_updated_by", Use_UpdatedBy);
                    Eduser.apply();

                    if (Use_Type.equals("4")) {
                        Intent intent = new Intent(SigninActivity.this, ParentActivity.class);
                        intent.putExtra("username", Use_Name);
                        intent.putExtra("type", Use_Type);
                        intent.putExtra("user_phone", Use_Mobile_No);
                        intent.putExtra("user_id", Use_Id);
                        intent.putExtra("user_email", Use_Email);
                        intent.putExtra("user_image", Use_Image);
                        intent.putExtra("user_reg_type", Use_Register_Type);
                        intent.putExtra("user_student_name", Use_Student_Name);
                        intent.putExtra("user_student_grnumber", Use_Student_Grnumber);
                        intent.putExtra("user_student_class", Use_Student_Class);
                        intent.putExtra("user_student_section", Use_Student_Section);
                        startActivity(intent);
                        overridePendingTransition(0, 0);
                        finish();
                    } else if (Use_Type.equals("2")) {
                        Intent intent = new Intent(SigninActivity.this, TeacherActivity.class);
                        intent.putExtra("username", Use_Name);
                        intent.putExtra("type", Use_Type);
                        intent.putExtra("user_phone", Use_Mobile_No);
                        intent.putExtra("user_id", Use_Id);
                        intent.putExtra("user_email", Use_Email);
                        intent.putExtra("user_image", Use_Image);
                        intent.putExtra("user_class", Use_Class);
                        intent.putExtra("user_section", Use_Section);
                        intent.putExtra("user_reg_type", Use_Register_Type);
                        intent.putExtra("Cla_Id", Cla_Id);
                        intent.putExtra("Cla_Bra_Id", Cla_Bra_Id);
                        startActivity(intent);
                        overridePendingTransition(0, 0);
                        finish();

                    } else if (Use_Type.equals("3")) {

                        Intent intent = new Intent(SigninActivity.this, DriverActivity.class);
                        startActivity(intent);
                    }


                } else {
                    Toast.makeText(SigninActivity.this, message, Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public String getPostDataString(JSONObject params) throws Exception {

        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while (itr.hasNext()) {

            String key = itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        return result.toString();
    }

    private class AsyncTaskRunnerfacebook extends AsyncTask<String, String, String> {

        private ProgressDialog Dialog = new ProgressDialog(SigninActivity.this);

        @Override
        protected String doInBackground(String... params) {

            MultipartBody.Builder builder = new MultipartBody.Builder();

            builder.setType(MultipartBody.FORM);
            final MediaType MEDIA_TYPE = MediaType.parse("image/jpg");

            builder.addFormDataPart("name", fb_name);
            builder.addFormDataPart("email", fb_email);
            builder.addFormDataPart("type", "4");
            builder.addFormDataPart("reg_type", "1");

            RequestBody requestBody = builder.build();

            try {

                OkHttpClient client = new OkHttpClient.Builder().build();
                Request request = new Request.Builder()
                        .url(Services.REGISTRATION)
                        .post(requestBody)
                        .build();
                Response resp = client.newCall(request).execute();
                responseimage = resp.body().string();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            Dialog.dismiss();
            Deliverydatafacebook(responseimage);
            super.onPostExecute(s);
        }

        @Override
        protected void onPreExecute() {
            Dialog.show();
            Dialog.setMessage("Please Wait....");
            Dialog.setCancelable(false);
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
        }

    }

    private void Deliverydatafacebook(String response) {

        if (!response.equals("")) {
            try {

                JSONObject jsobjectcategory = new JSONObject(response);

                String message = jsobjectcategory.getString("message");


                String error = jsobjectcategory.getString("error");

                if (error.equals("200")) {
                    JSONArray jsonArray = jsobjectcategory.getJSONArray("data");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        String username = jsonObject.getString("Use_Name");
                        String mobile = jsonObject.getString("Use_Mobile_No");
                        String email = jsonObject.getString("Use_Email");
                        String utype = jsonObject.getString("Use_Type");
                        String uid = jsonObject.getString("Use_Id");
                        String uimage = jsonObject.getString("Use_Image");
                        String ureg_type = jsonObject.getString("Use_Register_Type");

                        SharedPreferences spuser = SigninActivity.this.getSharedPreferences("login", 0);
                        SharedPreferences.Editor Eduser = spuser.edit();
                        Eduser.putString("username", username);
                        Eduser.putString("type", utype);
                        Eduser.putString("user_phone", mobile);
                        Eduser.putString("user_id", uid);
                        Eduser.putString("user_email", email);
                        if (uimage.equals("")) {
                            Eduser.putString("user_image", fb_imageid);
                        } else {
                            Eduser.putString("flag", "3");
                            Eduser.putString("user_image", uimage);
                        }
                        Eduser.putString("user_reg_type", ureg_type);
                        Eduser.apply();
//
                        Intent intent = new Intent(SigninActivity.this, ParentActivity.class);
                        intent.putExtra("username", username);
                        intent.putExtra("type", utype);
                        intent.putExtra("user_phone", mobile);
                        intent.putExtra("user_id", uid);
                        intent.putExtra("user_email", email);
                        if (uimage.equals("")) {
                            intent.putExtra("user_image", fb_imageid);
                        } else {
                            intent.putExtra("flag", "3");
                            intent.putExtra("user_image", uimage);
                        }
                        intent.putExtra("user_reg_type", ureg_type);
                        startActivity(intent);
                        overridePendingTransition(0, 0);
                        finish();

                    }


                } else {
                    Toast.makeText(SigninActivity.this, message, Toast.LENGTH_LONG).show();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private class AsyncTaskRunnergoogle extends AsyncTask<String, String, String> {

        private ProgressDialog Dialog = new ProgressDialog(SigninActivity.this);

        @Override
        protected String doInBackground(String... params) {

            MultipartBody.Builder builder = new MultipartBody.Builder();

            builder.setType(MultipartBody.FORM);
            final MediaType MEDIA_TYPE = MediaType.parse("image/jpg");
            builder.addFormDataPart("name", g_name);
            builder.addFormDataPart("email", g_email);
            builder.addFormDataPart("type", "4");
            builder.addFormDataPart("reg_type", "2");

            RequestBody requestBody = builder.build();

            try {

                OkHttpClient client = new OkHttpClient.Builder().build();
                Request request = new Request.Builder()
                        .url(Services.REGISTRATION)
                        .post(requestBody)
                        .build();
                Response resp = client.newCall(request).execute();
                responseimage = resp.body().string();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            Dialog.dismiss();
            Deliverydatagoogle(responseimage);
            super.onPostExecute(s);
        }

        @Override
        protected void onPreExecute() {
            Dialog.show();
            Dialog.setMessage("Please Wait....");
            Dialog.setCancelable(false);
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
        }

    }

    private void Deliverydatagoogle(String response) {

        if (!response.equals("")) {
            try {

                JSONObject jsobjectcategory = new JSONObject(response);

                String message = jsobjectcategory.getString("message");


                String error = jsobjectcategory.getString("error");

                if (error.equals("200")) {
                    JSONArray jsonArray = jsobjectcategory.getJSONArray("data");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        String username = jsonObject.getString("Use_Name");
                        String mobile = jsonObject.getString("Use_Mobile_No");
                        String email = jsonObject.getString("Use_Email");
                        String utype = jsonObject.getString("Use_Type");
                        String uid = jsonObject.getString("Use_Id");
                        String uimage = jsonObject.getString("Use_Image");
                        String ureg_type = jsonObject.getString("Use_Register_Type");

                        SharedPreferences spuser = SigninActivity.this.getSharedPreferences("login", 0);
                        SharedPreferences.Editor Eduser = spuser.edit();
                        Eduser.putString("username", username);
                        Eduser.putString("type", utype);
                        Eduser.putString("user_phone", mobile);
                        Eduser.putString("user_id", uid);
                        Eduser.putString("user_email", email);
                        if (uimage.equals("")) {
                            Eduser.putString("user_image", g_image);
                        } else {
                            Eduser.putString("flag", "3");
                            Eduser.putString("user_image", uimage);
                        }
                        Eduser.putString("user_reg_type", ureg_type);
                        Eduser.apply();
//
                        Intent intent = new Intent(SigninActivity.this, ParentActivity.class);
                        intent.putExtra("username", username);
                        intent.putExtra("type", utype);
                        intent.putExtra("user_phone", mobile);
                        intent.putExtra("user_id", uid);
                        intent.putExtra("user_email", email);
                        if (uimage.equals("")) {
                            intent.putExtra("user_image", g_image);
                        } else {
                            intent.putExtra("flag", "3");
                            intent.putExtra("user_image", uimage);
                        }
                        intent.putExtra("user_reg_type", ureg_type);
                        startActivity(intent);
                        overridePendingTransition(0, 0);
                        finish();
                    }
                } else {
                    Toast.makeText(SigninActivity.this, "Google", Toast.LENGTH_LONG).show();
                    Toast.makeText(SigninActivity.this, message, Toast.LENGTH_LONG).show();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
}
