package com.example.shine_rahul.gemstarkidz;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Handler;
import android.os.StrictMode;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WelcomeActivity extends Activity {
    TextView txt_home_content1;
    TextView txt_home_content2;
    TextView txt_home_content3;
    View view_home;
    /*Button Signup;
    Button Signin;*/
    ImageView btn_signup_deactive;
    ImageView btn_signin_deactive;
    ImageView btn_signup_active;
    ImageView btn_signin_active;
    String data;
    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;
    String user = "", type = "", phone = "", uid = "", email = "", image = "", user_class, user_section, user_reg_type, Cla_Id, Cla_Bra_Id, accept_event, accept_event_id, user_student_name, user_student_grnumber, user_student_class, user_student_section,user_class_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_welcome);

        SharedPreferences splogin = WelcomeActivity.this.getSharedPreferences("login", 0);
        user = splogin.getString("username", "");
        type = splogin.getString("type", "");
        phone = splogin.getString("user_phone", "");
        uid = splogin.getString("user_id", "");
        email = splogin.getString("user_email", "");
        image = splogin.getString("user_image", "");
        user_class_name = splogin.getString("user_class_name", "");
        user_section = splogin.getString("user_section", "");
        user_reg_type = splogin.getString("user_reg_type", "");
        Cla_Id = splogin.getString("Cla_Id", "");
        Cla_Bra_Id = splogin.getString("Cla_Bra_Id", "");
        user_student_name = splogin.getString("user_student_name", "");
        user_student_grnumber = splogin.getString("user_student_grnumber", "");
        user_student_class = splogin.getString("user_student_class", "");
        user_student_section = splogin.getString("user_student_section", "");
        user_class = splogin.getString("user_class", "");


        if (type.equals("4")) {
            Intent intent = new Intent(WelcomeActivity.this, ParentActivity.class);
            intent.putExtra("username", user);
            intent.putExtra("type", type);
            intent.putExtra("user_phone", phone);
            intent.putExtra("user_id", uid);
            intent.putExtra("user_email", email);
            intent.putExtra("user_image", image);
            intent.putExtra("user_reg_type", user_reg_type);
            startActivity(intent);
            overridePendingTransition(0, 0);
            finish();

        } else if (type.equals("2")) {
            Intent intent = new Intent(WelcomeActivity.this, TeacherActivity.class);
            startActivity(intent);
            finish();
        } else if (type.equals("3")) {
            Intent intent = new Intent(WelcomeActivity.this, DriverActivity.class);
            startActivity(intent);
            finish();
        } else {

        }

        openRun();

        StrictMode.VmPolicy.Builder newbuilder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(newbuilder.build());

        txt_home_content1 = (TextView) findViewById(R.id.txt_home_content1);
        txt_home_content2 = (TextView) findViewById(R.id.txt_home_content2);
        txt_home_content3 = (TextView) findViewById(R.id.txt_home_content3);
        view_home = (View) findViewById(R.id.view_home);
        btn_signup_deactive = (ImageView) findViewById(R.id.btn_signup_deactive);
        btn_signin_deactive = (ImageView) findViewById(R.id.btn_signin_deactive);
        Intent i = getIntent();
        data = i.getStringExtra("data");


        Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Raleway-Bold.ttf");
        txt_home_content1.setTypeface(font);
        txt_home_content2.setTypeface(font);
        txt_home_content3.setTypeface(font);


        btn_signup_deactive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                data = "hard";
                if (data.equals("normal")) {
                    btn_signup_deactive.setImageResource(R.drawable.signup_deactivate);
                } else {
                    btn_signup_deactive.setImageResource(R.drawable.signup_active);
                }

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent SignupActivity = new Intent(WelcomeActivity.this, SignupActivity.class);
                        startActivity(SignupActivity);
                        finish();
                        overridePendingTransition(0, 0);
                    }
                }, 200);

            }
        });
        btn_signin_deactive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                data = "hard";
                if (data.equals("normal")) {
                    btn_signin_deactive.setImageResource(R.drawable.signin_deactivate);
                } else {
                    btn_signin_deactive.setImageResource(R.drawable.signin_active);
                }

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent SignupActivity = new Intent(WelcomeActivity.this, SigninActivity.class);
                        startActivity(SignupActivity);
                        finish();
                        overridePendingTransition(0, 0);
                    }
                }, 200);
            }
        });
    }

    // if back press button app are close
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void openRun() {
        if (Build.VERSION.SDK_INT >= 23) {
            insertDummyContactWrapper();
// Marshmallow+
        } else {
// Pre-Marshmallow
        }
    }

    private void insertDummyContactWrapper() {
        List<String> permissionsNeeded = new ArrayList<String>();

        final List<String> permissionsList = new ArrayList<String>();
        if (!addPermission(permissionsList, android.Manifest.permission.CAMERA))
            permissionsNeeded.add("Camera");
        if (!addPermission(permissionsList, android.Manifest.permission.READ_EXTERNAL_STORAGE))
            permissionsNeeded.add("Read Storage");
        if (!addPermission(permissionsList, android.Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("Write Storage");
        if (!addPermission(permissionsList, android.Manifest.permission.ACCESS_FINE_LOCATION))
            permissionsNeeded.add("Access Location");
        if (!addPermission(permissionsList, android.Manifest.permission.ACCESS_COARSE_LOCATION))
            permissionsNeeded.add("Cross Location");
        if (!addPermission(permissionsList, Manifest.permission.RECORD_AUDIO))
            permissionsNeeded.add("Cross Location");
        if (!addPermission(permissionsList, Manifest.permission.RECEIVE_SMS))
            permissionsNeeded.add("Cross Location");
        if (!addPermission(permissionsList, Manifest.permission.READ_PHONE_STATE))
            permissionsNeeded.add("Cross Location");

        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
// Need Rationale
                String message = "You need to grant access to " + permissionsNeeded.get(0);
                for (int i = 1; i < permissionsNeeded.size(); i++)
                    message = message + ", " + permissionsNeeded.get(i);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                            REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                }
                return;
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            }
            return;
        }
    }

    private boolean addPermission(List<String> permissionsList, String permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(permission);
// Check for Rationale Option
                if (!shouldShowRequestPermissionRationale(permission))
                    return false;
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {
                Map<String, Integer> perms = new HashMap<String, Integer>();
// Initial
                perms.put(android.Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                perms.put(android.Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(android.Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(android.Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.ACCESS_COARSE_LOCATION, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.RECORD_AUDIO, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.RECEIVE_SMS, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_PHONE_STATE, PackageManager.PERMISSION_GRANTED);
// Fill with results
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);
// Check for ACCESS_FINE_LOCATION
                if (perms.get(android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                        && perms.get(android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                        && perms.get(android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                        && perms.get(android.Manifest.permission.RECEIVE_SMS) == PackageManager.PERMISSION_GRANTED
                        && perms.get(android.Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED) {
// All Permissions Granted
                } else {
// Permission Denied
                    openRun();
                }
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

}


