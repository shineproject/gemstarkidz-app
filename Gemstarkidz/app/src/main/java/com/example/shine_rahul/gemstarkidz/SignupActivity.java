package com.example.shine_rahul.gemstarkidz;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.TextInputEditText;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;


import com.example.shine_rahul.gemstarkidz.Session.Session;
import com.rilixtech.CountryCodePicker;

import org.apache.http.HttpEntity;

import java.io.File;

import de.hdodenhof.circleimageview.CircleImageView;


public class SignupActivity extends Activity {
    CircleImageView img_signup_profile, img_signup_profile_edit;
    TextView txt_signup_message;
    TextInputEditText edt_signup_first_name;
    EditText edt_signup_phone_number;
    EditText edt_signup_email;
    EditText edt_signup_password;
    TextView txt_signup_verify_message;
    ImageView btn_signup_next;
    TextView txt_signup_social;
    TextView txt_signup_twitter;
    TextView txt_signup_facebook;
    CountryCodePicker cpp_country;
    String type = "1";
    Session session;
    String message, Use_Type;
    String name, email, phone, password;
    int GALLERY_PICK = 101, CAMERA_PICK = 132;
    Bitmap bitmap;
    String image_path = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_signup);
        session = Session.getSession(SignupActivity.this);


        img_signup_profile = (CircleImageView) findViewById(R.id.img_signup_profile);
        img_signup_profile_edit = (CircleImageView) findViewById(R.id.img_signup_profile_edit);
        txt_signup_message = (TextView) findViewById(R.id.txt_signup_message);
        edt_signup_phone_number = (EditText) findViewById(R.id.edt_signup_phone_number);
        edt_signup_first_name = (TextInputEditText) findViewById(R.id.edt_signup_first_name);
        edt_signup_email = (EditText) findViewById(R.id.edt_signup_email);
        edt_signup_password = (EditText) findViewById(R.id.edt_signup_password);
        txt_signup_verify_message = (TextView) findViewById(R.id.txt_signup_verify_message);
        btn_signup_next = (ImageView) findViewById(R.id.btn_signup_next);
        txt_signup_social = (TextView) findViewById(R.id.txt_signup_social);
        txt_signup_twitter = (TextView) findViewById(R.id.txt_signup_twitter);
        txt_signup_facebook = (TextView) findViewById(R.id.txt_signup_facebook);

        Typeface font1 = Typeface.createFromAsset(getApplicationContext().getAssets(), "Raleway-SemiBold.ttf");

        txt_signup_message.setTypeface(font1);
        txt_signup_verify_message.setTypeface(font1);
        txt_signup_social.setTypeface(font1);
        txt_signup_twitter.setTypeface(font1);
        txt_signup_facebook.setTypeface(font1);

        img_signup_profile_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPictureDialog();
            }
        });


        btn_signup_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                name = edt_signup_first_name.getText().toString();
                phone = edt_signup_phone_number.getText().toString();
                email = edt_signup_email.getText().toString();
                password = edt_signup_password.getText().toString();
                if (isEmpty()) {
                    Intent intent = new Intent(SignupActivity.this, OTPActivity.class);
                    intent.putExtra("user_phone", phone);
                    intent.putExtra("username", name);
                    intent.putExtra("user_email", email);
                    intent.putExtra("user_type", "4");
                    intent.putExtra("user_pass", password);
                    intent.putExtra("user_image", image_path);
                    startActivity(intent);
                    finish();
                }
            }
        });

    }


    //set validation for signup user
    private boolean isEmpty() {

        if (edt_signup_first_name.getText().toString().trim().equals("")) {
            edt_signup_first_name.setError("Please Enter Name");
            edt_signup_first_name.requestFocus();
            return false;
        } else if (edt_signup_first_name.getText().toString().length() < 3) {
            edt_signup_first_name.setError("Name minimum three character are required");
            edt_signup_first_name.requestFocus();
            return false;
        } else if (edt_signup_phone_number.getText().toString().trim().equals("")) {
            edt_signup_phone_number.setError("Please Enter Phone Number");
            edt_signup_phone_number.requestFocus();
            return false;
        } else if (edt_signup_phone_number.getText().toString().length() < 10) {
            edt_signup_phone_number.setError("Please Enter Valid Number");
            edt_signup_phone_number.requestFocus();
            return false;
        } else if (edt_signup_email.getText().toString().trim().equals("")) {
            edt_signup_email.setError("Please Enter Email");
            edt_signup_email.requestFocus();
            return false;
        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(edt_signup_email.getText()).matches()) {
            edt_signup_email.setError("Please Enter Valid Email Address");
            edt_signup_email.requestFocus();
            return false;
        } else if (edt_signup_password.getText().toString().trim().equals("")) {
            edt_signup_password.setError("Please Enter Password");
            edt_signup_password.requestFocus();
            return false;
        } else if (edt_signup_password.getText().toString().length() < 8) {
            edt_signup_password.setError("Password minimum 8 charactre require");
            edt_signup_password.requestFocus();
            return false;
        } else {
            return true;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(SignupActivity.this, WelcomeActivity.class);
        intent.putExtra("data", "normal");
        startActivity(intent);
        finish();
        overridePendingTransition(0, 0);
    }


    private void showPictureDialog() {
        final String[] option = {"Select photo from gallery", "Capture photo from camera", "Cancel"};
        final AlertDialog.Builder pictureDialog = new AlertDialog.Builder(SignupActivity.this);
        pictureDialog.setTitle("Select Action");

        pictureDialog.setItems(option, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                if (option[i].equals("Select photo from gallery")) {
                    Intent GALLERY = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    GALLERY.setType("image/*");
                    startActivityForResult(Intent.createChooser(GALLERY, "Select Image"), GALLERY_PICK);
                } else if (option[i].equals("Capture photo from camera")) {

                    Intent intent_camera = new Intent("android.media.action.IMAGE_CAPTURE");
                    File file = new File(Environment.getExternalStorageDirectory() + File.separator + "image.jpg");
                    intent_camera.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
                    startActivityForResult(intent_camera, CAMERA_PICK);

                } else if (option[i].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        pictureDialog.show();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {

            if (resultCode == Activity.RESULT_OK) {
                if (requestCode == GALLERY_PICK) {
                    Uri uri = data.getData();
                    String[] FILE = {MediaStore.Images.Media.DATA, MediaStore.Video.Media.DATA};
                    Cursor cursor = getApplicationContext().getContentResolver().query(uri, FILE, null, null, null);
                    cursor.moveToFirst();
                    int columnIndex = cursor.getColumnIndex(FILE[0]);
                    image_path = cursor.getString(columnIndex);
                    cursor.close();
                    bitmap = BitmapFactory.decodeFile(image_path);
                    img_signup_profile.setImageBitmap(bitmap);

                } else if (requestCode == CAMERA_PICK) {
                    try {
                        image_path = Environment.getExternalStorageDirectory() + File.separator + "image.jpg";
                        bitmap = BitmapFactory.decodeFile(image_path);
                        img_signup_profile.setImageBitmap(bitmap);
                    } catch (Exception e) {

                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }


}
