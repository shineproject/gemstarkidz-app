package com.example.shine_rahul.gemstarkidz.Model;

/**
 * Created by Shine-Rahul on 23/3/2018.
 */

public class CommentListModel {

    String Sac_Id;
    String Sac_Comment;
    String Sac_CreatedAt;

    String Use_Id;
    String Use_Name;
    String Use_Type;
    String Use_Mobile_No;
    String Use_Status;
    String Use_Image;

    public String getSac_Id() {
        return Sac_Id;
    }

    public void setSac_Id(String sac_Id) {
        Sac_Id = sac_Id;
    }

    public String getSac_Comment() {
        return Sac_Comment;
    }

    public void setSac_Comment(String sac_Comment) {
        Sac_Comment = sac_Comment;
    }

    public String getSac_CreatedAt() {
        return Sac_CreatedAt;
    }

    public void setSac_CreatedAt(String sac_CreatedAt) {
        Sac_CreatedAt = sac_CreatedAt;
    }


// user detail
    public String getUse_Id() {
        return Use_Id;
    }

    public void setUse_Id(String use_Id) {
        Use_Id = use_Id;
    }

    public String getUse_Name() {
        return Use_Name;
    }

    public void setUse_Name(String use_Name) {
        Use_Name = use_Name;
    }

    public String getUse_Type() {
        return Use_Type;
    }

    public void setUse_Type(String use_Type) {
        Use_Type = use_Type;
    }

    public String getUse_Mobile_No() {
        return Use_Mobile_No;
    }

    public void setUse_Mobile_No(String use_Mobile_No) {
        Use_Mobile_No = use_Mobile_No;
    }

    public String getUse_Status() {
        return Use_Status;
    }

    public void setUse_Status(String use_Status) {
        Use_Status = use_Status;
    }

    public String getUse_Image() {
        return Use_Image;
    }

    public void setUse_Image(String use_Image) {
        Use_Image = use_Image;
    }
}
