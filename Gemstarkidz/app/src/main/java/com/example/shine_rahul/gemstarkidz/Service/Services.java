package com.example.shine_rahul.gemstarkidz.Service;


public class Services {



    public static final String URL = "http://birdfly.in/GemStarKidz/api/";
    public static final String IMAGES_PATH = "http://birdfly.in/GemStarKidz/public/images/";
    public static final String REGISTRATION = URL + "sign_Up";
    public static final String OTP = "https://api.textlocal.in/send/";
    public static final String LOGIN = URL + "sign_In";
    public static final String UPDATE_PROFILE = URL + "profile_Update";
    public static final String PARENT_CHAT_LIST = URL + "chat_parent_List";

    //homework teacher
    public static final String HOMEWORK_CLASS_SECTION_LIST_SCREEN = URL + "class_Section_List";
    public static final String HOMEWORK_CREATE_HOMEWORK = URL + "create_Homework";
    public static final String HOMEWORK_LIST = URL + "homework_List";

    //social teacher
    public static final String SOCIAL_SCREEN = URL + "create_Activity";
    public static final String SOCIAL_SCREEN_LIST = URL + "activity_List";
    public static final String SOCIAL_SCREEN_LIKE = URL + "activity_Like";
    public static final String SOCIAL_SCREEN_COMMENT_LIST = URL + "comment_List";
    public static final String SOCIAL_SCREEN_POST_COMMENT = URL + "activity_Comment";
    public static final String SOCIAL_SCREEN_TAG_LIST = URL + "tag_list";
    public static final String SOCIAL_SCREEN_REPORT = URL + "report";

    //social parent
    public static final String SOCIAL_SCREEN_LIST_PARENT = URL + "parents_activity_List";

    //driver
    public static final String DRIVER_GET_LAT_LONG = URL + "driver_Routes";

    public static final String EVENT_LIST = URL + "event_List";
    public static final String USER_ACCEPT_EVENT = URL + "event_accept_profile";
    public static final String EVENT_ADD = URL + "create_Event";
    public static final String EVENT_UPDATE = URL + "update_Event";
    public static final String EVENT_ACCEPT = URL + "event_Accept";
    public static final String ATTENDANCE_LIST = URL + "attendence_List";
    public static final String ACCEPT_DECLINE_EVENT = URL + "accept_decline_event";
    public static final String ATTENDANCE_SUBMIT = URL + "create_Attendence";
    public static final String PRESENT_ABSENT_LIST = URL + "absent_present_List";


    //chat list
    public static final String CHAT_LIST = URL + "chat_user_List";



}
