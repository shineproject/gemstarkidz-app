package com.example.shine_rahul.gemstarkidz.Model;

/**
 * Created by Shine-Rahul on 10/3/2018.
 */

public class TeacherReportModel {

    String Std_Id;
    String Std_Gr_No;
    String Std_Name;
    String Std_Image;
    int chart;
    String Cla_Class;
    String Cla_Section;

    public String getStd_Id() {
        return Std_Id;
    }

    public void setStd_Id(String std_Id) {
        Std_Id = std_Id;
    }

    public String getStd_Gr_No() {
        return Std_Gr_No;
    }

    public void setStd_Gr_No(String std_Gr_No) {
        Std_Gr_No = std_Gr_No;
    }

    public String getStd_Name() {
        return Std_Name;
    }

    public void setStd_Name(String std_Name) {
        Std_Name = std_Name;
    }

    public String getStd_Image() {
        return Std_Image;
    }

    public void setStd_Image(String std_Image) {
        Std_Image = std_Image;
    }

    public String getCla_Class() {
        return Cla_Class;
    }

    public void setCla_Class(String cla_Class) {
        Cla_Class = cla_Class;
    }

    public String getCla_Section() {
        return Cla_Section;
    }

    public void setCla_Section(String cla_Section) {
        Cla_Section = cla_Section;
    }

    public int getChart() {
        return chart;
    }

    public void setChart(int chart) {
        this.chart = chart;
    }
}
