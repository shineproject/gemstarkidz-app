package com.example.shine_rahul.gemstarkidz.Fragment;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.shine_rahul.gemstarkidz.Adapter.PostSocialImagePager;
import com.example.shine_rahul.gemstarkidz.CommentParentActivity;
import com.example.shine_rahul.gemstarkidz.Model.ParentSocialModel;
import com.example.shine_rahul.gemstarkidz.Model.ParentSocialModelImage;
import com.example.shine_rahul.gemstarkidz.Model.TeacherSocialModel;
import com.example.shine_rahul.gemstarkidz.Model.TeacherSocialTagList;
import com.example.shine_rahul.gemstarkidz.R;
import com.example.shine_rahul.gemstarkidz.Service.Services;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class SocialFragment extends android.app.Fragment {

    TextView txt_social_social;
    TextView txt_social_chat;
    TextView txt_social_event;
    TextView txt_social_attendance;
    TextView txt_social_readmore;
    ArrayList<ParentSocialModel> data;
    ArrayList<ParentSocialModelImage> dataImage;
    private ArrayList<TeacherSocialTagList> getTagList;
    SocialAdapter socialAdapter;
    RecyclerView rv_social;
    TeacherTagListAdapter teacherTagListAdapter;
    TextView txt_edit_toolbar;
    ImageView img_search, img_close;
    EditText edt_search;
    TextView txt_menu_title;

    public static int MAX_LINES = 2;

    String username = "", user_type = "", user_phone = "", user_id = "", user_email = "", user_image = "", user_reg_type = "";

    String resultPostDetail = "";

    private ArrayList<Uri> mArrayUri;
    private ArrayList<String> filePath;

    int height, width;
    public static String post_id = "", resultTagDetail = "";

    public SocialFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        txt_edit_toolbar = (TextView) getActivity().findViewById(R.id.txt_edit_toolbar);
        img_search = (ImageView) getActivity().findViewById(R.id.img_search);
        img_close = (ImageView) getActivity().findViewById(R.id.img_close);
        edt_search = (EditText) getActivity().findViewById(R.id.edt_search);
        txt_menu_title = (TextView) getActivity().findViewById(R.id.txt_menu_title);

        SharedPreferences splogin = getActivity().getSharedPreferences("login", 0);
        username = splogin.getString("username", "");
        user_type = splogin.getString("type", "");
        user_phone = splogin.getString("user_phone", "");
        user_id = splogin.getString("user_id", "");
        user_email = splogin.getString("user_email", "");
        user_image = splogin.getString("user_image", "");
        user_reg_type = splogin.getString("user_reg_type", "");

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;

        View view = inflater.inflate(R.layout.fragment_social, container, false);
        rv_social = (RecyclerView) view.findViewById(R.id.rv_social);
        txt_social_readmore = (TextView) view.findViewById(R.id.txt_social_readmore);
        txt_social_social = (TextView) view.findViewById(R.id.txt_social_social);
        txt_social_chat = (TextView) view.findViewById(R.id.txt_social_chat);
        txt_social_event = (TextView) view.findViewById(R.id.txt_social_event);
        txt_social_attendance = (TextView) view.findViewById(R.id.txt_social_attendance);
        txt_social_social.setTextColor(getResources().getColor(R.color.colorTheme));
        txt_edit_toolbar.setVisibility(View.GONE);
        txt_menu_title.setVisibility(View.VISIBLE);
        txt_menu_title.setText("Social");
        img_search.setVisibility(View.VISIBLE);
        edt_search.setVisibility(View.GONE);
        img_close.setVisibility(View.GONE);

        new AsyncTaskRunnerGetPostData().execute();

        Typeface style = Typeface.createFromAsset(getActivity().getAssets(), "Raleway-Bold.ttf");
        txt_social_social.setTypeface(style);
        txt_social_chat.setTypeface(style);
        txt_social_event.setTypeface(style);
        txt_social_attendance.setTypeface(style);

        img_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt_menu_title.setVisibility(View.GONE);
                img_search.setVisibility(View.GONE);
                edt_search.setVisibility(View.VISIBLE);
                img_close.setVisibility(View.VISIBLE);
                edt_search.setText("");
                edt_search.requestFocus();
                edt_search.setFocusable(true);
            }
        });

        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt_menu_title.setVisibility(View.VISIBLE);
                txt_menu_title.setText("Social");
                img_search.setVisibility(View.VISIBLE);
                edt_search.setVisibility(View.GONE);
                img_close.setVisibility(View.GONE);
            }
        });

        txt_social_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt_social_social.setTextColor(getResources().getColor(R.color.colorText));
                txt_social_chat.setTextColor(getResources().getColor(R.color.colorTheme));
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        android.app.Fragment fragment = new ParentChatFragment();
                        FragmentManager fragmentManager = getActivity().getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.content, fragment);
                        fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commit();
                        img_search.setVisibility(View.VISIBLE);
                        txt_menu_title.setText("Chat");
                    }
                }, 100);
            }
        });

        txt_social_event.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt_social_social.setTextColor(getResources().getColor(R.color.colorText));
                txt_social_event.setTextColor(getResources().getColor(R.color.colorTheme));
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        android.app.Fragment fragment = new EventFragment();
                        FragmentManager fragmentManager = getActivity().getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.content, fragment);
                        fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commit();
                        img_search.setVisibility(View.VISIBLE);
                        txt_menu_title.setText("Event");
                    }
                }, 100);
            }
        });

        txt_social_attendance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt_social_social.setTextColor(getResources().getColor(R.color.colorText));
                txt_social_attendance.setTextColor(getResources().getColor(R.color.colorTheme));
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        android.app.Fragment fragment = new AttendanceFragment();
                        FragmentManager fragmentManager = getActivity().getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.content, fragment);
                        fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commit();
                        img_search.setVisibility(View.INVISIBLE);
                        txt_menu_title.setText("Attendance Report");
                    }
                }, 100);
            }
        });

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                Log.i("HELLO", "keyCode: " + keyCode);
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                    getActivity().finish();
//                    Log.i("HELLO", "onKey Back listener is working!!!");
//                    getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    return true;
                }
                return false;
            }

        });

        edt_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {

                    socialAdapter.filter(edt_search.getText().toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        return view;
    }

    private class AsyncTaskRunnerGetPostData extends AsyncTask<String, String, String> {

        private ProgressDialog Dialog = new ProgressDialog(getActivity());

        @Override
        protected String doInBackground(String... params) {

            MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
            builder.addFormDataPart("user_id", user_id);
            RequestBody requestBody = builder.build();

            try {
                OkHttpClient client = new OkHttpClient.Builder().build();
                Request request = new Request.Builder()
                        .url(Services.SOCIAL_SCREEN_LIST_PARENT)
                        .post(requestBody)
                        .build();
                Response resp = client.newCall(request).execute();
                resultPostDetail = resp.body().string();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            Dialog.dismiss();
            GetPostData(resultPostDetail);
            super.onPostExecute(s);
        }

        @Override
        protected void onPreExecute() {
            Dialog.show();
            Dialog.setMessage("Please Wait....");
            Dialog.setCancelable(false);
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
        }

    }

    private void GetPostData(String response) {

        ParentSocialModel socialModel;
        ParentSocialModelImage socialModelImage;
        data = new ArrayList<>();
        dataImage = new ArrayList<>();

        if (!response.equals("")) {
            try {
                JSONObject jsobjectcategory = new JSONObject(response);
                String message = jsobjectcategory.getString("message");
                // Updated upstream
                String error = jsobjectcategory.getString("error");

                if (error.equals("200")) {

                    JSONArray jsonArray = jsobjectcategory.getJSONArray("data");
                    JSONObject jsonObject = null;
                    for (int i = 0; i < jsonArray.length(); i++) {
                        jsonObject = jsonArray.getJSONObject(i);

                        socialModel = new ParentSocialModel();

                        JSONArray jsonArrayD = jsonObject.getJSONArray("soa_images");
                        for (int j = 0; j < jsonArrayD.length(); j++) {
                            JSONObject jsonObjectD = jsonArrayD.getJSONObject(j);
                            socialModelImage = new ParentSocialModelImage();
                            socialModelImage.setSod_Id(jsonObjectD.getString("Sod_Id"));
                            socialModelImage.setSod_Use_Id(jsonObjectD.getString("Sod_Use_Id"));
                            socialModelImage.setSod_Soa_Id(jsonObjectD.getString("Sod_Soa_Id"));
                            socialModelImage.setSod_Uploadtype(jsonObjectD.getString("Sod_Uploadtype"));
                            socialModelImage.setSod_Img_Video(jsonObjectD.getString("Sod_Img_Video"));
                            socialModelImage.setSod_CreatedAt(jsonObjectD.getString("Sod_CreatedAt"));
                            socialModelImage.setSod_UpdatedAt(jsonObjectD.getString("Sod_UpdatedAt"));
                            dataImage.add(socialModelImage);
                        }

                        socialModel.setSoa_Id(jsonObject.getString("Soa_Id"));
                        socialModel.setSoa_Use_Id(jsonObject.getString("Soa_Use_Id"));
                        socialModel.setSoa_Comment(jsonObject.getString("Soa_Comment"));
                        socialModel.setSoa_Title(jsonObject.getString("Soa_Title"));
                        socialModel.setSoa_User_Type(jsonObject.getString("Soa_User_Type"));
                        socialModel.setSoa_Status(jsonObject.getString("Soa_Status"));
                        socialModel.setSoa_CreatedBy(jsonObject.getString("Soa_CreatedBy"));
                        socialModel.setSoa_CreatedAt(jsonObject.getString("Soa_CreatedAt"));
                        socialModel.setSoa_UpdateBy(jsonObject.getString("Soa_UpdatedBy"));
                        socialModel.setSoa_UpdatedAt(jsonObject.getString("Soa_UpdatedAt"));


                        if (jsonObject.has("soa_like")) {
                            JSONArray jsonArrayL = jsonObject.getJSONArray("soa_like");
                            try {
                                if (jsonArrayL != null) {
                                    if (jsonArrayL.length() > 0) {
                                        for (int l = 0; l < jsonArrayL.length(); l++) {
                                            JSONObject jsonObjectL = jsonArrayL.getJSONObject(l);
                                            socialModel.setSal_Like(jsonObjectL.getString("Sal_Like"));
                                        }
                                    } else {
                                        socialModel.setSal_Like("");
                                    }
                                } else {
                                    socialModel.setSal_Like("");
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            socialModel.setSal_Like("");
                        }

                        data.add(socialModel);
                    }

                    socialAdapter = new SocialAdapter(getActivity(), data, dataImage, width, user_id);
                    rv_social.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
                    rv_social.setAdapter(socialAdapter);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

        }
    }

    class SocialAdapter extends RecyclerView.Adapter<SocialAdapter.ViewHolder> {

        private Activity context;
        private ArrayList<ParentSocialModel> dataPost;
        private ArrayList<ParentSocialModel> dataPost1 = new ArrayList<ParentSocialModel>();
        private ArrayList<ParentSocialModelImage> dataPostImage;
        private boolean isSelect = false;
        private ArrayList<String> imageList;
        PostSocialImagePager postSocialImagePager;
        int width;
        String uid = "", resultPostLike = "";
        public String soc_id = "", image = "";
        public int flag = 0, positionV = 0;
        private Date dateNew;
        private Date datefi;

        public SocialAdapter(Activity context, ArrayList<ParentSocialModel> dataPost, ArrayList<ParentSocialModelImage> dataPostImage, int width, String uid) {
            this.context = context;
            this.dataPost = dataPost;
            this.dataPost1.addAll(dataPost);
            this.dataPostImage = dataPostImage;
            this.width = width;
            this.uid = uid;
        }

        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.parent_social_list, null);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
            final ParentSocialModel teacherSocialModel = dataPost.get(position);
            //   final TeacherSocialModelImage teacherSocialModelImage = dataPostImage.get(position);
            if (!teacherSocialModel.getSoa_Title().equals("null")) {
                holder.txt_teacher_social_title.setText(teacherSocialModel.getSoa_Title());
            }
            //  holder.txt_teacher_social_time.setText(teacherSocialModel.getSoa_CreatedAt());
            if (!teacherSocialModel.getSoa_Comment().equals("null")) {
                holder.txt_teacher_social_description.setText(teacherSocialModel.getSoa_Comment());
            }

            if (holder.txt_teacher_social_description.getText().toString().length() > 130) {
                holder.txt_teacher_social_readmore.setVisibility(View.VISIBLE);
            }

            holder.cardView.setMinimumWidth(width);
            holder.layoutL.setMinimumWidth(width);

            try {
                String strDate = teacherSocialModel.getSoa_CreatedAt();
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                Date c = Calendar.getInstance().getTime();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String formattedDate = df.format(c);

                try {
                    dateNew = format.parse(strDate);
                    datefi = df.parse(formattedDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                long different = datefi.getTime() - dateNew.getTime();
                long secondsInMilli = 1000;
                long minutesInMilli = secondsInMilli * 60;
                long hoursInMilli = minutesInMilli * 60;
                long daysInMilli = hoursInMilli * 24;

                long elapsedDays = different / daysInMilli;
                different = different % daysInMilli;
                long elapsedHours = different / hoursInMilli;
                different = different % hoursInMilli;
                long elapsedMinutes = different / minutesInMilli;
                different = different % minutesInMilli;
                long elapsedSeconds = different / secondsInMilli;

                if (elapsedDays >= 1) {
                    holder.txt_teacher_social_time.setText(String.valueOf(elapsedDays) + " days ago");
                } else if (elapsedHours <= 24) {
                    holder.txt_teacher_social_time.setText(String.valueOf(elapsedHours) + " hours ago");
                } else if (elapsedMinutes <= 60) {
                    holder.txt_teacher_social_time.setText(String.valueOf(elapsedMinutes) + " minute ago");
                } else {
                    holder.txt_teacher_social_time.setText("Just Now");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                imageList = new ArrayList<>();
                for (int i = 0; i < dataPostImage.size(); i++) {
                    if (teacherSocialModel.getSoa_Id().equals(dataPostImage.get(i).getSod_Soa_Id())) {
                        imageList.add(dataPostImage.get(i).getSod_Img_Video());
                    }
                }

                if (imageList.size() > 0) {
                    holder.layoutimage.setVisibility(View.VISIBLE);
                    postSocialImagePager = new PostSocialImagePager(imageList, context,teacherSocialModel.getSoa_Id(),teacherSocialModel.getSal_Like());
                    holder.viewPagerImage.setAdapter(postSocialImagePager);
                } else {
                    holder.layoutimage.setVisibility(View.GONE);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

//            holder.img_teacher_social_tag.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    post_id = teacherSocialModel.getSoa_Id();
//                    new AsyncTaskRunnerGetTagList().execute();
//                }
//            });

            holder.img_teacher_social_comment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, CommentParentActivity.class);
                    intent.putExtra("uid", uid);
                    intent.putExtra("soa_id", teacherSocialModel.getSoa_Id());
                    context.startActivity(intent);
                }
            });

            holder.img_teacher_social_share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    imageList = new ArrayList<>();
                    for (int i = 0; i < dataPostImage.size(); i++) {
                        if (teacherSocialModel.getSoa_Id().equals(dataPostImage.get(i).getSod_Soa_Id())) {
                            imageList.add(dataPostImage.get(i).getSod_Img_Video());
                        }
                    }

                    positionV = holder.viewPagerImage.getCurrentItem();
                    image = Services.IMAGES_PATH + "social/" + imageList.get(positionV);

                    try {
                        Uri uri = Uri.parse(image);
                        shareImage(String.valueOf(uri), context);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            if (teacherSocialModel.getSal_Like().equals("1")) {
                holder.img_teacher_social_like.setImageResource(R.mipmap.hand);
            } else {
                holder.img_teacher_social_like.setImageResource(R.mipmap.like_icon);
            }

            holder.li_teacherlike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (teacherSocialModel.getSal_Like().equals("1")) {
                        holder.img_teacher_social_like.setImageResource(R.mipmap.like_icon);
                        flag = 0;
                        soc_id = teacherSocialModel.getSoa_Id();
                        new AsyncTaskRunnerLikePostData().execute();
                    } else {
                        holder.img_teacher_social_like.setImageResource(R.mipmap.hand);
                        flag = 1;
                        soc_id = teacherSocialModel.getSoa_Id();
                        new AsyncTaskRunnerLikePostData().execute();
                    }
                }
            });

            holder.txt_teacher_social_readmore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (holder.txt_teacher_social_readmore.getText().equals("View More...")) {
                        holder.txt_teacher_social_readmore.setText("View Less...");
                        holder.txt_teacher_social_description.setMaxLines(Integer.MAX_VALUE);
                    } else {
                        holder.txt_teacher_social_readmore.setText("View More...");
                        holder.txt_teacher_social_description.setMaxLines(2);
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return dataPost.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            TextView txt_teacher_social_title, txt_teacher_social_time, txt_teacher_social_description, txt_teacher_social_readmore,
                    txt_teacher_count;
            ImageView img_teacher_social_like, img_teacher_social_profile, img_teacher_social_comment,
                    img_teacher_social_share;
                    //img_teacher_social_tag;
            ViewPager viewPagerImage;
            CardView cardView;
            LinearLayout layoutL, layoutimage, li_teacherlike;

            public ViewHolder(View itemView) {
                super(itemView);
                viewPagerImage = itemView.findViewById(R.id.viewPagerImage);
                cardView = itemView.findViewById(R.id.cv);
                layoutL = itemView.findViewById(R.id.layoutL);
                layoutimage = itemView.findViewById(R.id.layoutimage);
                txt_teacher_social_title = (TextView) itemView.findViewById(R.id.txt_teacher_social_title);
                txt_teacher_social_time = (TextView) itemView.findViewById(R.id.txt_teacher_social_time);
                txt_teacher_social_description = (TextView) itemView.findViewById(R.id.txt_teacher_social_description);
                txt_teacher_social_readmore = (TextView) itemView.findViewById(R.id.txt_teacher_social_readmore);
                li_teacherlike = (LinearLayout) itemView.findViewById(R.id.li_teacherlike);
                img_teacher_social_like = (ImageView) itemView.findViewById(R.id.img_teacher_social_like);
                txt_teacher_count = (TextView) itemView.findViewById(R.id.txt_teacher_count);
                img_teacher_social_profile = (ImageView) itemView.findViewById(R.id.img_teacher_social_profile);
                img_teacher_social_comment = (ImageView) itemView.findViewById(R.id.img_teacher_social_comment);
                img_teacher_social_share = (ImageView) itemView.findViewById(R.id.img_teacher_social_share);
             //  img_teacher_social_tag = (ImageView) itemView.findViewById(R.id.img_teacher_social_tag);
            }
        }

        private class AsyncTaskRunnerLikePostData extends AsyncTask<String, String, String> {

            private ProgressDialog Dialog = new ProgressDialog(context);

            @Override
            protected String doInBackground(String... params) {

                MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
                builder.addFormDataPart("user_id", uid);
                builder.addFormDataPart("soa_id", soc_id);
                builder.addFormDataPart("flag", String.valueOf(flag));
                builder.addFormDataPart("user_type", "4");

                RequestBody requestBody = builder.build();

                try {
                    OkHttpClient client = new OkHttpClient.Builder().build();
                    Request request = new Request.Builder()
                            .url(Services.SOCIAL_SCREEN_LIKE)
                            .post(requestBody)
                            .build();

                    Response resp = client.newCall(request).execute();
                    resultPostLike = resp.body().string();

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return "";
            }

            @Override
            protected void onPostExecute(String s) {
                Dialog.dismiss();
                GetPostData(resultPostLike);
                super.onPostExecute(s);
            }

            @Override
            protected void onPreExecute() {
                Dialog.show();
                Dialog.setMessage("Please Wait....");
                Dialog.setCancelable(false);
                super.onPreExecute();
            }

            @Override
            protected void onProgressUpdate(String... values) {
                super.onProgressUpdate(values);
            }
        }

        private void GetPostData(String response) {

            TeacherSocialModel teacherSocialModel;

            if (!response.equals("")) {
                try {

                    JSONObject jsobjectcategory = new JSONObject(response);
                    String message = jsobjectcategory.getString("message");
                    // Updated upstream
                    String error = jsobjectcategory.getString("error");

                    if (error.equals("200")) {
                        // Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                        new AsyncTaskRunnerGetPostData().execute();
                    } else {

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {

            }
        }

        public void filter(String charText) {
            charText = charText.toLowerCase(Locale.getDefault());
            dataPost.clear();
            if (charText.length() == 0) {
                dataPost.addAll(dataPost1);
            } else {
                for (ParentSocialModel wp : dataPost1) {
                    if (wp.getSoa_Title().toLowerCase(Locale.getDefault()).contains(charText)|| wp.getSoa_Comment().toLowerCase(Locale.getDefault()).contains(charText)) {
                        dataPost.add(wp);
                    }
                }
            }
            notifyDataSetChanged();
        }

    }

    static public void shareImage(String url, final Context context) {
        Picasso.with(context).load(url).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("image/*");
                i.putExtra(Intent.EXTRA_STREAM, getLocalBitmapUri(bitmap, context));
                context.startActivity(Intent.createChooser(i, "Share Image"));
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
            }
        });
    }

    static public Uri getLocalBitmapUri(Bitmap bmp, Context context) {
        Uri bmpUri = null;
        try {
            File file = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "share_image_" + System.currentTimeMillis() + ".png");
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }

    private class AsyncTaskRunnerGetTagList extends AsyncTask<String, String, String> {

        private ProgressDialog Dialog = new ProgressDialog(getActivity());

        @Override
        protected String doInBackground(String... params) {

            MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
            builder.addFormDataPart("soa_id", post_id);
            RequestBody requestBody = builder.build();

            try {
                OkHttpClient client = new OkHttpClient.Builder().build();
                Request request = new Request.Builder()
                        .url(Services.SOCIAL_SCREEN_TAG_LIST)
                        .post(requestBody)
                        .build();
                Response resp = client.newCall(request).execute();
                resultTagDetail = resp.body().string();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            Dialog.dismiss();
            GetTagData(resultTagDetail);
            super.onPostExecute(s);
        }

        @Override
        protected void onPreExecute() {
            Dialog.show();
            Dialog.setMessage("Please Wait....");
            Dialog.setCancelable(false);
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
        }

    }

    private void GetTagData(String response) {

        TeacherSocialTagList teacherSocialTagList;
        getTagList = new ArrayList<>();

        if (!response.equals("")) {
            try {
                JSONObject jsobjectcategory = new JSONObject(response);
                String message = jsobjectcategory.getString("message");
                // Updated upstream
                String error = jsobjectcategory.getString("error");

                if (error.equals("200")) {

                    JSONArray jsonArray = jsobjectcategory.getJSONArray("data");
                    JSONObject jsonObject = null;


                    for (int i = 0; i < jsonArray.length(); i++) {
                        jsonObject = jsonArray.getJSONObject(i);

                        teacherSocialTagList = new TeacherSocialTagList();

                        JSONArray jsonArrayD = jsonObject.getJSONArray("tag_student");
                        for (int j = 0; j < jsonArrayD.length(); j++) {
                            JSONObject jsonObjectD = jsonArrayD.getJSONObject(j);
                            teacherSocialTagList.setStd_Id(jsonObjectD.getString("Std_Id"));
                            teacherSocialTagList.setStd_Cla_Id(jsonObjectD.getString("Std_Cla_Id"));
                            teacherSocialTagList.setStd_Gr_No(jsonObjectD.getString("Std_Gr_No"));
                            teacherSocialTagList.setStd_Image(jsonObjectD.getString("Std_Image"));
                            teacherSocialTagList.setStd_Name(jsonObjectD.getString("Std_Name"));
                        }

                        teacherSocialTagList.setTag_Id(jsonObject.getString("Tag_Id"));
                        teacherSocialTagList.setTag_Flag(jsonObject.getString("Tag_Flag"));
                        teacherSocialTagList.setTag_Soa_Id(jsonObject.getString("Tag_Soa_Id"));
                        teacherSocialTagList.setTag_Use_Id(jsonObject.getString("Tag_Use_Id"));

                        getTagList.add(teacherSocialTagList);
                    }

                    teacherTagListAdapter = new TeacherTagListAdapter(getActivity(), getTagList);

                    if (getTagList.size() > 0) {
                        showTagListDialog();
                    }

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

        }
    }

    private void showTagListDialog() {
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View view1 = layoutInflater.inflate(R.layout.tag_student, null);
        final AlertDialog.Builder alertdialog = new AlertDialog.Builder(getActivity());
        alertdialog.setView(view1);
        final AlertDialog dialog = alertdialog.create();
        final ImageView btn_tag_entire_class = (ImageView) view1.findViewById(R.id.btn_selected_tag_student_submit);
        GridView gv_tag_select_student = (GridView) view1.findViewById(R.id.gv_tag_select_student);
        btn_tag_entire_class.setVisibility(View.GONE);
        gv_tag_select_student.setAdapter(teacherTagListAdapter);

        dialog.show();

    }

    private class TeacherTagListAdapter extends BaseAdapter {

        private Activity activity;
        private ArrayList<TeacherSocialTagList> getTagListData;
        private LayoutInflater inflater = null;

        public TeacherTagListAdapter(Activity activity, ArrayList<TeacherSocialTagList> getTagList) {
            this.activity = activity;
            this.getTagListData = getTagList;
        }

        @Override
        public int getCount() {
            return getTagListData.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, final View convertView, ViewGroup parent) {
            View view = convertView;
            if (convertView == null) {
                inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.teacher_class_student_list, null);
            }
            try {
                final CircleImageView img_teacher_class_student_list;
                TextView txt_student_name;
                txt_student_name = (TextView) view.findViewById(R.id.txt_student_name);
                img_teacher_class_student_list = (CircleImageView) view.findViewById(R.id.img_teacher_class_student_list);

                final TeacherSocialTagList teacherSocialTagList = getTagListData.get(position);
                txt_student_name.setText(teacherSocialTagList.getStd_Name());

                Picasso.with(activity)
                        .load(Services.IMAGES_PATH + "" + teacherSocialTagList.getStd_Image())
                        .error(R.drawable.logo)
                        .into(img_teacher_class_student_list);

                view.setOnClickListener(new View.OnClickListener() {
                    @SuppressLint("NewApi")
                    @Override
                    public void onClick(View v) {

//                        if (img_teacher_class_student_list.getBorderColor() == context.getResources().getColor(R.color.colorTheme)) {
//                            img_teacher_class_student_list.setBorderColor(context.getResources().getColor(R.color.colorTheme));
//                            img_teacher_class_student_list.setBorderWidth(10);
//
//                        } else {
//                            img_teacher_class_student_list.setBorderColor(context.getResources().getColor(R.color.colorTheme));
//                            img_teacher_class_student_list.setBorderWidth(10);
//                        }
                    }

                });
            } catch (Exception e) {
                e.printStackTrace();
                Log.d("Error", e.getMessage().toString());
            }
            return view;
        }
    }
}
