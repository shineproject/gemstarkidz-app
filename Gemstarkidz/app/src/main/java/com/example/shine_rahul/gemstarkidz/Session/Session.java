package com.example.shine_rahul.gemstarkidz.Session;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by ${Vrund} on 12/27/2017.
 */

public class Session {

    private static Session instance = null;
    Context context;
    SharedPreferences.Editor editor;

    // Context
    Context _context;


    public static Session getSession(Context context) {
        if (instance == null) {
            instance = new Session(context);
        }
        return instance;
    }

    protected Session(Context context) {
        this.context = context;
    }


    public String getUSER_ID() {
        return Utils.getShared(context, Utils.USER_ID, "");
    }

    public void setUSER_ID(String USER_ID) {
        Utils.setShared(context, Utils.USER_ID, USER_ID);
    }

    public String getUSER_NAME() {
        return Utils.getShared(context, Utils.USER_NAME, "");
    }

    public void setUSER_NAME(String USER_NAME) {
        Utils.setShared(context, Utils.USER_NAME, USER_NAME);
    }

    public String getUSER_EMAIL() {
        return Utils.getShared(context, Utils.USER_EMAIL, "");
    }

    public void setUSER_EMAIL(String USER_EMAIL) {
        Utils.setShared(context, Utils.USER_EMAIL, USER_EMAIL);
    }

    public String getUSER_MOBILE() {
        return Utils.getShared(context, Utils.USER_MOBILE, "");
    }

    public void setUSER_MOBILE(String USER_MOBILE) {
        Utils.setShared(context, Utils.USER_MOBILE, USER_MOBILE);
    }

    public String getUSER_ADDRESS() {
        return Utils.getShared(context, Utils.USER_ADDRESS, "");
    }

    public void setUSER_ADDRESS(String USER_ADDRESS) {
        Utils.setShared(context, Utils.USER_ADDRESS, USER_ADDRESS);
    }

    public String getUSER_PINCODE() {
        return Utils.getShared(context, Utils.USER_PINCODE, "");
    }

    public void setUSER_PINCODE(String USER_PINCODE) {
        Utils.setShared(context, Utils.USER_PINCODE, USER_PINCODE);
    }

    public String getUSER_CITY() {
        return Utils.getShared(context, Utils.USER_CITY, "");
    }

    public void setUSER_CITY(String USER_CITY) {
        Utils.setShared(context, Utils.USER_CITY, USER_CITY);
    }

    public String getUSER_GENDER() {
        return Utils.getShared(context, Utils.USER_GENDER, "");
    }

    public void setUSER_GENDER(String USER_GENDER) {
        Utils.setShared(context, Utils.USER_GENDER, USER_GENDER);
    }

    public String getUSER_BIRTHDATE() {
        return Utils.getShared(context, Utils.USER_BIRTHDATE, "");
    }

    public void setUSER_BIRTHDATE(String USER_BIRTHDATE) {
        Utils.setShared(context, Utils.USER_BIRTHDATE, USER_BIRTHDATE);
    }

    public String getLOGIN_CURRENT() {
        return Utils.getShared(context, Utils.LOGIN_CURRENT, "");
    }

    public void setLOGIN_CURRENT(String LOGIN_CURRENT) {
        Utils.setShared(context, Utils.LOGIN_CURRENT, LOGIN_CURRENT);
    }
}
