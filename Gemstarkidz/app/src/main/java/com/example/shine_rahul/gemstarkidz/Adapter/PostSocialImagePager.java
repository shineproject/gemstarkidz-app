package com.example.shine_rahul.gemstarkidz.Adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.net.Uri;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import com.example.shine_rahul.gemstarkidz.R;
import com.example.shine_rahul.gemstarkidz.Service.Services;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 10/3/2017.
 */
public class PostSocialImagePager extends PagerAdapter {

    private ArrayList<String> images;
    private Context activitys;
    LayoutInflater mLayoutInflater;

    public PostSocialImagePager(ArrayList<String> imageList, Activity context) {
        super();
        this.images = imageList;
        this.activitys = context;
        mLayoutInflater = (LayoutInflater) activitys.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        View itemView = mLayoutInflater.inflate(R.layout.layout_image_social_pager, container, false);
        ImageView imageView = itemView.findViewById(R.id.imageViewV);
        ImageView imageViewP = itemView.findViewById(R.id.imageViewP);

        try {
            if (images.get(position).contains(".mp4")) {
                Picasso.with(activitys)
                        .load(R.drawable.default_video)
                        .into(imageView);
                imageViewP.setVisibility(View.VISIBLE);

                imageViewP.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Uri videoUri = Uri.parse(Services.IMAGES_PATH + "social/" + images.get(position));
                        showTagListDialog(videoUri);
                    }
                });

            } else {
                Picasso.with(activitys)
                        .load(Services.IMAGES_PATH + "social/" + images.get(position))
                        .into(imageView);
                imageViewP.setVisibility(View.GONE);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        //   container.removeView(adpt.img.get(position));
        ((ViewPager) container).removeView((LinearLayout) object);
        //  container.removeView(images.get(position));
        // container.removeView();
    }

    private void showTagListDialog(Uri video) {

        LayoutInflater layoutInflater = LayoutInflater.from(activitys);
        View view1 = layoutInflater.inflate(R.layout.layout_video_play, null);
        final AlertDialog.Builder alertdialog = new AlertDialog.Builder(activitys);
        alertdialog.setView(view1);

        final AlertDialog dialog = alertdialog.create();

        VideoView videoView = (VideoView) view1.findViewById(R.id.videoView1);
        MediaController mediaController= new MediaController(activitys);
        mediaController.setAnchorView(videoView);

        Uri uri = video;

        videoView.setMediaController(mediaController);
        videoView.setVideoURI(uri);
        videoView.requestFocus();
        videoView.start();

        dialog.show();
    }
}
