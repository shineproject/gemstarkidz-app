package com.example.shine_rahul.gemstarkidz.Fragment;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.shine_rahul.gemstarkidz.Adapter.PaymentCardTypeAdapter;
import com.example.shine_rahul.gemstarkidz.Adapter.PaymentMonthAdapter;
import com.example.shine_rahul.gemstarkidz.Adapter.PaymentYearAdapter;
import com.example.shine_rahul.gemstarkidz.Model.PaymentModel;
import com.example.shine_rahul.gemstarkidz.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class PaymentFragment extends android.app.Fragment {
    TextView txt_edit_toolbar;
    TextView txt_payment_payment;
    Spinner sp_payment_cardtype;
    Spinner sp_payment_expire_month;
    Spinner sp_payment_expire_year;
    String[] Payment_Card_Type = {"Cradit Card", "Debit Card", "Visa", "Net Banking"};
    String[] Payment_Expire_Month = {"January", "February", "March", "April", "May", "June", "July", "Auguest", "September", "October", "November", "December"};
    String[] Payment_expire_Year = {"2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030"};
    ArrayList<PaymentModel> data;
    PaymentCardTypeAdapter paymentCardTypeAdapter;
    PaymentMonthAdapter paymentMonthAdapter;
    PaymentYearAdapter paymentYearAdapter;


    public PaymentFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_payment, container, false);
        txt_payment_payment = (TextView) view.findViewById(R.id.txt_payment_payment);
        sp_payment_cardtype = (Spinner) view.findViewById(R.id.sp_payment_cardtype);
        sp_payment_expire_month = (Spinner) view.findViewById(R.id.sp_payment_expire_month);
        sp_payment_expire_year = (Spinner) view.findViewById(R.id.sp_payment_expire_year);
        txt_edit_toolbar = (TextView) getActivity().findViewById(R.id.txt_edit_toolbar);
        txt_edit_toolbar.setVisibility(View.GONE);
        Typeface font1 = Typeface.createFromAsset(getActivity().getAssets(), "Raleway-SemiBold.ttf");
        txt_payment_payment.setTypeface(font1);

        data = new ArrayList<>();
        for (int i = 0; i < Payment_Card_Type.length; i++) {
            PaymentModel paymentModel = new PaymentModel();
            paymentModel.setCard_type(Payment_Card_Type[i]);
            data.add(paymentModel);
        }
        paymentCardTypeAdapter = new PaymentCardTypeAdapter(getActivity(), data);
        paymentCardTypeAdapter.notifyDataSetChanged();
        sp_payment_cardtype.setAdapter(paymentCardTypeAdapter);

        data = new ArrayList<>();
        for (int i = 0; i < Payment_Expire_Month.length; i++) {
            PaymentModel paymentModel = new PaymentModel();
            paymentModel.setExpire_month(Payment_Expire_Month[i]);
            data.add(paymentModel);
        }
        paymentMonthAdapter = new PaymentMonthAdapter(getActivity(), data);
        paymentMonthAdapter.notifyDataSetChanged();
        sp_payment_expire_month.setAdapter(paymentMonthAdapter);


        data = new ArrayList<>();
        for (int i = 0; i < Payment_expire_Year.length; i++) {
            PaymentModel paymentModel = new PaymentModel();
            paymentModel.setExpire_year(Payment_expire_Year[i]);
            data.add(paymentModel);
        }
        paymentYearAdapter = new PaymentYearAdapter(getActivity(), data);
        paymentYearAdapter.notifyDataSetChanged();
        sp_payment_expire_year.setAdapter(paymentYearAdapter);

        return view;
    }

}
