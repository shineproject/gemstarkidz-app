package com.example.shine_rahul.gemstarkidz.Fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shine_rahul.gemstarkidz.Adapter.EventAdapter;
import com.example.shine_rahul.gemstarkidz.EventUpdateActivity;
import com.example.shine_rahul.gemstarkidz.Helper.RecyclerItemClickListener;
import com.example.shine_rahul.gemstarkidz.Model.EventModel;
import com.example.shine_rahul.gemstarkidz.R;
import com.example.shine_rahul.gemstarkidz.Service.Services;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class TeacherEventListFragment extends android.app.Fragment {


    public static ImageView img_event;
    public static TextInputEditText edt_event_title;
    public static TextInputEditText edt_start_date;
    public static TextInputEditText edt_end_date;
    public static TextInputEditText edt_event_Description;
    int GALLERY_PICK = 101, CAMERA_PICK = 132;
    String user_id, user_image = "", title, start_date, end_date, description, responseimage, class_id, branch_id, section;
    Bitmap bitmap;

    TextView txt_event_social;
    TextView txt_event_chat;
    TextView txt_event_event;
    TextView txt_event_attendance;
    CalendarView event_calender;
    RecyclerView rv_event_list;
    ArrayList<EventModel> data = new ArrayList<>();
    EventModel eventModel;
    EventAdapter eventAdapter;
    ImageView calender_visible_parent;
    String Eve_Name, Eve_Date, Eve_Accept, Eve_Image, msg;
    JSONObject categories;
    String Eve_Id, Eve_Use_Id, Eve_user_Name, Eve_Start_Date, Eve_End_Date, Eve_Description, Eve_user_Image, Eve_user_Accept, accept_event, accept_event_id;
    SwipeRefreshLayout swiperefresh;
    String response_message;

    //search
    TextView txt_edit_toolbar, txt_teacher_event_edit;
    ImageView img_search, img_close;
    EditText edt_search;
    TextView txt_menu_title;

    public TeacherEventListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.event_list_fragment, container, false);

        txt_edit_toolbar = (TextView) getActivity().findViewById(R.id.txt_edit_toolbar);
        img_search = (ImageView) getActivity().findViewById(R.id.img_search);
        img_close = (ImageView) getActivity().findViewById(R.id.img_close);
        edt_search = (EditText) getActivity().findViewById(R.id.edt_search);
        txt_menu_title = (TextView) getActivity().findViewById(R.id.txt_menu_title);

        txt_event_social = (TextView) view.findViewById(R.id.txt_event_social);
        txt_event_chat = (TextView) view.findViewById(R.id.txt_event_chat);
        txt_event_event = (TextView) view.findViewById(R.id.txt_event_event);
        txt_event_attendance = (TextView) view.findViewById(R.id.txt_event_attendance);
        event_calender = (CalendarView) view.findViewById(R.id.event_calender);
        rv_event_list = (RecyclerView) view.findViewById(R.id.rv_event_list);
        txt_teacher_event_edit = (TextView) view.findViewById(R.id.txt_teacher_event_edit);
        calender_visible_parent = (ImageView) view.findViewById(R.id.calender_visible);
        swiperefresh = (SwipeRefreshLayout) view.findViewById(R.id.swiperefresh);
        event_calender.setVisibility(View.GONE);


        SharedPreferences splogin = getActivity().getSharedPreferences("login", 0);
        user_id = splogin.getString("user_id", "");
        class_id = splogin.getString("Cla_Id", "");
        branch_id = splogin.getString("Cla_Bra_Id", "");
        section = splogin.getString("user_section", "");


        txt_menu_title.setVisibility(View.VISIBLE);
        txt_menu_title.setText("Event");
        txt_edit_toolbar.setVisibility(View.GONE);
        img_search.setVisibility(View.VISIBLE);
        edt_search.setVisibility(View.GONE);
        img_close.setVisibility(View.GONE);
        new GetDetails().execute();


        txt_event_event.setTextColor(getResources().getColor(R.color.colorTheme));
        event_calender.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {

                String date = year + "-" + ((month + 1) > 9 ? (month + 1) : "0" + (month + 1)) + "-" +
                        (dayOfMonth > 9 ? dayOfMonth : "0" + dayOfMonth);
                eventAdapter.filter1(date);
                event_calender.setVisibility(View.GONE);
            }
        });

        Typeface style = Typeface.createFromAsset(getActivity().getAssets(), "Raleway-Bold.ttf");
        txt_event_social.setTypeface(style);
        txt_event_chat.setTypeface(style);
        txt_event_event.setTypeface(style);
        txt_event_attendance.setTypeface(style);

        SharedPreferences accept1 = getActivity().getSharedPreferences("accept", 0);
        accept_event = accept1.getString("accept_event", "");


        calender_visible_parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (event_calender.getVisibility() == View.GONE) {
                    event_calender.setVisibility(View.VISIBLE);
                } else {
                    event_calender.setVisibility(View.GONE);
                }
            }
        });

        rv_event_list.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Intent bundle = new Intent(getActivity(), EventUpdateActivity.class);
                        bundle.putExtra("Eve_Id", data.get(position).getId());
                        bundle.putExtra("Eve_CreatedBy", user_id);
                        bundle.putExtra("Eve_user_Name", data.get(position).getName());
                        bundle.putExtra("Eve_Start_Date", data.get(position).getDate());
                        bundle.putExtra("Eve_End_Date", data.get(position).getEnd_date());
                        bundle.putExtra("Eve_Description", data.get(position).getDescription());
                        bundle.putExtra("Eve_user_Image", data.get(position).getImage());
                        bundle.putExtra("Eve_user_Accept", Eve_user_Accept);
                        startActivity(bundle);
                    }
                })
        );

        txt_event_social.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt_event_event.setTextColor(getResources().getColor(R.color.colorText));
                txt_event_social.setTextColor(getResources().getColor(R.color.colorTheme));
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        android.app.Fragment fragment = new SocialFragment();
                        FragmentManager fragmentManager = getActivity().getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.content, fragment);
                        fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commit();
                        img_search.setVisibility(View.VISIBLE);
                        txt_menu_title.setText("Social");

                    }
                }, 100);

            }
        });
        txt_event_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt_event_event.setTextColor(getResources().getColor(R.color.colorText));
                txt_event_chat.setTextColor(getResources().getColor(R.color.colorTheme));
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        android.app.Fragment fragment = new ParentChatFragment();
                        FragmentManager fragmentManager = getActivity().getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.content, fragment);
                        fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commit();
                        img_search.setVisibility(View.VISIBLE);
                        txt_menu_title.setText("Chat");

                    }
                }, 100);

            }
        });
        txt_event_attendance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt_event_event.setTextColor(getResources().getColor(R.color.colorText));
                txt_event_attendance.setTextColor(getResources().getColor(R.color.colorTheme));
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        android.app.Fragment fragment = new AttendanceFragment();
                        FragmentManager fragmentManager = getActivity().getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.content, fragment);
                        fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commit();
                        img_search.setVisibility(View.INVISIBLE);
                        txt_menu_title.setText("Attendance Report");

                    }
                }, 100);

            }
        });


        txt_teacher_event_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editEvent();
            }
        });


        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                Log.i("HELLO", "keyCode: " + keyCode);
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                    android.app.Fragment someFragment = new TeacherSocialFragment();
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.content, someFragment); // give your fragment container id in first parameter
                    transaction.addToBackStack(null);  // if written, this transaction will be added to backstack
                    transaction.commit();
                    txt_menu_title.setText("Social ");
                    Typeface style = Typeface.createFromAsset(getActivity().getAssets(), "Raleway-SemiBold.ttf");
                    txt_menu_title.setTypeface(style);
                    return true;
                }
                return false;
            }

        });


        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                data.clear();
                swiperefresh.setRefreshing(true);
                new GetDetails().execute();
            }
        });
        img_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    img_search.setVisibility(View.GONE);
                    txt_menu_title.setVisibility(View.GONE);
                    edt_search.setVisibility(View.VISIBLE);
                    img_close.setVisibility(View.VISIBLE);
                    edt_search.setText("");
                    edt_search.requestFocus();
                    edt_search.setFocusable(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                img_close.setVisibility(View.GONE);
                edt_search.setVisibility(View.GONE);
                img_search.setVisibility(View.VISIBLE);
                txt_menu_title.setVisibility(View.VISIBLE);
                txt_menu_title.setText("Event");
                new GetDetails().execute();
                getActivity().overridePendingTransition(0, 0);

            }
        });

        edt_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                eventAdapter.filter(edt_search.getText().toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        return view;
    }

    public class GetDetails extends AsyncTask<String, Process, String> {

        String line, response;
        private ProgressDialog Dialog = new ProgressDialog(getActivity());

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Dialog.show();
            Dialog.setMessage("Please Wait....");
            Dialog.setCancelable(false);
        }

        @Override
        protected String doInBackground(String... params) {

            String url = Services.EVENT_LIST;

            try {
                URL url1 = new URL(url);
                URLConnection urlc = url1.openConnection();
                BufferedReader bfr = new BufferedReader(new InputStreamReader(urlc.getInputStream()));

                while ((line = bfr.readLine()) != null) {
                    Log.e("product list data", "in while postexecute" + line);
                    categories = new JSONObject(line);
                    msg = categories.getString("error");
                    response_message = categories.getString("message");

                    if (msg.equals("200")) {
                        JSONArray jsonArray = categories.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            eventModel = new EventModel();
                            Eve_Id = jsonObject.getString("Eve_Id");
                            Eve_Use_Id = jsonObject.getString("Eve_Use_Id");
                            Eve_user_Name = jsonObject.getString("Eve_Name");
                            Eve_Start_Date = jsonObject.getString("Eve_Date");
                            Eve_End_Date = jsonObject.getString("Eve_End_Date");
                            Eve_Description = jsonObject.getString("Eve_Description");
                            Eve_user_Image = jsonObject.getString("Eve_Image");
                            Eve_user_Accept = jsonObject.getString("Eve_Count");


                            if (Eve_user_Accept.equals("null")) {
                                eventModel.setPeople("0");
                            } else {
                                eventModel.setPeople(Eve_user_Accept);
                            }

                            eventModel.setId(Eve_Id);
                            eventModel.setName(Eve_user_Name);
                            eventModel.setDate(Eve_Start_Date);
                            eventModel.setImage(Eve_user_Image);
                            eventModel.setDescription(Eve_Description);
                            eventModel.setEnd_date(Eve_End_Date);
                            data.add(eventModel);
                        }

                    }

                    response = bfr.toString();
                    Log.e("response", response);
                }

            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            Dialog.dismiss();

            if (msg != null) {

                if (msg.equals("200")) {

                    eventAdapter = new EventAdapter(getActivity(), data);
                    rv_event_list.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayout.VERTICAL, false));
                    rv_event_list.setAdapter(eventAdapter);


                } else {
                    Toast.makeText(getActivity(), response_message, Toast.LENGTH_LONG).show();
                }
                swiperefresh.setRefreshing(false);
            }
        }
    }

    private void editEvent() {
        LayoutInflater li = LayoutInflater.from(getActivity());
        View promptsView = li.inflate(R.layout.edit_event, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);
        img_event = (ImageView) promptsView.findViewById(R.id.img_event);
        edt_event_title = (TextInputEditText) promptsView.findViewById(R.id.edt_event_title);
        edt_start_date = (TextInputEditText) promptsView.findViewById(R.id.edt_start_date);
        edt_end_date = (TextInputEditText) promptsView.findViewById(R.id.edt_end_date);
        edt_event_Description = (TextInputEditText) promptsView.findViewById(R.id.edt_event_Description);
        final ImageView img_edit_event = (ImageView) promptsView.findViewById(R.id.img_edit_event);
        final ImageView btn_add_event = (ImageView) promptsView.findViewById(R.id.btn_add_event);


        //edt_event_date.setClickable(false);
        edt_start_date.setFocusable(false);
        edt_end_date.setFocusable(false);
        edt_start_date.setFocusable(false);
        edt_end_date.setFocusable(false);
        edt_start_date.setFocusableInTouchMode(false);
        edt_end_date.setFocusableInTouchMode(false);
        // edt_event_date.setEnabled(false);
        img_edit_event.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPictureDialog();
            }
        });


        edt_start_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar calendar = Calendar.getInstance();

                int YEAR, MONTH, DAY;
                YEAR = calendar.get(Calendar.YEAR);
                MONTH = calendar.get(Calendar.MONTH);
                DAY = calendar.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                start_date = year + "-" + ((monthOfYear + 1) > 9 ? (monthOfYear + 1) : "0" + (monthOfYear + 1)) + "-" +
                                        (dayOfMonth > 9 ? dayOfMonth : "0" + dayOfMonth);

                                SimpleDateFormat update_start_date = new SimpleDateFormat("yyyy-MM-dd");
                                Date date = null;
                                String set_start_date = "";
                                try {
                                    date = update_start_date.parse(start_date);
                                    update_start_date.applyPattern("dd/MM/yyyy");
                                    if (date != null) {
                                        set_start_date = update_start_date.format(date);
                                    }
                                    edt_start_date.setText(set_start_date);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }


                            }
                        }, YEAR, MONTH, DAY);

                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePickerDialog.show();
            }
        });
        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();


        edt_end_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar calendar = Calendar.getInstance();
                int YEAR, MONTH, DAY;
                YEAR = calendar.get(Calendar.YEAR);
                MONTH = calendar.get(Calendar.MONTH);
                DAY = calendar.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                end_date = year + "-" + ((monthOfYear + 1) > 9 ? (monthOfYear + 1) : "0" + (monthOfYear + 1)) + "-" +
                                        (dayOfMonth > 9 ? dayOfMonth : "0" + dayOfMonth);
                                if (end_date.compareTo(start_date) < 0) {
                                    Toast.makeText(getActivity(), "start date are grater for end date", Toast.LENGTH_LONG).show();
                                } else {
                                    SimpleDateFormat update_start_date = new SimpleDateFormat("yyyy-MM-dd");
                                    Date date = null;
                                    String set_start_date = "";
                                    try {
                                        date = update_start_date.parse(end_date);
                                        update_start_date.applyPattern("dd/MM/yyyy");
                                        if (date != null) {
                                            set_start_date = update_start_date.format(date);
                                        }
                                        edt_end_date.setText(set_start_date);
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                }

                            }
                        }, YEAR, MONTH, DAY);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePickerDialog.show();
            }
        });


        btn_add_event.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                title = edt_event_title.getText().toString();
                description = edt_event_Description.getText().toString();

                if (isEmpty()) {
                    new AsyncTaskEventAdd().execute();
                    alertDialog.dismiss();

                }

            }
        });
    }

    private void showPictureDialog() {
        final String[] option = {"Select photo from gallery", "Capture photo from camera", "Cancel"};
        final AlertDialog.Builder pictureDialog = new AlertDialog.Builder(getActivity());
        pictureDialog.setTitle("Select Action");

        pictureDialog.setItems(option, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                if (option[i].equals("Select photo from gallery")) {
                    Intent GALLERY = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    GALLERY.setType("image/*");
                    startActivityForResult(Intent.createChooser(GALLERY, "Select Image"), GALLERY_PICK);
                } else if (option[i].equals("Capture photo from camera")) {

                    Intent intent_camera = new Intent("android.media.action.IMAGE_CAPTURE");
                    File file = new File(Environment.getExternalStorageDirectory() + File.separator + "image.jpg");
                    intent_camera.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
                    startActivityForResult(intent_camera, CAMERA_PICK);

                } else if (option[i].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        pictureDialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == GALLERY_PICK) {
                Uri uri = data.getData();
                String[] FILE = {MediaStore.Images.Media.DATA, MediaStore.Video.Media.DATA};
                Cursor cursor = getActivity().getContentResolver().query(uri, FILE, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(FILE[0]);
                user_image = cursor.getString(columnIndex);
                cursor.close();
                bitmap = BitmapFactory.decodeFile(user_image);
                img_event.setImageBitmap(bitmap);

            } else if (requestCode == CAMERA_PICK) {
                try {
                    user_image = Environment.getExternalStorageDirectory() + File.separator + "image.jpg";
                    bitmap = BitmapFactory.decodeFile(user_image);
                    img_event.setImageBitmap(bitmap);
                } catch (Exception e) {

                }

            }
        }
    }

    private class AsyncTaskEventAdd extends AsyncTask<String, String, String> {

        private ProgressDialog Dialog = new ProgressDialog(getActivity());

        @Override
        protected String doInBackground(String... params) {

            MultipartBody.Builder builder = new MultipartBody.Builder();

            builder.setType(MultipartBody.FORM);
            final MediaType MEDIA_TYPE = MediaType.parse("image/jpg");
            if (user_image.equals("")) {

            } else {
                builder.addFormDataPart("image", user_image, RequestBody.create(MEDIA_TYPE, new File(user_image)));
            }

            builder.addFormDataPart("user_id", user_id);
            builder.addFormDataPart("title", title);
            builder.addFormDataPart("start_date", start_date);
            builder.addFormDataPart("end_date", end_date);
            builder.addFormDataPart("description", description);
            builder.addFormDataPart("class_id", class_id);
            builder.addFormDataPart("branch_id", branch_id);
            builder.addFormDataPart("section", section);

            RequestBody requestBody = builder.build();

            try {

                OkHttpClient client = new OkHttpClient.Builder().build();
                Request request = new Request.Builder()
                        .url(Services.EVENT_ADD)
                        .post(requestBody)
                        .build();
                Response resp = client.newCall(request).execute();
                responseimage = resp.body().string();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            Dialog.dismiss();
            Deliverydatacategory(responseimage);
            super.onPostExecute(s);

        }

        @Override
        protected void onPreExecute() {
            Dialog.show();
            Dialog.setMessage("Please Wait....");
            Dialog.setCancelable(false);
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
        }

    }

    private void Deliverydatacategory(String response) {
        try {
            if (!response.equals("")) {


                JSONObject jsobjectcategory = new JSONObject(response);

                String message = jsobjectcategory.getString("message");
                String error = jsobjectcategory.getString("error");


                if (error.equals("200")) {
                    String profile = jsobjectcategory.getString("data");
                    JSONObject jsonObject = new JSONObject(profile);


                    //  Eve_Id = jsonObject.getString("Eve_Id");
                    Eve_Use_Id = jsonObject.getString("Eve_CreatedBy");
                    Eve_user_Name = jsonObject.getString("Eve_Name");
                    Eve_Start_Date = jsonObject.getString("Eve_Date");
                    Eve_End_Date = jsonObject.getString("Eve_End_Date");
                    Eve_Description = jsonObject.getString("Eve_Description");
                    Eve_user_Image = jsonObject.getString("Eve_Image");
                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();

                } else {
                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private boolean isEmpty() {

        if (edt_event_title.getText().toString().trim().equals("")) {
            edt_event_title.setError("Please enter name for event");
            edt_event_title.requestFocus();
            return false;
        } else if (edt_start_date.getText().toString().trim().equals("")) {
            edt_start_date.setError("Please select event start date");
            edt_start_date.requestFocus();
            return false;
        } else if (edt_end_date.getText().toString().trim().equals("")) {
            edt_end_date.setError("Please select event end date");
            edt_end_date.requestFocus();
            return false;
        } else if (edt_event_Description.getText().toString().trim().equals("")) {
            edt_event_Description.setError("Please select some detail of event");
            edt_event_Description.requestFocus();
            return false;
        } else if (end_date.compareTo(start_date) < 0) {
            Toast.makeText(getActivity(), "start date are grater for end date", Toast.LENGTH_LONG).show();
            return false;
        } else {
            return true;
        }

    }

}

