package com.example.shine_rahul.gemstarkidz.Session;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by ${Vrund} on 12/27/2017.
 */

public class Utils {

    public static String USER_ID = "USER_ID";
    public static String USER_NAME = "USER_NAME";
    public static String USER_EMAIL = "USER_EMAIL";
    public static String USER_MOBILE = "USER_MOBILE";
    public static String USER_ADDRESS = "USER_ADDRESS";
    public static String USER_PINCODE = "USER_PINCODE";
    public static String USER_CITY = "USER_CITY";
    public static String USER_GENDER = "USER_GENDER";
    public static String USER_BIRTHDATE = "USER_BIRTHDATE";
    public static String LOGIN_CURRENT = "login_current";


    // shared methods
    public static void setShared(Context context, String name, String value) {
        SharedPreferences prefs = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(name, value);
        editor.commit();
    }

    public static String getShared(Context context, String name, String defaultValue) {
        SharedPreferences prefs = PreferenceManager
                .getDefaultSharedPreferences(context);
        return prefs.getString(name, defaultValue);

    }
}
