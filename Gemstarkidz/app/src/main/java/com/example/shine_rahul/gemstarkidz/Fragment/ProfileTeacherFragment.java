package com.example.shine_rahul.gemstarkidz.Fragment;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.shine_rahul.gemstarkidz.Model.Student_Class_Model;
import com.example.shine_rahul.gemstarkidz.R;
import com.example.shine_rahul.gemstarkidz.Service.Services;
import com.example.shine_rahul.gemstarkidz.TeacherActivity;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileTeacherFragment extends android.app.Fragment {
    ImageView img_profile_teacher_call1;
    ImageView img_profile_teacher_email1;
    TextView txt_profile_teacher_mobile;
    TextView txt_profile_email;
    TextView txt_profile_teacher_email;
    ArrayList<Student_Class_Model> data;
    String number;
    String email;
    CircleImageView img_teacher_profile;
    CircleImageView img_profile_teacher_edit;
    ImageView btn_profile_done;
    TextView txt_profile_teacher_edit;
    TextView txt_show_class, txt_show_section;
    TextView txt_profile_mobile_teacher_update;
    TextView txt_profile_email_teacher_update;
    TextView txt_profile_teacher_username;
    LinearLayout li_snack;
    String user_phone = "", username = "", user_id = "", user_email = "", user_type = "", user_image = "", user_class = "", user_section = "", user_reg_type = "";
    Bundle bundle;
    String image_path = "", responseimage, uimage = "";
    Context context;

    private Bitmap bitmap;
    int GALLERY_PICK = 101, CAMERA_PICK = 132;
    ImageView img_search, img_menu, img_close;
    TextView txt_menu_title;
    TextView txt_edit_toolbar;
    EditText edt_search;

    public ProfileTeacherFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.profile_detail, container, false);

        context = this.getActivity();

        SharedPreferences splogin = getActivity().getSharedPreferences("login", 0);
        username = splogin.getString("username", "");
        user_type = splogin.getString("type", "");
        user_phone = splogin.getString("user_phone", "");
        user_id = splogin.getString("user_id", "");
        user_email = splogin.getString("user_email", "");
        user_image = splogin.getString("user_image", "");
        user_class = splogin.getString("user_class_name", "");
        user_section = splogin.getString("user_section", "");

        img_search = (ImageView) getActivity().findViewById(R.id.img_search);
        img_menu = (ImageView) getActivity().findViewById(R.id.img_menu);
        img_close = (ImageView) getActivity().findViewById(R.id.img_close);
        txt_menu_title = (TextView) getActivity().findViewById(R.id.txt_menu_title);
        txt_edit_toolbar = (TextView) getActivity().findViewById(R.id.txt_edit_toolbar);
        edt_search = (EditText) getActivity().findViewById(R.id.edt_search);
        img_profile_teacher_call1 = (ImageView) view.findViewById(R.id.img_profile_teacher_call1);
        img_profile_teacher_email1 = (ImageView) view.findViewById(R.id.img_profile_teacher_email1);
        txt_profile_teacher_mobile = (TextView) view.findViewById(R.id.txt_profile_teacher_mobile);
        txt_profile_teacher_email = (TextView) view.findViewById(R.id.txt_profile_teacher_email);
        txt_show_class = (TextView) view.findViewById(R.id.txt_show_class);
        txt_profile_email = (TextView) view.findViewById(R.id.txt_profile_email);
        txt_show_section = (TextView) view.findViewById(R.id.txt_show_section);
        number = txt_profile_teacher_mobile.getText().toString();
        email = txt_profile_teacher_email.getText().toString();
        img_search.setVisibility(View.GONE);
        img_menu.setVisibility(View.VISIBLE);
        img_close.setVisibility(View.GONE);
        txt_menu_title.setVisibility(View.VISIBLE);
        txt_menu_title.setText("Profile");
        txt_edit_toolbar.setVisibility(View.VISIBLE);
        txt_edit_toolbar.setText("Edit");
        edt_search.setVisibility(View.GONE);


        img_teacher_profile = (CircleImageView) view.findViewById(R.id.img_teacher_profile);
        img_profile_teacher_edit = (CircleImageView) view.findViewById(R.id.img_profile_teacher_edit);
        btn_profile_done = (ImageView) view.findViewById(R.id.btn_profile_done);
        txt_profile_teacher_edit = (TextView) view.findViewById(R.id.txt_profile_teacher_edit);
        txt_profile_mobile_teacher_update = (TextView) view.findViewById(R.id.txt_profile_mobile_teacher_update);
        txt_profile_email_teacher_update = (TextView) view.findViewById(R.id.txt_profile_email_teacher_update);
        txt_profile_teacher_username = (TextView) view.findViewById(R.id.txt_profile_teacher_username);
        li_snack = (LinearLayout) view.findViewById(R.id.li_snack);
        txt_edit_toolbar.setVisibility(View.VISIBLE);


        if (username.equals("null")) {
        } else {
            txt_profile_teacher_username.setText(username);
        }

        if (user_phone.equals("null")) {
        } else {
            txt_profile_teacher_mobile.setText(user_phone);
        }

        if (user_email.equals("null")) {
        } else {
            txt_profile_teacher_email.setText(user_email);
        }
        if (user_class.equals("null")) {
        } else {
            txt_show_class.setText(user_class);
        }
        if (user_section.equals("null")) {
        } else {
            txt_show_section.setText(user_section);
        }

        Picasso.with(context)
                .load(Services.IMAGES_PATH + "profile/" + user_image)
                .placeholder(R.drawable.logo)
                .into(img_teacher_profile);

        img_profile_teacher_call1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent call1 = new Intent(Intent.ACTION_DIAL);
                call1.setData(Uri.parse("tel:" + user_phone));
                call1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(call1);
            }
        });

        img_profile_teacher_email1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String data = txt_profile_teacher_email.getText().toString();
                Intent email = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", user_email, null));
                startActivity(Intent.createChooser(email, "Choose an Email client :"));

                Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto", user_email, null));
                intent.putExtra(Intent.EXTRA_SUBJECT, "Feedback");
                intent.putExtra(Intent.EXTRA_TEXT, email);
                startActivity(Intent.createChooser(intent, "Choose an Email client :"));
            }
        });

        img_profile_teacher_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPictureDialog();
            }
        });
        txt_edit_toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateProfileName();
            }
        });
        txt_profile_mobile_teacher_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateProfileMobile();
            }
        });
        txt_profile_email_teacher_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateProfileEmail();
            }
        });


        return view;
    }


    private void showPictureDialog() {
        final String[] option = {"Select photo from gallery", "Capture photo from camera", "Cancel"};
        final AlertDialog.Builder pictureDialog = new AlertDialog.Builder(getActivity());
        pictureDialog.setTitle("Select Action");

        pictureDialog.setItems(option, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                if (option[i].equals("Select photo from gallery")) {
                    Intent GALLERY = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    GALLERY.setType("image/*");
                    startActivityForResult(Intent.createChooser(GALLERY, "Select Image"), GALLERY_PICK);
                } else if (option[i].equals("Capture photo from camera")) {
                    Intent intent_camera = new Intent("android.media.action.IMAGE_CAPTURE");
                    File file = new File(Environment.getExternalStorageDirectory() + File.separator + "image.jpg");
                    intent_camera.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
                    startActivityForResult(intent_camera, CAMERA_PICK);
                } else if (option[i].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        pictureDialog.show();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == GALLERY_PICK) {
                Uri uri = data.getData();
                String[] FILE = {MediaStore.Images.Media.DATA, MediaStore.Video.Media.DATA};
                Cursor cursor = getActivity().getContentResolver().query(uri, FILE, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(FILE[0]);
                image_path = cursor.getString(columnIndex);
                cursor.close();

                new AsyncTaskRunnerimage().execute();

            } else if (requestCode == CAMERA_PICK) {
                try {
                    image_path = Environment.getExternalStorageDirectory() + File.separator + "image.jpg";
//                    Uri uri = Uri.parse(path);
//                    String[] FILE = {MediaStore.Images.Media.DATA, MediaStore.Video.Media.DATA};
//                    Cursor cursor = getApplicationContext().getContentResolver().query(uri, FILE, null, null, null);
//                    cursor.moveToFirst();
//                    int columnIndex = cursor.getColumnIndex(FILE[0]);
//                    image_path = cursor.getString(columnIndex);
//                    cursor.close();
                    bitmap = BitmapFactory.decodeFile(image_path);
                    img_teacher_profile.setImageBitmap(bitmap);

                    new AsyncTaskRunnerimage().execute();

                } catch (Exception e) {

                }

            }
        }
    }


    private void updateProfileName() {
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View view1 = layoutInflater.inflate(R.layout.name_update, null);
        final AlertDialog.Builder alertdialog = new AlertDialog.Builder(getActivity());
        alertdialog.setView(view1);
        final AlertDialog dialog = alertdialog.create();
        dialog.setCanceledOnTouchOutside(false);
        ImageView img_name_save = (ImageView) view1.findViewById(R.id.img_name_save);
        ImageView img_name_cancel = (ImageView) view1.findViewById(R.id.img_name_cancel);
        final EditText edt_save_detail = (EditText) view1.findViewById(R.id.edt_name_update);
        edt_save_detail.setHint("Enter Name");
        img_name_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (edt_save_detail.getText().toString().trim().equals("")) {
                    edt_save_detail.setError("Please Enter Name");
                    edt_save_detail.requestFocus();
                } else {
                    //  file = new File(user_image);
                    username = edt_save_detail.getText().toString().trim();
                    new AsyncTaskRunnername().execute();
                    dialog.dismiss();
                }


            }
        });
        img_name_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();

    }


    private void updateProfileMobile() {
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View view1 = layoutInflater.inflate(R.layout.mobile_update, null);
        final AlertDialog.Builder alertdialog = new AlertDialog.Builder(getActivity());
        alertdialog.setView(view1);
        final AlertDialog dialog = alertdialog.create();
        dialog.setCanceledOnTouchOutside(false);
        ImageView img_mobile_save = (ImageView) view1.findViewById(R.id.img_mobile_save);
        ImageView img_mobile_cancel = (ImageView) view1.findViewById(R.id.img_mobile_cancel);
        final EditText edt_mobile_update = (EditText) view1.findViewById(R.id.edt_mobile_update);
        edt_mobile_update.setHint("Enter Mobile");
        img_mobile_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (edt_mobile_update.getText().toString().trim().equals("")) {
                    edt_mobile_update.setError("Please Enter Mobile Number");
                    edt_mobile_update.requestFocus();
                } else if (edt_mobile_update.getText().toString().length() < 10) {
                    edt_mobile_update.setError("Mobile Number Must be 10 Digit");
                    edt_mobile_update.requestFocus();
                } else {
                    user_phone = edt_mobile_update.getText().toString().trim();
                    new AsyncTaskRunnermobile().execute();
                    dialog.dismiss();
                }


            }
        });
        img_mobile_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();

    }


    private void updateProfileEmail() {
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View view1 = layoutInflater.inflate(R.layout.update_email, null);
        final AlertDialog.Builder alertdialog = new AlertDialog.Builder(getActivity());
        alertdialog.setView(view1);
        final AlertDialog dialog = alertdialog.create();
        dialog.setCanceledOnTouchOutside(false);
        ImageView img_email_save = (ImageView) view1.findViewById(R.id.img_email_save);
        ImageView img_email_cancel = (ImageView) view1.findViewById(R.id.img_email_cancel);
        final EditText edt_email_update = (EditText) view1.findViewById(R.id.edt_email_update);
        edt_email_update.setHint("Enter Email");
        img_email_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_email_update.getText().toString().trim().equals("")) {
                    edt_email_update.setError("Please Email Address");
                    edt_email_update.requestFocus();
                } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(edt_email_update.getText()).matches()) {
                    edt_email_update.setError("Please Enter Valid Email Address");
                    edt_email_update.requestFocus();
                } else {
                    user_email = edt_email_update.getText().toString().trim();
                    new AsyncTaskRunneremail().execute();
                    dialog.dismiss();
                }
            }
        });
        img_email_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();

    }


    private class AsyncTaskRunnername extends AsyncTask<String, String, String> {

        private ProgressDialog Dialog = new ProgressDialog(getActivity());

        @Override
        protected String doInBackground(String... params) {

            MultipartBody.Builder builder = new MultipartBody.Builder();

            builder.setType(MultipartBody.FORM);
            builder.addFormDataPart("name", username);
            builder.addFormDataPart("user_id", user_id);

            RequestBody requestBody = builder.build();

            try {

                OkHttpClient client = new OkHttpClient.Builder().build();
                Request request = new Request.Builder()
                        .url(Services.UPDATE_PROFILE)
                        .post(requestBody)
                        .build();
                Response resp = client.newCall(request).execute();
                responseimage = resp.body().string();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            Dialog.dismiss();
            Deliverydataname(responseimage);
            super.onPostExecute(s);

        }

        @Override
        protected void onPreExecute() {
            Dialog.show();
            Dialog.setMessage("Please Wait....");
            Dialog.setCancelable(false);
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
        }

    }

    private void Deliverydataname(String response) {

        if (!response.equals("")) {
            try {

                JSONObject jsobjectcategory = new JSONObject(response);

                String message = jsobjectcategory.getString("message");

                String error = jsobjectcategory.getString("error");

                if (error.equals("200")) {
                    String profile = jsobjectcategory.getString("data");
                    JSONObject jsonObject = new JSONObject(profile);
                    String name = jsonObject.getString("Use_Name");

                    username = name;
                    txt_profile_teacher_username.setText(username);

                    SharedPreferences spuser = getActivity().getSharedPreferences("login", 0);
                    SharedPreferences.Editor Eduser = spuser.edit();
                    Eduser.putString("username", username);
                    Eduser.apply();

                    Intent intent = new Intent(getActivity(), TeacherActivity.class);
                    startActivity(intent);
                    getActivity().overridePendingTransition(0, 0);
                    getActivity().finish();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class AsyncTaskRunneremail extends AsyncTask<String, String, String> {

        private ProgressDialog Dialog = new ProgressDialog(getActivity());

        @Override
        protected String doInBackground(String... params) {

            MultipartBody.Builder builder = new MultipartBody.Builder();

            builder.setType(MultipartBody.FORM);
            builder.addFormDataPart("email", user_email);
            builder.addFormDataPart("user_id", user_id);

            RequestBody requestBody = builder.build();

            try {

                OkHttpClient client = new OkHttpClient.Builder().build();
                Request request = new Request.Builder()
                        .url(Services.UPDATE_PROFILE)
                        .post(requestBody)
                        .build();
                Response resp = client.newCall(request).execute();
                responseimage = resp.body().string();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            Dialog.dismiss();
            Deliverydataemail(responseimage);
            super.onPostExecute(s);

        }

        @Override
        protected void onPreExecute() {
            Dialog.show();
            Dialog.setMessage("Please Wait....");
            Dialog.setCancelable(false);
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
        }

    }

    private void Deliverydataemail(String response) {

        if (!response.equals("")) {
            try {

                JSONObject jsobjectcategory = new JSONObject(response);

                String message = jsobjectcategory.getString("message");

                String error = jsobjectcategory.getString("error");

                if (error.equals("200")) {
                    String profile = jsobjectcategory.getString("data");
                    JSONObject jsonObject = new JSONObject(profile);
                    String email = jsonObject.getString("Use_Email");

                    user_email = email;
                    txt_profile_teacher_email.setText(user_email);

                    SharedPreferences spuser = getActivity().getSharedPreferences("login", 0);
                    SharedPreferences.Editor Eduser = spuser.edit();
                    Eduser.putString("user_email", user_email);
                    Eduser.apply();
                    Intent intent = new Intent(getActivity(), TeacherActivity.class);
                    startActivity(intent);
                    getActivity().overridePendingTransition(0, 0);
                    getActivity().finish();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class AsyncTaskRunnermobile extends AsyncTask<String, String, String> {

        private ProgressDialog Dialog = new ProgressDialog(getActivity());

        @Override
        protected String doInBackground(String... params) {

            MultipartBody.Builder builder = new MultipartBody.Builder();

            builder.setType(MultipartBody.FORM);
            builder.addFormDataPart("mobile_no", user_phone);
            builder.addFormDataPart("user_id", user_id);

            RequestBody requestBody = builder.build();

            try {

                OkHttpClient client = new OkHttpClient.Builder().build();
                Request request = new Request.Builder()
                        .url(Services.UPDATE_PROFILE)
                        .post(requestBody)
                        .build();
                Response resp = client.newCall(request).execute();
                responseimage = resp.body().string();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            Dialog.dismiss();
            Deliverydatamobile(responseimage);
            super.onPostExecute(s);

        }

        @Override
        protected void onPreExecute() {
            Dialog.show();
            Dialog.setMessage("Please Wait....");
            Dialog.setCancelable(false);
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
        }

    }

    private void Deliverydatamobile(String response) {

        if (!response.equals("")) {
            try {

                JSONObject jsobjectcategory = new JSONObject(response);

                String message = jsobjectcategory.getString("message");

                String error = jsobjectcategory.getString("error");

                if (error.equals("200")) {
                    String profile = jsobjectcategory.getString("data");
                    JSONObject jsonObject = new JSONObject(profile);
                    String mobile = jsonObject.getString("Use_Mobile_No");

                    user_phone = mobile;
                    txt_profile_teacher_mobile.setText(user_phone);

                    SharedPreferences spuser = getActivity().getSharedPreferences("login", 0);
                    SharedPreferences.Editor Eduser = spuser.edit();
                    Eduser.putString("user_phone", user_phone);
                    Eduser.apply();
                    Intent intent = new Intent(getActivity(), TeacherActivity.class);
                    startActivity(intent);
                    getActivity().overridePendingTransition(0, 0);
                    getActivity().finish();

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private class AsyncTaskRunnerimage extends AsyncTask<String, String, String> {

        private ProgressDialog Dialog = new ProgressDialog(getActivity());

        @Override
        protected String doInBackground(String... params) {

            MultipartBody.Builder builder = new MultipartBody.Builder();

            builder.setType(MultipartBody.FORM);
            final MediaType MEDIA_TYPE = MediaType.parse("image/jpg");
            builder.addFormDataPart("image", image_path, RequestBody.create(MEDIA_TYPE, new File(image_path)));
            builder.addFormDataPart("user_id", user_id);

            RequestBody requestBody = builder.build();

            try {

                OkHttpClient client = new OkHttpClient.Builder().build();
                Request request = new Request.Builder()
                        .url(Services.UPDATE_PROFILE)
                        .post(requestBody)
                        .build();
                Response resp = client.newCall(request).execute();
                responseimage = resp.body().string();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            Dialog.dismiss();
            Deliverydataimage(responseimage);
            super.onPostExecute(s);

        }

        @Override
        protected void onPreExecute() {
            Dialog.show();
            Dialog.setMessage("Please Wait....");
            Dialog.setCancelable(false);
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
        }

    }

    private void Deliverydataimage(String response) {

        if (!response.equals("")) {
            try {

                JSONObject jsobjectcategory = new JSONObject(response);

                String message = jsobjectcategory.getString("message");

                String error = jsobjectcategory.getString("error");

                if (error.equals("200")) {
                    String profile = jsobjectcategory.getString("data");
                    JSONObject jsonObject = new JSONObject(profile);
                    String image = jsonObject.getString("Use_Image");

                    user_image = image;
                    Picasso.with(context)
                            .load(Services.IMAGES_PATH + "profile/" + user_image)
                            .placeholder(R.drawable.logo)
                            .into(img_teacher_profile);


                    SharedPreferences spuser = getActivity().getSharedPreferences("login", 0);
                    SharedPreferences.Editor Eduser = spuser.edit();
                    Eduser.putString("user_image", user_image);
                    Eduser.apply();
                    Intent intent = new Intent(getActivity(), TeacherActivity.class);
                    startActivity(intent);
                    getActivity().overridePendingTransition(0, 0);
                    getActivity().finish();

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
