package com.example.shine_rahul.gemstarkidz.Model;

public class ParentSocialModel {

    String Soa_Id;
    String Soa_Use_Id;
    String Soa_Comment;
    String Soa_Title;
    String Soa_User_Type;
    String Soa_Status;
    String Soa_CreatedBy;
    String Soa_CreatedAt;
    String Soa_UpdateBy;
    String Soa_UpdatedAt;
    int soa_images;
    String Sal_Like;

    public String getSoa_Id() {
        return Soa_Id;
    }

    public void setSoa_Id(String soa_Id) {
        Soa_Id = soa_Id;
    }

    public String getSoa_Use_Id() {
        return Soa_Use_Id;
    }

    public void setSoa_Use_Id(String soa_Use_Id) {
        Soa_Use_Id = soa_Use_Id;
    }

    public String getSoa_Comment() {
        return Soa_Comment;
    }

    public void setSoa_Comment(String soa_Comment) {
        Soa_Comment = soa_Comment;
    }

    public String getSoa_Title() {
        return Soa_Title;
    }

    public void setSoa_Title(String soa_Title) {
        Soa_Title = soa_Title;
    }

    public String getSoa_User_Type() {
        return Soa_User_Type;
    }

    public void setSoa_User_Type(String soa_User_Type) {
        Soa_User_Type = soa_User_Type;
    }

    public String getSoa_Status() {
        return Soa_Status;
    }

    public void setSoa_Status(String soa_Status) {
        Soa_Status = soa_Status;
    }

    public String getSoa_CreatedBy() {
        return Soa_CreatedBy;
    }

    public void setSoa_CreatedBy(String soa_CreatedBy) {
        Soa_CreatedBy = soa_CreatedBy;
    }

    public String getSoa_CreatedAt() {
        return Soa_CreatedAt;
    }

    public void setSoa_CreatedAt(String soa_CreatedAt) {
        Soa_CreatedAt = soa_CreatedAt;
    }

    public String getSoa_UpdateBy() {
        return Soa_UpdateBy;
    }

    public void setSoa_UpdateBy(String soa_UpdateBy) {
        Soa_UpdateBy = soa_UpdateBy;
    }

    public String getSoa_UpdatedAt() {
        return Soa_UpdatedAt;
    }

    public void setSoa_UpdatedAt(String soa_UpdatedAt) {
        Soa_UpdatedAt = soa_UpdatedAt;
    }

    public int getSoa_images() {
        return soa_images;
    }

    public void setSoa_images(int soa_images) {
        this.soa_images = soa_images;
    }

    public String getSal_Like() {
        return Sal_Like;
    }

    public void setSal_Like(String sal_Like) {
        Sal_Like = sal_Like;
    }
}
