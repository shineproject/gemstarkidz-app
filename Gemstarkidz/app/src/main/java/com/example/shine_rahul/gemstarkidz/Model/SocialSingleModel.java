package com.example.shine_rahul.gemstarkidz.Model;

/**
 * Created by Shine-Rahul on 28/2/2018.
 */

public class SocialSingleModel {
    String id;
    int image;
    String name;
    String time;
    String description;
    String url;

    public SocialSingleModel() {
    }

    public SocialSingleModel(String id, int image, String name, String time, String description, String url) {
        this.id = id;
        this.image = image;
        this.name = name;
        this.time = time;
        this.description = description;
        this.url = url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
