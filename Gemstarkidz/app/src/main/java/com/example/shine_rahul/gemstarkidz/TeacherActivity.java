package com.example.shine_rahul.gemstarkidz;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.daasuu.bl.BubbleLayout;
import com.daasuu.bl.BubblePopupHelper;
import com.example.shine_rahul.gemstarkidz.Fragment.HomeworkFragment;
import com.example.shine_rahul.gemstarkidz.Fragment.ProfileTeacherFragment;
import com.example.shine_rahul.gemstarkidz.Fragment.TeacherAttendanceEntireClassFragment;
import com.example.shine_rahul.gemstarkidz.Fragment.TeacherAttendanceReportFragment;
import com.example.shine_rahul.gemstarkidz.Fragment.TeacherChatFragment;
import com.example.shine_rahul.gemstarkidz.Fragment.TeacherClassFragment;
import com.example.shine_rahul.gemstarkidz.Fragment.TeacherEventFragment;
import com.example.shine_rahul.gemstarkidz.Fragment.TeacherEventListFragment;
import com.example.shine_rahul.gemstarkidz.Fragment.TeacherReportFragment;
import com.example.shine_rahul.gemstarkidz.Fragment.TeacherSocialFragment;
import com.example.shine_rahul.gemstarkidz.Model.MenuModel;
import com.example.shine_rahul.gemstarkidz.Service.Services;
import com.example.shine_rahul.gemstarkidz.Session.Session;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class TeacherActivity extends Activity
        implements NavigationView.OnNavigationItemSelectedListener {
    ImageView img_menu;
    RecyclerView rv_parent_drawer_menu_list;
    TeacherActivity.MenuAdapter menuAdapter;
    ArrayList<MenuModel> data;
    String[] name = {"Profile", "Social", "Chat", "Event", "Attendance", "Class", "Homework", "Report", "Tracker", "Logout"};
    int[] image = {R.drawable.profile, R.drawable.social, R.drawable.chatmenu, R.drawable.event, R.drawable.attendance, R.drawable.class_, R.drawable.homework, R.drawable.report, R.drawable.map, R.drawable.logout};
    ImageView img_parent_drawer_close;
    ImageView img_search;
    TextView txt_menu_title;
    String attendance = "Attendance";
    Session session;
    TextView txt_parent_drawer_user_name;
    boolean isOpen = true;
    String user_phone, username, user_id, user_email, user_type, user_image, user_class, user_section, user_reg_type, Cla_Id, Cla_Bra_Id;
    CircleImageView img_parent_drawer_user;
    String image_path = "", responseimage, uimage = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_parent);
        session = Session.getSession(TeacherActivity.this);
        img_search = (ImageView) findViewById(R.id.img_search);


        SharedPreferences splogin = getApplicationContext().getSharedPreferences("login", 0);
        username = splogin.getString("username", "");
        user_type = splogin.getString("type", "");
        user_phone = splogin.getString("user_phone", "");
        user_id = splogin.getString("user_id", "");
        user_email = splogin.getString("user_email", "");
        user_image = splogin.getString("user_image", "");
        user_class = splogin.getString("user_class", "");
        user_section = splogin.getString("user_section", "");
        user_reg_type = splogin.getString("user_reg_type", "");
        Cla_Id = splogin.getString("Cla_Id", "");
        Cla_Bra_Id = splogin.getString("Cla_Bra_Id", "");


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        img_parent_drawer_close = (ImageView) findViewById(R.id.img_parent_drawer_close);
        //setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        /*fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.parent_drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
//        drawer.addDrawerListener(toggle);
//        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_parent_view);
        navigationView.setNavigationItemSelectedListener(this);
        txt_parent_drawer_user_name = (TextView) navigationView.findViewById(R.id.txt_parent_drawer_user_name);
        img_parent_drawer_user = (CircleImageView) navigationView.findViewById(R.id.img_parent_drawer_user);
        //txt_parent_drawer_user_name.setText(session.getUSER_EMAIL());
        txt_parent_drawer_user_name.setText(username);

        Picasso.with(getApplicationContext())
                .load(Services.IMAGES_PATH + "profile/" + user_image)
                .placeholder(R.drawable.logo)
                .into(img_parent_drawer_user);

        rv_parent_drawer_menu_list = (RecyclerView) findViewById(R.id.rv_parent_drawer_menu_list);
        txt_menu_title = (TextView) findViewById(R.id.txt_menu_title);

        img_menu = (ImageView) findViewById(R.id.img_menu);
        img_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.parent_drawer_layout);
                boolean mSlideState = false;
                if (drawer.isDrawerOpen(Gravity.START)) {
                    drawer.closeDrawer(Gravity.START);
                } else {
                    drawer.openDrawer(Gravity.START);
                }
            }
        });

        img_parent_drawer_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.closeDrawers();
            }
        });

        data = new ArrayList<>();
        for (int i = 0; i < image.length; i++) {
            MenuModel menuModel = new MenuModel();
            menuModel.setName(name[i]);
            menuModel.setImage(image[i]);
            data.add(menuModel);
        }

        menuAdapter = new TeacherActivity.MenuAdapter(TeacherActivity.this, data);
        menuAdapter.notifyDataSetChanged();
        rv_parent_drawer_menu_list.setLayoutManager(new LinearLayoutManager(TeacherActivity.this, LinearLayout.VERTICAL, false));
        rv_parent_drawer_menu_list.setAdapter(menuAdapter);

        Fragment someFragment = new ProfileTeacherFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.content, someFragment);
        transaction.addToBackStack(null);
        transaction.commit();
        DrawerLayout drawer1 = (DrawerLayout) findViewById(R.id.parent_drawer_layout);
        drawer1.closeDrawer(GravityCompat.START);
        txt_menu_title.setText("Profile");
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.parent_drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (!isOpen) {
            // super.onBackPressed();
            Fragment someFragment = new TeacherSocialFragment();
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.replace(R.id.content, someFragment); // give your fragment container id in first parameter
            transaction.addToBackStack(null);  // if written, this transaction will be added to backstack
            transaction.commit();
            drawer.closeDrawer(GravityCompat.START);
            txt_menu_title.setText("Social");
            img_search.setVisibility(View.VISIBLE);
            Typeface style = Typeface.createFromAsset(getApplicationContext().getAssets(), "Raleway-SemiBold.ttf");
            txt_menu_title.setTypeface(style);
            isOpen = true;
        } else if (isOpen) {
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.parent, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.parent_drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    class MenuAdapter extends RecyclerView.Adapter<TeacherActivity.MenuAdapter.ViewHolder> {
        private Context context;
        private ArrayList<MenuModel> data;

        public MenuAdapter(Context context, ArrayList<MenuModel> data) {
            this.context = context;
            this.data = data;
        }

        @Override
        public TeacherActivity.MenuAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.menu_list, parent, false);

            return new TeacherActivity.MenuAdapter.ViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final TeacherActivity.MenuAdapter.ViewHolder holder, final int position) {

            final MenuModel menuModel = data.get(position);
            holder.txt_menu.setText("" + menuModel.getName());
            Glide.with(context).load(menuModel.getImage()).placeholder(R.mipmap.ic_launcher).error(R.mipmap.ic_launcher).into(holder.img_menu);
            holder.view.setVisibility(View.GONE);
            holder.img_popup.setImageResource(R.drawable.popup);
            // holder.img_popup.setImageResource(R.drawable.popup);

//            if (position == 4) {
//                holder.img_popup.setVisibility(View.VISIBLE);
//            }

            holder.img_popup.setOnClickListener(new View.OnClickListener() {
                @SuppressLint("ResourceType")
                @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                @Override
                public void onClick(View v) {

                    BubbleLayout bubbleLayout = (BubbleLayout) LayoutInflater.from(TeacherActivity.this).inflate(R.layout.parent_bubble, null);
                    final PopupWindow popupWindow = BubblePopupHelper.create(TeacherActivity.this, bubbleLayout);
                    popupWindow.showAsDropDown(v);
                    final TextView txt_entire = (TextView) bubbleLayout.findViewById(R.id.txt_entire);
                    final TextView txt_select = (TextView) bubbleLayout.findViewById(R.id.txt_select);
                    txt_entire.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            txt_entire.setTextColor(getResources().getColor(R.color.colorTheme));
                            txt_select.setTextColor(getResources().getColor(R.color.colorText));

                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    Fragment someFragment1 = new TeacherAttendanceEntireClassFragment();
                                    FragmentTransaction transaction1 = getFragmentManager().beginTransaction();
                                    transaction1.replace(R.id.content, someFragment1); // give your fragment container id in first parameter
                                    transaction1.addToBackStack(null);
                                    transaction1.commit();
                                    DrawerLayout drawer1 = (DrawerLayout) findViewById(R.id.parent_drawer_layout);
                                    drawer1.closeDrawer(GravityCompat.START);
                                    txt_menu_title.setText("Attendance");
                                    popupWindow.dismiss();
                                }
                            }, 200);
                        }
                    });

                    txt_select.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            txt_select.setTextColor(getResources().getColor(R.color.colorTheme));
                            txt_entire.setTextColor(getResources().getColor(R.color.colorText));
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    Fragment someFragment1 = new TeacherAttendanceReportFragment();
                                    FragmentTransaction transaction1 = getFragmentManager().beginTransaction();
                                    transaction1.replace(R.id.content, someFragment1); // give your fragment container id in first parameter
                                    transaction1.addToBackStack(null);  // if written, this transaction will be added to backstack
                                    transaction1.commit();
                                    DrawerLayout drawer1 = (DrawerLayout) findViewById(R.id.parent_drawer_layout);
                                    drawer1.closeDrawer(GravityCompat.START);
                                    txt_menu_title.setText("Attendance Report");
                                    popupWindow.dismiss();
                                }
                            }, 200);

                        }
                    });
                }
            });

            holder.txt_menu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    isOpen = false;
                    String name = menuModel.getName();
                    if (name.equals("Profile")) {
                        Fragment someFragment = new ProfileTeacherFragment();
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.replace(R.id.content, someFragment);
                        transaction.addToBackStack(null);
                        transaction.commit();
                        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.parent_drawer_layout);
                        drawer.closeDrawer(GravityCompat.START);
                        txt_menu_title.setText(name);
                    } else if (name.equals("Social")) {
                        Fragment someFragment = new TeacherSocialFragment();
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.replace(R.id.content, someFragment); // give your fragment container id in first parameter
                        transaction.addToBackStack(null);  // if written, this transaction will be added to backstack
                        transaction.commit();
                        img_search.setVisibility(View.VISIBLE);
                        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.parent_drawer_layout);
                        drawer.closeDrawer(GravityCompat.START);
                        txt_menu_title.setText(name);
                    } else if (name.equals("Chat")) {
                        Fragment someFragment = new TeacherChatFragment();
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.replace(R.id.content, someFragment); // give your fragment container id in first parameter
                        transaction.addToBackStack(null);  // if written, this transaction will be added to backstack
                        transaction.commit();
                        img_search.setVisibility(View.VISIBLE);
                        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.parent_drawer_layout);
                        drawer.closeDrawer(GravityCompat.START);
                        txt_menu_title.setText(name);
                    } else if (name.equals("Event")) {
                        Fragment someFragment = new TeacherEventListFragment();
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.replace(R.id.content, someFragment); // give your fragment container id in first parameter
                        transaction.addToBackStack(null);  // if written, this transaction will be added to backstack
                        transaction.commit();
                        img_search.setVisibility(View.VISIBLE);
                        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.parent_drawer_layout);
                        drawer.closeDrawer(GravityCompat.START);
                        txt_menu_title.setText(name);
                    } else if (name.equals("Attendance")) {
                        Fragment someFragment = new TeacherAttendanceReportFragment();
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.replace(R.id.content, someFragment); // give your fragment container id in first parameter
                        transaction.addToBackStack(null);  // if written, this transaction will be added to backstack
                        transaction.commit();
                        img_search.setVisibility(View.VISIBLE);
                        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.parent_drawer_layout);
                        drawer.closeDrawer(GravityCompat.START);
                        txt_menu_title.setText(name);
                    } else if (name.equals("Class")) {
                        Fragment someFragment = new TeacherClassFragment();
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.replace(R.id.content, someFragment); // give your fragment container id in first parameter
                        transaction.addToBackStack(null);  // if written, this transaction will be added to backstack
                        transaction.commit();
                        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.parent_drawer_layout);
                        drawer.closeDrawer(GravityCompat.START);
                        txt_menu_title.setText(name);
                    } else if (name.equals("Homework")) {
                        Fragment someFragment = new HomeworkFragment();
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.replace(R.id.content, someFragment); // give your fragment container id in first parameter
                        transaction.addToBackStack(null);  // if written, this transaction will be added to backstack
                        transaction.commit();
                        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.parent_drawer_layout);
                        drawer.closeDrawer(GravityCompat.START);
                        txt_menu_title.setText(name);
                    } else if (name.equals("Report")) {
                        Fragment someFragment = new TeacherReportFragment();
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.replace(R.id.content, someFragment); // give your fragment container id in first parameter
                        transaction.addToBackStack(null);  // if written, this transaction will be added to backstack
                        transaction.commit();
                        img_search.setVisibility(View.VISIBLE);
                        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.parent_drawer_layout);
                        drawer.closeDrawer(GravityCompat.START);
                        txt_menu_title.setText(name);
                    } else if (name.equals("Tracker")) {
                        Intent intent = new Intent(TeacherActivity.this, MapsActivity.class);
                        startActivity(intent);

//                        Fragment someFragment = new TrackerFragment();
//                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
//                        transaction.replace(R.id.content, someFragment); // give your fragment container id in first parameter
//                        transaction.addToBackStack(null);  // if written, this transaction will be added to backstack
//                        transaction.commit();
//                        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.parent_drawer_layout);
//                        drawer.closeDrawer(GravityCompat.START);
//                        txt_menu_title.setText(name);
                    } else if (name.equals("Logout")) {

                        SharedPreferences spuser = TeacherActivity.this.getSharedPreferences("login", 0);
                        SharedPreferences.Editor Eduser = spuser.edit();
                        Eduser.putString("username", null);
                        Eduser.putString("type", null);
                        Eduser.putString("password", null);
                        Eduser.putString("user_phone", null);
                        Eduser.putString("user_id", null);
                        Eduser.putString("user_email", null);
                        Eduser.apply();

                        Intent intent = new Intent(TeacherActivity.this, WelcomeActivity.class);
                        startActivity(intent);
                        finish();
                    }

                }
            });

        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            ImageView img_menu;
            TextView txt_menu;
            View view;
            ImageView img_popup;


            public ViewHolder(View itemView) {
                super(itemView);
                img_menu = (ImageView) itemView.findViewById(R.id.img_menu);
                txt_menu = (TextView) itemView.findViewById(R.id.txt_menu);
                img_popup = (ImageView) itemView.findViewById(R.id.img_popup);
                view = (View) itemView.findViewById(R.id.view);
            }
        }

    }


}

