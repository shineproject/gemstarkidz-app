package com.example.shine_rahul.gemstarkidz.Fragment;


import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.example.shine_rahul.gemstarkidz.Adapter.SocialSingleAdapter;
import com.example.shine_rahul.gemstarkidz.Adapter.TagAdapter;
import com.example.shine_rahul.gemstarkidz.Adapter.Teacher_Social_Single_Adapter;
import com.example.shine_rahul.gemstarkidz.CommentActivity;
import com.example.shine_rahul.gemstarkidz.Model.SocialSingleModel;
import com.example.shine_rahul.gemstarkidz.Model.TeacherClassModel;
import com.example.shine_rahul.gemstarkidz.Model.Teacher_Social_Single_Model;
import com.example.shine_rahul.gemstarkidz.R;
import com.example.shine_rahul.gemstarkidz.SearchActivity;

import java.io.File;
import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class SocialSingleFragment extends android.app.Fragment {
    TextView txt_social11_single_social;
    TextView txt_social11_single_chat;
    TextView txt_social11_single_event;
    TextView txt_social11_single_attendance;
    TextView txt_menu_title;
    ImageView search;
    VideoView social_social11_video;
    MediaController mediaController;
    ImageView img_social11_like;
    ImageView img_social11_comment;
    ImageView img_social11_tag;
    ImageView img_social11_share;


    private boolean isSelect = true;
    TeacherClassModel teacherClassModel;
    TagAdapter teacherClassStudentListAdapter;
    ArrayList<TeacherClassModel> teacherClassModelArrayList;
    int[] images22 = {R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1};
    String[] name22 = {"Kamlesh", "Bhargav", "Dhruv", "Jaymin", "Bilal", "Akshay", "Harshal", "Vineet", "Lucky", "Vipul", "Kamlesh", "Bhargav", "Dhruv", "Jaymin", "Bilal", "Akshay", "Harshal", "Vineet", "Lucky", "Vipul", "Kamlesh", "Bhargav", "Dhruv", "Jaymin", "Bilal", "Akshay", "Harshal", "Vineet", "Lucky", "Vipul"};


    RecyclerView rv_social11_social_list;
    Teacher_Social_Single_Adapter teacher_social_single_adapter;
    Teacher_Social_Single_Model teacher_social_single_model;
    ArrayList<Teacher_Social_Single_Model> data123;
    int[] image123 = {R.drawable.image1, R.drawable.image8, R.drawable.image3, R.drawable.image4, R.drawable.image5, R.drawable.image6, R.drawable.image7, R.drawable.image8, R.drawable.image9, R.drawable.image10};
    String[] name123 = {"Kamlesh", "Bhargav", "Dhruv", "Jaymin", "Bilal", "Akshay", "Harshal", "Vineet", "Lucky", "Vipul"};
    String[] time123 = {"03:30 pm", "10:33 pm", "01:00 pm", "03:22 am", "4:30 am", "06:30 pm", "08:30 pm", "05:30 pm", "07:44 pm", "11:30 pm"};
    String[] message123 = {"Offers almost all of the same options as the mobile app",
            "Chatting is a lot faster using a keyboard",
            "Offers real time synchronization between app and PC",
            "Well-designed interface",
            "Mobile device must be connected in order to use it",
            "ome options are missing",
            "Was it worth the wait?",
            "by Matarazy Stunna Cull Hello And how are you doing I really like what's up I hope i.",
            "chat with friends and new friends on word and see photos friend.",
            "I love wats because it increases the knowledge and brings closer friends and m"};
    String[] url123 = {"https://android--code.blogspot.in/2015/05/android-textview-underline.html",
            "https://android--code.blogspot.in/2015/05/android-textview-underline.html",
            "https://android--code.blogspot.in/2015/05/android-textview-underline.html",
            "https://android--code.blogspot.in/2015/05/android-textview-underline.html",
            "https://android--code.blogspot.in/2015/05/android-textview-underline.html",
            "https://android--code.blogspot.in/2015/05/android-textview-underline.html",
            "https://android--code.blogspot.in/2015/05/android-textview-underline.html",
            "https://android--code.blogspot.in/2015/05/android-textview-underline.html",
            "https://android--code.blogspot.in/2015/05/android-textview-underline.html",
            "https://android--code.blogspot.in/2015/05/android-textview-underline.html"};


    public SocialSingleFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_social_single, container, false);
        rv_social11_social_list = (RecyclerView) view.findViewById(R.id.rv_social11_social_list);
        img_social11_like = (ImageView) view.findViewById(R.id.img_social11_like);
        img_social11_comment = (ImageView) view.findViewById(R.id.img_social11_comment);
        img_social11_tag = (ImageView) view.findViewById(R.id.img_social11_tag);
        img_social11_share = (ImageView) view.findViewById(R.id.img_social11_share);
        txt_social11_single_social = (TextView) view.findViewById(R.id.txt_social11_single_social);
        txt_social11_single_chat = (TextView) view.findViewById(R.id.txt_social11_single_chat);
        txt_social11_single_event = (TextView) view.findViewById(R.id.txt_social11_single_event);
        txt_social11_single_attendance = (TextView) view.findViewById(R.id.txt_social11_single_attendance);
        txt_social11_single_social.setTextColor(getResources().getColor(R.color.colorTheme));
        search = (ImageView) getActivity().findViewById(R.id.img_search);
        txt_menu_title = (TextView) getActivity().findViewById(R.id.txt_menu_title);
        social_social11_video = (VideoView) view.findViewById(R.id.social_social11_video);
        mediaController = new MediaController(getActivity());
        final Uri uri = Uri.parse("http://abhiandroid-8fb4.kxcdn.com/ui/wp-content/uploads/2016/04/videoviewtestingvideo.mp4");
        social_social11_video.setVideoURI(uri);
        social_social11_video.setMediaController(mediaController);
        mediaController.setAnchorView(social_social11_video);
        social_social11_video.requestFocus();
        social_social11_video.start();


        data123 = new ArrayList<>();
        for (int i = 0; i < name123.length; i++) {
            teacher_social_single_model = new Teacher_Social_Single_Model();
            teacher_social_single_model.setImage(image123[i]);
            teacher_social_single_model.setName(name123[i]);
            teacher_social_single_model.setTime(time123[i]);
            teacher_social_single_model.setDescription(message123[i]);
            teacher_social_single_model.setUrl(url123[i]);
            data123.add(teacher_social_single_model);
        }
        teacher_social_single_adapter = new Teacher_Social_Single_Adapter(getActivity(), data123);
        teacher_social_single_adapter.notifyDataSetChanged();
        rv_social11_social_list.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayout.VERTICAL, false));
        rv_social11_social_list.setAdapter(teacher_social_single_adapter);


        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SearchActivity.class);
                startActivity(intent);
            }
        });


        txt_social11_single_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt_social11_single_social.setTextColor(getResources().getColor(R.color.colorText));
                txt_social11_single_chat.setTextColor(getResources().getColor(R.color.colorTheme));
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        android.app.Fragment fragment = new TeacherChatFragment();
                        FragmentManager fragmentManager = getActivity().getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.content, fragment);
                        fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commit();
                        search.setVisibility(View.VISIBLE);
                        txt_menu_title.setText("Chat");
                    }
                }, 100);

            }
        });
        txt_social11_single_event.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt_social11_single_social.setTextColor(getResources().getColor(R.color.colorText));
                txt_social11_single_event.setTextColor(getResources().getColor(R.color.colorTheme));
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        android.app.Fragment fragment = new TeacherEventFragment();
                        FragmentManager fragmentManager = getActivity().getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.content, fragment);
                        fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commit();
                        search.setVisibility(View.VISIBLE);
                        txt_menu_title.setText("Event");
                    }
                }, 100);

            }
        });

        txt_social11_single_attendance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt_social11_single_social.setTextColor(getResources().getColor(R.color.colorText));
                txt_social11_single_attendance.setTextColor(getResources().getColor(R.color.colorTheme));
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        android.app.Fragment fragment = new TeacherAttendanceEntireClassFragment();
                        FragmentManager fragmentManager = getActivity().getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.content, fragment);
                        fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commit();
                        search.setVisibility(View.VISIBLE);
                        txt_menu_title.setText("Attendance");
                    }
                }, 100);

            }
        });


        img_social11_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File f = new File(String.valueOf(uri));
                Uri uriPath = Uri.parse("http://abhiandroid-8fb4.kxcdn.com/ui/wp-content/uploads/2016/04/videoviewtestingvideo.mp4");
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.putExtra(Intent.EXTRA_TEXT, "Text");
                shareIntent.putExtra(Intent.EXTRA_STREAM, uriPath);
                shareIntent.setType("video/*");
                shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(Intent.createChooser(shareIntent, "send"));
            }
        });

        img_social11_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), CommentActivity.class);
                startActivity(intent);
            }
        });

        img_social11_tag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTagDialog1();
            }
        });


        img_social11_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isSelect) {
                    img_social11_like.setImageResource(R.mipmap.like_icon);
                    isSelect = true;
                } else {
                    img_social11_like.setImageResource(R.mipmap.hand);
                    isSelect = false;
                }
            }
        });


//        data123 = new ArrayList<>();
//        int i;
//        for (i = 0; i < image123.length; i++) ;
//        {
//            teacher_social_single_model = new Teacher_Social_Single_Model();
//            teacher_social_single_model.setImage(image123[i]);
//            teacher_social_single_model.setName(name123[i]);
//            teacher_social_single_model.setTime(time123[i]);
//            teacher_social_single_model.setDescription(message123[i]);
//            teacher_social_single_model.setUrl(url123[i]);
//            data123.add(teacher_social_single_model);
//        }
//        teacher_social_single_adapter = new Teacher_Social_Single_Adapter(getActivity(), data123);
//        teacher_social_single_adapter.notifyDataSetChanged();
//        rv_social11_social_list.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
//        rv_social11_social_list.setAdapter(teacher_social_single_adapter);
        return view;
    }
    private void showTagDialog1() {
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View view1 = layoutInflater.inflate(R.layout.entire_class_tag, null);
        final AlertDialog.Builder alertdialog = new AlertDialog.Builder(getActivity());
        alertdialog.setView(view1);
        final AlertDialog dialog = alertdialog.create();
        final TextView textv = view1.findViewById(R.id.textv);
        final ImageView btn_entire_tag_student_submit = (ImageView) view1.findViewById(R.id.btn_entire_tag_student_submit);
        final GridView gv_entire_select_student = (GridView) view1.findViewById(R.id.gv_entire_select_student);
        textv.setVisibility(View.GONE);

        teacherClassModelArrayList = new ArrayList<>();
        for (int i = 0; i < images22.length; i++) {
            teacherClassModel = new TeacherClassModel();
            teacherClassModel.setStudent_name(name22[i]);
           // teacherClassModel.setStudent_image(images22[i]);
            teacherClassModelArrayList.add(teacherClassModel);
        }
        teacherClassStudentListAdapter = new TagAdapter(getActivity(), teacherClassModelArrayList);
        teacherClassStudentListAdapter.notifyDataSetChanged();
        gv_entire_select_student.setAdapter(teacherClassStudentListAdapter);
        dialog.show();
        btn_entire_tag_student_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Tag Successfully...", Toast.LENGTH_LONG).show();
                dialog.dismiss();
            }
        });
    }

}
