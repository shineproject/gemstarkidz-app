package com.example.shine_rahul.gemstarkidz;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.example.shine_rahul.gemstarkidz.Model.DirectionsJSONParser;
import com.example.shine_rahul.gemstarkidz.Model.DriverRoute;
import com.example.shine_rahul.gemstarkidz.Service.Services;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


public class DriverActivity extends FragmentActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    String Use_Id, Use_Name, Use_Email, Use_Type, Use_Address, Use_City, Use_State, Use_Country, Use_Phone_No, Use_Mobile_No,
            Use_Status, Use_Image, Use_Cla_Id, Use_Register_Type,
            Use_CreatedBy, Use_CreatedAt, Use_UpdatedBy, Use_UpdatedAt;

    String resultDriverDetail = "";
    ArrayList<DriverRoute> driverRoutesList;
    Button start, end;

    private GoogleMap mMap;
    Location location;
    MarkerOptions markerOptions;
    private GoogleApiClient googleApiClient;
    static int count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_driver);

        SharedPreferences splogin = getApplicationContext().getSharedPreferences("login", 0);

        Use_Id = splogin.getString("user_id", "");
        Use_Name = splogin.getString("username", "");
        Use_Type = splogin.getString("type", "");
        Use_Address = splogin.getString("user_address", "");
        Use_City = splogin.getString("user_city", "");
        Use_State = splogin.getString("user_state", "");
        Use_Country = splogin.getString("user_country", "");
        Use_Phone_No = splogin.getString("user_phone", "");
        Use_Mobile_No = splogin.getString("user_mobile", "");
        Use_Status = splogin.getString("user_status", "");
        Use_Email = splogin.getString("user_email", "");
        Use_Register_Type = splogin.getString("user_reg_type", "");
        Use_Image = splogin.getString("user_image", "");
        Use_Cla_Id = splogin.getString("user_class", "");
        Use_CreatedAt = splogin.getString("user_created_at", "");
        Use_CreatedBy = splogin.getString("user_created_by", "");
        Use_UpdatedAt = splogin.getString("user_updated_at", "");
        Use_UpdatedBy = splogin.getString("user_updated_by", "");

        googleApiClient = new GoogleApiClient.Builder(DriverActivity.this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        start = findViewById(R.id.start);
        end = findViewById(R.id.end);

        new AsyncTaskRunnerGetPostData().execute();
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
//        mMap = googleMap;
//
//
//
//        // Add a marker in Sydney and move the camera
//        LatLng sydney = new LatLng(-34, 151);
//        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));

        mMap = googleMap;
        //checkLocationandAddToMap();
        try {

            if (count == 0) {
                location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
                //    markerOptions = new MarkerOptions().position(new LatLng(51.509865, -0.118092)).title("You are Here");
                markerOptions = new MarkerOptions().position(new LatLng(location.getLatitude(), location.getLongitude())).title("You are Here");
                mMap.addMarker(markerOptions);
                //  mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(), location.getLongitude())));

                mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(), location.getLongitude())));
                mMap.animateCamera(CameraUpdateFactory.zoomTo(12.0f));
                mMap.setMyLocationEnabled(true);
            }
//            else if (count == 1) {
//                getLocation(driverRoutesList);
//            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapDriver);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    public void onStart() {
        super.onStart();
        if (isNetworkConnected()) {
            googleApiClient.connect();
        }
    }

    public void onStop() {
        super.onStop();
        if (isNetworkConnected()) {
            googleApiClient.disconnect();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    private class AsyncTaskRunnerGetPostData extends AsyncTask<String, String, String> {

        private ProgressDialog Dialog = new ProgressDialog(DriverActivity.this);

        @Override
        protected String doInBackground(String... params) {

            MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
            builder.addFormDataPart("user_id", Use_Id);
            RequestBody requestBody = builder.build();

            try {
                OkHttpClient client = new OkHttpClient.Builder().build();
                Request request = new Request.Builder()
                        .url(Services.DRIVER_GET_LAT_LONG)
                        .post(requestBody)
                        .build();
                Response resp = client.newCall(request).execute();
                resultDriverDetail = resp.body().string();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            Dialog.dismiss();
            GetPostData(resultDriverDetail);
            super.onPostExecute(s);
        }

        @Override
        protected void onPreExecute() {
            Dialog.show();
            Dialog.setMessage("Please Wait....");
            Dialog.setCancelable(false);
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
        }

    }

    private void GetPostData(String response) {

        DriverRoute driverRoute;
        driverRoutesList = new ArrayList<>();

        if (!response.equals("")) {
            try {
                JSONObject jsobjectcategory = new JSONObject(response);
                String message = jsobjectcategory.getString("message");
                // Updated upstream
                String error = jsobjectcategory.getString("error");

                if (error.equals("200")) {

                  //  String jsonArray = String.valueOf(jsobjectcategory.getJSONArray("data"));
                //    Toast.makeText(getApplicationContext(),"",Toast.LENGTH_LONG).show();
//                    JSONObject jsonObject = null;
//                    for (int i = 0; i < jsonArray.length(); i++) {
//                        jsonObject = jsonArray.getJSONObject(i);
//
//                        driverRoute = new DriverRoute();
//
//                        JSONArray jsonArrayF = jsonObject.getJSONArray("from");
//                        for (int j = 0; j < jsonArrayF.length(); j++) {
//                            JSONObject jsonObjectF = jsonArrayF.getJSONObject(j);
//
//                            JSONArray jsonArrayf2 = jsonObjectF.getJSONArray("lat");
//
//                            driverRoute.setLatFrom(jsonObjectF.getString("lat"));
//                            driverRoute.setLngFrom(jsonObjectF.getString("lng"));
//                        }
//
//                        JSONArray jsonArrayT = jsonObject.getJSONArray("to");
//                        for (int j = 0; j < jsonArrayT.length(); j++) {
//                            JSONObject jsonObjectT = jsonArrayT.getJSONObject(j);
//                            driverRoute.setLatTo(jsonObjectT.getString("lat"));
//                            driverRoute.setLngTo(jsonObjectT.getString("lng"));
//                        }
//
//                        driverRoutesList.add(driverRoute);
//                        if (driverRoutesList.size() > 0) {
//                            getLocation(driverRoutesList);
//                            count = 1;
//                        }
//
//                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

        }
    }

    public void getLocation(ArrayList<DriverRoute> driverRoutesList) {

        LatLng origin = null;
        LatLng dest = null;
        for (int i = 0; i < driverRoutesList.size(); i++) {
            origin = new LatLng(Double.parseDouble(driverRoutesList.get(i).getLatFrom()), Double.parseDouble(driverRoutesList.get(i).getLngFrom()));
            dest = new LatLng(Double.parseDouble(driverRoutesList.get(i).getLatTo()), Double.parseDouble(driverRoutesList.get(i).getLngTo()));
        }

// Getting URL to the Google Directions API
        String url = getDirectionsUrl(origin, dest);

        DownloadTask downloadTask = new DownloadTask();

// Start downloading json data from Google Directions API
        downloadTask.execute(url);
    }

    private String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;
        return url;
    }

    /**
     * A method to download json data from url
     */
    @SuppressLint("LongLogTag")
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("Exception while downloading url", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }

    /**
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();

            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(2);
                lineOptions.color(Color.RED);
            }

            // Drawing polyline in the Google Map for the i-th route
            mMap.addPolyline(lineOptions);
        }
    }
}
