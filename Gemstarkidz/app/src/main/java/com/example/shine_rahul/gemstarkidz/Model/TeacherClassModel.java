package com.example.shine_rahul.gemstarkidz.Model;

/**
 * Created by Shine-Rahul on 10/3/2018.
 */

public class TeacherClassModel {
    String student_id;
    String teacher_id;
    String student_image;
    String student_name;
    String student_abs_pre;

    public TeacherClassModel() {
    }

    public TeacherClassModel(String student_id, String teacher_id, String student_image, String student_name, String student_abs_pre) {
        this.student_id = student_id;
        this.teacher_id = teacher_id;
        this.student_image = student_image;
        this.student_name = student_name;
        this.student_abs_pre = student_abs_pre;
    }

    public String getStudent_id() {
        return student_id;
    }

    public void setStudent_id(String student_id) {
        this.student_id = student_id;
    }

    public String getTeacher_id() {
        return teacher_id;
    }

    public void setTeacher_id(String teacher_id) {
        this.teacher_id = teacher_id;
    }

    public String getStudent_image() {
        return student_image;
    }

    public void setStudent_image(String student_image) {
        this.student_image = student_image;
    }

    public String getStudent_name() {
        return student_name;
    }

    public void setStudent_name(String student_name) {
        this.student_name = student_name;
    }

    public String getStudent_abs_pre() {
        return student_abs_pre;
    }

    public void setStudent_abs_pre(String student_abs_pre) {
        this.student_abs_pre = student_abs_pre;
    }
}
