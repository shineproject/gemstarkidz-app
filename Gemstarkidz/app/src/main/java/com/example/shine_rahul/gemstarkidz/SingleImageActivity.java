package com.example.shine_rahul.gemstarkidz;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

public class SingleImageActivity extends Activity {
    ImageView img_single;
    ImageView img_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_single_image);
        img_single = (ImageView) findViewById(R.id.img_single);
        img_back = (ImageView) findViewById(R.id.img_back);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        Intent intent = this.getIntent();
        int image = intent.getIntExtra("Image", 0);
        img_single.setImageResource(intent.getIntExtra("Image", 0));
        Log.d("IMAGE", String.valueOf(image));
    }
}
