package com.example.shine_rahul.gemstarkidz.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shine_rahul.gemstarkidz.Adapter.TeacherClassStudentListAdapter;
import com.example.shine_rahul.gemstarkidz.Model.TeacherClassModel;
import com.example.shine_rahul.gemstarkidz.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class TeacherAttendanceEntireClassFragment extends android.app.Fragment {
    GridView gv_attendance_entire_class;
    ImageView btn_submit;
    TeacherClassModel teacherClassModel;
    TextView txt_edit_toolbar;
    TeacherClassStudentListAdapter teacherClassStudentListAdapter;
    ArrayList<TeacherClassModel> teacherClassModelArrayList;
    int[] images = {R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1};
    String[] names = {"Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh", "Kamlesh"};


    public TeacherAttendanceEntireClassFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_teacher_attendance_entire_class, container, false);
        gv_attendance_entire_class = (GridView) view.findViewById(R.id.gv_attendance_entire_class);
        txt_edit_toolbar = (TextView) getActivity().findViewById(R.id.txt_edit_toolbar);
        txt_edit_toolbar.setVisibility(View.GONE);
        btn_submit = (ImageView) view.findViewById(R.id.btn_submit);
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(),"Attendance submit sucessfully",Toast.LENGTH_LONG).show();
            }
        });

        teacherClassModelArrayList = new ArrayList<>();
        for (int i = 0; i < images.length; i++) {
            teacherClassModel = new TeacherClassModel();
            teacherClassModel.setStudent_name(names[i]);
//            teacherClassModel.setStudent_image(images[i]);
            teacherClassModelArrayList.add(teacherClassModel);
        }
        teacherClassStudentListAdapter = new TeacherClassStudentListAdapter(getActivity(), teacherClassModelArrayList);
        teacherClassStudentListAdapter.notifyDataSetChanged();
        gv_attendance_entire_class.setAdapter(teacherClassStudentListAdapter);
        return view;
    }

}
